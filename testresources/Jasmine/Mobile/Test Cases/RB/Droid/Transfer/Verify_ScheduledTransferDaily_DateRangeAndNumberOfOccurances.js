it("Verify_ScheduledTransferDaily_DateRangeAndNumberOfOccurances", async function() {
  
  SearchInFromAndToScreen("normal");
  EnterAmount();
  SelectFrequencyDaily();
  SelectDateRangeAndConfirmTransfer("Daily","normal");

  SearchInFromAndToScreen("normal");
  EnterAmount();
  SelectFrequencyDaily();
  SelectNumberOfTransfersAndThenConfirmTransfer("Daily","normal");
  Logout();
},180000);