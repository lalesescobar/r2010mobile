it("Verify_ScheduledTransferMonthly_DateRangeAndNumberOfOccurance", async function() {
	SearchInFromAndToScreen("normal");
	EnterAmount();
	SelectFrequencyMonthly();
	SelectDateRangeAndConfirmTransfer("Monthly","normal");
  
	SearchInFromAndToScreen("normal");
	EnterAmount();
	SelectFrequencyMonthly();
	SelectNumberOfTransfersAndThenConfirmTransfer("Monthly","normal");
    Logout();
},180000);