define({
  	init : function(){
      var navManager = applicationManager.getNavigationManager();
      var currentForm=navManager.getCurrentForm();
      applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
    },
    preShow: function () {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      this.view.flxMainContainer.skin="slfSbox";
     if (this.view.flxHeaderSearchbox.height === "40dp") {
       this.view.flxHeaderSearchbox.height = "0dp";
            // this.view.flxHeaderSearchbox.isVisible = false;
            // this.view.flxSearch.isVisible = true;
        this.view.flxSearch.height = "55dp";
             this.view.flxHeader.isVisible = true;
        }
    if (applicationManager.getPresentationFormUtility().getDeviceName() === "iPhone") {
      this.view.flxHeader.isVisible = false;
      this.view.flxMainContainer.top = "0dp";
    }
     else{
        this.view.flxHeader.isVisible = true;
       this.view.flxMainContainer.top = "56dp";
     }
      this.initActions();
      var navManager = applicationManager.getNavigationManager();
      var currentForm=navManager.getCurrentForm();
      applicationManager.getPresentationFormUtility().logFormName(currentForm);
      applicationManager.getPresentationUtility().dismissLoadingScreen();
  },
    initActions: function () {
        var scope = this;
    this.view.flxNoTransactions.isVisible=false;
    this.view.flxHeaderNT.isVisible=true;
    this.view.flxSeperator3.isVisible=true;
    this.view.segAccounts.isVisible=true;
    this.view.tbxSearch.text="";
    this.setSegmentData();
    this.view.customHeader.flxBack.onClick = function () {
      var navMan=applicationManager.getNavigationManager();
      navMan.goBack();
    }
    this.view.segAccounts.onRowClick = function () {
      scope.segmentRowClick();
    }
    this.view.customHeader.btnRight.onClick = function () {
       var bPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        bPayModule.presentationController.cancelCommon();
    }
    this.view.tbxSearch.onTouchStart = this.showSearch;
    // this.view.tbxSearch.onTextChange = this.showSearch;
    this.view.customSearchbox.btnCancel.onClick = this.showSearch;
    },
    segmentRowClick: function () {
        var selaccdata=[];
        var navMan=applicationManager.getNavigationManager();
        var rowindex=this.view.segAccounts.selectedRowIndex[1];
        var frmaccdata=this.view.segAccounts.data[rowindex];
        selaccdata.push(frmaccdata);
        var bPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
      	bPayModule.presentationController.setFromAccountsForTransactions(selaccdata[0]);
      	var isFromAcc = bPayModule.presentationController.isDefaultFromAccount();
      	var showPopup = bPayModule.presentationController.isSetAccountPopupEnabled();
      if(!isFromAcc && showPopup){
      	var basicConfig={
            "alertType": constants.ALERT_TYPE_CONFIRMATION,
            "yesLabel":applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.yesSetAsDefault"),
            "noLabel": applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.notnow"),
            "message": applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.BillPay.setDefaultAccountMessage"),
            "alertHandler": this.setDefaultAcc
          };
          applicationManager.getPresentationUtility().showAlertMessage(basicConfig,{});
      }
        else{
          
          bPayModule.presentationController.getBillPayTransactionalLimits();
        }
    },
  	setDefaultAcc: function (response) {
      applicationManager.getPresentationUtility().showLoadingScreen();
      var dataJSON={};
      if(response){
      	var selectedAcntRow = this.view.segAccounts.selectedItems[0];
        this.selAccountId = selectedAcntRow.accountID;
        dataJSON["default_account_billPay"] = this.selAccountId;
      }
      dataJSON.showBillPayFromAccPopup = false;
      var bPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
      bPayModule.presentationController.updateBillPayFromAcc(dataJSON);
    },
    setSegmentData: function () {
        var frmaccdata=[];
        var navMan=applicationManager.getNavigationManager();
        var accdata=  navMan.getCustomInfo("frmBillPayFromAccount");
        var accountsData=accdata.fromaccounts;
        var segData=[];
        frmaccdata=accountsData;
        var bPayModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        var processedData=bPayModule.presentationController.processAccountsData(frmaccdata);
      var configManager = applicationManager.getConfigurationManager();
      var orgAccounts={};
      if(configManager.isCombinedUser === "true"){
        orgAccounts = {
          "Personal Accounts": [],
          "Business Accounts": []
        };
        for (var i = 0; i < processedData.length; i++) {
          if (processedData[i].isBusinessAccount === "true") {
            processedData[i].flxAccountType={"isVisible":true};
            processedData[i].imgAccountType={"src":"businessaccount.png",
                   "isVisible":true};
            orgAccounts["Business Accounts"].push(processedData[i]);
          } else {
            processedData[i].flxAccountType={"isVisible":true};
            processedData[i].imgAccountType={"src":"personalaccount.png",
                   "isVisible":true};
            orgAccounts["Personal Accounts"].push(processedData[i]);
          }
        }
          for (var key in orgAccounts) {
             var headerJson = {
              "template": "flxTransHeader",
              "flximgUp": {"isVisible": true},
              "imgUpArrow":{"src":"arrowup.png"},
              "lblHeader": {"text": key +" ("+orgAccounts[key].length+")"}
            };
               segData.push([headerJson,orgAccounts[key]]);
          }
            if(segData.length>0)
          {
            this.view.flxNoTransactions.isVisible=false;
            this.view.segAccounts.isVisible=true;
            this.view.segAccounts.widgetDataMap={
              lblAccountName:"processedAccountName",
              lblBankName:"bankName",
              lblHeader:"lblHeader",
              imgUpArrow:"imgUpArrow",
              flximgUp:"flximgUp",
              lblAccountBalValue:"availableBalance",
              lblAccountBal:"accountBalanceType",
              accountNumber:"accountNumber",
              imgAccountType:"imgAccountType",
              flxAccountType:"flxAccountType"
            };
         
            this.view.segAccounts.setData(segData);
            this.segmentData=this.view.segAccounts.data;
          }
          else
          {
            this.segmentData=[];
            this.view.flxNoTransactions.isVisible=true;
            this.view.flxHeaderNT.isVisible=false;
            this.view.flxSeperator3.isVisible=false;
            this.view.segAccounts.isVisible=false;
          }
            

      }else{
          if(processedData.length>0)
          {
            for(var i =0;i<processedData.length;i++){
              processedData[i].flxAccountType={"isVisible":false};
              processedData[i].imgAccountType={"isVisible":false};
            }
            this.view.flxNoTransactions.isVisible=false;
            this.view.segAccounts.isVisible=true;
            this.view.segAccounts.widgetDataMap={
              lblAccountName:"processedAccountName",
              lblBankName:"bankName",
              lblAccountBalValue:"availableBalance",
              lblAccountBal:"accountBalanceType",
              accountNumber:"accountNumber",
              lblHeader:"",
              imgUpArrow:"",
              flximgUp:"",
              imgAccountType:"imgAccountType",
             flxAccountType:"flxAccountType"
            };
            this.view.segAccounts.setData(processedData);
            this.segmentData=this.view.segAccounts.data;
          }
          else
          {
            this.segmentData=[];
            this.view.flxNoTransactions.isVisible=true;
            this.view.flxHeaderNT.isVisible=false;
            this.view.flxSeperator3.isVisible=false;
            this.view.segAccounts.isVisible=false;
          }
        }        
      },
    showSearch: function () {
      var scope = this;
        if (this.view.flxHeaderSearchbox.height === "40dp") {
          this.view.customSearchbox.tbxSearch.text = ""
          this.view.flxHeaderSearchbox.height = "0dp";
          this.view.flxMainContainer.skin="slfSbox";
          //is.view.flxSearch.isVisible = true;
          this.view.flxSearch.height = "55dp";
          if (applicationManager.getPresentationFormUtility().getDeviceName() === "iPhone") {
            this.view.flxHeader.isVisible = false;
            this.view.flxMainContainer.top = "0dp";
          }
          else{
            this.view.flxHeader.isVisible = true;
            this.view.flxMainContainer.top = "56dp";
          }
          if(this.segmentData && this.segmentData.length>0)
          {
            this.view.segAccounts.setData(this.segmentData);
            this.view.flxNoTransactions.isVisible=false;
            this.view.flxHeaderNT.isVisible=true;
            this.view.flxSeperator3.isVisible=true;
            this.view.segAccounts.isVisible=true;
          }
          else
          {
            this.view.flxNoTransactions.isVisible=true;
            this.view.flxHeaderNT.isVisible=false;
            this.view.flxSeperator3.isVisible=false;
            this.view.segAccounts.isVisible=false;
          }
        } else {
          this.view.flxMainContainer.skin="sknFlxScrlffffff";
          this.view.flxSearch.height = "0dp";
          //is.view.flxSearch.isVisible = false;
          this.view.flxHeader.isVisible = false;
          this.view.flxMainContainer.top = "40dp";
          this.view.customSearchbox.tbxSearch.text="";
          this.view.flxHeaderSearchbox.height = "40dp";
         // this.view.flxHeaderSearchbox.isVisible = true;
       //   this.view.customSearchbox.tbxSearch.setFocus(true);
          kony.timer.schedule("timerId", function() {
                           scope.view.customSearchbox.tbxSearch.setFocus(true);
                           }, 0.2, false);
          this.view.customSearchbox.tbxSearch.onTextChange=this.searchdata;
        }
    },
  searchdata:function()
  {
    var searchData;
    var navMan=applicationManager.getNavigationManager();
    var searchtext=this.view.customSearchbox.tbxSearch.text.toLowerCase();
    if(searchtext)
    {
      this.view.segAccounts.removeAll();
      var data = this.segmentData;
      searchData = applicationManager.getDataProcessorUtility().commonSegmentSearch("accountName",searchtext,data);
      if(searchData && searchData.length>0)
      {
        this.view.segAccounts.setData(searchData);
        this.view.flxNoTransactions.isVisible=false;
        this.view.flxHeaderNT.isVisible=true;
        this.view.flxSeperator3.isVisible=true;
        this.view.segAccounts.isVisible=true;
      }
      else
      {
        this.view.segAccounts.isVisible=false;
        this.view.flxNoTransactions.isVisible=true;
        this.view.flxHeaderNT.isVisible=false;
        this.view.flxSeperator3.isVisible=false;
      }
    }
    else
    {
      if(this.segmentData && this.segmentData.length>0)
      {
        this.view.segAccounts.setData(this.segmentData);
        this.view.flxNoTransactions.isVisible=false;
        this.view.flxHeaderNT.isVisible=true;
        this.view.flxSeperator3.isVisible=true;
        this.view.segAccounts.isVisible=true;
      }
      else
      {
        this.view.flxNoTransactions.isVisible=true;
        this.view.flxHeaderNT.isVisible=false;
        this.view.flxSeperator3.isVisible=false;
        this.view.segAccounts.isVisible=false;
      }
    }
  }
});