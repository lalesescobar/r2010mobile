it("Verify_ScheduledTransferYearly_DateRangeAndNoOccurance_P2P", async function() {
  
  SearchInFromAndToScreen("P2P");
  EnterAmount();
  SelectFrequencyYearly();
  SelectDateRangeAndConfirmTransfer("Yearly","P2P");
  
  SearchInFromAndToScreen("P2P");
  EnterAmount();
  SelectFrequencyYearly();
  SelectNumberOfTransfersAndThenConfirmTransfer("Yearly","P2P");
  Logout();
},180000);