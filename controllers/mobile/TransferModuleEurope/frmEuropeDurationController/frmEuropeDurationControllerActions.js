define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Form_ad965cc1f05d46f0876932d018e12367: function AS_Form_ad965cc1f05d46f0876932d018e12367(eventobject) {
        var self = this;
        this.init();
    },
    AS_Form_f2eab1a4c4714040be2b93a53f52f55b: function AS_Form_f2eab1a4c4714040be2b93a53f52f55b(eventobject) {
        var self = this;
        this.preShow();
    },
    AS_BarButtonItem_e44aaf3e5bb5429ab508e902064a22d9: function AS_BarButtonItem_e44aaf3e5bb5429ab508e902064a22d9(eventobject) {
        var self = this;
        this.navigateCustomBack();
    },
    AS_BarButtonItem_d20f0c005f4a4dae88ac5f1f26e1dee8: function AS_BarButtonItem_d20f0c005f4a4dae88ac5f1f26e1dee8(eventobject) {
        var self = this;
        this.cancelOnClick();
    }
});