define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_ia5d8e92e1f549f39218caad96e92201: function AS_BarButtonItem_ia5d8e92e1f549f39218caad96e92201(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    /** init defined for frmMMTransferFromAccount **/
    AS_Form_caa81d63c8fe4c149172040b14f72299: function AS_Form_caa81d63c8fe4c149172040b14f72299(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmMMTransferFromAccount **/
    AS_Form_fada45cfeec8461f9973b7c4dde4a865: function AS_Form_fada45cfeec8461f9973b7c4dde4a865(eventobject) {
        var self = this;
        this.preShow();
    }
});