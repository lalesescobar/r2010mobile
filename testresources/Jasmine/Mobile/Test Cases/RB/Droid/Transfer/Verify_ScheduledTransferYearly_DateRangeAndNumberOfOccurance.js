it("Verify_ScheduledTransferYearly_DateRangeAndNumberOfOccurance", async function() {
	SearchInFromAndToScreen("normal");
	EnterAmount();
	SelectFrequencyYearly();
	SelectDateRangeAndConfirmTransfer("Yearly","normal");
  
	SearchInFromAndToScreen("normal");
	EnterAmount();
	SelectFrequencyYearly();
	SelectNumberOfTransfersAndThenConfirmTransfer("Yearly","normal");
  Logout();
},180000);