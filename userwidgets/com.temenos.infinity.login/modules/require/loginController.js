define(['./LoginUtility','./LoginDAO'],function(LoginUtility, LoginDAO) {
  return {
    
    constructor: function(baseConfig, layoutConfig, pspConfig) {
      this._identityServiceName = "";
      
      this._textBoxNormalSkin="";
      this._rememberMeLabelSkin="";
      this._rememberMeSwitchSkin="";
      this._btnLoginDisabledSkin="";
      this._btnLoginSkin="";
      this._btnTouchPinFaceIdSkin="";
      this._pinPopupTitleSkin="";
      this._pinPopupNumbersSkin="";
      this._pinPopupClearButtonSkin="";
      this._popupCancelButtonSkin="";
      this._faceIDPopupTitleSkin="";
      this._faceIDPopupDescSkin="";
      
      this._textVisiblityOffIcon="";
      this._textVisiblityOnIcon="";
      this._faceIDPopupImage="";
      
      this._tbx1PlaceholderText="";
      this._tbx2PlaceholderText="";
      this._lblRememberMeText="";
      this._lblForgotPasswordText="";
      this._submitButtonText="";
      this._touchIDBtnText="";
      this._pinIDBtnText="";
      this._faceIDBtnText="";
      this._pinPopupTitleText="";
      this._popupCancelBtnText="";
      this._faceIDPopupTitleText="";
      this._faceIDPopupDescText="";
      
      this.LoginUtility = new LoginUtility();
      this.LoginDAO = new LoginDAO();
      this.dialPadNo="";
      this.lengthOfDialNo=0;
      this.currentAuthMode="";
      this.JSONUsernamePassword="";
    },
    
    //Logic for getters/setters of custom properties
    initGettersSetters: function() {
      defineSetter(this, "identityServiceName", function(val){
        if((typeof val=='string') && (val != "")){
          this._identityServiceName=val;
        }
      });
      defineGetter(this, "identityServiceName", function(){
        return this._identityServiceName;
      });
      defineSetter(this, "textBoxNormalSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._textBoxNormalSkin=val;
        }
      });
      defineGetter(this, "textBoxNormalSkin", function(){
        return this._textBoxNormalSkin;
      });
      defineSetter(this, "rememberMeLabelSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._rememberMeLabelSkin=val;
        }
      });
      defineGetter(this, "rememberMeLabelSkin", function(){
        return this._rememberMeLabelSkin;
      });
      defineSetter(this, "rememberMeSwitchSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._rememberMeSwitchSkin=val;
        }
      });
      defineGetter(this, "rememberMeSwitchSkin", function(){
        return this._rememberMeSwitchSkin;
      });
      defineSetter(this, "btnLoginSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._btnLoginSkin=val;
        }
      });
      defineGetter(this, "btnLoginSkin", function(){
        return this._btnLoginSkin;
      });
      defineSetter(this, "btnLoginDisabledSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._btnLoginDisabledSkin=val;
        }
      });
      defineGetter(this, "btnLoginDisabledSkin", function(){
        return this._btnLoginDisabledSkin;
      });
      defineSetter(this, "btnTouchPinFaceIdSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._btnTouchPinFaceIdSkin=val;
        }
      });
      defineGetter(this, "btnTouchPinFaceIdSkin", function(){
        return this._btnTouchPinFaceIdSkin;
      });
      defineSetter(this, "pinPopupTitleSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._pinPopupTitleSkin=val;
        }
      });
      defineGetter(this, "pinPopupTitleSkin", function(){
        return this._pinPopupTitleSkin;
      });
      defineSetter(this, "pinPopupNumbersSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._pinPopupNumbersSkin=val;
        }
      });
      defineGetter(this, "pinPopupNumbersSkin", function(){
        return this._pinPopupNumbersSkin;
      });
      defineSetter(this, "pinPopupClearButtonSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._pinPopupClearButtonSkin=val;
        }
      });
      defineGetter(this, "pinPopupClearButtonSkin", function(){
        return this._pinPopupClearButtonSkin;
      });
      defineSetter(this, "popupCancelButtonSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._popupCancelButtonSkin=val;
        }
      });
      defineGetter(this, "popupCancelButtonSkin", function(){
        return this._popupCancelButtonSkin;
      });
      defineSetter(this, "faceIDPopupTitleSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._faceIDPopupTitleSkin=val;
        }
      });
      defineGetter(this, "faceIDPopupTitleSkin", function(){
        return this._faceIDPopupTitleSkin;
      });
      defineSetter(this, "faceIDPopupDescSkin", function(val){
        if((typeof val=="string") && (val != "")){
          this._faceIDPopupDescSkin=val;
        }
      });
      defineGetter(this, "faceIDPopupDescSkin", function(){
        return this._faceIDPopupDescSkin;
      });
      defineSetter(this, "textVisiblityOffIcon", function(val){
        if((typeof val=="string") && (val != "")){
          this._textVisiblityOffIcon=val;
        }
      });
      defineGetter(this, "textVisiblityOffIcon", function(){
        return this._textVisiblityOffIcon;
      });
      defineSetter(this, "textVisiblityOnIcon", function(val){
        if((typeof val=="string") && (val != "")){
          this._textVisiblityOnIcon=val;
        }
      });
      defineGetter(this, "textVisiblityOnIcon", function(){
        return this._textVisiblityOnIcon;
      });
      defineSetter(this, "faceIDPopupImage", function(val){
        if((typeof val=="string") && (val != "")){
          this._faceIDPopupImage=val;
        }
      });
      defineGetter(this, "faceIDPopupImage", function(){
        return this._faceIDPopupImage;
      });
      defineSetter(this, "tbx1PlaceholderText", function(val){
        if((typeof val=="string") && (val != "")){
          this._tbx1PlaceholderText=val;
        }
      });
      defineGetter(this, "tbx1PlaceholderText", function(){
        return this._tbx1PlaceholderText;
      });
      defineSetter(this, "tbx2PlaceholderText", function(val){
        if((typeof val=="string") && (val != "")){
          this._tbx2PlaceholderText=val;
        }
      });
      defineGetter(this, "tbx2PlaceholderText", function(){
        return this._tbx2PlaceholderText;
      });
      defineSetter(this, "lblRememberMeText", function(val){
        if((typeof val=="string") && (val != "")){
          this._lblRememberMeText=val;
        }
      });
      defineGetter(this, "lblRememberMeText", function(){
        return this._lblRememberMeText;
      });
      defineSetter(this, "lblForgotPasswordText", function(val){
        if((typeof val=="string") && (val != "")){
          this._lblForgotPasswordText=val;
        }
      });
      defineGetter(this, "lblForgotPasswordText", function(){
        return this._lblForgotPasswordText;
      });
      defineSetter(this, "submitButtonText", function(val){
        if((typeof val=="string") && (val != "")){
          this._submitButtonText=val;
        }
      });
      defineGetter(this, "submitButtonText", function(){
        return this._submitButtonText;
      });
      defineSetter(this, "touchIDBtnText", function(val){
        if((typeof val=="string") && (val != "")){
          this._touchIDBtnText=val;
        }
      });
      defineGetter(this, "touchIDBtnText", function(){
        return this._touchIDBtnText;
      });
      defineSetter(this, "pinIDBtnText", function(val){
        if((typeof val=="string") && (val != "")){
          this._pinIDBtnText=val;
        }
      });
      defineGetter(this, "pinIDBtnText", function(){
        return this._pinIDBtnText;
      });
      defineSetter(this, "faceIDBtnText", function(val){
        if((typeof val=="string") && (val != "")){
          this._faceIDBtnText=val;
        }
      });
      defineGetter(this, "faceIDBtnText", function(){
        return this._faceIDBtnText;
      });
      defineSetter(this, "pinPopupTitleText", function(val){
        if((typeof val=="string") && (val != "")){
          this._pinPopupTitleText=val;
        }
      });
      defineGetter(this, "pinPopupTitleText", function(){
        return this._pinPopupTitleText;
      });
      defineSetter(this, "popupCancelBtnText", function(val){
        if((typeof val=="string") && (val != "")){
          this._popupCancelBtnText=val;
        }
      });
      defineGetter(this, "popupCancelBtnText", function(){
        return this._popupCancelBtnText;
      });
      defineSetter(this, "faceIDPopupTitleText", function(val){
        if((typeof val=="string") && (val != "")){
          this._faceIDPopupTitleText=val;
        }
      });
      defineGetter(this, "faceIDPopupTitleText", function(){
        return this._faceIDPopupTitleText;
      });
      defineSetter(this, "faceIDPopupDescText", function(val){
        if((typeof val=="string") && (val != "")){
          this._faceIDPopupDescText=val;
        }
      });
      defineGetter(this, "faceIDPopupDescText", function(){
        return this._faceIDPopupDescText;
      });
    },
    
    performActionOnForm: function(methodName, argument=null){
      // This method invokes the appropriate event exposed  by the component
      const scopeObj = this;
      switch(methodName){
        case 'onLoginSuccess':
          if(scopeObj.onLoginSuccess) scopeObj.onLoginSuccess(argument);
          break;
        case 'onPopupVisible':
          if(scopeObj.onPopupVisible) scopeObj.onPopupVisible(argument);
          break;
        case 'hideDashboardIcon':
          if(scopeObj.hideDashboardIcon) scopeObj.hideDashboardIcon();
          break;
        case 'onFocusStart':
          if(scopeObj.onFocusStart) scopeObj.onFocusStart();          
          break;
        case 'onFocusEnd':
          if(scopeObj.onFocusEnd) scopeObj.onFocusEnd();          
          break;
        case 'setErrorStatus':
          if(scopeObj.setErrorStatus) scopeObj.setErrorStatus(argument);
          break;
        case 'forgotNavigation':
          if(scopeObj.forgotNavigation) scopeObj.forgotNavigation(argument);
          break;
      }
    },
    
    preShow: function(){
      this.setTextFromi18n();
      this.resetUI();
      this.setFlowActions();      
      this.setDialPadActions();
      this.view.flxLoginPopups.setVisibility(false);
      this.view.flxButtons.setVisibility(false);
      let navData = applicationManager.getNavigationManager().getCustomInfo("frmLogin");
      this.setVisibilityForSpecificLoginType("password");
      this.manageUname(navData);
      this.showDefaultLoginScreen(navData);
    },
    
    setTextFromi18n: function(){
      this._tbx1PlaceholderText=this.getStringFromi18n(this._tbx1PlaceholderText);
      this._tbx2PlaceholderText=this.getStringFromi18n(this._tbx2PlaceholderText);
      this._lblRememberMeText=this.getStringFromi18n(this._lblRememberMeText);
      this._lblForgotPasswordText=this.getStringFromi18n(this._lblForgotPasswordText);
      this._submitButtonText=this.getStringFromi18n(this._submitButtonText);
      this._touchIDBtnText=this.getStringFromi18n(this._touchIDBtnText);
      this._pinIDBtnText=this.getStringFromi18n(this._pinIDBtnText);
      this._faceIDBtnText=this.getStringFromi18n(this._faceIDBtnText);
      this._pinPopupTitleText=this.getStringFromi18n(this._pinPopupTitleText);
      this._popupCancelBtnText=this.getStringFromi18n(this._popupCancelBtnText);
      this._faceIDPopupTitleText=this.getStringFromi18n(this._faceIDPopupTitleText);
      this._faceIDPopupDescText=this.getStringFromi18n(this._faceIDPopupDescText);
    },
    
    getStringFromi18n: function(stringValue){
      return  kony.i18n.getLocalizedString(stringValue) ? kony.i18n.getLocalizedString(stringValue) : stringValue;
    },
    
    setFlowActions: function() {
      const scopeObj = this;
      this.view.btnLogIn.onClick = function(){
        scopeObj.btnLoginOnClick();
      };
      this.view.tbxUsername.onTextChange = function(){
        scopeObj.enableLoginButton();
      };
      this.view.tbxUsername.onTouchStart = function(){
        scopeObj.performActionOnForm('onFocusStart', null);
        scopeObj.animateLoginFlex("21.7%");
      };
      this.view.tbxUsername.onDone = function(){
        scopeObj.performActionOnForm('onFocusEnd', null);
        scopeObj.animateLoginFlex("27%");
      };
      this.view.tbxPassword.onTextChange = function(){
        scopeObj.enableLoginButton();
      };
      this.view.tbxPassword.onTouchStart = function(){
        scopeObj.performActionOnForm('onFocusStart', null);
        scopeObj.animateLoginFlex("21.7%");
      };
      this.view.tbxPassword.onDone = function(){
        scopeObj.performActionOnForm('onFocusEnd', null);
        scopeObj.animateLoginFlex("27%");
      };
      this.view.flxPwdVisiblityToggle.onClick = function(){
        scopeObj.flxPwdVisiblityToggleOnClick();
      };
      this.view.switchRememberMe.onSlide = function(){
        scopeObj.rememberMeOption();
      };
      this.view.flxForgot.onTouchEnd = function(){
        let enteredUsername = scopeObj.view.tbxUsername.text;
        scopeObj.performActionOnForm('forgotNavigation', enteredUsername);
      };
      // Events for Popups
      this.view.flxCross.onClick = function(){
        scopeObj.flxCancelDialPadOnClick();
      };
      this.view.btnTouchId.onTouchEnd = function(){
        scopeObj.showTouchIdAndroid();
        kony.application.setApplicationCallbacks({onbackground: ()=>{
            scopeObj.cancelTouchIdAuth();            
          }});
        scopeObj.setVisibilityForSpecificLoginType('touchid');        
        scopeObj.performActionOnForm('onPopupVisible', false);
      };
      this.view.btnPinId.onTouchEnd = function(){
        scopeObj.setVisibilityForSpecificLoginType('pin');
        scopeObj.performActionOnForm('onPopupVisible', false);
        this.view.flxLoginPassword.setEnabled(false);
      };
      this.view.flxbottomContainer.onTouchEnd = function(){
        scopeObj.closePopup('pin');
        scopeObj.performActionOnForm('onPopupVisible', true);
        this.view.flxLoginPassword.setEnabled(true);
      };
      this.view.btnFaceId.onTouchEnd = function(){
        scopeObj.setVisibilityForSpecificLoginType('faceid');
        scopeObj.performActionOnForm('onPopupVisible', false);
      };
      this.view.flxCancelFI.onTouchEnd = function(){
        scopeObj.closePopup('faceid');
        scopeObj.performActionOnForm('onPopupVisible', true);
      };
    },
    
    setDeviceRegisterflag: function(value) {
      let userManager = applicationManager.getUserPreferencesManager();
      userManager.updateDeviceRegisterFlag(value);
    },

    setDefaultMode: function(authMode) {
      let userManager = applicationManager.getUserPreferencesManager();
      userManager.setDefaultAuthMode(authMode);
    },
    
    setFaceIdflag: function(value) {
      let userManager = applicationManager.getUserPreferencesManager();
      userManager.updateFaceIdFlag(value);
    },
    
    setTouchIdflag: function(value) {
      let userManager = applicationManager.getUserPreferencesManager();
      userManager.upadateTouchIdFlag(value);
    },
    
    setRememberMeFlag: function(value) {
      let userManager = applicationManager.getUserPreferencesManager();
      userManager.updateRememberMeFlag(value);
    },
    
    setLoginFeaturesOff: function(){
      let userManager = applicationManager.getUserPreferencesManager();
      userManager.updateRememberMeFlag(false);
      userManager.setDefaultAuthMode("password");
      userManager.updateAccountPreviewFlag(false);
      userManager.upadateTouchIdFlag(false);
      userManager.updateFaceIdFlag(false);
      userManager.updatePinFlag(false);
      userManager.clearUserCredentials();
      applicationManager.getDataforLogin();
    },
    
    showDefaultLoginScreen: function(loginData){
      const scopeObj = this;
      if (loginData.isFirstTimeLogin){
        scopeObj.setVisibilityForSpecificLoginType("password");
        scopeObj.performActionOnForm('onPopupVisible', true);
      }
      if(loginData.usernameFromForgotUsername && (loginData.usernameFromForgotUsername !== undefined || loginData.usernameFromForgotUsername !== "")){
        scopeObj.setVisibilityForSpecificLoginType("password");
        scopeObj.performActionOnForm('onPopupVisible', true);
        scopeObj.populateUserName(loginData.usernameFromForgotUsername);
      } else if (loginData.NUOUsername && (loginData.NUOUsername !== undefined || loginData.NUOUsername !== "")){
        scopeObj.setVisibilityForSpecificLoginType("password");
        scopeObj.performActionOnForm('onPopupVisible', true);
        scopeObj.populateUserName(loginData.NUOUsername);
      } else {
        var userObj = applicationManager.getUserPreferencesManager();
        var checkDeviceReg = userObj.isDeviceRegistered();
        if(checkDeviceReg == true && (!loginData.isFirstTimeLogin)){
          scopeObj.checkDeviceRegistrationStatus();
        }
      }
    },
    
    checkDeviceRegistrationStatus: function() {
      const scopeObj = this;
      this.LoginUtility.showLoadingScreen();
      let registrationManager = applicationManager.getRegistrationManager();
      let userMan = applicationManager.getUserPreferencesManager();
      let userName = userMan.getUserName();
      let criteria = kony.mvc.Expression.eq("UserName", userName);
      registrationManager.fetchDeviceRegistrationStatus(criteria, scopeObj.checkDeviceRegistrationSuccess, scopeObj.checkDeviceRegistrationError);
    },
    
    checkDeviceRegistrationSuccess: function(resDeviceSuc) {
      const scopeObj = this;
      var configManager = applicationManager.getConfigurationManager();      
      if (resDeviceSuc[0].status !== "false"){
        scopeObj.setDeviceRegisterflag(true);
        scopeObj.checkLoginType(true);
      }  else{
        scopeObj.setDeviceRegisterflag(false);
        scopeObj.checkLoginType(false);
      }
      this.LoginUtility.dismissLoadingScreen();
    },
    
    checkDeviceRegistrationError: function(resDeviceErr){      
      this.performActionOnForm('setErrorStatus', {"serviceNumber":1, "serviceResponse":resDeviceErr});
      this.LoginUtility.dismissLoadingScreen();
      if (resDeviceErr["isServerUnreachable"])
        applicationManager.getPresentationInterruptHandler().showErrorMessage("preLogin", resDeviceErr);
    },    
    
    checkLoginType : function(checkDeviceReg){
      const scopeObj = this;
      applicationManager.getPresentationUtility().showLoadingScreen();
      if(checkDeviceReg == true){
        var loginData=applicationManager.getNavigationManager().getCustomInfo("frmLogin");
        this.selectLoginMode(loginData.defaultAuthMode);
      } else {
        this.setVisibilityForSpecificLoginType("password");
        scopeObj.performActionOnForm('onPopupVisible', true);
        scopeObj.setDefaultMode("password");
      }
    },
    
    selectLoginMode: function(loginMode){
      const scopeObj = this;     
      if(loginMode=="pin"){
        this.setVisibilityForSpecificLoginType(loginMode);
        scopeObj.performActionOnForm('onPopupVisible', false);
        this.view.flxLoginPassword.setEnabled(false);
      } else if(loginMode=="faceid"){
        this.faceIdLogin();        
      } else if(loginMode=="touchid"){
        this.touchIdLogin();
      } else { // Normal Login Flow
        this.setVisibilityForSpecificLoginType(loginMode);
        scopeObj.performActionOnForm('onPopupVisible', true);        
        scopeObj.setDefaultMode("password");
      }
    },
    
    faceIdLogin: function(){
      const scopeObj = this;
      let deviceManager = applicationManager.getDeviceUtilManager();
      let navData = applicationManager.getNavigationManager().getCustomInfo("frmLogin");
      let identifierTouch = {"identifier": navData.userName};
      var authMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
      let authData = kony.keychain.retrieve(identifierTouch);
      if(deviceManager.isFaceIdAvilable() && !kony.sdk.isNullOrUndefined(authData) && !kony.sdk.isNullOrUndefined(authData.secureaccount) && !kony.sdk.isEmptyObject(authData.secureaccount) && !kony.sdk.isNullOrUndefined(authData.securedata)){
        this.setVisibilityForSpecificLoginType('faceid');
        scopeObj.performActionOnForm('onPopupVisible', false);
        kony.print(" showFaceIdScreen Secure data not empty"+JSON.stringify(authData));
        authMod.presentationController.isFaceLoginInProgress = true;
        applicationManager.getPresentationUtility().showLoadingScreen();
        this.statusCB(authData);
      } else {
        scopeObj.setFaceIdflag(false);
        scopeObj.setDefaultMode("password");
        scopeObj.performActionOnForm('onPopupVisible', true);        
        this.setVisibilityForSpecificLoginType("password");
        authMod.presentationController.isFaceLoginInProgress = false;
      }
    },
    
    touchIdLogin: function(){
      const scopeObj = this;
      let deviceManager = applicationManager.getDeviceUtilManager();
      let navData = applicationManager.getNavigationManager().getCustomInfo("frmLogin");
      let identifierTouch = {"identifier": navData.userName};
      let authData = kony.keychain.retrieve(identifierTouch);
      if(deviceManager.isTouchIDSupported()){
        scopeObj.setVisibilityForSpecificLoginType('touchid');
        if(navData.isIphone){
          if (!kony.sdk.isNullOrUndefined(authData.secureaccount) && !kony.sdk.isEmptyObject(authData.secureaccount) && !kony.sdk.isNullOrUndefined(authData.securedata)){
            scopeObj.statusCB(authData);
          } else {
            scopeObj.setTouchIdflag(false);
            scopeObj.setDefaultMode("password");
            scopeObj.performActionOnForm('onPopupVisible', true);
            scopeObj.setVisibilityForSpecificLoginType("password");
          }
        } else {
          scopeObj.showTouchIdAndroid();
          kony.application.setApplicationCallbacks({onbackground: ()=>{
            scopeObj.cancelTouchIdAuth();            
          }});
          scopeObj.performActionOnForm('onPopupVisible', false);
          //           this.login.showTouchIdAndroid();
          //           kony.application.setApplicationCallbacks({onbackground: ()=>{
          //             this.view.loginPopups.cancelTouchIdAuth();
          //             this.view.loginPopups.closeTouchIdScreenPopup();
          //           }});
        }
      } else {
        scopeObj.setTouchIdflag(false);
        scopeObj.setDefaultMode("password");
        scopeObj.performActionOnForm('onPopupVisible', true);
        this.setVisibilityForSpecificLoginType("password");
      }
    },
    
    statusCB : function(status){
      const scopeObj = this;
      this.doLoginonTouchId();
      scopeObj.performActionOnForm('onPopupVisible', true);      
    },
    
    resetUI: function(){
      this.assignDefaultSkins();
      this.assignDefaultText();
      this.view.imgPwdVisiblityToggle.src = this._textVisiblityOffIcon;
      this.view.btnLogIn.setEnabled(false);
      this.view.tbxPassword.secureTextEntry = true;
      this.view.flxLoginPassword.top = "27%";
      this.view.flxContent.forceLayout();
      this.view.forceLayout();
    },
    
    assignDefaultSkins: function(){
      this.view.tbxUsername.skin = this._textBoxNormalSkin;
      this.view.tbxPassword.skin = this._textBoxNormalSkin;
      this.view.lblRememberMe.skin = this._rememberMeLabelSkin;
      if(this.LoginUtility.getDeviceName() === "iPhone"){
        this.view.switchRememberMe.skin = this._rememberMeSwitchSkin;
      } else {
        this.view.switchRememberMe.skin = this._rememberMeSwitchSkin;
      }
      this.view.btnLogIn.skin = this._btnLoginDisabledSkin;
      this.view.btnTouchId.skin = this._btnTouchPinFaceIdSkin;
      this.view.btnFaceId.skin = this._btnTouchPinFaceIdSkin;
      this.view.btnPinId.skin = this._btnTouchPinFaceIdSkin;
      this.view.PinEntryLabel.skin = this._pinPopupTitleSkin;
      this.view.btnOne.skin = this._pinPopupNumbersSkin;
      this.view.btnTwo.skin = this._pinPopupNumbersSkin;
      this.view.btnThree.skin = this._pinPopupNumbersSkin;
      this.view.btnFour.skin = this._pinPopupNumbersSkin;
      this.view.btnFive.skin = this._pinPopupNumbersSkin;
      this.view.btnSix.skin = this._pinPopupNumbersSkin;
      this.view.btnSeven.skin = this._pinPopupNumbersSkin;
      this.view.btnEight.skin = this._pinPopupNumbersSkin;
      this.view.btnNine.skin = this._pinPopupNumbersSkin;
      this.view.btnZero.skin = this._pinPopupNumbersSkin;
      this.view.flxCross.skin = this._pinPopupClearButtonSkin;
      this.view.lblCancel.skin = this._popupCancelButtonSkin;
      this.view.lblFaceIdTitle.skin = this._faceIDPopupTitleSkin;
      this.view.lblAuthenticateMsgFI.skin = this._faceIDPopupDescSkin;
      this.view.lblCancelFI.skin = this._popupCancelButtonSkin;
      this.view.imgFaceId.src = this._faceIDPopupImage;
    },
    
    assignDefaultText: function(){
      this.view.tbxUsername.placeholder = this._tbx1PlaceholderText;
      this.view.tbxPassword.placeholder = this._tbx2PlaceholderText;
      this.view.lblRememberMe.text = this._lblRememberMeText;
      this.view.lblForgotPwd.text = this._lblForgotPasswordText;
      this.view.btnLogIn.text = this._submitButtonText;
      if(this.LoginUtility.getDeviceName() === "iPhone"){
        this.view.btnTouchId.text = this._touchIDBtnText;
      }
      else{
        this.view.btnTouchId.text = kony.i18n.getLocalizedString("kony.mb.devReg.Biometric");
      }
      this.view.btnFaceId.text = this._faceIDBtnText;
      this.view.btnPinId.text = this._pinIDBtnText;
      this.view.PinEntryLabel.text = this._pinPopupTitleText;
      this.view.lblCancel.text = this._popupCancelBtnText;
      this.view.lblFaceIdTitle.text = this._faceIDPopupTitleText;
      this.view.lblAuthenticateMsgFI.text = this._faceIDPopupDescText;
      this.view.lblCancelFI.text = this._popupCancelBtnText;
    },
    
    btnLoginOnClick: function() {
      const scopeObj = this;
      this.LoginUtility.detectDynamicInstrumentation();
      this.LoginUtility.showLoadingScreen();
      let enteredUserName = this.view.tbxUsername.text.trim();
      let navManager = applicationManager.getNavigationManager();
      navManager.setCustomInfo("frmLoginusername",enteredUserName);
      var userNameDetails=applicationManager.getStorageManager().getStoredItem("maskUserName");
      if (scopeObj.LoginUtility.isUserNameMasked(enteredUserName) && userNameDetails["maskedUserName"]===enteredUserName) {
        enteredUserName=userNameDetails["backendUserName"];
        navManager.setCustomInfo("frmLoginusername",enteredUserName);
      }
      let enteredPassword = this.view.tbxPassword.text.trim();      
      let UsernamePasswordJSON = {"username":enteredUserName,"password":enteredPassword};
      this.currentAuthMode = "password";
      this.JSONUsernamePassword = UsernamePasswordJSON;
      if (UsernamePasswordJSON.username && UsernamePasswordJSON.password) {
      	this.login(UsernamePasswordJSON);
      } else {
        this.LoginUtility.dismissLoadingScreen();
        this.bindLoginErrorMessage(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Invalid.Username.or.Password"));
      }
    },
    
    login: function(UsernamePasswordJSON) {
      const scopeObj = this;
      scopeObj.currentAuthMode = "password";
      let authParams = {
        "UserName": UsernamePasswordJSON.username,
        "Password": UsernamePasswordJSON.password,
        "loginOptions": {
          "isOfflineEnabled": false
        }
      };
      let identityServiceName = this._identityServiceName;
      this.LoginDAO.login(authParams, scopeObj.onLoginSuccessCallback, scopeObj.onLoginFailure, identityServiceName);
    },
    
    onLoginSuccessCallback: function(resSuccess){
      const scopeObj = this;
      let loginSuccessObj = {
        "resSuccess": resSuccess,
        "currentAuthMode": scopeObj.currentAuthMode,
        "rememberdeviceregflag": false,
        "UsernamePasswordJSON": scopeObj.JSONUsernamePassword
      };
      this.performActionOnForm('onLoginSuccess', loginSuccessObj);
    },
    
    onLoginFailure: function(resError) {
      this.LoginUtility.dismissLoadingScreen();
      let errMsg = resError.errmsg.errorMessage;
      this.bindLoginErrorMessage(errMsg);
    },
    
    bindLoginErrorMessage(err){
      const scopeObj = this;
      applicationManager.getDataProcessorUtility().showToastMessageError(this,err,scopeObj.clearUsernamePwd);
    },
    
    rememberMeOption: function(){
      const scopeObj = this;
      var rememberMeSwitchValue = this.view.switchRememberMe.selectedIndex;
      var loginData = applicationManager.getNavigationManager().getCustomInfo("frmLogin");
      if(rememberMeSwitchValue === 0) {
        scopeObj.setRememberMeFlag(true);
        applicationManager.getDataforLogin();
      } else {
        if (loginData.istouchIdEnabled || loginData.isPinModeEnabled || loginData.isFacialAuthEnabled)
          this.showTouchIdOffAlert(applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.rememberMe.Msg"),applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.rememberMeTittle"));
        else
          this.OffLoginFeatures_RememberOff();
      }
    },
    
    showTouchIdOffAlert : function(msg,title){
      kony.ui.Alert({
        "message": msg,
        "alertHandler": this.alertrememberCallback,
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "yesLabel": "Disable",
        "noLabel": "Cancel",
        "alertTitle": title
      },{});
    },
    
    alertrememberCallback : function(response){
      const scopeObj = this;
      if (response === true){
        this.OffLoginFeatures_RememberOff();
      } else {
        this.view.imgCheckBox.src = "remembermetick.png";
        scopeObj.setRememberMeFlag(true);
      }
    },
    
    OffLoginFeatures_RememberOff : function(){
      const scopeObj = this;
      this.setVisibilityForSpecificLoginType("password");
      scopeObj.performActionOnForm('hideDashboardIcon', null);
      scopeObj.setLoginFeaturesOff();
    },
    
    flxPwdVisiblityToggleOnClick: function() {
      if (this.view.imgPwdVisiblityToggle.src === this._textVisiblityOffIcon) {
        this.view.imgPwdVisiblityToggle.src = this._textVisiblityOnIcon;;
        this.view.tbxPassword.secureTextEntry = false;
        this.view.flxContent.forceLayout();
      } else {
        this.view.imgPwdVisiblityToggle.src = this._textVisiblityOffIcon;
        this.view.tbxPassword.secureTextEntry = true;
        this.view.flxContent.forceLayout();
      }
    },
    
    enableLoginButton: function(){        
      const scopeObj = this;
      if(scopeObj.view.tbxUsername.text!=='' && scopeObj.view.tbxUsername.text!==null && scopeObj.view.tbxUsername.text!==undefined && scopeObj.view.tbxPassword.text!=='' && scopeObj.view.tbxPassword.text!==null && scopeObj.view.tbxPassword.text!==undefined){
        scopeObj.view.btnLogIn.setEnabled(true);
        scopeObj.view.btnLogIn.skin = "sknBtn0095e426pxEnabled";
      } else {
        scopeObj.view.btnLogIn.setEnabled(false);
        scopeObj.view.btnLogIn.skin = "sknBtna0a0a0SSPReg26px";
      }
    },
    
    clearUsernamePwd: function(){
      const scopeObj = this;
      var userNameDetails = applicationManager.getStorageManager().getStoredItem("maskUserName");
      if(userNameDetails && userNameDetails["maskedUserName"]){
        scopeObj.view.tbxUsername.text = userNameDetails["maskedUserName"];
      } else{
        scopeObj.view.tbxUsername.text = "";
      }
      scopeObj.view.tbxPassword.text = "";
      scopeObj.view.btnLogIn.skin = "sknBtna0a0a0SSPReg26px";
      scopeObj.view.btnLogIn.setEnabled(false);
      scopeObj.view.flxContent.forceLayout();
    },
    
    manageUname: function(loginData){
      if(loginData.isRememberMeOn !== true){
        this.view.tbxUsername.text = "";
        this.view.tbxPassword.text = "";
        this.view.switchRememberMe.selectedIndex = 1;
      } else {
        if(loginData.isFirstTimeLogin !== true){
          this.view.tbxUsername.text = this.LoginUtility.maskUserName(loginData.userName);
          var maskedUserName=this.view.tbxUsername.text;
          var userNameDetails={};
          userNameDetails["maskedUserName"]=maskedUserName;
          userNameDetails["backendUserName"]=loginData.userName;
          applicationManager.getStorageManager().setStoredItem("maskUserName",userNameDetails);
        } else{
          this.view.tbxUsername.text = "";
        }
        this.view.tbxPassword.text = "";
        this.view.switchRememberMe.selectedIndex = 0;
      }
    },
    
    populateUserName:function(userName){
      this.view.tbxUsername.text = this.LoginUtility.maskUserName(userName);
      var maskedUserName = this.view.tbxUsername.text;
      var userNameDetails={};
      userNameDetails["maskedUserName"]=maskedUserName;
      userNameDetails["backendUserName"]=userName;
      applicationManager.getStorageManager().setStoredItem("maskUserName",userNameDetails);
    },
    
    showPopupIncorrectCredentials: function() {
        var scopeObj=this;
        this.timerCounter=parseInt(this.timerCounter)+1;
        var timerId="timerPopupError"+this.timerCounter;
        this.view.flxPopup.skin = "sknFlxf54b5e";
        this.view.customPopup.imgPopup.src = "errormessage.png";
        this.view.customPopup.lblPopup.text = kony.i18n.getLocalizedString("kony.mb.login.incorrectCredentialsMsg");
        this.view.flxPopup.setVisibility(true);
        kony.timer.schedule(timerId, function() {
            scopeObj.view.flxPopup.setVisibility(false);
            scopeObj.resetSkinsOfUsernameAndPwd();
        }, 1.5, false);
    },
    
    resetSkinsOfUsernameAndPwd: function(){
      this.view.tbxUsername.skin = "sknTbx424242SSPRegular28px";
      this.view.tbxPassword.skin = "sknTbx424242SSPRegular28px";
      if(this.view.tbxPassword.text !=='' && this.view.tbxPassword.text!==null && this.view.tbxUsername.text!==undefined){
        this.view.btnLogIn.skin = "sknBtn0095e4RoundedffffffSSP26px";
        this.view.btnLogIn.setEnabled(true);
      }
      this.view.flxContent.forceLayout();
    },
    
    animateLoginFlex: function(topValue){
      this.view.flxLoginPassword.animate(
        kony.ui.createAnimation({
          "100": {
            "anchorPoint": {
              "x": 0.5,
              "y": 0.5
            },
            "stepConfig": {
              "timingFunction": kony.anim.EASE
            },
            "rectified": true,
            "top": topValue,
          }
        }), {
          "delay": 0,
          "iterationCount": 1,
          "fillMode": kony.anim.FILL_MODE_FORWARDS,
          "duration": 0.3
        }, {
          "animationEnd": function() {}
        });
    },
    
    /*LOGIN POPUPS COMMON FUNCTIONALITY BEGIN ===============================================================================*/   
    setVisibilityForSpecificLoginType: function(loginType="password"){
      // Method Exposed
      /*
      	loginType is expected to be one of the following:        
        'pin': Login using 6-Digit Pin 
        'touchid': Login using TouchID
        'faceid': Login using FaceID (Available only in iPhone)
        'password' : Normal Login Flow using Password
      */
      switch(loginType){
        case 'pin':
          this.view.flxLoginPopups.setVisibility(true);
          this.view.flxButtons.setVisibility(true);
          this.view.flxEnterPin.setVisibility(true);
          this.view.flxFaceIdPopUp.setVisibility(false);          
          this.clearProgressFlexLogin();
          break;
        case 'touchid':
          this.view.flxLoginPopups.setVisibility(false);
          this.view.flxButtons.setVisibility(true);
          break;
        case 'faceid':
          this.view.flxLoginPopups.setVisibility(true);
          this.view.flxButtons.setVisibility(true);
          this.view.flxEnterPin.setVisibility(false);
          // AAC-7468
          //this.view.flxFaceIdPopUp.setVisibility(true);
          break;
        case 'password':
          this.view.flxLoginPopups.setVisibility(false);
          this.view.flxButtons.setVisibility(false);
          break;
      }
      this.setButtonVisibility(loginType);
    },
    
    setButtonVisibility: function(loginType){
      switch(loginType){
        case 'pin':
          this.view.btnTouchId.setVisibility(false);
          this.view.btnFaceId.setVisibility(false);
          this.view.btnPinId.setVisibility(true);
          break;
        case 'touchid':
          this.view.btnTouchId.setVisibility(true);
          this.view.btnFaceId.setVisibility(false);
          this.view.btnPinId.setVisibility(false);
          break;
        case 'faceid':
          this.view.btnTouchId.setVisibility(false);
          this.view.btnFaceId.setVisibility(true);
          this.view.btnPinId.setVisibility(false);
          break;
        case 'password':
          this.view.btnTouchId.setVisibility(false);
          this.view.btnFaceId.setVisibility(false);
          this.view.btnPinId.setVisibility(false);
          break;
      }
    },
    
    closePopup:function(loginType){
      switch(loginType){
        case 'pin':
          this.dialPadNo = "";
          this.lengthOfDialNo = 0;
          this.view.flxLoginPopups.setVisibility(false);
          this.view.flxEnterPin.setVisibility(false);
          break;
        case 'faceid':
          this.view.flxLoginPopups.setVisibility(false);
          this.view.flxFaceIdPopUp.setVisibility(false);
          break;
      }
    },    
    /* LOGIN POPUPS COMMON FUNCTIONALITY END =============================================================================== */
    

    /* TOUCH-ID BEGIN=============================================================================== */
    showTouchIdAndroid : function(){
      let loginData = applicationManager.getNavigationManager().getCustomInfo("frmLogin");
      let deviceManager = applicationManager.getDeviceUtilManager();
      if(loginData && loginData.defaultAuthMode == "touchid" &&  deviceManager.isTouchIDSupported()){
        var config = {
          "promptMessage": "Unlock Infinity",                   
          "fallbackTitle": "",
          "description": "Please verify the account",  
          "subTitle" : "",  
          "deviceCredentialAllowed" : false,   
          "confirmationRequired" : false,  
          "negativeButtonText" : "Cancel" 
        };
        kony.localAuthentication.authenticate(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID,this.authCallBack,config);
      }
    },
    
    authCallBack: function(status,msg){
      const scopeObj = this;
      scopeObj.performActionOnForm('onPopupVisible', true);
      if (status == 5000){
        this.doLoginonTouchId();      
      } else if (status == 5001){
        kony.print("Please Try Again");
      } else if (status == 5002 || status == 5003 || status == 5004){
        kony.print("Authentication cancelled");
      } else if(status == 5009){
        kony.print("Touch id temporarily locked due to invalid attempts");
      } else if(status == 5011){
        kony.print("Authentication locked due to multiple invalid attempts")
      } else{
        kony.print("Auth error");
      }
    },
    
    doLoginonTouchId : function (){
      this.LoginUtility.showLoadingScreen();
      let navData = applicationManager.getNavigationManager().getCustomInfo("frmLogin");
      let uName = navData.userName;
      let password = navData.password;
      this.currentAuthMode = "";
      let UsernamePasswordJSON = {"username":uName,"password":password};
      this.loginTouch(UsernamePasswordJSON);
    },
    
    loginTouch: function(UsernamePasswordJSON) {
      const scopeObj = this;     
      let authParams = {
        "UserName": UsernamePasswordJSON.username,
        "Password": UsernamePasswordJSON.password,
        "loginOptions": {
          "isOfflineEnabled": false
        }
      };
      let identityServiceName = this._identityServiceName;
      this.LoginDAO.login(authParams, scopeObj.onLoginSuccessCallback, scopeObj.onLoginFailure, identityServiceName);      
    },
    
    cancelTouchIdAuth : function(){
      let deviceManager = applicationManager.getDeviceUtilManager();
      let loginData = applicationManager.getNavigationManager().getCustomInfo("frmLogin");
      if(loginData && loginData.defaultAuthMode === "touchid" &&  deviceManager.isTouchIDSupported()){
        kony.localAuthentication.cancelAuthentication();
      }
    }, 
    /* TOUCH-ID END=============================================================================== */
    
    
    /* PIN-SCREEN BEGIN=============================================================================== */
    getNumber: function(num) {      
      this.lengthOfDialNo = this.dialPadNo.length;
      this.changeSkinOfProgressBartoActive();
      if (this.lengthOfDialNo < 6) {
        this.dialPadNo = "" + this.dialPadNo + num;
      }
      if (this.dialPadNo.length === 6){
        let pinNo = this.dialPadNo;        
        this.currentAuthMode = "";
        this.onPinLogin(pinNo);
        this.dialPadNo = "";
        this.lengthOfDialNo = 0;
      }
    },
    
    onPinLogin: function(pin) {
      const scopeObj = this;
      this.LoginUtility.showLoadingScreen();
      let userMan = applicationManager.getUserPreferencesManager();
      let userName = userMan.getUserName();
      let deviceUtilManager = applicationManager.getDeviceUtilManager();
      let deviceID = deviceUtilManager.getDeviceInfo().deviceID;
      let authParams = {
        "UserName": userName,
        "pin": pin,
        "deviceId": deviceID
      };
      let identityServiceName = this._identityServiceName;
      this.LoginDAO.login(authParams, scopeObj.onLoginSuccessCallback, scopeObj.onPinFailure, identityServiceName);
    },
    
    onPinFailure: function(err) {
      this.LoginUtility.dismissLoadingScreen();
      if (err.errmsg["isServerUnreachable"]){
        applicationManager.getPresentationInterruptHandler().showErrorMessage("postLogin", err.errmsg);
      } else {
        this.bindPinError(kony.i18n.getLocalizedString("kony.mb.Please.enter.a.valild.pin"));
      }
    },
       
    bindPinError(err){
      const scopeObj = this;      
      applicationManager.getDataProcessorUtility().showToastMessageError(this,err,scopeObj.clearProgressFlexLogin);
    },
    
    setDialPadActions: function() {
      var scopeObj = this;
      this.view.btnOne.onTouchStart = function() {
        scopeObj.getNumber("1");
      };
      this.view.btnTwo.onTouchStart = function() {
        scopeObj.getNumber("2");
      };
      this.view.btnThree.onTouchStart = function() {
        scopeObj.getNumber("3");
      };
      this.view.btnFour.onTouchStart = function() {
        scopeObj.getNumber("4");
      };
      this.view.btnFive.onTouchStart = function() {
        scopeObj.getNumber("5");
      };
      this.view.btnSix.onTouchStart = function() {
        scopeObj.getNumber("6");
      };
      this.view.btnSeven.onTouchStart = function() {
        scopeObj.getNumber("7");
      };
      this.view.btnEight.onTouchStart = function() {
        scopeObj.getNumber("8");
      };
      this.view.btnNine.onTouchStart = function() {
        scopeObj.getNumber("9");
      };
      this.view.btnZero.onTouchStart = function() {
        scopeObj.getNumber("0");
      };
      this.view.flxCross.onTouchStart = function() {
        scopeObj.flxCancelDialPadOnClick();
      };
      this.view.flxCross.onClick = function() {
        // overwritting the actions onclick
      };
    },
    
    flxCancelDialPadOnClick: function() {
      this.dialPadNo = this.dialPadNo.slice(0, -1);
      this.lengthOfDialNo = this.dialPadNo.length;
      this.changeSkinOfProgressBartoInactive();
      this.view.flxDialPad.forceLayout();
    },
    
    clearProgressFlexLogin:function(){
      for(let i=6; i>=1;i--){
        this.view["flxProgressButton"+i].skin="sknFlxa0a0a0B";
      }
    },
    
    changeSkinOfProgressBartoActive: function(){
      this.view["flxProgressButton"+(this.lengthOfDialNo + 1)].skin = "sknFlxa0a0a0filled";
      this.view.flxProgressButtons.forceLayout();
    },
    
    changeSkinOfProgressBartoInactive: function(){
      this.view["flxProgressButton"+(this.lengthOfDialNo + 1)].skin = "sknFlxa0a0a0B";
      this.view.flxProgressButtons.forceLayout();
    },
    /* PIN-SCREEN END=============================================================================== */
  };
});