define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Form_b3b86cb88da6464d87897d6fb4e034eb: function AS_Form_b3b86cb88da6464d87897d6fb4e034eb(eventobject) {
        var self = this;
        this.init();
    },
    AS_Form_c7d212c165ce46cda338b5d745a4bd25: function AS_Form_c7d212c165ce46cda338b5d745a4bd25(eventobject) {
        var self = this;
        this.preShow();
    },
    AS_BarButtonItem_eaf2af79cb4446a18d7530296c912028: function AS_BarButtonItem_eaf2af79cb4446a18d7530296c912028(eventobject) {
        var self = this;
        this.navigateCustomBack();
    },
    AS_BarButtonItem_a5433a01046644fbb31081a4df532618: function AS_BarButtonItem_a5433a01046644fbb31081a4df532618(eventobject) {
        var self = this;
        this.cancelOnClick();
    }
});