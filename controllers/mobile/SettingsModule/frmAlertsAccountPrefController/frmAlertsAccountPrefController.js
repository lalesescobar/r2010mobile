define({

  sec: -1,
  row: -1,
  segmentHeight:0,
  payeesList:0,
  list: [],
  rowTemplateHeight:70,
  sectionTemplateHeight:60,
  rowTemplateP2PHeight:110,
  searchData: [],
  isUpdate: false,

  
  init: function () {
  },

 
  //Purpose is to scroll to the selected row while moving to and fro between screens.
  postShow: function() {
  },
  
  preShow: function () {
    var self = this;
    if (kony.os.deviceInfo().name === "iPhone") {
      this.view.flxHeader.isVisible = false;
      this.view.flxDescription.top = "0dp";
      this.view.segTransactions.top = "0dp";
    } else {
      this.view.flxHeader.isVisible = true;
      this.view.flxDescription.top = 55 + "dp";
     
    }
    this.view.flxNoTransactions.isVisible = false;
	var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    if((!this.isUpdate) && applicationManager.getConfigurationManager().getAccountIDLevelAlertsFlag() === true){
		this.view.flxAlertsAccountLevel.isVisible = true;
		this.view.flxAlertsAccountType.isVisible = false;
		this.setSegmentDataAccountLevel();
    }
    else if(!this.isUpdate){
		this.view.flxAlertsAccountLevel.isVisible = false;
		this.view.flxAlertsAccountType.isVisible = true;
		this.setSegmentDataAccountType();
	}
    this.view.customHeader.btnRight.isVisible =false; 
    var headerText = navManager.getCustomInfo("accountAlertsData").lblTitle;
    this.view.customHeader.lblLocateUs.text = headerText;
    this.view.title = headerText;
    this.initActions();
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
  },
 
  cancelOnClick:function(){
 
 },
  
  initActions: function () {
    this.view.customHeader.flxBack.onClick = function() {
      applicationManager.getPresentationUtility().showLoadingScreen();
      var navMan=applicationManager.getNavigationManager();
      navMan.navigateTo("frmSettings");
    };
    this.view.segTransactions.onRowClick = this.onSegRowClick;
    this.view.segTransactions1.onRowClick = this.onSegRowClick;
  },
  
  onSegRowClick : function() {
    var secindex,rowindex,selectedAccount,params;
    var settingsModule = applicationManager.getModulesPresentationController("SettingsModule");
    if(this.view.flxAlertsAccountType.isVisible){
      if (Array.isArray(this.view.segTransactions1.data[0])) {
        secindex = Math.floor(this.view.segTransactions1.selectedRowIndex[0]);
        rowindex = Math.floor(this.view.segTransactions1.selectedRowIndex[1]);
        selectedAccount = this.view.segTransactions1.data[secindex][1][rowindex];
      } else {
        rowindex = Math.floor(this.view.segTransactions1.selectedRowIndex[1]);
        selectedAccount = this.view.segTransactions1.data[rowindex];
      }
      params={
        "AlertCategoryId":"ALERT_CAT_ACCOUNTS",
        "AccountTypeId":selectedAccount["accountTypeId"]
      };
       var accountTypeID = selectedAccount.accountTypeId;
       settingsModule.setAccountTypeID(accountTypeID);
	   var navManager = applicationManager.getNavigationManager();
      navManager.setCustomInfo("frmAlertsListHeader",selectedAccount["accountType"]);
    }
    else{
      secindex = Math.floor(this.view.segTransactions.selectedRowIndex[0]);
      rowindex = Math.floor(this.view.segTransactions.selectedRowIndex[1]);
      selectedAccount = this.view.segTransactions.data[secindex][1][rowindex];
      params={
        "AlertCategoryId":"ALERT_CAT_ACCOUNTS",
        "AccountId": selectedAccount["accountID"]
      };
    var accountID = selectedAccount.accountID;
    settingsModule.setAccountID(accountID);
	var navManager = applicationManager.getNavigationManager();
    navManager.setCustomInfo("frmAlertsListHeader",selectedAccount["processedName"]);
    }
    settingsModule.alertsCurrency=selectedAccount["transactionCurrency"];
    settingsModule.getAlertsBasedOnAccounts(params);
  },
  //This executes when the form is loaded ahead and services are in transition state and after service execution this is invoked from presentation layer.
  bindDataAfterTransition:function(flag){
    if (flag) {
      applicationManager.getPresentationUtility().dismissLoadingScreen();
    }
    if(this.segmentHeight===0){
      this.setSegmentData();
    }
  },
    setAccountsData: function () {
        var data = [
            [{
                "lblHeader": "Saving Accounts (3)"
            },
            [{
                "lblValue": "On",
                "lblAccountName": "SavingsAcc...",
                "lblAccountNumber": "2343",
                "lblBankName": "Bank of America"
            },
            {
                "lblValue": "On",
                "lblAccountName": "SavingsAcc...",
                "lblAccountNumber": "5486",
              	"imgArrow": "chevron.png",
                "lblBankName": "Bank of America"
            },
            {
                "lblValue": "On",
                "lblAccountName": "SavingsAcc...",
                "lblAccountNumber": "2223",
                "lblBankName": "Bank of America"
            },
            ]
            ],
            [{
                "lblHeader": "Checking Accounts (3)"
               
            },
            [{
                "lblValue": "On",
                "lblAccountName": "CheckingAcc...",
                "lblAccountNumber": "2343",
                "lblBankName": "Bank of America"
            },
            {
               "lblValue": "On",
                "lblAccountName": "CheckingAcc...",
                "lblAccountNumber": "2111",
                "lblBankName": "Bank of America"
            },
            ]
            ],
            [{
                "lblHeader": "Credit Cards (3)",
              
            },
            [{
                "lblValue": "On",
                "lblAccountName": "CreditCard...",
                "lblAccountNumber": "2233",
                "lblBankName": "Bank of America"
            },
            {
                "lblValue": "On",
                "lblAccountName": "CreditCard...",
                "lblAccountNumber": "2353",
                "lblBankName": "Bank of America"
            },
            ]
            ]
            
        ];
       this.view.segTransactions.setData(data);
      
    },
  setSegmentDataAccountLevel(){
    var navManager = applicationManager.getNavigationManager();
    var accountList = navManager.getCustomInfo("frmAlertsAccountPref");
    this.processedData = this.processData(accountList);
    var viewFormatData = this.processViewFormattedData(this.processedData);
    viewFormatData=this.orderByPriority(viewFormatData);
      var segData=[];
      if(this.processedData.length>0){
        for(var key in viewFormatData){
          var sectionHeaderData={};
          var combinedData=[];
          if(key != "CreditCard"){
            if (viewFormatData[key].length > 1) {
              sectionHeaderData["lblHeader"] =  key + " "+ applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.mm.accounts");
            } else {
              sectionHeaderData["lblHeader"] =  key + " "+ applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.mm.account");
            }
          }
          else{
            if (viewFormatData[key].length > 1) {
              sectionHeaderData["lblHeader"] = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.mm.creditcards");
            } else {
              sectionHeaderData["lblHeader"] = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.mm.creditcard");
            }
          }
              var rowDataForSection=this.sortByPrefrence(viewFormatData[key]);
          if(rowDataForSection.length>0){
            combinedData.push(sectionHeaderData);
            combinedData.push(rowDataForSection);
            combinedData.push(rowDataForSection);
     //       this.calcualteSegmentRenderedDataHeight(1,rowDataForSection.length);
            segData.push(combinedData);
        }
          
      }
          
      }
      this.view.segTransactions.widgetDataMap=this.getWidgetDataMap();
          this.view.segTransactions.setData(segData);
  },
  processData(data){
     var accProcessedData = [];
    for (var i = 0; i < data.length; i++) {
      accProcessedData[i] = {};
      //var name = "";
      var name = data[i].nickName;
	  accProcessedData[i].nickName = data[i].nickName;
      accProcessedData[i].accountID = data[i].accountID; 
      accProcessedData[i].processedName = applicationManager.getPresentationUtility().formatText(name, 10, data[i].accountID, 4);
      accProcessedData[i].accountTypeId = data[i].accountTypeId;
	  accProcessedData[i].accountType = data[i].accountType;
      accProcessedData[i].bankName = data[i].bankName;
      accProcessedData[i].isEnable =  data[i].isEnabled ==="true"?"On":"Off";
      accProcessedData[i].transactionCurrency=data[i].transactionCurrency;
      
    }
    return accProcessedData;
  },
  processViewFormattedData(data) {
    var processedData = {}
    for (var i = 0; i < data.length; i++) {
      if (!processedData.hasOwnProperty(data[i].accountType)) {
        processedData[data[i].accountType] = [];
      }
      if (processedData.hasOwnProperty(data[i].accountType)) {
        processedData[data[i].accountType].push(data[i]);
      }
    }
   return processedData;
  },
  updateData(accountName){
    this.isUpdate = true;
    if(applicationManager.getConfigurationManager().getAccountIDLevelAlertsFlag() === true){
    var segData = this.view.segTransactions.data;
     var recordID,recNameID;
        for(var i=0;i<segData.length;i++){
          if(recNameID != null && recNameID != "" && recNameID != undefined)break;
          for(var j=0;j<segData[i][1].length;j++){
            if(segData[i][1][j].processedName===accountName){
                recordID = i;
                recNameID = j;
                break;
            }
          }
        }
        segData[recordID][1][recNameID].isEnable = segData[recordID][1][recNameID].isEnable==="On"?"Off":"On";
        this.view.segTransactions.setData(segData);
    }
    else{
      var segData = this.view.segTransactions1.data;
     var recordID;
        for(var i=0;i<segData.length;i++){
            if(segData[i].accountType===accountName){
                recordID = i;break;
            }
        }
        segData[recordID].isEnable = segData[recordID].isEnable==="On"?"Off":"On";
        this.view.segTransactions1.setData(segData);
    }
  },
  orderByPriority(data) {
    var cm = applicationManager.getConfigurationManager();
    var prioritizedData = {};
    var metaData = cm.getAccountTypesMetaData();
    for (var key1 in metaData) {
      if (data[metaData[key1].backendValue]) {
        prioritizedData[metaData[key1].backendValue] = data[metaData[key1].backendValue];
      }
    }
    return prioritizedData;
  },
    sortByPrefrence(accountsCollection) {
    if (accountsCollection.length > 1) accountsCollection.sort(function(record1, record2) {
      return record1.accountPreference - record2.accountPreference;
    });
    return accountsCollection;
  },
   getWidgetDataMap:function(){
    var dataMap={
      lblAccountName:"processedName",
      lblBankName:"bankName",
      lblValue: "isEnable",
      lblHeader: "lblHeader"
    };
	 return dataMap;
   },
	getWidgetDataMapAT:function(){
    var dataMap={
      lblName:"accountType",
      lblValue:"isEnable"
    
    };
    return dataMap;
  },
  setSegmentDataAccountType: function(){
    var navManager = applicationManager.getNavigationManager();
    var accountList = navManager.getCustomInfo("frmAlertsAccountPref");
    this.processedData = this.processAccountTypeData(accountList);
    this.view.segTransactions1.widgetDataMap=this.getWidgetDataMapAT();
    this.view.segTransactions1.setData(this.processedData);
     
  },
   processAccountTypeData(data){
     var accProcessedData = [];
    for (var i = 0; i < data.length; i++) {
      accProcessedData[i] = {};
	   accProcessedData[i].accountTypeId = data[i].accountTypeId;
      accProcessedData[i].accountType = data[i].accountType;
      accProcessedData[i].isEnable =  data[i].isEnabled ==="true"?"On":"Off";
    }
 
    return accProcessedData;
  },
  setUpdateFlag(){
    this.isUpdate = false;
  }
});