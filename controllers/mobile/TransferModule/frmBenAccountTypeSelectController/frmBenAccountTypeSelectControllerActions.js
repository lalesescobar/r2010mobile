define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmBenAccountTypeSelect **/
    AS_Form_ffea859fded64f40b039fc2d0471c8df: function AS_Form_ffea859fded64f40b039fc2d0471c8df(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmBenAccountTypeSelect **/
    AS_Form_c60f77fcf4a84e019a92b122079bcd18: function AS_Form_c60f77fcf4a84e019a92b122079bcd18(eventobject) {
        var self = this;
        this.frmPreShow();
    },
    /** postShow defined for frmBenAccountTypeSelect **/
    AS_Form_d7a7620f99c14e16902942d8b2b31f99: function AS_Form_d7a7620f99c14e16902942d8b2b31f99(eventobject) {
        var self = this;
        this.frmPostShow();
    }
});