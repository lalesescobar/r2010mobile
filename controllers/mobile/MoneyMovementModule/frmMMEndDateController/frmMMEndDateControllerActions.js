define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnContinue **/
    AS_Button_j54a5368ed2c44f9a2fe6fbde1ef9424: function AS_Button_j54a5368ed2c44f9a2fe6fbde1ef9424(eventobject) {
        var self = this;
        var billPayMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        billPayMod.presentationController.commonFunctionForNavigation("frmBillPayConfirmation");
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_b6df400409f44712bf532797b3d6912f: function AS_BarButtonItem_b6df400409f44712bf532797b3d6912f(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        transferModule.presentationController.cancelCommon();
    },
    /** init defined for frmMMEndDate **/
    AS_Form_dc763720df0c41eb8458bebf0740ff0b: function AS_Form_dc763720df0c41eb8458bebf0740ff0b(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmMMEndDate **/
    AS_Form_fbe2cc6c3d964351817bfb4422c7274b: function AS_Form_fbe2cc6c3d964351817bfb4422c7274b(eventobject) {
        var self = this;
        this.preShow();
    },
    /** init defined for frmMMEndDate **/
    AS_Form_a4302bfb08c54f6086513b6c9b12d627: function AS_Form_a4302bfb08c54f6086513b6c9b12d627(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmMMEndDate **/
    AS_Form_c48e962bf79d4ce694e26ae53610be7c: function AS_Form_c48e962bf79d4ce694e26ae53610be7c(eventobject) {
        var self = this;
        this.preShow();
    }
});