define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Form_aef1a87d6ee64102ad411f718f5eb49c: function AS_Form_aef1a87d6ee64102ad411f718f5eb49c(eventobject) {
        var self = this;
        this.init();
    },
    AS_Form_a9ea13cb6e09400ba18961e96ba25d13: function AS_Form_a9ea13cb6e09400ba18961e96ba25d13(eventobject) {
        var self = this;
        this.preShow();
    },
    AS_BarButtonItem_d3c0dbac917a476281f34498b720e18a: function AS_BarButtonItem_d3c0dbac917a476281f34498b720e18a(eventobject) {
        var self = this;
        var navMan = applicationManager.getNavigationManager();
        navMan.goBack();
    }
});