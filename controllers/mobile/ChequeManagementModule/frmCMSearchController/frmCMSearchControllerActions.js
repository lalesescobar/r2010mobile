define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_d6b695d7802c4c4e805886a49b7b3bb2: function AS_FlexContainer_d6b695d7802c4c4e805886a49b7b3bb2(eventobject) {
        var self = this;
        this.navigateToBack();
    },
    AS_Form_b6b5a5f8688c4b47a27d8e8a56d5f7c8: function AS_Form_b6b5a5f8688c4b47a27d8e8a56d5f7c8(eventobject) {
        var self = this;
        this.initActions();
    },
    AS_Form_cb63aca867a14972a5e10f4e5fb2d7c6: function AS_Form_cb63aca867a14972a5e10f4e5fb2d7c6(eventobject) {
        var self = this;
        this.postShow();
    },
    AS_Form_bb10d60d6f50477cac15242d35ea849a: function AS_Form_bb10d60d6f50477cac15242d35ea849a(eventobject) {
        var self = this;
        this.preShow();
    }
});