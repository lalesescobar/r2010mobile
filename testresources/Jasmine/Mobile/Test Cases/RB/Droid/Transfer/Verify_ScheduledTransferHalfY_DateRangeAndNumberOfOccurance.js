it("Verify_ScheduledTransferHalfY_DateRangeAndNumberOfOccurance", async function() {
  SearchInFromAndToScreen("normal");
  EnterAmount();
  SelectFrequencyHalfYearly();
  SelectDateRangeAndConfirmTransfer("HalfY","normal");

  SearchInFromAndToScreen("normal");
  EnterAmount();
  SelectFrequencyHalfYearly();
  SelectNumberOfTransfersAndThenConfirmTransfer("HalfY","normal");
  Logout();
},180000);