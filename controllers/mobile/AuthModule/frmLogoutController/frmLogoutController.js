define({ 

  //Type your controller code here 
  init:function(){
    var scope=this;
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(scope,"YES",currentForm);
    this.view.btnLogIn.onClick=scope.signInOnClick;
  },
  preshow:function(){
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  },
  signInOnClick:function(){
    var authMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
    authMod.presentationController.signInFromLogoutScreen();
  }

});