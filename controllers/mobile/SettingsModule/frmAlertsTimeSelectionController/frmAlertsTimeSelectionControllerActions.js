define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_gfcc7b14d0b24bc6915779b89cc987c3: function AS_BarButtonItem_gfcc7b14d0b24bc6915779b89cc987c3(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        transferModule.presentationController.cancelCommon();
    },
    /** init defined for frmAlertsTimeSelection **/
    AS_Form_e559df3e2c604950a61a6727120350f1: function AS_Form_e559df3e2c604950a61a6727120350f1(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmAlertsTimeSelection **/
    AS_Form_d22db0f11c9d45cb8cf3b054a36cab3c: function AS_Form_d22db0f11c9d45cb8cf3b054a36cab3c(eventobject) {
        var self = this;
        this.preShow();
    }
});