define({
  popupMsg: '',
  timerCounter: 0,
  filterViewRendered: false,
  onNavigate: function(obj) {
    if (obj === undefined) {
      var newObj = {
        "popup": "none"
      };
      obj = newObj;
    }
    if (obj.popup === "successAddRecipient") {
      this.popupMsg = kony.i18n.getLocalizedString("kony.mb.p2p.successAddRecipient");
    }
    if (obj.popup === "none") {
      this.popupMsg = '';
    }
  },
  segmentData: [],

  init: function() {
    var currentFormObject = kony.application.getCurrentForm();
    var currentForm=currentFormObject.id;
    applicationManager.getPresentationFormUtility().initCommonActions(this, "YES", currentForm);
  },
  preShow: function() {
    var self = this;
    if (this.view.flxHeaderSearchbox.height === "40dp") {
      //this.view.flxHeaderSearchbox.isVisible = false;
      this.view.flxHeaderSearchbox.height = "0dp";
    //this.view.flxSearch.isVisible = true;
      this.view.flxSearch.height = "55dp";
      this.view.flxHeader.isVisible = true;
    }
    if (kony.os.deviceInfo().name === "iPhone") {
      this.view.flxHeader.isVisible = false;
      this.view.flxFooter.isVisible = true;
      this.view.flxMainContainer.top = "0dp";
      var MenuHandler =  applicationManager.getMenuHandler();
      MenuHandler.setUpHamburgerForForm(self);
    } else {
      this.view.flxFooter.isVisible = false;
      this.view.flxMainContainer.top = "55dp";
    }
    this.view.flxSearch.top="0dp";
    this.view.flxGradient.top = 0 + "dp";
    this.view.flxSearch.isVisible = true;
    this.view.flxSearch.height = "55dp";
    this.view.flxGradient.isVisible = true;
    this.resetUI();
    this.checkForPopup();
    this.setTabSelectedSkins();
    this.setDataBasedOnSelectedTab();
    //this.setSegmentData();
    //this.addDummyRows(); // added function for docking the header
    this.initActions();
    // if ((this.popupMsg !== null) && (this.popupMsg !== '')) {
    //   this.showPopupSuccess();
    // }
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    this.view.customSearchbox.tbxSearch.placeholder = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.MM.SearchByAmount");
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
    this.view.segTransactions.onScrolling = function() {
      self.transactionsSegmentOnScrolling();
    };
  },

  setTabSelectedSkins: function() {
    var transferPresentationController = applicationManager.getModulesPresentationController("TransferModule");
    if (transferPresentationController.transactionTabSelected === "SCHEDULED") {
      this.view.btnScheduled.skin = "sknTabSwitchWhite";
      this.view.btnPast.skin = "sknTabSwitchGrey";
      this.view.btnScheduled.left = "6dp";
      this.view.btnPast.right = "1dp";
      this.view.btnScheduled.zIndex = 2;
      this.view.btnPast.zIndex = 1;
    }
    else {
      this.view.btnScheduled.skin = "sknTabSwitchGrey";
      this.view.btnPast.skin = "sknTabSwitchWhite";
      this.view.btnScheduled.left = "2dp";
      this.view.btnPast.right = "5dp";
      this.view.btnScheduled.zIndex = 1;
      this.view.btnPast.zIndex = 2;
    }
  },

  setDataBasedOnSelectedTab: function() {
    applicationManager.getPresentationUtility().showLoadingScreen();
    var transferPresentationController = applicationManager.getModulesPresentationController("TransferModule");
    var navManager = applicationManager.getNavigationManager();
    var data = navManager.getCustomInfo("frmEuropeTransferActivities");
    if (transferPresentationController.transactionTabSelected === "SCHEDULED") {
      this.setDataToSegment(data.scheduledTransactionsDetails);
    }
    else {
      this.setDataToSegment(data.pastTransactionsDetails);
    }
  },

  setDataToSegment: function(segmentData) {
    var transferPresentationController = applicationManager.getModulesPresentationController("TransferModule");
    if (segmentData && segmentData.length !== 0) {
      var processedSegmentData = transferPresentationController.filterSegmentDataBasedOnType(segmentData, transferPresentationController.filterTypeSelected);
      var title = (transferPresentationController.transactionTabSelected === "SCHEDULED") ? "description" : "title";
      if (processedSegmentData) {
        this.view.segTransactions.isVisible = true;
        this.view.flxNoTransactions.isVisible = false;
        this.view.segTransactions.widgetDataMap = {
          lblTransaction: title,
          lblDate: "scheduledDate",
          lblAmount: "amount",
          imgTransaction: "transferImage"
        };
        this.segmentData = processedSegmentData;
        this.view.segTransactions.setData(processedSegmentData);
      }
      else {
        this.view.segTransactions.isVisible = false;
        this.view.flxNoTransactions.isVisible = true;
        this.segmentData = [];
      }
    }
    else {
      this.view.segTransactions.isVisible = false;
      this.view.flxNoTransactions.isVisible = true;
    }
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  },

  // addDummyRows: function() {
  //   var segWidgetDataMap = this.view.segTransactions.widgetDataMap;
  //   segWidgetDataMap["flxEmptyHeader"] = "flxEmptyHeader";
  //   segWidgetDataMap["flxEmptyRow"] = "flxEmptyRow";
  //   this.view.segTransactions.widgetDataMap = segWidgetDataMap;
  //   var segData = this.view.segTransactions.data;
  //   if (segData == null || segData == undefined) {
  //     segData = [];
  //   }
  //   var segLength = 0;
  //   for (let i = 0; i < segData.length; i++) {
  //     segLength = segLength + (segData[i][1].length * 70) + 49; //66 is the row height and 49 is the header height
  //   }
  //   segData.unshift([{
  //     "template": "flxEmptyHeader",
  //     "flxEmptyHeader": {
  //       "height": "0dp"
  //     }
  //   },
  //                    [{
  //                      "template": "flxEmptyRow",
  //                      "flxEmptyRow": {
  //                        "height": "55dp"
  //                      }
  //                    }]
  //                   ]);
  //   segLength = segLength + 55;
  //   this.view.segTransactions.setData(segData);
  //   this.segLength = segLength;
  // },
  // transactionsSegmentOnScrolling: function() {
  //   var parallaxSpeed = 1;
  //   var yOffset = this.view.segTransactions.contentOffsetMeasured.y;
  //   if(this.view.flxHeaderSearchbox.height === "40dp")
  //     this.view.flxTransferOptions.top = 40 - (yOffset * parallaxSpeed) + "dp";
  //   else
  //     this.view.flxTransferOptions.top = 55 - (yOffset * parallaxSpeed) + "dp";
  //   this.view.flxSearch.top = 0 - (yOffset * parallaxSpeed) + "dp";
  //   this.view.flxGradient.top = 0 - (yOffset * parallaxSpeed) + "dp";
  // },

  initActions: function() {
    var navMan=applicationManager.getNavigationManager();
    this.view.tbxSearch.onTouchStart = this.showSearch;
    this.view.customSearchbox.btnCancel.onClick = this.cancelSearch;
    this.view.segTransactions.onRowClick=this.segTransactionsOnRowClick;
    this.view.customHeader.flxBack.onClick = function () {
      navMan.goBack();
    };
    this.view.btnScheduled.onClick = this.onScheduledTabSelection;
    this.view.btnPast.onClick = this.onPastTabSelection;
    this.view.customHeader.flxSearch.onClick = this.navigateToFilterTransactions;
  },

  onScheduledTabSelection: function() {
    applicationManager.getPresentationUtility().showLoadingScreen();
    this.view.customSearchbox.tbxSearch.text = "";
    var transactionPresentationController = applicationManager.getModulesPresentationController("TransactionModule");
    transactionPresentationController.getScheduledTransactionsEurope();

  },

  onPastTabSelection: function() {
    applicationManager.getPresentationUtility().showLoadingScreen();
    this.view.customSearchbox.tbxSearch.text = "";
    var transactionPresentationController = applicationManager.getModulesPresentationController("TransactionModule");
    transactionPresentationController.getPastTransactionsEurope();
  },


  segTransactionsOnRowClick: function() {
    var navMan = applicationManager.getNavigationManager();
    var selectedRowIndex=Math.floor(this.view.segTransactions.selectedRowIndex[1]);
    var transactionData = this.view.segTransactions.data[selectedRowIndex];
    navMan.setCustomInfo("frmEuropeTransactionDetails", transactionData);
    navMan.setEntryPoint("frmEuropeTransactionDetails","Transfer");
    navMan.navigateTo("frmEuropeTransactionDetails");
  },

  navigateToFilterTransactions: function() {
    var navMan = applicationManager.getNavigationManager();
    navMan.navigateTo("frmEuropeFilterTransaction");
  },

  // setSegmentData: function() {
  //   var navMan = applicationManager.getNavigationManager();
  //   var transactions = navMan.getCustomInfo("frmEuropeTransferActivities");
  //   if (transactions) {
  //     if (transactions.res !== undefined && transactions.res !== null) {
  //       if (transactions.type === "error") this.showErrorPopup(transactions.res);
  //       else this.showSuccessPopup(transactions.res, transactions.typeOfTransaction);
  //     }
  //     transactions.res = null;
  //     navMan.setCustomInfo("frmEuropeTransferActivities", transactions);
  //     var postedTransaction = transactions.postedTransaction;
  //     var scheduledTransactions = transactions.scheduledTransactions;
  //     this.view.segTransactions.widgetDataMap = {
  //       lblTransaction: "description",
  //       lblDate: "scheduledDate",
  //       lblAmount: "amount",
  //       transactionId: "transactionId",
  //       lblAccount: "fromAccountName",
  //       lblHeader: "lblHeader",
  //       imgAccount: "image"
  //     };
  //     if (postedTransaction.length > 0 && scheduledTransactions.length > 0) {
  //       var data = [
  //         [{
  //           "lblHeader": "Scheduled Transactions"
  //         }, scheduledTransactions],
  //         [{
  //           "lblHeader": "Posted Payments" /*"Posted Transactions"*/
  //         }, postedTransaction]
  //       ];
  //       this.segmentData = data;
  //       this.view.segTransactions.setData(data);
  //       this.pendingaccounts = this.view.segTransactions.data[0][1];
  //       this.postedaccounts = this.view.segTransactions.data[1][1];
  //       this.view.segTransactions.isVisible = true;
  //       this.view.flxNoTransactions.isVisible = false;
  //     } else if (scheduledTransactions.length > 0) {
  //       var data = [
  //         [{
  //           "lblHeader": "Scheduled Transactions"
  //         }, scheduledTransactions]
  //       ];
  //       this.segmentData = data;
  //       this.view.segTransactions.setData(data);
  //       this.pendingaccounts = this.view.segTransactions.data[0][1];
  //       this.postedaccounts = [];
  //       this.view.segTransactions.isVisible = true;
  //       this.view.flxNoTransactions.isVisible = false;
  //     } else if (postedTransaction.length > 0) {
  //       var data = [
  //         [{
  //           "lblHeader": "Posted Payments" /*"Posted Transactions"*/
  //         }, postedTransaction]
  //       ];
  //       this.segmentData = data;
  //       this.view.segTransactions.setData(data);
  //       this.postedaccounts = this.view.segTransactions.data[0][1];
  //       this.pendingaccounts = [];
  //       this.view.segTransactions.isVisible = true;
  //       this.view.flxNoTransactions.isVisible = false;
  //     } else {
  //       this.segmentData = [];
  //       this.pendingaccounts = [];
  //       this.postedaccounts = [];
  //       this.view.segTransactions.isVisible = false;
  //       this.view.flxNoTransactions.isVisible = true;
  //     }
  //   }
  // },

  showSearch: function() {
    var scope = this;
    this.view.flxOuterGradientBlueKA.isVisible = false;
    this.view.flxMainContainer.skin = "sknFlxffffff";
    if (kony.os.deviceInfo().name === "iPhone") {
      if (this.view.flxHeaderSearchbox.height === "40dp") {
        this.view.flxHeaderSearchbox.height = "0dp";
       // this.view.flxHeaderSearchbox.isVisible = false;
        //this.view.flxSearch.isVisible = true;
        this.view.flxSearch.height = "55dp";
        this.view.flxTransactionTabs.isVisible = true;
        this.view.flxSegment.top = "125dp";
        this.view.flxMainContainer.top = "0dp";
        this.view.flxGradient.isVisible = false;
      } else {
        //this.view.flxHeaderSearchbox.isVisible = true;
        this.view.flxHeaderSearchbox.height = "40dp";
        this.view.flxSearch.height = "0dp";
        this.view.flxTransactionTabs.isVisible = false;
        this.view.flxSegment.top = "0dp";
        //this.view.flxSearch.isVisible = false;
        this.view.flxMainContainer.top = "40dp";
        this.view.flxGradient.isVisible = false;
        this.view.customSearchbox.tbxSearch.text = "";
        this.view.customSearchbox.tbxSearch.setFocus(true);
        kony.timer.schedule("timerId", function() {
          scope.view.customSearchbox.tbxSearch.setFocus(true);
        }, 0.1, false);
        this.view.customSearchbox.tbxSearch.onTextChange = this.tbxSearchOnTextChange;
      }
    } else {
      if (this.view.flxHeaderSearchbox.height === "40dp") {
        this.view.flxHeaderSearchbox.height = "0dp";
        //this.view.flxHeaderSearchbox.isVisible = false;
        this.view.flxSearch.height = "55dp";
        //this.view.flxSearch.isVisible = true;
        this.view.flxHeader.isVisible = true;
        this.view.flxMainContainer.top = "56dp";
        this.view.flxTransactionTabs.isVisible = true;
        this.view.flxSegment.top = "125dp";
        this.view.flxGradient.top = "0dp";
      } else {
        this.view.flxGradient.isVisible = false;
        this.view.flxSearch.height = "0dp";
        //this.view.flxSearch.isVisible = false;
        this.view.flxHeader.isVisible = false;
        this.view.flxMainContainer.top = "40dp";
        this.view.flxHeaderSearchbox.height = "40dp";
        this.view.flxTransactionTabs.isVisible = false;
        this.view.flxSegment.top = "0dp";
       // this.view.flxHeaderSearchbox.isVisible = true;
        this.view.customSearchbox.tbxSearch.text = "";
        kony.timer.schedule("timerId", function() {
          scope.view.customSearchbox.tbxSearch.setFocus(true);
        }, 0.1, false);
        this.view.customSearchbox.tbxSearch.onTextChange = this.tbxSearchOnTextChange;
      }
    }
    this.view.flxOuterGradientBlueKA.isVisible = false;
  },

  // removeDummyRows: function() {
  //   var data = this.view.segTransactions.data;
  //   if (data == null || data == undefined) {
  //     kony.print("no data");
  //   } else {
  //     data.shift();
  //     this.view.segTransactions.setData(data);
  //   }
  // },
  
  resetUI: function() {
    this.view.flxHeaderSearchbox.height = "0dp";
    this.view.flxSearch.isVisible = true;
    this.view.flxSearch.height = "55dp";
    this.view.flxGradient.isVisible = true;
    this.view.flxSearch.top = 0 + "dp";
    this.view.flxGradient.top = "0dp";
    this.view.flxTransactionTabs.isVisible = true;
    this.view.flxSegment.top = "125dp";
    if (kony.os.deviceInfo().name === "iPhone") {
      this.view.flxHeader.isVisible = false;
      this.view.flxMainContainer.top = "0dp";
    } else {
      this.view.flxHeader.isVisible = true;
      this.view.flxMainContainer.top = "56dp";
    }
  },

  cancelSearch: function() {
    //this.view.flxOuterGradientBlueKA.isVisible = true;
    // this.view.flxMainContainer.skin = "slFbox";
    this.view.flxHeaderSearchbox.height = "0dp";
    //this.view.flxHeaderSearchbox.isVisible = false;
    this.view.flxSearch.isVisible = true;
    this.view.flxSearch.height = "55dp";
    this.view.flxGradient.isVisible = true;
    this.view.flxSearch.top = 0 + "dp";
    this.view.flxGradient.top = "0dp";
    this.view.flxTransactionTabs.isVisible = true;
    this.view.flxSegment.top = (this.filterViewRendered) ? "155dp" : "125dp";
    if (kony.os.deviceInfo().name === "iPhone") {
      this.view.flxHeader.isVisible = false;
      this.view.flxMainContainer.top = "0dp";
    } else {
      this.view.flxHeader.isVisible = true;
      this.view.flxMainContainer.top = "56dp";
    }
    if (this.segmentData.length > 0) {
      this.view.segTransactions.setData(this.segmentData);
      this.view.segTransactions.isVisible = true;
      this.view.flxNoTransactions.isVisible = false;
    } else {
      this.view.segTransactions.isVisible = false;
      this.view.flxNoTransactions.isVisible = true;
      this.view.flxNoTransactions.top = (this.filterViewRendered) ? "155dp" : "125dp";
      // this.view.flxHeaderNT.isVisible = false;
    }
    //this.view.flxOuterGradientBlueKA.isVisible = true;
  },
  showPopupSuccess: function() {
    var scopeObj = this;
    this.timerCounter = parseInt(this.timerCounter) + 1;
    var timerId = "timerPopupSuccess" + this.timerCounter;
    this.view.flxPopup.skin = "sknFlx43ce6e";
    this.view.customPopup.imgPopup.src = "confirmation.png";
    this.view.customPopup.lblPopup.text = this.popupMsg;
    this.view.flxPopup.setVisibility(true);
    kony.timer.schedule(timerId, function() {
      scopeObj.view.flxPopup.setVisibility(false);
    }, 3, false);
  },
  showSuccessPopup: function(refID, type) {
    // TO DO i18n's
    var msg;
    if (type === "delete") {
      msg = "Transaction was cancelled successfully with reference ID : " + (refID.transactionId || refID.referenceId);
    } else {
      if (refID.referenceId) msg = "Transfer completed successfully. Transaction ID: " + refID.referenceId;
      else msg = "Transaction was edited successfully with reference ID : " + refID.transactionId;
    }
    applicationManager.getDataProcessorUtility().showToastMessageSuccess(this, msg);
  },
  showErrorPopup: function(err) {
    applicationManager.getDataProcessorUtility().showToastMessageError(this, JSON.stringify(err));
  },
  tbxSearchOnTextChange: function() {
    var searchtext = this.view.customSearchbox.tbxSearch.text.toLowerCase();
    if (searchtext) {
      var data = this.segmentData;
      this.view.segTransactions.isVisible = true;
      this.view.flxNoTransactions.isVisible = false;
      this.view.segTransactions.removeAll();
      var searchobj = applicationManager.getDataProcessorUtility().multipleCommonSegmentSearch(["description", "scheduledDate","unformattedAmount","title"], searchtext, data);
      if (searchobj.length > 0) {
        this.view.segTransactions.setData(searchobj);
      } else {
        this.view.segTransactions.isVisible = false;
        this.view.flxNoTransactions.isVisible = true;
      }
    } else {
      if (this.segmentData.length > 0) {
        this.view.segTransactions.setData(this.segmentData);
        this.view.segTransactions.isVisible = true;
        this.view.flxNoTransactions.isVisible = false;
      } else {
        this.view.segTransactions.isVisible = false;
        this.view.flxNoTransactions.isVisible = true;
        // this.view.flxHeaderNT.isVisible = false;
      }
    }
  },
  checkForPopup: function () {
    var navMan = applicationManager.getNavigationManager();
    var transactions = navMan.getCustomInfo("frmEuropeTransferActivities");
    if (transactions) {
      if (transactions.res !== undefined && transactions.res !== null) {
        if (transactions.type === "error") this.showErrorPopup(transactions.res);
        else this.showSuccessPopup(transactions.res, transactions.typeOfTransaction);
      }
      transactions.res = null;
    }
  },
  
  postShow: function() {
    var transferPresentationController = applicationManager.getModulesPresentationController("TransferModule");
    if (transferPresentationController.filterTypeSelected === "TRANSFER") {
      this.renderViewWithFilter();
      this.view.lblFilter.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.TransferEurope.Filter") + ": " + applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.TransferEurope.Showing") + " " + "Transfers";
    }
    else if (transferPresentationController.filterTypeSelected === "PAYMENT") {
      this.renderViewWithFilter();
      this.view.lblFilter.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.TransferEurope.Filter") + ": " + applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.TransferEurope.Showing") + " " + "Payments";
    }
    else if (transferPresentationController.filterTypeSelected === "BOTH") {
      this.renderViewWithoutFilter();
      this.view.lblFilter.text = "";
    }
  },
  
  renderViewWithFilter: function() {
    this.view.lblFilter.isVisible = true;
    this.view.flxSegment.top = "155dp";
    this.view.flxNoTransactions.top = "155dp";
    this.filterViewRendered = true;
  },
  
  renderViewWithoutFilter: function() {
    this.view.lblFilter.isVisible = false;
    this.view.flxSegment.top = "125dp";
    this.view.flxNoTransactions.top = "125dp";
    this.filterViewRendered = false;
  }
  
});