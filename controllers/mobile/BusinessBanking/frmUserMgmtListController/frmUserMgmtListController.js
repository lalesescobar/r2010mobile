define(['CommonUtilities'], function (CommonUtilities) { 

  /*This function is first called when a form is ready to use */

  return{
    navManager:"",
    timerCounter:0,
    onNavigate:function(){
      try{
        var navManager = applicationManager.getNavigationManager();
        this.navManager = navManager;
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        kony.print("onNavigate inside");

      }catch(er){
        //alert(er);
        kony.print("onNavigate"+er);
      }
    },
    /*This function is called during the app lifecycle*/
    formPreShow : function(){
      try{
        this.view.tbxSearch.setFocus(false);
        this.view.tbxSearch.text = "";
        this.view.flxSearch.isVisible = true;
        this.view.flxPopup.isVisible = false;
        this.view.flxNoTransactions.setVisibility(false);
        this.view.segUserManagement.setVisibility(true);
        if(applicationManager.getPresentationFormUtility().getDeviceName() !== "iPhone"){
          this.view.flxHeader.isVisible = true;
          this.view.flxFooter.isVisible = false;
        }
        else{
          this.view.flxHeader.isVisible = false;
          this.view.flxFooter.isVisible = true;
          var MenuHandler = applicationManager.getMenuHandler();
          MenuHandler.setUpHamburgerForForm(this, "Manage User");
          var navManager = applicationManager.getNavigationManager();
          var currentForm = navManager.getCurrentForm();
          applicationManager.getPresentationFormUtility().logFormName(currentForm);
        }
        var searchText=this.navManager.getCustomInfo("frmUserMgmntSearch");
        if (searchText !== ""&& searchText !== null && searchText !== undefined) {
          this.view.tbxSearch.setFocus(true);
        }
        else
        {
          this.view.tbxSearch.setFocus(false);
        }

        this.configureSearch(searchText);
        this.setUserManagementSegment();
        this.view.onDeviceBack=this.flxBackOnClick;
        this.view.customHeader.flxBack.onClick=this.flxBackOnClick;
        this.view.segUserManagement.onRowClick = this.userManagementSegmentOnClick;

      }catch(er){
        //alert(er);
      }
    },
    /*This function is called during the app lifecycle*/
    formPostShow : function(){
      try{
      }catch(er){
      }
    },
    /*This function is used to perform onRowfClcik events of user list segment*/
    userManagementSegmentOnClick:function(){
      try{
        kony.print("try of userSegmentClick");
        var selectedItems = this.view.segUserManagement.selectedRowItems[0];
        var navManager = applicationManager.getNavigationManager();
        navManager.setCustomInfo("frmUserMgmntSearch","");
        navManager.setCustomInfo("frmUserMgmtListdetails","");
        navManager.setCustomInfo("Usermanagement",selectedItems);
        navManager.navigateTo("UserDetails");
      }catch(er){
        kony.print("catch of userSegmentClick"+er);
      }
    },

    /*This function is used to populate  user list on segment*/

    setUserManagementSegment:function()
    {
      var navManager = applicationManager.getNavigationManager();
      var userList = navManager.getCustomInfo("frmUserMgmtListdetails");
      var userData = [],rowObj,lastloginDet;
      var userValues = userList.userExists;
      var isIphone = applicationManager.getPresentationFormUtility().getDeviceName() === "iPhone";
      if(userList.userExists !== false)
      {
        for (i = 0; i < userValues.length; i++) {
          var lastlogin = "";
          if (userValues[i].Lastlogintime !== undefined) {
            if(isIphone && userValues[i].Lastlogintime.includes(" ") )
            {
              userValues[i].Lastlogintime = userValues[i].Lastlogintime.replace(" ", "T") + "Z";
              lastlogin = CommonUtilities.getFrontendDateString(userValues[i].Lastlogintime,"mm/dd/yyyy");         
              lastloginDet = CommonUtilities.getFrontendDateString(userValues[i].Lastlogintime, "mm/dd/yyyy") + " " + userValues[i].Lastlogintime.split("T")[1] ;

            }
            else
            {
              lastlogin = CommonUtilities.getFrontendDateString(userValues[i].Lastlogintime,"mm/dd/yyyy");         
              lastloginDet = CommonUtilities.getFrontendDateString(userValues[i].Lastlogintime, "mm/dd/yyyy") + " " + userValues[i].Lastlogintime.split(" ")[1] ;

            }
          } else {
            lastlogin = "N/A";
            lastloginDet ="N/A";
          }
          var status = userValues[i].status.toLowerCase();
          status = status.substring(0,1).toUpperCase() + status.substring(1);
          rowObj = {
            "lblUserName": {
              "text": "" + userValues[i].FirstName + " " + userValues[i].LastName
            },
            "lblTimeStamp": {"text":""+lastlogin}, 
            "lblUserLevel": {"text": "" + (userValues[i].role_name)},
            "lblUserStatus": { "text": "" +status},
            "UserName": userValues[i].UserName,
            "Lastlogin":lastloginDet,
            "DateOfBirth":CommonUtilities.getFrontendDateString(userValues[i].DateOfBirth,"mm/dd/yyyy"),
            "Status":kony.sdk.isNullOrUndefined(userValues[i].status)?"-": userValues[i].status,
            "Email":kony.sdk.isNullOrUndefined(userValues[i].Email)?"-": userValues[i].Email,
            "FirstName":kony.sdk.isNullOrUndefined(userValues[i].FirstName)?"-": userValues[i].FirstName,
            "LastName":kony.sdk.isNullOrUndefined(userValues[i].LastName)?"-": userValues[i].LastName,
            "DrivingLicenseNumber":kony.sdk.isNullOrUndefined(userValues[i].DrivingLicenseNumber)?"-": userValues[i].DrivingLicenseNumber,
            "Ssn":kony.sdk.isNullOrUndefined(userValues[i].Ssn)?"-": userValues[i].Ssn,
            "Phone":kony.sdk.isNullOrUndefined(userValues[i].Phone)?"-": userValues[i].Phone,
            "Rolename":kony.sdk.isNullOrUndefined(userValues[i].role_name)?"-": userValues[i].role_name,
          };
          if(userValues[i].UserName === applicationManager.getUserPreferencesManager().getCurrentUserName()){
            rowObj.lblUserName.text += " " + kony.i18n.getLocalizedString("kony.mb.userMgmt.loggedInUserTitle");
          }
          userData.push(rowObj);
        }
        this.view.flxuserList.setVisibility(true);
        this.view.segUserManagement.setData(userData);
        this.view.flxNoTransactions.setVisibility(false);
      }
      else
      {
        this.view.flxuserList.setVisibility(false);
        this.view.segUserManagement.setVisibility(false);
        this.view.flxNoTransactions.setVisibility(true);
      }

    },
    /*This function is used to search users 
    *@param {String} obj  -searchtext
    */
    tbxSearchOnTextDone: function(obj) {
      var userManagementModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BusinessBanking");
      userManagementModule.presentationController.navigatetoallusers(obj);
    },

    /**
     * Method to configure search logic  for the list of users
     * @param {String} searchText - search String
     */  
    configureSearch: function(searchText) {
      this.view.tbxSearch.text = searchText || "";
      var scope=this;
      this.view.tbxSearch.onTextChange=function()
      {
        if(scope.view.tbxSearch.text.length===0)
        {
          scope.tbxSearchOnTextDone({
            searchString: scope.view.tbxSearch.text.trim()
          }); 
        }
      };
      this.view.tbxSearch.onDone = function() {
        scope.tbxSearchOnTextDone({
          searchString: scope.view.tbxSearch.text.trim()
        });
      };

      this.view.imgSearchIcon.onTouchEnd = function() {
        scope.tbxSearchOnTextDone({
          searchString: scope.view.tbxSearch.text.trim()
        });
      };
    },

    flxBackOnClick: function() {
      var navMan = applicationManager.getNavigationManager();
      navMan.setCustomInfo("frmUserMgmntSearch","");
      navMan.setCustomInfo("frmUserMgmtListdetails","");
      var accountMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
      accountMod.presentationController.showDashboard();

    },
    fetchErrorBack:function(response)
    {
      try {   
        if(!kony.sdk.isNullOrUndefined(response)){
          var scopeObj=this;
          var errorResponse = "";
          if(!kony.sdk.isNullOrUndefined(response.errorMessage)){
            errorResponse = response.errorMessage;
          }
          else{
            errorResponse = kony.i18n.getLocalizedString("kony.mb.servicesErrormsg");
          }
          this.view.customPopup.lblPopup.text = errorResponse;    
          if(!kony.sdk.isNullOrUndefined(this.timerCounter)){
            this.timerCounter = parseInt(this.timerCounter)+1;
          }
          else{
            this.timerCounter = 1;
          }
          var timerId="timerPopupError"+this.timerCounter;
          this.view.flxPopup.skin = "sknFlxf54b5e";
          this.view.customPopup.imgPopup.src = "errormessage.png";   
          this.view.flxPopup.setVisibility(true);
          kony.timer.schedule(timerId, function() {
            scopeObj.view.flxPopup.setVisibility(false);
          }, 1.5, false);            
        }
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }catch(error){
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }  }    

  };
});
