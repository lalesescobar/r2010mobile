define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Button_e6793ad26a2547b28c7522367a8035e2: function AS_Button_e6793ad26a2547b28c7522367a8035e2(eventobject) {
        var self = this;
        return self.gotoDownload.call(this);
    },
    AS_Form_g8aecb3b12f147a48c6d32c2adf6dd14: function AS_Form_g8aecb3b12f147a48c6d32c2adf6dd14(eventobject) {
        var self = this;
        this.init();
    },
    AS_Form_h7b35f64326141c1aa767618f4db23d4: function AS_Form_h7b35f64326141c1aa767618f4db23d4(eventobject) {
        var self = this;
        this.preshow();
    },
    AS_Segment_h22beba7db694c8db1517644f76825b4: function AS_Segment_h22beba7db694c8db1517644f76825b4(eventobject) {
        var self = this;
        this.onScrollEndTransactions();
    }
});