define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_fdfee87ed0b54437ac8680292c30cc37: function AS_BarButtonItem_fdfee87ed0b54437ac8680292c30cc37(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    /** init defined for frmMaturityInstructions **/
    AS_Form_i02f0673192e4468950657955c84c0a4: function AS_Form_i02f0673192e4468950657955c84c0a4(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmMaturityInstructions **/
    AS_Form_hb557ff8e1594f33a64c9f9fba262053: function AS_Form_hb557ff8e1594f33a64c9f9fba262053(eventobject) {
        var self = this;
        this.preShow();
    }
});