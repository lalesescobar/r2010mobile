define(["CommonUtilities"], function (CommonUtilities) {
  return {
    init: function () {
      var navManager = applicationManager.getNavigationManager();
      var currentForm = navManager.getCurrentForm();
      applicationManager.getPresentationFormUtility().initCommonActions(this, "NO", currentForm);
    },
    preShow: function () {
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      if (applicationManager.getPresentationFormUtility().getDeviceName() === "iPhone") {
        this.view.flxHeader.setVisibility(false);
      }
      var SavingsPotMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SavingsPotModule");
      SavingsPotMod.presentationController.setInitiateTransfer("");
      this.view.lblBalanceValue1.text = "$0.00";
      var accountName = SavingsPotMod.presentationController.getMaskedAccountName();
      this.view.lblAvailableBalance.text = accountName;
      this.view.lblAvailableBalance1.text = accountName;
      this.view.btnCreateSavingsPot.onClick = this.continueOnClick;
      this.view.btnCreateSavingsPot1.onClick = this.continueOnClick;
      this.view.customHeader.flxBack.onClick = this.onBack;
      this.view.segMyGoals.onRowClick = this.viewGoalDetails;
      this.view.segMyBudgets.onRowClick = this.viewBudgetDetails;
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      var navManager = applicationManager.getNavigationManager();
      var accountsDetails = navManager.getCustomInfo("frmMySavingsPot");
      this.setDataToForm(accountsDetails);
    },
    setDataToForm: function (potDetails) {
      scope = this;
      var formatUtil = applicationManager.getFormatUtilManager();
      var totalGoalsPots = [];
      var totalBudgetPots = [];
      var totalBalGoals = 0.00;
      var totalBalBudget = 0.00;
      var potcolor = "lbldotgreen";
      for (var TitleNo in potDetails) {
        var data = potDetails[TitleNo];
        var bal = formatUtil.formatAmountandAppendCurrencySymbol(data.availableBalance, data.currency);
        var percentage = (data.availableBalance * 100) / data.targetAmount;
        if (percentage > 100) {
          percentage = 100;
        }
        else {
          percentage = parseInt(percentage);
        }
        if (data.potCurrentStatus === "Not On Track") {
          potcolor = "lbldotred";
        }
        else {
          if (!data.potCurrentStatus) {
            data.potCurrentStatus = "On Track"
          }
          potcolor = "lbldotgreen";
        }
        var targetamt = formatUtil.formatAmountandAppendCurrencySymbol(data.targetAmount, data.currency);
        var storeData;
        if (data.potType == "Budget" && (data.availableBalance == "0" || data.availableBalance == "0.00" || data.availableBalance == "" || data.availableBalance == undefined)) {
          storeData = {
            potName: CommonUtilities.truncateStringWithGivenLength(data.potName, 20),
            potId: data.savingsPotId,
            targetAmount: targetamt,
            lblPend: "Yet to Fund",
            minper: "",
            maxper: "",
            lblGoalDurationLeft:"",
            availableBalance:"",
            lblGoalDurationLeft: "",
            flxProgressBar: {
              top: "810Dp"
            },
            isRowClicked: false,
            lblTransfer: "Initiate Transfer",
            flxClick: {
              "onClick": function (event, context) {
                scope.onSegmentRowClick(event, context);
              }.bind(this)
            },
            imgWarnImg: {
              src: "alert.png"
            }
          };
        }
        else {
          var rightDim = "110dp";
          if(data.potCurrentStatus === "Not On Track"){
            rightDim = "120dp";
          }
          else if(data.potCurrentStatus === "On Track"){
            rightDim= "95dp";
          }
          storeData = {
            potName: CommonUtilities.truncateStringWithGivenLength(data.potName, 20),
            availableBalance: bal,
            targetAmount: targetamt,
            flxSpent: {
              "width": percentage + "%"
            },
            minper: "0%",
            maxper: "100%",
            lblPend:"",
            monthLeft: data.monthsLeftForCompletion,
            status: data.potCurrentStatus,
            potId: data.savingsPotId,
            percentComp: percentage + "%",
            statuscolor: {
              skin: potcolor,
              right: rightDim
            },
            lblGoalDurationLeft: "Current Balance",
            flxProgressBar: {
              top: "81Dp"
            },
            imgWarnImg: {
              src: ""
            },
            lblTransfer: "",
            flxClick: {
              "onClick": function () {

              }.bind(this)
            },
          };
        }
        if (data.potType == "Goal") {
          totalGoalsPots.push(storeData);
          totalBalGoals = (parseFloat(totalBalGoals) + parseFloat(data.availableBalance)).toFixed(2);
        }
        else if (data.potType == "Budget") {
          totalBudgetPots.push(storeData);
          totalBalBudget = (parseFloat(totalBalBudget) + parseFloat(data.availableBalance)).toFixed(2);
        }
      }
      if (totalGoalsPots.length === 0 && totalBudgetPots.length === 0) {
        this.view.flxMainContainer.setVisibility(false);
        this.view.flxNoTransactions.setVisibility(true);
      } else {
        this.view.flxMainContainer.setVisibility(true);
        this.view.flxNoTransactions.setVisibility(false);
        if (totalGoalsPots.length === 0) {
          this.view.segMyGoals.setVisibility(false);
          this.view.flxNoGoals.setVisibility(true);
        } else {
          this.view.segMyGoals.setVisibility(true);
          this.view.flxNoGoals.setVisibility(false);
          this.view.segMyGoals.widgetDataMap = {
            lblMinGoalAmount: "availableBalance",
            lblMaxGoalAmount: "targetAmount",
            lblGoalCompleteStatusPercentage: "percentComp",
            lblGoalName: "potName",
            lblGoalDurationLeft: "monthLeft",
            lblGoalStatus: "status",
            flxSpent: "flxSpent",
            lblPotId: "potId",
            lblCur: "statuscolor"
          };
          this.view.segMyGoals.setData(totalGoalsPots);
        }
        if (totalBudgetPots.length === 0) {
          this.view.segMyBudgets.setVisibility(false);
          this.view.flxNoBudgets.setVisibility(true);
        } else {
          this.view.segMyBudgets.setVisibility(true);
          this.view.flxNoBudgets.setVisibility(false);
          this.view.segMyBudgets.widgetDataMap = {
            lblMinBudgetVal: "minper",
            lblMaxBudgetVal: "maxper",
            lblBudgetAmount: "targetAmount",
            lblGoalCompleteStatus: "availableBalance",
            lblBudgetName: "potName",
            flxSpent: "flxSpent",
            lblWarnImg: "lblPend",
            lblGoalDurationLeft: "lblGoalDurationLeft",
            flxProgressBar: "flxProgressBar",
            lblTransfer: "lblTransfer",
            imgWarnImg: "imgWarnImg",
            lblPotId: "potId",
            flxClick: "flxClick"
          };
          this.view.segMyBudgets.setData(totalBudgetPots);
        }
      }

      this.view.lblMyGoals.text = "My Goals (" + totalGoalsPots.length + ")";
      this.view.lblMyBudgets.text = "My Budgets (" + totalBudgetPots.length + ")";
      this.view.lblAvailableBalanceValue.text = formatUtil.formatAmountandAppendCurrencySymbol(totalBalGoals, potDetails[0].currency);
      this.view.lblBudgetAvailBalValue.text = formatUtil.formatAmountandAppendCurrencySymbol(totalBalBudget, potDetails[0].currency);
      var temp = parseFloat(totalBalGoals) + parseFloat(totalBalBudget);
      this.view.lblBalanceValue.text = formatUtil.formatAmountandAppendCurrencySymbol(temp, potDetails[0].currency);
      applicationManager.getPresentationUtility().dismissLoadingScreen();

    },
    continueOnClick: function () {
      var navManager = applicationManager.getNavigationManager();
      navManager.navigateTo("frmSavingsType");
    },
    viewGoalDetails: function () {
      var selectedObj = this.view.segMyGoals.selectedRowItems[0];
      var selectedPot = selectedObj.potId;
      var selectedPotDetails;
      var finalFreqDay;
      var navManager = applicationManager.getNavigationManager();
      var potDetails = navManager.getCustomInfo("frmMySavingsPot");
      for (var TitleNo in potDetails) {
        var data = potDetails[TitleNo];
        if (data.savingsPotId === selectedPot) {
          selectedPotDetails = data;
        }
      }
      if (!selectedPotDetails.frequencyDay) {
        if (selectedPotDetails.startDate) {
          var formatUtil = applicationManager.getFormatUtilManager();
          var endDateobj = formatUtil.getDateObjectfromString(selectedPotDetails.startDate, "MM/DD/YYYY");
          var endDate = formatUtil.getFormatedDateString(endDateobj, formatUtil.getApplicationDateFormat());
          var date = endDate;
          date = date.split("/");
          var d = new Date(date[2], date[0] - 1, date[1]);
          var weekday = new Array(7);
          weekday[0] = "Sunday";
          weekday[1] = "Monday";
          weekday[2] = "Tuesday";
          weekday[3] = "Wednesday";
          weekday[4] = "Thursday";
          weekday[5] = "Friday";
          weekday[6] = "Saturday";
          var selectedDay = weekday[d.getDay()];
          if (selectedPotDetails.frequency == "Biweekly") {
            var temp = selectedDay;
            finalFreqDay = "Every 2 weeks on " + temp.substring(0, 3);
          }
          else {
            if (date[1].length == 1) {
              finalFreqDay = "0" + date[1] + "th of every month";
            }
            else {
              finalFreqDay = date[1] + "th of every month";
            }
          }
          selectedPotDetails.frequencyDay = finalFreqDay;
        }
      }
      navManager.setCustomInfo("frmSavingsGoalViewDetails", selectedPotDetails);
      var SavingsPotMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SavingsPotModule");
      SavingsPotMod.presentationController.setEditObj(selectedPotDetails);
      navManager.navigateTo("frmSavingsGoalViewDetails");
    },
    onSegmentRowClick: function (event, context) {
      var rowindex = context.rowIndex;
      this.view.segMyBudgets.selectedRow = rowindex;
      var selectedObj = this.view.segMyBudgets.selectedRowItems[0];
      var selectedPot = selectedObj.potId;
      var selectedPotDetails;
      var navManager = applicationManager.getNavigationManager();
      var potDetails = navManager.getCustomInfo("frmMySavingsPot");
      for (var TitleNo in potDetails) {
        var data = potDetails[TitleNo];
        if (data.savingsPotId === selectedPot) {
          selectedPotDetails = data;
        }
      }
      navManager.setCustomInfo("frmBudgetPotDetails", selectedPotDetails);
      var SavingsPotMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SavingsPotModule");
      SavingsPotMod.presentationController.setInitiateTransfer("YES");
      navManager.navigateTo("frmBudgetInitiateTransferDetails");
    },
    viewBudgetDetails: function () {
      var selectedObj = this.view.segMyBudgets.selectedRowItems[0];
      var selectedPot = selectedObj.potId;
      var selectedPotDetails;
      var navManager = applicationManager.getNavigationManager();
      var potDetails = navManager.getCustomInfo("frmMySavingsPot");
      for (var TitleNo in potDetails) {
        var data = potDetails[TitleNo];
        if (data.savingsPotId === selectedPot) {
          selectedPotDetails = data;
        }
      }
      navManager.setCustomInfo("frmBudgetPotDetails", selectedPotDetails);
      var SavingsPotMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SavingsPotModule");
      SavingsPotMod.presentationController.setEditObj(selectedPotDetails);
      navManager.navigateTo("frmBudgetPotDetails");
    },
    onBack: function () {
      var navigationMan = applicationManager.getNavigationManager();
      navigationMan.goBack();
    }
  };
});