describe("Transfers", function() {
	beforeEach(function() {
	
	  var flgDashboard = kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],10000);
	  kony.print("Dashboard : "+flgDashboard);
	  if(flgDashboard){
	
	    // Do Nothing
	
	  }else{
	    var currentwidget;
	
	    try{
	      kony.automation.playback.wait(8000);
	      currentwidget = kony.automation.widget.getWidgetProperty(["frmLogin","login","btnLogIn"], "text");
	      kony.print("The current Form Name ::"+currentwidget);
	    }catch(err){
	      kony.print("Error::"+err.message);
	    }
	    
	    try{
	      kony.automation.playback.wait(5000);
	        var currentwidget1 = kony.automation.widget.getWidgetProperty(["frmLogout","btnLogIn"], "text");
	        kony.print("The current Form Name ::"+currentwidget1);
	        if(currentwidget1 === "Sign In"){
	
	          kony.automation.button.click(["frmLogout","btnLogIn"]);
	          kony.automation.playback.waitFor(["frmLogin","tbxUsername"],10000);
	          currentwidget=currentwidget1;
	        }     
	
	      }catch(err1){
	        kony.print("Error::"+err1.message);
	      }
	
	    if(currentwidget === "Sign In"){
	
	      kony.automation.playback.waitFor(["frmLogin","login","tbxUsername"],10000);
	      kony.automation.textbox.enterText(["frmLogin","login","tbxUsername"],"dbpolbuser");
	      kony.automation.textbox.enterText(["frmLogin","login","tbxPassword"],"Kony@1234");
	      kony.automation.button.click(["frmLogin","login","btnLogIn"]);
	
	      // Verifying Terms and conditions screen
	
	      var frmTnC = kony.automation.playback.waitFor(["frmTermsAndCondition","flxCheckBox"],10000);
	      if(frmTnC){
	        kony.automation.flexcontainer.click(["frmTermsAndCondition","flxCheckBox"]);
	        kony.automation.playback.waitFor(["frmTermsAndCondition","btnContinue"]);
	        kony.automation.button.click(["frmTermsAndCondition","btnContinue"]);
	      }
	      kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],5000);
	    }
	
	  }
	
	},90000);
	
	function SearchInFromAndToScreen(valType) {
	
	  try{
	
	
	    var currentwidget1 = kony.automation.widget.getWidgetProperty(["frmLogout","btnLogIn"], "text");
	    kony.print("The current Form Name ::"+currentwidget1);
	    if(currentwidget1 === "Sign In"){
	
	      kony.automation.button.click(["frmLogout","btnLogIn"]);
	      kony.automation.playback.waitFor(["frmLogin","login","tbxUsername"],10000);
	      kony.automation.textbox.enterText(["frmLogin","login","tbxUsername"],"dbpolbuser");
	      kony.automation.textbox.enterText(["frmLogin","login","tbxPassword"],"Kony@1234");
	      kony.automation.button.click(["frmLogin","login","btnLogIn"]);
	    }    
	
	  }catch(err){
	    kony.print("Unable to find widget");
	  }
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	  kony.automation.playback.wait(2000);
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"],15000);
	  kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0,1]"]);
	  kony.automation.playback.waitFor(["frmMMTransferFromAccount","tbxSearch"],15000);
	  kony.automation.widget.touch(["frmMMTransferFromAccount","tbxSearch"], [100,13],null,[100,56]);
	  kony.automation.textbox.enterText(["frmMMTransferFromAccount","customSearchbox","tbxSearch"],"check");
	  kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	  // :User Injected Code Snippet [//Expect Checking Account - [2 lines]]
	  //var checkingAcc = ;
	  expect(kony.automation.widget.getWidgetProperty(["frmMMTransferFromAccount","segTransactions[0,0]]","flxAccountsNoImageTransfers","flxMain","flxAccountName","lblAccountName"],"text")).toContain("Checki");
	  // :End User Injected Code Snippet {391e3878-1a4a-6cd5-312f-c42e3dc249f3}
	  kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	  kony.automation.segmentedui.click(["frmMMTransferFromAccount","segTransactions[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMTransferToAccount","tbxSearch"]);
	  kony.automation.widget.touch(["frmMMTransferToAccount","tbxSearch"], [72,22],null,[72,65]);
	  if(valType==="normal"){
	    kony.automation.textbox.enterText(["frmMMTransferToAccount","customSearchbox","tbxSearch"],"credit");
	  }else{
	    kony.automation.textbox.enterText(["frmMMTransferToAccount","customSearchbox","tbxSearch"],"P2P");
	  }  
	  kony.automation.playback.wait(1000);
	  kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],15000);
	  var creditCard = kony.automation.widget.getWidgetProperty(["frmMMTransferToAccount","segTransactions[0,0]]","flxAccountsNoImageTransfers","flxMain","flxAccountName","lblAccountName"],"text");
	  if(valType==="normal"){
	    expect(creditCard).toContain("Credit");
	  }else{
	    expect(creditCard).toContain("P2P");
	  }
	
	
	  kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],10000);
	  kony.automation.segmentedui.click(["frmMMTransferToAccount","segTransactions[0,0]"]);
	}
	
	
	function EnterAmount() {
	  kony.automation.playback.waitFor(["frmMMTransferAmount","keypad","btnThree"],10000);
	  kony.automation.button.click(["frmMMTransferAmount","keypad","btnThree"]);
	  kony.automation.button.click(["frmMMTransferAmount","keypad","btnZero"]);
	  kony.automation.button.click(["frmMMTransferAmount","keypad","btnZero"]);
	  kony.automation.button.click(["frmMMTransferAmount","btnContinue"]);
	  kony.automation.playback.waitFor(["frmMMReview","btnTransfer"],10000);
	}
	
	
	function SelectFrequencyOnceAndTransfer() {
	
	  kony.automation.playback.waitFor(["frmMMReview","segDetails"],10000);
	  kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	  kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMStartDate","btnContinue"],15000);
	  kony.automation.button.click(["frmMMStartDate","btnContinue"]);
	  kony.automation.playback.waitFor(["frmMMReview","btnTransfer"],15000);
	  kony.automation.button.click(["frmMMReview","btnTransfer"]);
	  expect(kony.automation.playback.waitFor(["frmMMConfirmation","lblSuccessMessage"],15000)).toEqual(true);
	  kony.automation.button.click(["frmMMConfirmation","btnDashboard"]);
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	}
	
	
	function TransferScheduleOnce() {
	  // :User Injected Code Snippet [//Prerequisite Verify_Search_In_FromAndToScreen - []]
	
	  // :End User Injected Code Snippet {0d4c824b-a9ad-8c0e-5c44-a3292aac96a1}
	  kony.automation.playback.waitFor(["frmMMTransferAmount","keypad","btnThree"],10000);
	  kony.automation.button.click(["frmMMTransferAmount","keypad","btnThree"]);
	  kony.automation.button.click(["frmMMTransferAmount","keypad","btnZero"]);
	  kony.automation.button.click(["frmMMTransferAmount","keypad","btnZero"]);
	  kony.automation.button.click(["frmMMTransferAmount","btnContinue"]);
	  kony.automation.playback.waitFor(["frmMMReview","segDetails"],15000);
	  kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	  kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMStartDate","customCalendar","flxNextMonth"],15000);
	  kony.automation.flexcontainer.click(["frmMMStartDate","customCalendar","flxNextMonth"]);
	  // :User Injected Code Snippet [//Select a date - [3 lines]]
	  kony.automation.playback.waitFor(["frmMMStartDate","customCalendar","flxNextMonth"],15000);
	  kony.automation.widget.touch(["frmMMStartDate","customCalendar","flxNextMonth"], [178,125],null,[178,125]);
	  kony.automation.widget.touch(["frmMMStartDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
	  // :End User Injected Code Snippet {414f83d0-e0a2-735c-c437-a564878872a6}
	  kony.automation.playback.waitFor(["frmMMStartDate","btnContinue"],15000);
	  kony.automation.button.click(["frmMMStartDate","btnContinue"]);
	  kony.automation.playback.waitFor(["frmMMReview","btnTransfer"]);
	  kony.automation.button.click(["frmMMReview","btnTransfer"]);
	  expect(kony.automation.playback.waitFor(["frmMMConfirmation","btnNewTransfer"],15000)).toContain(true);
	  kony.automation.button.click(["frmMMConfirmation","btnDashboard"]);
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	}
	
	
	function SelectFrequencyDaily() {
	  kony.automation.playback.waitFor(["frmMMReview","segDetails"],15000);
	  kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	  kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,1]"]);
	}
	
	
	function SelectNumberOfTransfersAndThenConfirmTransfer(valTimePeriod,valueType) {
	  kony.automation.playback.waitFor(["frmMMDuration","segOptions"],10000);
	  kony.automation.segmentedui.click(["frmMMDuration","segOptions[0,1]"]);
	  kony.automation.playback.waitFor(["frmMMStartDate","customCalendar","flxNextMonth"],15000);
	  kony.automation.flexcontainer.click(["frmMMStartDate","customCalendar","flxNextMonth"]);
	  // :User Injected Code Snippet [//Select a Start Date - [1 lines]]
	  kony.automation.widget.touch(["frmMMStartDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
	  // :End User Injected Code Snippet {83f58aab-137d-4051-a051-d2801329c71d}
	  kony.automation.playback.waitFor(["frmMMNumberOfTransfers","keypad","btnFive"],15000);
	  kony.automation.button.click(["frmMMNumberOfTransfers","keypad","btnFive"]);
	  kony.automation.playback.waitFor(["frmMMNumberOfTransfers","btnContinue"],15000);
	  kony.automation.button.click(["frmMMNumberOfTransfers","btnContinue"]);
	  kony.automation.playback.waitFor(["frmMMReview","btnTransfer"],15000);
	  kony.automation.button.click(["frmMMReview","btnTransfer"]);
	  expect(kony.automation.playback.waitFor(["frmMMConfirmation","btnNewTransfer"],15000)).toContain(true);
	  kony.automation.playback.waitFor(["frmMMConfirmation","btnDashboard"],10000);
	  kony.automation.button.click(["frmMMConfirmation","btnDashboard"]);
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	}
	
	
	function ScheduledTransferDailyDateRange() {
	  kony.automation.playback.waitFor(["frmMMTransferAmount","keypad","btnThree"],10000);
	  kony.automation.button.click(["frmMMTransferAmount","keypad","btnThree"]);
	  kony.automation.button.click(["frmMMTransferAmount","keypad","btnZero"]);
	  kony.automation.button.click(["frmMMTransferAmount","keypad","btnZero"]);
	  kony.automation.playback.waitFor(["frmMMTransferAmount","btnContinue"],15000);
	  kony.automation.button.click(["frmMMTransferAmount","btnContinue"]);
	  kony.automation.playback.waitFor(["frmMMReview","segDetails"],15000);
	  kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	  kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,1]"]);
	  kony.automation.playback.waitFor(["frmMMDuration","segOptions"],15000);
	  kony.automation.segmentedui.click(["frmMMDuration","segOptions[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMStartDate","customCalendar","flxNextMonth"],15000);
	  kony.automation.flexcontainer.click(["frmMMStartDate","customCalendar","flxNextMonth"]);
	  // :User Injected Code Snippet [//Select "Start" date - [1 lines]]
	  kony.automation.widget.touch(["frmMMStartDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
	  // :End User Injected Code Snippet {1b928f08-dcde-b4a4-8a34-66c9403263c8}
	  kony.automation.playback.waitFor(["frmMMEndDate","customCalendar","flxNextMonth"],15000);
	  kony.automation.flexcontainer.click(["frmMMEndDate","customCalendar","flxNextMonth"]);
	  kony.automation.playback.waitFor(["frmMMEndDate","customCalendar","flxNextMonth"],15000);
	  kony.automation.flexcontainer.click(["frmMMEndDate","customCalendar","flxNextMonth"]);
	  // :User Injected Code Snippet [//select "End" date - [3 lines]]
	  // kony.automation.playback.waitFor(["frmMMEndDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"]);
	
	  kony.automation.widget.touch(["frmMMEndDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
	  // :End User Injected Code Snippet {52f4e47a-c3c2-0551-1005-1c35e4be2ef7}
	  kony.automation.playback.waitFor(["frmMMEndDate","btnContinue"],15000);
	  kony.automation.button.click(["frmMMEndDate","btnContinue"]);
	  kony.automation.playback.waitFor(["frmMMReview","btnTransfer"],15000);
	  kony.automation.button.click(["frmMMReview","btnTransfer"]);
	  kony.automation.playback.waitFor(["frmMMConfirmation","btnDashboard"],15000);
	  kony.automation.button.click(["frmMMConfirmation","btnDashboard"]);
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","lblLocateUs"],10000);
	}
	
	
	
	function SearchP2PInFromAndToScreen() {
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],10000);
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(2000);
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"],15000);
	  kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0,1]"]);
	  kony.automation.playback.waitFor(["frmMMTransferFromAccount","tbxSearch"],15000);
	  kony.automation.widget.touch(["frmMMTransferFromAccount","tbxSearch"], [100,13],null,[100,56]);
	  kony.automation.textbox.enterText(["frmMMTransferFromAccount","customSearchbox","tbxSearch"],"checki");
	  kony.automation.playback.wait(1000);
	  kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	  // :User Injected Code Snippet [//Expect Checking Account - [2 lines]]
	  //var checkingAcc = ;
	  expect(kony.automation.widget.getWidgetProperty(["frmMMTransferFromAccount","segTransactions[0,0]]","flxAccountsNoImageTransfers","flxMain","flxAccountName","lblAccountName"],"text")).toContain("Checki");
	  // :End User Injected Code Snippet {391e3878-1a4a-6cd5-312f-c42e3dc249f3}
	  kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	  kony.automation.segmentedui.click(["frmMMTransferFromAccount","segTransactions[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMTransferToAccount","tbxSearch"],15000);
	  kony.automation.widget.touch(["frmMMTransferToAccount","tbxSearch"], [72,22],null,[72,65]);
	  kony.automation.textbox.enterText(["frmMMTransferToAccount","customSearchbox","tbxSearch"],"P2P");
	  kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],15000);
	  // :User Injected Code Snippet [//Expect P2P recipient - [2 lines]]
	  var creditCard = kony.automation.widget.getWidgetProperty(["frmMMTransferToAccount","segTransactions[0,0]]","flxAccountsNoImageTransfers","flxMain","flxAccountName","lblAccountName"],"text");
	  expect(creditCard).toContain("P2P");
	  // :End User Injected Code Snippet {2875156c-0fc0-2414-6f01-69c86f59b306}
	  kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],10000);
	  kony.automation.segmentedui.click(["frmMMTransferToAccount","segTransactions[0,0]"]);
	}
	
	function SearchSameBankInFromAndToScreen() {
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],10000);
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"],15000);
	  kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0,1]"]);
	  kony.automation.playback.waitFor(["frmMMTransferFromAccount","tbxSearch"],15000);
	  kony.automation.widget.touch(["frmMMTransferFromAccount","tbxSearch"], [100,13],null,[100,56]);
	  kony.automation.textbox.enterText(["frmMMTransferFromAccount","customSearchbox","tbxSearch"],"checki");
	  kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	  // :User Injected Code Snippet [//Expect Checking Account - [2 lines]]
	  //var checkingAcc = ;
	  expect(kony.automation.widget.getWidgetProperty(["frmMMTransferFromAccount","segTransactions[0,0]]","flxAccountsNoImageTransfers","flxMain","flxAccountName","lblAccountName"],"text")).toContain("Checki");
	  // :End User Injected Code Snippet {391e3878-1a4a-6cd5-312f-c42e3dc249f3}
	  kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	  kony.automation.segmentedui.click(["frmMMTransferFromAccount","segTransactions[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMTransferToAccount","tbxSearch"],15000);
	  kony.automation.widget.touch(["frmMMTransferToAccount","tbxSearch"], [72,22],null,[72,65]);
	  kony.automation.textbox.enterText(["frmMMTransferToAccount","customSearchbox","tbxSearch"],"samebank2");
	  kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],15000);
	  // :User Injected Code Snippet [//Expect Credit Card - [2 lines]]
	  var creditCard = kony.automation.widget.getWidgetProperty(["frmMMTransferToAccount","segTransactions[0,0]]","flxAccountsNoImageTransfers","flxMain","flxAccountName","lblAccountName"],"text");
	  expect(creditCard).toContain("samebank2");
	  // :End User Injected Code Snippet {2875156c-0fc0-2414-6f01-69c86f59b306}
	  kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],10000);
	  kony.automation.segmentedui.click(["frmMMTransferToAccount","segTransactions[0,0]"]);
	}
	
	
	
	function SelectDateRangeAndConfirmTransfer(ValTimePeriod, valueType) {
	  //write your automation code here
	  kony.automation.playback.waitFor(["frmMMDuration","segOptions[0,0]"],10000);
	  kony.automation.segmentedui.click(["frmMMDuration","segOptions[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMStartDate","customCalendar","flxNextMonth"],15000);
	  kony.automation.flexcontainer.click(["frmMMStartDate","customCalendar","flxNextMonth"]);
	  // :User Injected Code Snippet [//Select "Start" date - [1 lines]]
	  kony.automation.widget.touch(["frmMMStartDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
	  // :End User Injected Code Snippet {1b928f08-dcde-b4a4-8a34-66c9403263c8}
	  kony.automation.playback.waitFor(["frmMMEndDate","customCalendar","flxNextMonth"],15000);
	  kony.automation.flexcontainer.click(["frmMMEndDate","customCalendar","flxNextMonth"]);
	  kony.automation.playback.waitFor(["frmMMEndDate","customCalendar","flxNextMonth"],15000);
	  kony.automation.flexcontainer.click(["frmMMEndDate","customCalendar","flxNextMonth"]);
	  // :User Injected Code Snippet [//select "End" date - [3 lines]]
	  // kony.automation.playback.waitFor(["frmMMEndDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"]);
	
	  kony.automation.widget.touch(["frmMMEndDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
	  // :End User Injected Code Snippet {52f4e47a-c3c2-0551-1005-1c35e4be2ef7}
	  kony.automation.playback.waitFor(["frmMMEndDate","btnContinue"],15000);
	  kony.automation.button.click(["frmMMEndDate","btnContinue"]);
	  kony.automation.playback.waitFor(["frmMMReview","btnTransfer"],10000);
	  kony.automation.button.click(["frmMMReview","btnTransfer"]);
	  try{
	    kony.automation.playback.wait(10000);
	    var errorMsg1 = kony.automation.widget.getWidgetProperty(["frmMMConfirmation","btnNewTransfer"], "text");
	
	    if(errorMsg1==="New Transfer"){
	      kony.print("Erro::Inside New Transfer");
	      kony.automation.playback.waitFor(["frmMMConfirmation","btnDashboard"],15000);
	      kony.automation.button.click(["frmMMConfirmation","btnDashboard"]);
	
	    }else{
	      kony.print("***********************");
	      var tempB=kony.automation.widget.getWidgetProperty(["frmMMConfirmation","lblMessage"], "text");
	      kony.print("TeLL Me the Value :"+tempB);
	      expect(tempB).toContain("Successfully Scheduled Transfer");
	      kony.automation.playback.waitFor(["frmMMConfirmation","btnToAccount"],15000);
	      kony.automation.button.click(["frmMMConfirmation","btnToAccount"]);
	    }
	
	  }catch(err){
	    kony.print("Erro::Unable to find element");
	
	    try{
	      kony.automation.playback.wait(10000);
	      var currentwidget1 = kony.automation.widget.getWidgetProperty(["frmLogout","btnLogIn"], "text");
	      kony.print("The current Form Name ::"+currentwidget1);
	      if(currentwidget1 === "Sign In"){
	
	        kony.automation.button.click(["frmLogout","btnLogIn"]);
	        kony.automation.playback.waitFor(["frmLogin","login","tbxUsername"],10000);
	        kony.automation.textbox.enterText(["frmLogin","login","tbxUsername"],"dbpolbuser");
	        kony.automation.textbox.enterText(["frmLogin","login","tbxPassword"],"Kony@1234");
	        kony.automation.button.click(["frmLogin","login","btnLogIn"]);
	        kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],5000);
	        /***********************************/
	
	        //ValTimePeriod, ValueType
	        SearchInFromAndToScreen(valueType);
	        EnterAmount();
	
	        switch(ValTimePeriod){
	          case "Daily":
	            SelectFrequencyDaily();
	            break;
	          case "Weekly":
	            SelectFrequencyWeekly();
	            break;
	          case "HalfY":
	            SelectFrequencyHalfYearly();
	            break;
	          case "Yearly":
	            SelectFrequencyYearly();
	            break;
	          case "QTR":
	            SelectFrequencyQuarterly();
	            break;
	          case "Monthly":
	            SelectFrequencyMonthly();
	            break;
	        }
	        SelectDateRangeAndConfirmTransfer(ValTimePeriod, valueType);
	
	      }     
	
	    }catch(err1){
	      kony.print("Error::"+err1.message);
	    }
	  }
	
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	}
	
	
	
	function SelectFrequencyHalfYearly() {
	  kony.automation.playback.waitFor(["frmMMReview","segDetails"],15000);
	  kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	  kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,6]"]);
	}
	
	
	function SelectFrequencyMonthly() {
	  kony.automation.playback.waitFor(["frmMMReview","segDetails"],15000);
	  kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	  kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,4]"]);
	}
	
	function SelectFrequencyQuarterly() {
	  kony.automation.playback.waitFor(["frmMMReview","segDetails"],15000);
	  kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	  kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,5]"]);
	}
	
	function SelectFrequencyWeekly() {
	  kony.automation.playback.waitFor(["frmMMReview","segDetails"]);
	  kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMFrequency","segOptions"]);
	  kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,2]"]);
	}
	
	
	function SelectFrequencyYearly() {
	  kony.automation.playback.waitFor(["frmMMReview","segDetails"],15000);
	  kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	  kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,7]"]);
	}
	
	function OpenManageRecipientP2P() {
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"],15000);
	  kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0,4]"]);
	  kony.automation.playback.waitFor(["frmManageRecipientType","segRecipientType"],15000);
	  kony.automation.segmentedui.click(["frmManageRecipientType","segRecipientType[1,0]"]);
	  kony.automation.playback.waitFor(["frmManageRecipientList","btnAddRecipient"],15000);
	}
	
	
	function SearchP2PInFromAndToScreen() {
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	  kony.automation.playback.wait(2000);
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"],15000);
	  kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0,1]"]);
	  kony.automation.playback.waitFor(["frmMMTransferFromAccount","tbxSearch"],15000);
	  kony.automation.widget.touch(["frmMMTransferFromAccount","tbxSearch"], [100,13],null,[100,56]);
	  kony.automation.textbox.enterText(["frmMMTransferFromAccount","customSearchbox","tbxSearch"],"checki");
	  kony.automation.playback.wait(1000);
	  kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	  // :User Injected Code Snippet [//Expect Checking Account - [2 lines]]
	  //var checkingAcc = ;
	  expect(kony.automation.widget.getWidgetProperty(["frmMMTransferFromAccount","segTransactions[0,0]]","flxAccountsNoImageTransfers","flxMain","flxAccountName","lblAccountName"],"text")).toContain("Checki");
	  // :End User Injected Code Snippet {391e3878-1a4a-6cd5-312f-c42e3dc249f3}
	  kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	  kony.automation.segmentedui.click(["frmMMTransferFromAccount","segTransactions[0,0]"]);
	  kony.automation.playback.waitFor(["frmMMTransferToAccount","tbxSearch"],15000);
	  kony.automation.widget.touch(["frmMMTransferToAccount","tbxSearch"], [72,22],null,[72,65]);
	  kony.automation.textbox.enterText(["frmMMTransferToAccount","customSearchbox","tbxSearch"],"P2P");
	  kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],15000);
	  // :User Injected Code Snippet [//Expect P2P recipient - [2 lines]]
	  var creditCard = kony.automation.widget.getWidgetProperty(["frmMMTransferToAccount","segTransactions[0,0]]","flxAccountsNoImageTransfers","flxMain","flxAccountName","lblAccountName"],"text");
	  expect(creditCard).toContain("P2P");
	  // :End User Injected Code Snippet {2875156c-0fc0-2414-6f01-69c86f59b306}
	  kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],15000);
	  kony.automation.segmentedui.click(["frmMMTransferToAccount","segTransactions[0,0]"]);
	}
	
	function GoBackToDB() {
	  kony.automation.playback.waitFor(["frmManageRecipientList","btnAddRecipient"],10000);
	  kony.automation.playback.waitFor(["frmManageRecipientList","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmManageRecipientList","customHeader","flxBack"]);
	  kony.automation.playback.waitFor(["frmManageRecipientType","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmManageRecipientType","customHeader","flxBack"]);
	
	}
	
	function Logout() {
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],5000);
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","flxLogout"],5000);
	  kony.automation.widget.touch(["frmUnifiedDashboard","Hamburger","flxLogout"], null,null,[34,28]);
	  kony.automation.playback.waitFor(["frmLogout","btnLogIn"],10000);
	  kony.automation.button.click(["frmLogout","btnLogIn"]);
	  kony.automation.playback.waitFor(["frmLogin","login","tbxUsername"],10000);
	}
	
	
	it("Verify_ScheduledTransferDaily_DateRangeAndNumberOfOccurances", async function() {
	  
	  SearchInFromAndToScreen("normal");
	  EnterAmount();
	  SelectFrequencyDaily();
	  SelectDateRangeAndConfirmTransfer("Daily","normal");
	
	  SearchInFromAndToScreen("normal");
	  EnterAmount();
	  SelectFrequencyDaily();
	  SelectNumberOfTransfersAndThenConfirmTransfer("Daily","normal");
	  Logout();
	},180000);
	
	it("Verify_ScheduledTransferDaily_DateRangeAndNoOfOccurances_P2P", async function() {
	  
	  SearchInFromAndToScreen("P2P");
	  EnterAmount();
	  SelectFrequencyDaily();
	  SelectDateRangeAndConfirmTransfer("Daily","P2P");
	  
	  SearchInFromAndToScreen("P2P");
	  EnterAmount();
	  SelectFrequencyDaily();
	  SelectNumberOfTransfersAndThenConfirmTransfer("Daily","P2P");
	  Logout();
	},180000);
	
	it("Verify_ScheduledTransferMonthly_DateRangeAndNumberOfOccurance", async function() {
		SearchInFromAndToScreen("normal");
		EnterAmount();
		SelectFrequencyMonthly();
		SelectDateRangeAndConfirmTransfer("Monthly","normal");
	  
		SearchInFromAndToScreen("normal");
		EnterAmount();
		SelectFrequencyMonthly();
		SelectNumberOfTransfersAndThenConfirmTransfer("Monthly","normal");
	    Logout();
	},180000);
	
	it("Verify_ScheduledTransferMonthly_DateRangeAndNoOfOccurance_P2P", async function() {
	  
	  SearchInFromAndToScreen("P2P");
	  EnterAmount();
	  SelectFrequencyMonthly();
	  SelectDateRangeAndConfirmTransfer("Monthly","P2P");
	  
	  SearchInFromAndToScreen("P2P");
	  EnterAmount();
	  SelectFrequencyMonthly();
	  SelectNumberOfTransfersAndThenConfirmTransfer("Monthly","P2P");
	  Logout();
	},180000);
	
	it("Verify_ScheduledTransferWeekly_DateRangeAndNumberOfOccurance", async function() {
		SearchInFromAndToScreen("normal");
		EnterAmount();
		SelectFrequencyWeekly();
		SelectDateRangeAndConfirmTransfer("Weekly","normal");
	  
		SearchInFromAndToScreen("normal");
		EnterAmount();
		SelectFrequencyWeekly();
		SelectNumberOfTransfersAndThenConfirmTransfer("Weekly","normal");
	  Logout();
	},180000);
	
	it("Verify_ScheduledTransferWeekly_DateRangeAndNoOfOccurance_P2P", async function() {
	  
	  SearchInFromAndToScreen("P2P");
	  EnterAmount();
	  SelectFrequencyWeekly();
	  SelectDateRangeAndConfirmTransfer("Weekly","P2P");
	  
	  SearchInFromAndToScreen("P2P");
	  EnterAmount();
	  SelectFrequencyWeekly();
	  SelectNumberOfTransfersAndThenConfirmTransfer("Weekly","P2P");
	  Logout();
	},180000);
	
	it("Verify_ScheduledTransferQTR_DateRangeAndNumberOfOccurance", async function() {
		SearchInFromAndToScreen("normal");
		EnterAmount();
		SelectFrequencyQuarterly();
		SelectDateRangeAndConfirmTransfer("QTR","normal");
	  
		SearchInFromAndToScreen("normal");
		EnterAmount();
		SelectFrequencyQuarterly();
		SelectNumberOfTransfersAndThenConfirmTransfer("QTR","normal");
	  Logout();
	},180000);
	
	it("Verify_ScheduledTransferQTR_DateRangeAndNoOfOccurance_P2P", async function() {
	  
	  SearchInFromAndToScreen("P2P");
	  EnterAmount();
	  SelectFrequencyQuarterly();
	  SelectDateRangeAndConfirmTransfer("QTR","P2P");
	  
	  SearchInFromAndToScreen("P2P");
	  EnterAmount();
	  SelectFrequencyQuarterly();
	  SelectNumberOfTransfersAndThenConfirmTransfer("QTR","P2P");
	  Logout();
	},180000);
	
	it("Verify_ScheduledTransferHalfY_DateRangeAndNumberOfOccurance", async function() {
	  SearchInFromAndToScreen("normal");
	  EnterAmount();
	  SelectFrequencyHalfYearly();
	  SelectDateRangeAndConfirmTransfer("HalfY","normal");
	
	  SearchInFromAndToScreen("normal");
	  EnterAmount();
	  SelectFrequencyHalfYearly();
	  SelectNumberOfTransfersAndThenConfirmTransfer("HalfY","normal");
	  Logout();
	},180000);
	
	it("Verify_ScheduledTransferHalfY_DateRangeAndNoOfOccurance_P2P", async function() {
	  
	  SearchInFromAndToScreen("P2P");
	  EnterAmount();
	  SelectFrequencyHalfYearly();
	  SelectDateRangeAndConfirmTransfer("HalfY","P2P");
	  
	  SearchInFromAndToScreen("P2P");
	  EnterAmount();
	  SelectFrequencyHalfYearly();
	  SelectNumberOfTransfersAndThenConfirmTransfer("HalfY","P2P");
	  Logout();
	},180000);
	
	it("Verify_ScheduledTransferYearly_DateRangeAndNumberOfOccurance", async function() {
		SearchInFromAndToScreen("normal");
		EnterAmount();
		SelectFrequencyYearly();
		SelectDateRangeAndConfirmTransfer("Yearly","normal");
	  
		SearchInFromAndToScreen("normal");
		EnterAmount();
		SelectFrequencyYearly();
		SelectNumberOfTransfersAndThenConfirmTransfer("Yearly","normal");
	  Logout();
	},180000);
	
	it("Verify_ScheduledTransferYearly_DateRangeAndNoOccurance_P2P", async function() {
	  
	  SearchInFromAndToScreen("P2P");
	  EnterAmount();
	  SelectFrequencyYearly();
	  SelectDateRangeAndConfirmTransfer("Yearly","P2P");
	  
	  SearchInFromAndToScreen("P2P");
	  EnterAmount();
	  SelectFrequencyYearly();
	  SelectNumberOfTransfersAndThenConfirmTransfer("Yearly","P2P");
	  Logout();
	},180000);
	
	it("Verify_OneTimeTransfer", async function() {
	  // :User Injected Code Snippet [//Search and enter Amount - [2 lines]]
	  SearchInFromAndToScreen("normal");
	  EnterAmount();
	  // :End User Injected Code Snippet {77fb80f9-55cf-a5b4-3a11-b38cc89f3d99}
	  await kony.automation.playback.waitFor(["frmMMReview","segDetails"],10000);
	  kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	  await kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],10000);
	  kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,0]"]);
	  await kony.automation.playback.waitFor(["frmMMStartDate","btnContinue"],10000);
	  kony.automation.button.click(["frmMMStartDate","btnContinue"]);
	  await kony.automation.playback.waitFor(["frmMMReview","btnTransfer"],10000);
	  kony.automation.button.click(["frmMMReview","btnTransfer"]);
	  expect(kony.automation.playback.waitFor(["frmMMConfirmation","btnNewTransfer"],15000)).toContain(true);
	  await kony.automation.playback.waitFor(["frmMMConfirmation","btnDashboard"],10000);
	  kony.automation.button.click(["frmMMConfirmation","btnDashboard"]);
	  await kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	  Logout();
	});
	
	it("Verify_OneTimeTransfer_P2P", async function() {
	
	  SearchInFromAndToScreen("P2P");
	  EnterAmount();
	  SelectFrequencyOnceAndTransfer();
	  SearchInFromAndToScreen("P2P");
	  TransferScheduleOnce();
	  Logout();
	},180000);
	
	it("Verify_TransferScheduleOnce", async function() {
		// :User Injected Code Snippet [//Prerequisite Verify_Search_In_FromAndToScreen - [1 lines]]
		SearchInFromAndToScreen("normal");
		// :End User Injected Code Snippet {0d4c824b-a9ad-8c0e-5c44-a3292aac96a1}
		await kony.automation.playback.waitFor(["frmMMTransferAmount","keypad","btnThree"]);
		kony.automation.button.click(["frmMMTransferAmount","keypad","btnThree"]);
		kony.automation.button.click(["frmMMTransferAmount","keypad","btnZero"]);
		kony.automation.button.click(["frmMMTransferAmount","keypad","btnZero"]);
		kony.automation.button.click(["frmMMTransferAmount","btnContinue"]);
		await kony.automation.playback.waitFor(["frmMMReview","segDetails"]);
		kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
		await kony.automation.playback.waitFor(["frmMMFrequency","segOptions"]);
		kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,0]"]);
		await kony.automation.playback.waitFor(["frmMMStartDate","customCalendar","flxNextMonth"]);
		kony.automation.flexcontainer.click(["frmMMStartDate","customCalendar","flxNextMonth"]);
		// :User Injected Code Snippet [//Select a date - [3 lines]]
		await kony.automation.playback.waitFor(["frmMMStartDate","customCalendar","flxNextMonth"]);
			kony.automation.widget.touch(["frmMMStartDate","customCalendar","flxNextMonth"], [178,125],null,[178,125]);
			kony.automation.widget.touch(["frmMMStartDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
		// :End User Injected Code Snippet {414f83d0-e0a2-735c-c437-a564878872a6}
		await kony.automation.playback.waitFor(["frmMMStartDate","btnContinue"]);
		kony.automation.button.click(["frmMMStartDate","btnContinue"]);
		await kony.automation.playback.waitFor(["frmMMReview","btnTransfer"]);
		kony.automation.button.click(["frmMMReview","btnTransfer"]);
		await kony.automation.playback.waitFor(["frmMMConfirmation","lblSuccessMessage"]);
		kony.automation.button.click(["frmMMConfirmation","btnDashboard"]);
		
	},180000);
	
	it("Verify_AddP2PRecipient", async function() {
	  OpenManageRecipientP2P();
	  await kony.automation.playback.waitFor(["frmManageRecipientList","btnAddRecipient"],10000);
	  kony.automation.button.click(["frmManageRecipientList","btnAddRecipient"]);
	  await kony.automation.playback.waitFor(["frmRegP2PContactType","btnPhoneNumber"],10000);
	  kony.automation.button.click(["frmRegP2PContactType","btnPhoneNumber"]);
	  await kony.automation.playback.waitFor(["frmP2PRecPhoneNo","keypad","btnOne"],10000);
	  kony.automation.button.click(["frmP2PRecPhoneNo","keypad","btnOne"]);
	  kony.automation.button.click(["frmP2PRecPhoneNo","keypad","btnTwo"]);
	  kony.automation.button.click(["frmP2PRecPhoneNo","keypad","btnThree"]);
	  kony.automation.button.click(["frmP2PRecPhoneNo","keypad","btnFour"]);
	  kony.automation.button.click(["frmP2PRecPhoneNo","keypad","btnFive"]);
	  kony.automation.button.click(["frmP2PRecPhoneNo","keypad","btnSix"]);
	  kony.automation.button.click(["frmP2PRecPhoneNo","btnContinue"]);
	  await kony.automation.playback.waitFor(["frmP2PRecipientName","txtRecipientName"],10000);
	  kony.automation.textbox.enterText(["frmP2PRecipientName","txtRecipientName"],"P2PRecipient");
	  kony.automation.button.click(["frmP2PRecipientName","btnContinue"]);
	  await kony.automation.playback.waitFor(["frmP2PVerifyDetails","btnContinue"]);
	  kony.automation.button.click(["frmP2PVerifyDetails","btnContinue"]);
	  await kony.automation.playback.waitFor(["frmManageRecipientList","customPopup","lblPopup"],20000);
	  // :User Injected Code Snippet [// - [1 lines]]
	  expect(kony.automation.widget.getWidgetProperty(["frmManageRecipientList","customPopup","lblPopup"], "text")).toContain("added");
	  // :End User Injected Code Snippet {eb9cb05e-b740-791b-3eef-9d35dcadcb47}
	  await kony.automation.playback.waitFor(["frmManageRecipientList","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmManageRecipientList","customHeader","flxBack"]);
	  await kony.automation.playback.waitFor(["frmManageRecipientType","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmManageRecipientType","customHeader","flxBack"]);
	},180000);
	
	it("Verify_DeleteP2PRecipient_P2P", async function() {
	  OpenManageRecipientP2P();
	  await kony.automation.playback.waitFor(["frmManageRecipientList","tbxSearch"],10000);
	  kony.automation.widget.touch(["frmManageRecipientList","tbxSearch"], [101,9],null,null);
	  await kony.automation.playback.waitFor(["frmManageRecipientList","customSearchbox","tbxSearch"],10000);
	  kony.automation.textbox.enterText(["frmManageRecipientList","customSearchbox","tbxSearch"],"P2P");
	  await kony.automation.playback.waitFor(["frmManageRecipientList","segRecipients"],10000);
	  kony.automation.segmentedui.click(["frmManageRecipientList","segRecipients[0,0]"]);
	  await kony.automation.playback.waitFor(["frmManageP2pRecipient","btnDeleteRecipient"],10000);
	  kony.automation.button.click(["frmManageP2pRecipient","btnDeleteRecipient"]);
	  kony.automation.alert.click(0);
	  // :User Injected Code Snippet [// - [2 lines]]
	  await kony.automation.playback.waitFor(["frmManageRecipientList","customPopup","lblPopup"],10000);
	  expect(kony.automation.widget.getWidgetProperty(["frmManageRecipientList","customPopup","lblPopup"], "text")).toContain("deleted");
	  // :End User Injected Code Snippet {e2be7ffa-f94b-2680-c773-a2012940db2d}
	  await kony.automation.playback.waitFor(["frmManageRecipientList","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmManageRecipientList","customHeader","flxBack"]);
	  await kony.automation.playback.waitFor(["frmManageRecipientType","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmManageRecipientType","customHeader","flxBack"]);
	},180000);
});