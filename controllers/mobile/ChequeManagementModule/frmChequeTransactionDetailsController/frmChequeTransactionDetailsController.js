define({
  initActions: function () {
    var scope=this;
    var currentFormObject = kony.application.getCurrentForm();
    var currentForm=currentFormObject.id;
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
    this.view.customHeader.flxBack.onTouchEnd = scope.navigateBack;
  },
  preShow:function(){
    if (kony.os.deviceInfo().name === "iPhone") {
      this.view.flxHeader.isVisible = false;
    } else {
      this.view.flxHeader.isVisible = true;
    }
    var navMan=applicationManager.getNavigationManager();
    var details=navMan.getCustomInfo("frmChequeTransactionDetails");
    if(details.tabSelection==="ChequeBook"){
      this.renderChequeBookView(details.data);
    }
    else if(details.tabSelection==="StopCheque"){
      this.renderStopChequeView(details.data);
    }
    else{
      this.renderMyChequeView(details.data);
    }
  },
  postShow:function(){

  },
  renderChequeBookView:function(data){
    this.view.flxStopChequeDetails.isVisible=false;
    this.view.flxChequeBookDetails.isVisible=true;
    this.view.flxChequeDetails.isVisible=false;
    var presentation = applicationManager.getModulesPresentationController("ChequeManagementModule");
    this.view.lblNumberOfBooks.text=data.bookCount;
    this.view.lblStatus.text=data.lblStatus;
    this.view.lblAccountValue.text=presentation.processedName;
    if(data.description)
      this.view.lblAccountType.text=data.description;
    else
      this.view.lblAccountType.text="";
    if(data.lblDate)
      this.view.lblRequestDateValue.text=data.lblDate;
    else
      this.view.lblRequestDateValue.text="-";
    this.view.lblRefValue.text=data.chequeIssueId;
    if(data.notes)
      this.view.lblNotesValue.text=data.notes;
    else
      this.view.lblNotesValue.text="-";
    this.view.flxDeliveryType.isVisible=false;
    this.view.flxAddress.isVisible=false;
    this.view.flxFee.isVisible=false;
    this.view.flxButtons.isVisible=false;
  },
  renderStopChequeView:function(data){
    var presentation = applicationManager.getModulesPresentationController("ChequeManagementModule");
    this.view.flxStopChequeDetails.isVisible=true;
    this.view.flxChequeBookDetails.isVisible=false;
    this.view.flxChequeDetails.isVisible=false;
    this.view.lblTransferValue.text=data.booksCount;
    this.view.lblSuccess.text=data.lblStatus;
    this.view.lblTransferredToValueTrans.text=presentation.processedName;
    if(data.accountType)
      this.view.lblAccTypeTrans.text=data.accountType;
    else
      this.view.lblAccTypeTrans.text="";
    if(data.payeeName)
      this.view.lblTransferredToValueP2P.text=data.payeeName;
    else
      this.view.lblTransferredToValueP2P.text="-";
    if(data.lblDate)
      this.view.lblDescValueTrans.text=data.lblDate;
    else
      this.view.lblDescValueTrans="-";
    if(data.checkDateOfIssue)
      this.view.lblTransferredFromValueTrans.text=data.checkDateOfIssue;
    else
      this.view.lblTransferredFromValueTrans.text="-";
    this.view.lblTransDateValueTrans.text=data.lblAccountNo;
    this.view.lblFreqTransValue.text=data.checkReason;
    if(data.amount)
      this.view.lblRecurrenceValueTrans.text=data.amount;
    else
      this.view.lblRecurrenceValueTrans.text="-";
    if (data.booksCount && data.booksCount.indexOf("-") > -1) {
      var res = data.booksCount.split("-");
      this.view.lblReferenceNoValueTrans.text=parseInt((res[1])-parseInt(res[0])+1);
    }
    else
      this.view.lblReferenceNoValueTrans.text="-";
    if(data.fee)
      this.view.lblIBANValue.text=data.fee;
    else
      this.view.lblIBANValue.text="-";
    if(data.notes)
      this.view.lblNotesValueTrans.text=data.notes;
    else
      this.view.lblNotesValueTrans.text="";
    this.view.flxButtonsTrans.isVisible=false;
  },
  renderMyChequeView:function(data){
    var presentation = applicationManager.getModulesPresentationController("ChequeManagementModule");
    this.view.flxStopChequeDetails.isVisible=false;
    this.view.flxChequeBookDetails.isVisible=false;
    this.view.flxChequeDetails.isVisible=true;
    this.view.flexButtons.isVisible=false;
    this.view.CopylblTransferValue0jc8c6047197b40.text=data.booksCount;
    this.view.CopylblSuccess0c5ec6f15010f45.text=data.lblStatus;
    this.view.lblFromValue.text=presentation.processedName;
    if(data.accountType)
      this.view.lblAcType.text=data.accountType;
    else
      this.view.lblAcType.text="";
    if(data.lblAccountNo)
      this.view.lblPayeeValue.text=data.lblAccountNo;
    else
      this.view.lblPayeeValue.text=data.lblAccountNo="-";
    this.view.lblAmountValue.text=data.lblDate;
    this.view.lblDateValue.text=data.issueDate;
    this.view.lblRefeValue.text=data.referenceNumber;
    this.view.flexReason.isVisible=false;
    this.view.flexNotes.isVisible=false;
  },
  navigateBack:function(){
    var navMan=applicationManager.getNavigationManager();
    navMan.goBack();
  }


});