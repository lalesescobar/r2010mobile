define({ 

  //Type your controller code here 
   downloadSingleFile: function(){
     var index = kony.application.getCurrentForm().segDownloadAttachments.selectedRowIndex;
     if(index.length>0){ 
       var rowIndex = index[1];
       if(rowIndex !== null)
       {
         var downloadedAttachment = kony.application.getCurrentForm().segDownloadAttachments.data[rowIndex];
         var controller = _kony.mvc.GetController('frmAttachments', true);
         controller.downloadSingleFile(downloadedAttachment);
       }
     }
   }
 
  });