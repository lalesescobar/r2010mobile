define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Form_a0aa105fe7204bf3b6b9ee2e74bb6b02: function AS_Form_a0aa105fe7204bf3b6b9ee2e74bb6b02(eventobject) {
        var self = this;
        this.init();
    },
    AS_Form_c5ecfb7cc9cd4d0e9026da18c295a4d7: function AS_Form_c5ecfb7cc9cd4d0e9026da18c295a4d7(eventobject) {
        var self = this;
        this.registerActions();
    },
    AS_BarButtonItem_g0d1af7892564a29b0769ade73679912: function AS_BarButtonItem_g0d1af7892564a29b0769ade73679912(eventobject) {
        var self = this;
        var navMan = applicationManager.getNavigationManager();
        navMan.goBack();
    },
    AS_BarButtonItem_b4d19838ec30472fb029f392f267db91: function AS_BarButtonItem_b4d19838ec30472fb029f392f267db91(eventobject) {
        var self = this;
        this.btnEditOnClick();
    }
});