define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_i770d8443e3f45ddaab58257c32cff87: function AS_FlexContainer_i770d8443e3f45ddaab58257c32cff87(eventobject) {
        var self = this;
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
        accountModule.presentationController.commonFunctionForNavigation("frmMessages");
    },
    AS_FlexContainer_f5b09e6930c444848c6ee6aae4b475de: function AS_FlexContainer_f5b09e6930c444848c6ee6aae4b475de(eventobject) {
        var self = this;
        var accountMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
        accountMod.presentationController.commonFunctionForNavigation("frmTransfers");
    },
    AS_Form_d5565ab84c334bf7bfb5c5498bf6ec09: function AS_Form_d5565ab84c334bf7bfb5c5498bf6ec09(eventobject) {
        var self = this;
        this.init();
    },
    AS_Form_d294f3984b074da48045b8a35f521cfd: function AS_Form_d294f3984b074da48045b8a35f521cfd(eventobject) {
        var self = this;
        this.postShow();
    },
    AS_Form_d4e155095c454c8daab39d1e29f4bf5e: function AS_Form_d4e155095c454c8daab39d1e29f4bf5e(eventobject) {
        var self = this;
        this.preshow();
    },
    AS_Image_d60d388eb00b4be7a43826b93d1052e1: function AS_Image_d60d388eb00b4be7a43826b93d1052e1(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 1);
    },
    AS_Image_c5a3882d05b44e89bce51527166245d4: function AS_Image_c5a3882d05b44e89bce51527166245d4(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 2);
    },
    AS_Image_b0086d129aa64368a59d449db6ec61d3: function AS_Image_b0086d129aa64368a59d449db6ec61d3(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 3);
    },
    AS_Image_bb77ccc24e8c4484a9eaf246729b16ff: function AS_Image_bb77ccc24e8c4484a9eaf246729b16ff(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 4);
    },
    AS_Image_a54307dd07024f1bb2bfeda05ba140ca: function AS_Image_a54307dd07024f1bb2bfeda05ba140ca(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 5);
    }
});