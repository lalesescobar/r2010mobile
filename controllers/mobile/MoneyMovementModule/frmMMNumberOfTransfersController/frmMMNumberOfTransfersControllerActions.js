define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxBack **/
    AS_FlexContainer_d2416954b5d6438ebf3434c66e6a5d6d: function AS_FlexContainer_d2416954b5d6438ebf3434c66e6a5d6d(eventobject) {
        var self = this;
        var billPayMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        billPayMod.presentationController.commonFunctionForNavigation("frmTransfersDuration");
    },
    /** onClick defined for btnOne **/
    AS_Button_j2066a7bffe545828aca5cb1629d3517: function AS_Button_j2066a7bffe545828aca5cb1629d3517(eventobject) {
        var self = this;
        this.setKeypadChar(1);
    },
    /** onClick defined for btnTwo **/
    AS_Button_a00291d362f441aab9d6f460a6809514: function AS_Button_a00291d362f441aab9d6f460a6809514(eventobject) {
        var self = this;
        this.setKeypadChar(2);
    },
    /** onClick defined for btnThree **/
    AS_Button_dca40ccc1a574504835faf8dab90cb02: function AS_Button_dca40ccc1a574504835faf8dab90cb02(eventobject) {
        var self = this;
        this.setKeypadChar(3);
    },
    /** onClick defined for btnFour **/
    AS_Button_c6b5ed3337074cd0996871e66184be70: function AS_Button_c6b5ed3337074cd0996871e66184be70(eventobject) {
        var self = this;
        this.setKeypadChar(4);
    },
    /** onClick defined for btnFive **/
    AS_Button_aab70d801cdf470db17d1d14ed1c1a72: function AS_Button_aab70d801cdf470db17d1d14ed1c1a72(eventobject) {
        var self = this;
        this.setKeypadChar(5);
    },
    /** onClick defined for btnSix **/
    AS_Button_deee8d02ce324e76b236cb1d0e2a7ee9: function AS_Button_deee8d02ce324e76b236cb1d0e2a7ee9(eventobject) {
        var self = this;
        this.setKeypadChar(6);
    },
    /** onClick defined for btnSeven **/
    AS_Button_dfc3232031b744fbaa8a15e3bf7b7ce0: function AS_Button_dfc3232031b744fbaa8a15e3bf7b7ce0(eventobject) {
        var self = this;
        this.setKeypadChar(7);
    },
    /** onClick defined for btnEight **/
    AS_Button_gc752c937ed04c5d98edae6219bd7548: function AS_Button_gc752c937ed04c5d98edae6219bd7548(eventobject) {
        var self = this;
        this.setKeypadChar(8);
    },
    /** onClick defined for btnNine **/
    AS_Button_f395a79bb4074d50879c205b2a4837af: function AS_Button_f395a79bb4074d50879c205b2a4837af(eventobject) {
        var self = this;
        this.setKeypadChar(9);
    },
    /** onClick defined for btnZero **/
    AS_Button_d05055cc355b42f58ee74be11b5c9ab4: function AS_Button_d05055cc355b42f58ee74be11b5c9ab4(eventobject) {
        var self = this;
        this.setKeypadChar(0);
    },
    /** onTouchEnd defined for imgClearKeypad **/
    AS_Image_ef0037fbbe084f2095afba09f3c9f575: function AS_Image_ef0037fbbe084f2095afba09f3c9f575(eventobject, x, y) {
        var self = this;
        this.clearKeypadChar();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_c6c1d0b834234837a968d52beb97bcaa: function AS_BarButtonItem_c6c1d0b834234837a968d52beb97bcaa(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        transferModule.presentationController.cancelCommon();
    },
    /** init defined for frmMMNumberOfTransfers **/
    AS_Form_i99b8f8fd0484c4c9d254c50f539e5ec: function AS_Form_i99b8f8fd0484c4c9d254c50f539e5ec(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmMMNumberOfTransfers **/
    AS_Form_gc918ba4d9ea4c85bf619fd2d9cd44ea: function AS_Form_gc918ba4d9ea4c85bf619fd2d9cd44ea(eventobject) {
        var self = this;
        this.preShow();
    },
    /** init defined for frmMMNumberOfTransfers **/
    AS_Form_f1bd45b61fde4a02a0723776727b7c97: function AS_Form_f1bd45b61fde4a02a0723776727b7c97(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmMMNumberOfTransfers **/
    AS_Form_i750284c64d346f4a8acf92c04ab7950: function AS_Form_i750284c64d346f4a8acf92c04ab7950(eventobject) {
        var self = this;
        this.preShow();
    }
});