it("Verify_ScheduledTransferDaily_DateRangeAndNoOfOccurances_P2P", async function() {
  
  SearchInFromAndToScreen("P2P");
  EnterAmount();
  SelectFrequencyDaily();
  SelectDateRangeAndConfirmTransfer("Daily","P2P");
  
  SearchInFromAndToScreen("P2P");
  EnterAmount();
  SelectFrequencyDaily();
  SelectNumberOfTransfersAndThenConfirmTransfer("Daily","P2P");
  Logout();
},180000);