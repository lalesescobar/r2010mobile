define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmCardTransactionDetails **/
    AS_Form_c4266d74d18840e1900b74a48da2255a: function AS_Form_c4266d74d18840e1900b74a48da2255a(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmCardTransactionDetails **/
    AS_Form_b509aa3e315344478baa2e0add296c9a: function AS_Form_b509aa3e315344478baa2e0add296c9a(eventobject) {
        var self = this;
        this.frmCardTransactionDetailsPreShow();
    }
});