define({
  onNavigate: function (obj) {
    if (obj === undefined) {
      return;
    }
  },
  init:function(){
    this.initActions();
  },
  preShow: function () {
    if (applicationManager.getPresentationFormUtility().getDeviceName() === "iPhone") {
      this.view.flxHeader.isVisible = false;
    }
    var transferModPresentationController = applicationManager.getModulesPresentationController("TransferModule");
    var benificiaryDetails=transferModPresentationController.getBenificiaryData();
    this.setDetails(benificiaryDetails);
    this.setUiBasedOnPermissions(benificiaryDetails);
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
  },
  setUiBasedOnPermissions : function(account) {
	var configManager = applicationManager.getConfigurationManager();
    var createOrEditPayeePermission = this.checkEditRecipientPermission(account);
    var deletePermission = this.checkRemoveRecipientPermission(account);
    this.view.customHeader.btnRight.isVisible = createOrEditPayeePermission;
    if(applicationManager.getDeviceUtilManager().isIPhone() && !(createOrEditPayeePermission)) {
      var rightBarButtonItem = new kony.ui.BarButtonItem({
        type: configManager.constants.BAR_BUTTON_TITLE,
        style: configManager.constants.BAR_ITEM_STYLE_PLAIN,
        enabled: true,
        tintColor: "FFFFFF00",
        metaData: {
          title: " "
        }
      });
      this.view.setRightBarButtonItems({
        items: [rightBarButtonItem],
        animated: true
      });      
    } 
    
    this.view.btnDeleteRecipient.isVisible = deletePermission;
  },
  
  checkEditRecipientPermission : function(account){
    if(account.isInternationalAccount==="true"){
      return applicationManager.getConfigurationManager().checkUserPermission("INTERNATIONAL_ACCOUNT_FUND_TRANSFER_CREATE_RECEPIENT");
    }
    else if(account.isSameBankAccount === "true"){
      return applicationManager.getConfigurationManager().checkUserPermission("INTRA_BANK_FUND_TRANSFER_CREATE_RECEPIENT");
    }
    else if(account.isSameBankAccount === "false" && account.isInternationalAccount==="false"){
      return applicationManager.getConfigurationManager().checkUserPermission("INTER_BANK_ACCOUNT_FUND_TRANSFER_CREATE_RECEPIENT");
    }
    return false;
  },

  checkRemoveRecipientPermission : function(account){
    if(account.isInternationalAccount==="true"){
      return applicationManager.getConfigurationManager().checkUserPermission("INTERNATIONAL_ACCOUNT_FUND_TRANSFER_DELETE_RECEPIENT");
    }
    else if(account.isSameBankAccount === "true"){
      return applicationManager.getConfigurationManager().checkUserPermission("INTRA_BANK_FUND_TRANSFER_DELETE_RECEPIENT");
    }
    else if(account.isSameBankAccount === "false" && account.isInternationalAccount==="false"){
      return applicationManager.getConfigurationManager().checkUserPermission("INTER_BANK_ACCOUNT_FUND_TRANSFER_DELETE_RECEPIENT");
    }
    return false;
  }, 
  initActions: function () {
    var scope = this;
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
    this.view.btnTransfer.onClick=function(){
      applicationManager.getPresentationUtility().showLoadingScreen();
      var cm=applicationManager.getConfigurationManager();
      if(cm.isFastTransfersFlowEnabled()){
        var moneyMovementPresentationController = applicationManager.getModulesPresentationController("MoneyMovementModule");
        moneyMovementPresentationController.haveLimitsBeenFetched = false;
        moneyMovementPresentationController.initializeStateData(false, "");
        moneyMovementPresentationController.setTransferToAccountFromManageFlow();
      }
      else{
        var transMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("TransferModule");
        transMod.presentationController.setTransferToInfo();
        navManager.setEntryPoint("makeatransfer","frmManageTransferRecipientInfo");
      }
    };
    this.view.customHeader.flxBack.onClick = function () {
		var navMan=applicationManager.getNavigationManager();
		navMan.goBack();
    };
    this.view.customHeader.btnRight.onClick = function () {
      scope.editBenificiaryName();
    };
    this.view.btnDeleteRecipient.onClick = function () {
      var basicConfig = {message: applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.transfers.deleteAlertMessage"),alertIcon:null,alertType: constants.ALERT_TYPE_CONFIRMATION,yesLabel:applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.AlertYes"),
                         noLabel: applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.AlertNo"), alertHandler: scope.deleteHandler
                        };
      var pspConfig = {};
      applicationManager.getPresentationUtility().showAlertMessage(basicConfig, pspConfig);
    };
  },
  editBenificiaryName:function(){
    applicationManager.getPresentationUtility().showLoadingScreen();
    var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
    transferModulePresentationController.commonFunctionForNavigation("frmManageEditRecipient");
  },
  deleteHandler:function(response){
    if(response === true){
      applicationManager.getPresentationUtility().showLoadingScreen();
      var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
      transferModulePresentationController.deleteSameBankBenificiary();
    }
  },
  setDetails:function(accountDetails){
    this.view.lblRecipientNameValue.text=accountDetails.beneficiaryName;

    var maskedAccountNumber = "";
    if(typeof accountDetails.accountNumber ==="string"){
        maskedAccountNumber = applicationManager.getDataProcessorUtility().maskAccountNumber(accountDetails.accountNumber);
    }else{
        maskedAccountNumber = applicationManager.getDataProcessorUtility().maskAccountNumber(JSON.stringify(JSON.parse(accountDetails.accountNumber)));
    }

    this.view.lblAccountNumberValue.text=maskedAccountNumber;
    this.view.lblAccountTypeValue.text=accountDetails.accountType;
    this.view.lblNickNameValue.text=accountDetails.nickName;
    this.view.lblAccountBal.text=accountDetails.accountType;
    this.view.title=accountDetails.nickName;
    this.view.customHeader.lblLocateUs.text=accountDetails.nickName;
    var transferModulePresentationController = applicationManager.getModulesPresentationController("TransferModule");
    if(transferModulePresentationController.getFlowType()==="InternationalRecipients"){
      if(accountDetails.countryName){
      		this.view.lblBankName.text=accountDetails.bankName+","+accountDetails.countryName;
      		this.view.lblBankBranchValue.text=accountDetails.bankName+","+accountDetails.countryName;
      }
      else{
        this.view.lblBankName.text=accountDetails.bankName;
      	this.view.lblBankBranchValue.text=accountDetails.bankName;
      }
      this.view.flxAccounts.isVisible=true;
      this.view.lblRoutingNumberValue.isVisible=true;
      this.view.lblRoutingNumberValue.text=accountDetails.swiftCode;
      this.view.lblRoutingNumber.isVisible=true;
      this.view.flxSeparatorRounteNo.isVisible = true;
      this.view.lblRoutingNumber.text=applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Manage.SwiftCode");
      this.view.imgBank.isVisible=true;
      this.view.imgBank.src="externalbank.png";
    }
    else if(transferModulePresentationController.getFlowType()==="SameBankRecipients"){
      this.view.lblBankName.text=accountDetails.bankName;
      this.view.lblBankBranchValue.text=accountDetails.bankName;
      this.view.flxAccounts.isVisible=false;
      this.view.lblRoutingNumberValue.isVisible=false;
      this.view.lblRoutingNumber.isVisible=false;
      this.view.flxSeparatorRounteNo.isVisible = false;
      this.view.imgBank.isVisible=false;
    }
    else{
      this.view.lblBankName.text=accountDetails.bankName;
      this.view.lblBankBranchValue.text=accountDetails.bankName;
      this.view.flxAccounts.isVisible=true;
      this.view.lblRoutingNumberValue.isVisible=true;
      this.view.lblRoutingNumberValue.text=accountDetails.routingNumber;
      this.view.lblRoutingNumber.isVisible=true;
      this.view.flxSeparatorRounteNo.isVisible = true;
      this.view.lblRoutingNumber.text=applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Manage.RoutingNumber");
      this.view.imgBank.isVisible=true;
      this.view.imgBank.src="externalbank.png";
    }
  }
});