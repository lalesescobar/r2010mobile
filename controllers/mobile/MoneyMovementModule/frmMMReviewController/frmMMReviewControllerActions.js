define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_d094b7f59087475485b6a87f0c45f242: function AS_BarButtonItem_d094b7f59087475485b6a87f0c45f242(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        transferModule.presentationController.cancelCommon();
    },
    /** init defined for frmMMReview **/
    AS_Form_i11f1a60ffb64bc8ab4ae080ccc9a1c8: function AS_Form_i11f1a60ffb64bc8ab4ae080ccc9a1c8(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmMMReview **/
    AS_Form_ic2730a370824202b52e0b2c8c6a3a9e: function AS_Form_ic2730a370824202b52e0b2c8c6a3a9e(eventobject) {
        var self = this;
        this.preShow();
    }
});