define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_f9e3c61ef4bf4771a5b21729acd8e994: function AS_BarButtonItem_f9e3c61ef4bf4771a5b21729acd8e994(eventobject) {
        var self = this;
        this.navigateCustomBack();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_b54fe03a1b4d48308e38210ebbfbba0c: function AS_BarButtonItem_b54fe03a1b4d48308e38210ebbfbba0c(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    /** init defined for frmMMVerifyP2PDetails **/
    AS_Form_e5150ec27dee4e4a912e7d65ef316dfd: function AS_Form_e5150ec27dee4e4a912e7d65ef316dfd(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmMMVerifyP2PDetails **/
    AS_Form_a6ecfc01e577405eb8f614852467a8c9: function AS_Form_a6ecfc01e577405eb8f614852467a8c9(eventobject) {
        var self = this;
        this.preShow();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_i7bdb084660f49d59a4ff4446c91a875: function AS_BarButtonItem_i7bdb084660f49d59a4ff4446c91a875(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_h1ea97ddefc74fa7b26f24409085e102: function AS_BarButtonItem_h1ea97ddefc74fa7b26f24409085e102(eventobject) {
        var self = this;
        this.navigateCustomBack();
    }
});