define({ 

  //init calls after init
  init : function(){
    try{
      kony.print("Entered init");
      var navManager = applicationManager.getNavigationManager();
      var currentForm=navManager.getCurrentForm();
      applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
      this.view.preShow = this.preShowForm;
      this.view.postShow = this.postShowForm;
    }catch(e){
      kony.print("Exception in init::"+e);}
  },

  //onNavigate calls after init
  onNavigate : function(){
    try{

    }catch(e){
      kony.print("Exception in onNavigate"+e);
    }
  },

  //preShowForm is called when the form is pre loaded 
  preShowForm : function(){
    try{
      kony.print("Entered preShowForm");
      var navManager = applicationManager.getNavigationManager();
      var keyvalue="ApprovalAndRequest";
      navManager.setCustomInfo("backFormFlow",keyvalue);
      if(applicationManager.getPresentationFormUtility().getDeviceName()==="iPhone"){
        this.view.flxHeader.isVisible = false;
        this.view.title = kony.i18n.getLocalizedString("kony.mb.approvalAndRequest");
      }else{
        this.view.flxHeader.isVisible = true;
        this.view.customHeader.lblLocateUs.text = "Approvals & Requests";
      }
      this.setupNavBarSkinForiPhone();
      this.initActions();
      this.setDataInSegment();

    }catch(e){
      kony.print("Exception in preShowForm::"+e);}
  },

  //postShowForm is called when the form is post loaded 
  postShowForm : function(){
    try{
      kony.print("Entered postShowForm");
      this.setupNavBarSkinForiPhone();
    }catch(e){
      kony.print("Exception in postShowForm::"+e);}
  },

  setupNavBarSkinForiPhone : function () {
      if (applicationManager.getPresentationFormUtility().getDeviceName() !== "iPhone") return;
      try{
        var titleBarAttributes = this.view.titleBarAttributes;
        titleBarAttributes["tintColor"] = "003e7500";
        titleBarAttributes["translucent"] = false;
        this.view.titleBarAttributes = titleBarAttributes;
      }
      catch(er){
      }
    },
  /*
    *initActions is to bind the actions form widgets
    *Author : Sibhi
    */
  initActions: function(){
    try{
      kony.print("Entered initActions");
      this.view.customHeader.flxBack.onClick = this.navToPrevForm;
      this.view.segApprovals.onRowClick = this.segOnRowClickAppOrReq;
      this.view.onDeviceBack=this.dummyFunction;
    }catch(e){kony.print("Exception in initActions"+e);}
  },
  
   /*
     *navToPrevForm - This function is to called to navigate back to prev form
     **
     */
  dummyFunction: function() {},
    navToPrevForm: function() {
      try {
        kony.print("entered navToPrevForm");
        var accountMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
        accountMod.presentationController.showDashboard();
      } catch (e) {
        kony.print("exception navToPrevForm" + e);
      }
    },


  setDataInSegment : function(){
    try{
      var configManager = applicationManager.getConfigurationManager();
      var dataArr = [];
      if(configManager.getApprovalsFeaturePermissionsList().some(configManager.checkUserPermission.bind(configManager))){
        dataArr.push({
          "lblHeader":{
            "text":"Approvals"
          },
          "imgArrow":{
            "src":"chevron.png"
          }
        });
      }
      if(configManager.getRequestsFeaturePermissionsList().some(configManager.checkUserPermission.bind(configManager))){
        dataArr.push({
          "lblHeader":{
            "text":"Requests"
          },
          "imgArrow":{
            "src":"chevron.png"
          }
        });
      }
      this.view.segApprovals.setData(dataArr);
    }catch(e){
      kony.print("Exception in setDataInSegment"+e);
    }
  },

  /*
    *segOnRowClickAppOrReq is called when we click on the segment
    *Author : Sibhi
    */
  segOnRowClickAppOrReq: function(){
    try{
      kony.print("Entered segOnRowClickAppOrReq");

      var selRowItems = this.view.segApprovals.selectedRowItems[0];
      var navManager = applicationManager.getNavigationManager();

      if(selRowItems.lblHeader.text === "Approvals"){
        navManager.navigateTo("ApprovalReqMain"); 
      }else if(selRowItems.lblHeader.text === "Requests"){
        navManager.navigateTo("RequestList"); 
      }
    }catch(e){
      kony.print("Exception in setDataInSegment"+e);
    }
  }
});