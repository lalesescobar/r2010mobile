define({ 

  //Type your controller code here 
  onNavigate: function(){
    this.postShow();
  },
  postShow: function(){
    var transMod = applicationManager.getModulesPresentationController(
      "TransferModule"
    );
    var navMan = applicationManager.getNavigationManager();
    var transferData = navMan.getCustomInfo("frmEuropeTransactionDetails");
    var downloadAttachments = navMan.getCustomInfo("downloadAttachments");
    if(downloadAttachments.length === 1){
      this.view.btnDownload.text = applicationManager.getPresentationUtility().getStringFromi18n("i18n.common.Download");
    }
    else{
      this.view.btnDownload.text = applicationManager.getPresentationUtility().getStringFromi18n("i18n.common.DownloadAll");
    }
    var attachmentsData = [];
    for (var i = 0; i < downloadAttachments.length; i++) {
      attachmentsData[i] = {};
      attachmentsData[i].filename = downloadAttachments[i];
      attachmentsData[i]["imgDownloadAttachment"] = {
        "src": "download.png"
      };
    }
    this.view.segDownloadAttachments.widgetDataMap = {
      "lblAttachment": "filename",
      "imgDownloadAttachment": "imgDownloadAttachment",
    }; 
    this.view.flxBack.onClick = this.navigateToTransferDetails;
    this.view.segDownloadAttachments.setData(attachmentsData);
    this.view.btnDownload.onClick = function() {
     if(transferData.fileNames.length>0){
          for (var i = 0; i < transferData.fileNames.length; i++) {
            var transMod = applicationManager.getModulesPresentationController(
              "TransferModule"
            );
            transMod.downloadAttachments(null, transferData, i);
            var downloadURL = navMan.getCustomInfo("downloadURL");
            kony.application.openURL(downloadURL);
          }
        }
    };
  },
  navigateToTransferDetails: function(){
    var ntf = new kony.mvc.Navigation("frmEuropeTransactionDetails");
    ntf.navigate();
  },
  downloadSingleFile: function(dataItem){
    var transMod = applicationManager.getModulesPresentationController(
      "TransferModule"
    );
    var navMan = applicationManager.getNavigationManager();
    var transferData = navMan.getCustomInfo("frmEuropeTransactionDetails");
    transMod.downloadAttachments(transferData, dataItem, 0);
    var downloadURL = navMan.getCustomInfo("downloadURL");
    kony.application.openURL(downloadURL);
  },
});