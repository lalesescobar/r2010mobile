//This is the entry point for automation. You can either:
//1.Require any one of the created test plans like this:
//require([/*<test plan file>*/]);

// or
//2.  require the test suites along with executing jasmine as below
//Nested require for test suites will ensure the order of test suite exectuion
/*require(["Test Suites/AccountsTestSuite"],function(){
 
 jasmine.getEnv().execute();
});*/

require(["Test Suites/Droid/RB/xyz"],function(){
 
 jasmine.getEnv().execute();
}); 

//Since this is file is to be manually edited, make sure to update 
//any changes (rename/delete) to the test suites/plans.
/*
var deviceInfo = kony.os.deviceInfo();
if(deviceInfo.name === "iPhone"){
  jasmine.DEFAULT_TIMEOUT_INTERVAL=60000;
  require(["Test Plans/POCIOS"]);
  //require(["Test Plans/TestPlanIOSRB"]);
  //require(["Test Plans/consolidatedTestPlanIOS"]);
}else {
  require(["Test Plans/TestPlanAndroidRB"]);
  //require(["Test Plans/consolidatedTestPlanAndroid"]);
}

*/


  /*var AllureReporter = require('jasmine-allure-reporter');
  jasmine.getEnv().addReporter(new AllureReporter({resultsDir: 'allure-results'}));
  */
