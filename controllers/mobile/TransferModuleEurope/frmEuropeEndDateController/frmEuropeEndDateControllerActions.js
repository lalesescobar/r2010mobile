define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Form_d0934201db79496280125fa38ad4c5de: function AS_Form_d0934201db79496280125fa38ad4c5de(eventobject) {
        var self = this;
        this.init();
    },
    AS_Form_e8cc0effa70c489e866accd46805a19c: function AS_Form_e8cc0effa70c489e866accd46805a19c(eventobject) {
        var self = this;
        this.preShow();
    },
    AS_BarButtonItem_bc399b2d1c12494090e1f28fa747fbea: function AS_BarButtonItem_bc399b2d1c12494090e1f28fa747fbea(eventobject) {
        var self = this;
        this.navigateCustomBack();
    },
    AS_BarButtonItem_c30e4a5904604c0fb688f936cea15648: function AS_BarButtonItem_c30e4a5904604c0fb688f936cea15648(eventobject) {
        var self = this;
        this.cancelOnClick();
    }
});