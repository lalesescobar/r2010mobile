it("Verify_ScheduledTransferHalfY_DateRangeAndNoOfOccurance_P2P", async function() {
  
  SearchInFromAndToScreen("P2P");
  EnterAmount();
  SelectFrequencyHalfYearly();
  SelectDateRangeAndConfirmTransfer("HalfY","P2P");
  
  SearchInFromAndToScreen("P2P");
  EnterAmount();
  SelectFrequencyHalfYearly();
  SelectNumberOfTransfersAndThenConfirmTransfer("HalfY","P2P");
  Logout();
},180000);