it("LoginAndWaitForDashboard", async function() {
	await kony.automation.playback.waitFor(["frmLogin","tbxUsername"]);
	kony.automation.textbox.enterText(["frmLogin","tbxUsername"],"dbpolbuser");
	kony.automation.textbox.enterText(["frmLogin","tbxPassword"],"Kony@1234");
	kony.automation.button.click(["frmLogin","btnLogIn"]);
	
	// Verifying Terms and conditions screen
	var frmTnC = await kony.automation.playback.waitFor(["frmTermsAndCondition","flxCheckBox"],20000);
	if(frmTnC){
		kony.automation.flexcontainer.click(["frmTermsAndCondition","flxCheckBox"]);
		await kony.automation.playback.waitFor(["frmTermsAndCondition","btnContinue"]);
		kony.automation.button.click(["frmTermsAndCondition","btnContinue"]);
    }
	await kony.automation.playback.waitFor(["frmDashboardAggregated","customHeader","flxBack"],5000);
},26000);