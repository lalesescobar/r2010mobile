/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
	var mappings = {
		"myRequestsWaiting": "myRequestsWaiting",
		"myRequestsRejected": "myRequestsRejected",
		"myRequestsApproved": "myRequestsApproved",
		"ACHFilesForMyApproval": "ACHFilesForMyApproval",
		"ACHTransactionsForMyApproval": "ACHTransactionsForMyApproval",
		"GeneralTransactionsForMyApproval": "GeneralTransactionsForMyApproval",
		"dbpErrCode": "dbpErrCode",
		"dbpErrMsg": "dbpErrMsg",
		"opstatus": "opstatus",
		"httpStatusCode": "httpStatusCode",
	};

	Object.freeze(mappings);

	var typings = {
		"myRequestsWaiting": "string",
		"myRequestsRejected": "string",
		"myRequestsApproved": "string",
		"ACHFilesForMyApproval": "string",
		"ACHTransactionsForMyApproval": "string",
		"GeneralTransactionsForMyApproval": "string",
		"dbpErrCode": "string",
		"dbpErrMsg": "string",
		"opstatus": "string",
		"httpStatusCode": "string",
	}

	Object.freeze(typings);

	var primaryKeys = [
					"myRequestsWaiting",
	];

	Object.freeze(primaryKeys);

	var config = {
		mappings: mappings,
		typings: typings,
		primaryKeys: primaryKeys,
		serviceName: "ApprovalRequestObjects",
		tableName: "Counts"
	};

	Object.freeze(config);

	return config;
})