define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Form_i2d5339139a54eacb28493d4c6207b9d: function AS_Form_i2d5339139a54eacb28493d4c6207b9d(eventobject) {
        var self = this;
        this.init();
    },
    AS_Form_ab96ab15230843e5ab459642f9fd8769: function AS_Form_ab96ab15230843e5ab459642f9fd8769(eventobject) {
        var self = this;
        this.preShow();
    },
    AS_BarButtonItem_jdd7652b7e524b248e9a33071a05c2f2: function AS_BarButtonItem_jdd7652b7e524b248e9a33071a05c2f2(eventobject) {
        var self = this;
        this.navigateCustomBack();
    },
    AS_BarButtonItem_da0b375e33d644299cdda9b9ebb9ba16: function AS_BarButtonItem_da0b375e33d644299cdda9b9ebb9ba16(eventobject) {
        var self = this;
        this.cancelOnClick();
    }
});