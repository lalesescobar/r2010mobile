define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Form_c0f1c4e78e804902a4933ea5a1bf78a4: function AS_Form_c0f1c4e78e804902a4933ea5a1bf78a4(eventobject) {
        var self = this;
        this.init();
    },
    AS_Form_d686085e54614600a624b86561801478: function AS_Form_d686085e54614600a624b86561801478(eventobject) {
        var self = this;
        this.preShow();
    },
    AS_Image_c69013e91ff748beb6a33671417ad73f: function AS_Image_c69013e91ff748beb6a33671417ad73f(eventobject, x, y) {
        var self = this;
        this.shareAcknowledgement();
    },
    AS_BarButtonItem_ib62de5b54df4e12aaa2189b41e9f013: function AS_BarButtonItem_ib62de5b54df4e12aaa2189b41e9f013(eventobject) {
        var self = this;
        this.shareAcknowledgement();
    }
});