define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_ee5fadf9ae0e4b5daddb7a761c1429f7: function AS_BarButtonItem_ee5fadf9ae0e4b5daddb7a761c1429f7(eventobject) {
        var self = this;
        this.navtoSelectProduct();
    },
    /** init defined for frmPersonalLoans **/
    AS_Form_b54b76d573b44f6e855fe28ae7f3d0f4: function AS_Form_b54b76d573b44f6e855fe28ae7f3d0f4(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmPersonalLoans **/
    AS_Form_bf6a339a426b4304b9d8c93ca6344aaa: function AS_Form_bf6a339a426b4304b9d8c93ca6344aaa(eventobject) {
        var self = this;
        this.preShow();
    },
    /** onClick defined for btnContinue **/
    AS_Button_ce0b678c86b84e429fce8453ad27ac00: function AS_Button_ce0b678c86b84e429fce8453ad27ac00(eventobject) {
        var self = this;
        var transModPresentationController = applicationManager.getModulesPresentationController("TransferModule");
        transModPresentationController.commonFunctionForNavigation("frmTransfers");
    },
    /** onClick defined for btnContinue **/
    AS_Button_ge4b2d7528a3480286d0639c3ac27250: function AS_Button_ge4b2d7528a3480286d0639c3ac27250(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmLoanAmount");
        ntf.navigate();
    }
});