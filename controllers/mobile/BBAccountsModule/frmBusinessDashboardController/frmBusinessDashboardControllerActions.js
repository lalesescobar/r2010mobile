define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_i635d71212d643ad85f71d29f652f620: function AS_FlexContainer_i635d71212d643ad85f71d29f652f620(eventobject) {
        var self = this;
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
        accountModule.presentationController.commonFunctionForNavigation("frmMessages");
    },
    AS_FlexContainer_c989d9a0b02c493b8c3167cb28e18b2f: function AS_FlexContainer_c989d9a0b02c493b8c3167cb28e18b2f(eventobject) {
        var self = this;
        var accountMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
        accountMod.presentationController.commonFunctionForNavigation("frmTransfers");
    },
    AS_Form_f9c1302f7d314729a5c40ecdc7bd5374: function AS_Form_f9c1302f7d314729a5c40ecdc7bd5374(eventobject) {
        var self = this;
        this.init();
    },
    AS_Form_e622ed9f48a44f8db528c7c156fb5ac0: function AS_Form_e622ed9f48a44f8db528c7c156fb5ac0(eventobject) {
        var self = this;
        this.postShow();
    },
    AS_Form_fb991be7614a448bb2fa98b9accb2b54: function AS_Form_fb991be7614a448bb2fa98b9accb2b54(eventobject) {
        var self = this;
        this.preshow();
    },
    AS_Image_a0b67342068145c2ae79335952217e33: function AS_Image_a0b67342068145c2ae79335952217e33(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 1);
    },
    AS_Image_cb36a002d75a462a935aba6f41e1461f: function AS_Image_cb36a002d75a462a935aba6f41e1461f(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 2);
    },
    AS_Image_g06484ce769c4778869d6628d95da715: function AS_Image_g06484ce769c4778869d6628d95da715(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 3);
    },
    AS_Image_d3ded4b6bcde43379a21d3bdf4e49885: function AS_Image_d3ded4b6bcde43379a21d3bdf4e49885(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 4);
    },
    AS_Image_d59f1bd99619453aa8679bcfe0b07eb5: function AS_Image_d59f1bd99619453aa8679bcfe0b07eb5(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 5);
    }
});