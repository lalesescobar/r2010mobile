define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Form_g55853d2f106496b879f3a316eb11337: function AS_Form_g55853d2f106496b879f3a316eb11337(eventobject) {
        var self = this;
        this.init();
    },
    AS_Form_c5da5dce6cd544b090b9b728dfab9778: function AS_Form_c5da5dce6cd544b090b9b728dfab9778(eventobject) {
        var self = this;
        this.postShow();
    },
    AS_Form_h8e4f0ec96c54cc0a06dcb34b0773117: function AS_Form_h8e4f0ec96c54cc0a06dcb34b0773117(eventobject) {
        var self = this;
        this.preShow();
    },
    AS_BarButtonItem_f923d1f5de7d463580c73e686b0c258c: function AS_BarButtonItem_f923d1f5de7d463580c73e686b0c258c(eventobject) {
        var self = this;
        this.navigateCustomBack();
    },
    AS_BarButtonItem_he0b389ce8514acd859603a3c5f56f90: function AS_BarButtonItem_he0b389ce8514acd859603a3c5f56f90(eventobject) {
        var self = this;
        this.cancelOnClick();
    }
});