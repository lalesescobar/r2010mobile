define({
  tabSelection:"",
  scope:this,
  initActions: function () {
    var scope=this;
    var currentFormObject = kony.application.getCurrentForm();
    var currentForm=currentFormObject.id;
    applicationManager.getPresentationFormUtility().initCommonActions(this, "CALLBACK", currentForm, scope.navigateCustomBack);
    this.view.segStopCheque.onRowClick=scope.navigateToTransactionDetails;
    this.view.flxSearchIcon.onClick=scope.navigateToSearch;
    this.view.flxAccountDetails.onClick=scope.navigateToAccounts;
    this.view.customHeader.flxBack.onTouchEnd = scope.navigateCustomBack;
    this.view.flxStopChequeLabel.onClick=scope.renderStopChequeView;
    this.view.flxChequeBooksLabel.onClick=scope.renderChequeBooksView;
    this.view.flxMyChequesLabel.onClick=scope.renderMyChequesView;
    this.view.segStopCheque.removeAll();
  },
  navigateCustomBack: function() {
    var accountMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
    accountMod.presentationController.showDashboard();
  },
  renderStopChequeView: function() {
    applicationManager.getPresentationUtility().showLoadingScreen();
    this.view.segStopCheque.removeAll();
    var presentation = applicationManager.getModulesPresentationController("ChequeManagementModule");
    this.tabSelection="StopCheque";
    presentation.fetchStopChequeRequestsForAccount();
    this.view.segStopCheque.selectedRowIndex = [0, 0];
    this.view.flxStopChequeLabel.skin = "sknFlx1a98ff";
    this.view.lblStopCheque.skin="sknLblffffffSSPReg26px";
    this.view.flxChequeBooksLabel.skin = "sknFlxffffff";
    this.view.lblChequeBooks.skin="sknlbl003E7536px";
    this.view.flxMyChequesLabel.skin = "sknFlxffffff";
    this.view.lblMyCheques.skin="sknlbl003E7536px";
    this.view.flxStopCheques.top="110dp";
    this.view.btnNewRequest.text = kony.i18n.getLocalizedString("kony.mb.chequemanagement.newstopchequerequest");
    this.view.btnNewRequest.onClick = function() {
        var presentationController = applicationManager.getModulesPresentationController("ChequeManagementModule");
        presentationController.commonFunctionForNavigation("frmAmount");
    };
    this.view.flxSearchInfo.isVisible = false;
    scope_ChequePresentationController.isReview="";
scope_ChequePresentationController.isInitial=true;
  },
  renderChequeBooksView: function() {
    this.tabSelection="ChequeBook";
    applicationManager.getPresentationUtility().showLoadingScreen();
    this.view.segStopCheque.removeAll();
    this.view.segStopCheque.selectedRowIndex = [0, 0];
    var presentation = applicationManager.getModulesPresentationController("ChequeManagementModule");
    presentation.fetchTransactionForAccount();
    this.view.flxStopChequeLabel.skin = "sknFlxffffff";
    this.view.lblStopCheque.skin="sknlbl003E7536px";
    this.view.flxChequeBooksLabel.skin = "sknFlx1a98ff";
    this.view.lblChequeBooks.skin="sknLblffffffSSPReg26px";
    this.view.flxMyChequesLabel.skin = "sknFlxffffff";
    this.view.lblMyCheques.skin="sknlbl003E7536px";
    this.view.flxStopCheques.top="110dp";
    this.view.btnNewRequest.text = kony.i18n.getLocalizedString("kony.mb.chequeManagement.newchequeBookRequest");
    this.view.btnNewRequest.onClick = function() {
      presentation.btnNewRequestOnClick();
    };
    this.view.flxSearchInfo.isVisible = false;
  },
  renderMyChequesView: function() {
    this.tabSelection="MyCheque";
    applicationManager.getPresentationUtility().showLoadingScreen();
    this.view.segStopCheque.removeAll();
    var presentation = applicationManager.getModulesPresentationController("ChequeManagementModule");
    presentation.getChequeSupplements();
    this.view.segStopCheque.selectedRowIndex = [0, 0];
    this.view.flxStopChequeLabel.skin = "sknFlxffffff";
    this.view.lblStopCheque.skin="sknlbl003E7536px";
    this.view.flxChequeBooksLabel.skin = "sknFlxffffff";
    this.view.lblChequeBooks.skin="sknlbl003E7536px";
    this.view.flxMyChequesLabel.skin = "sknFlx1a98ff";
    this.view.lblMyCheques.skin="sknLblffffffSSPReg26px";
    this.view.flxStopCheques.top="180dp";
    this.view.btnNewRequest.text = kony.i18n.getLocalizedString("kony.mb.chequemanagement.newstopchequerequest");
    this.view.btnNewRequest.onClick = function() {
        var presentationController = applicationManager.getModulesPresentationController("ChequeManagementModule");
        presentationController.commonFunctionForNavigation("frmAmount");
    };
    this.view.flxSearchInfo.isVisible = true;
  },
  preShow:function(){
    if (kony.os.deviceInfo().name === "iPhone") {
      this.view.flxHeader.isVisible = false;
      this.view.flxMenu.isVisible = true;
    } else {
      this.view.flxHeader.isVisible = true;
      this.view.flxMenu.isVisible = false;
    }
    this.view. flxSearchInfo.isVisible=false;
    this.setFooter();
    var configManager = applicationManager.getConfigurationManager();
    var MenuHandler =  applicationManager.getMenuHandler();
    MenuHandler.setUpHamburgerForForm(this,configManager.constants.MENUCHEQUEMANAGEMENT);
    var presentation = applicationManager.getModulesPresentationController("ChequeManagementModule");
    this.view.lblAccountDetails.text=presentation.processedName;
    var navMan=applicationManager.getNavigationManager();
    navMan.setEntryPoint("chequemanagement", "frmChequeManagement");
    this.renderStopChequeView();
  },
  postShow:function(){
    //applicationManager.getPresentationUtility().dismissLoadingScreen();
    var deviceManager = applicationManager.getDeviceUtilManager();
    deviceManager.detectDynamicInstrumentation();
  },
  navigateToReviewScreen:function(){
    var review = applicationManager.getModulesPresentationController("ChequeManagementModule");
    review.commonFunctionForNavigation("frmCMReview");
  },
  navigateToTransactionDetails:function(){
    var navMan=applicationManager.getNavigationManager();
    var data = this.view.segStopCheque.data[this.view.segStopCheque.selectedIndex[1]];
    navMan.setCustomInfo("frmChequeTransactionDetails",{"data":data,"tabSelection":this.tabSelection});
    navMan.navigateTo("frmChequeTransactionDetails");
  },
  navigateToSearch:function(){
    var search = applicationManager.getModulesPresentationController("ChequeManagementModule");
    search.commonFunctionForNavigation("frmCMSearch");
  },
  navigateToAccounts:function(){
    var presentation = applicationManager.getModulesPresentationController("ChequeManagementModule");
    presentation.navigateToAccountScreen();
    presentation.entryFormForAccounts="frmChequeManagement";
  },
  getWidgetDataMap:function(){
    return {
      "lblChequeNo":"booksCount",
      "lblDate":"lblDate",
      "lblStatus":"lblStatus",
      "flxDot":"image",
      "lblAccountNo":"lblAccountNo"
    };
  },
  bindTransactions:function(data){
    this.view.segStopCheque.widgetDataMap=this.getWidgetDataMap();
    this.view.segStopCheque.setData(data);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  },
  navigateBack:function(){
    var navMan=applicationManager.getNavigationManager();
    navMan.goBack();
  },
  setFooter:function(){
    this.view.customFooter.lblAccounts.skin = "sknLblA0A0A0SSP20px";
    this.view.customFooter.flxAccSelect.setVisibility(false);
    this.view.customFooter.lblTransfer.skin = "sknLblA0A0A0SSP20px";
    this.view.customFooter.flxTransferSel.setVisibility(false);
    this.view.customFooter.lblBillPay.skin = "sknLblA0A0A0SSP20px";
    this.view.customFooter.flxBillSelected.setVisibility(false);
    this.view.customFooter.lblMore.skin = "sknLbl424242SSP20px";
    this.view.customFooter.flxMoreSelect.setVisibility(true);
  },
});