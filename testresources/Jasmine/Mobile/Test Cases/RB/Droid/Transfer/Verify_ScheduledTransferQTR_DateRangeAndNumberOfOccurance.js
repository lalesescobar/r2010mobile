it("Verify_ScheduledTransferQTR_DateRangeAndNumberOfOccurance", async function() {
	SearchInFromAndToScreen("normal");
	EnterAmount();
	SelectFrequencyQuarterly();
	SelectDateRangeAndConfirmTransfer("QTR","normal");
  
	SearchInFromAndToScreen("normal");
	EnterAmount();
	SelectFrequencyQuarterly();
	SelectNumberOfTransfersAndThenConfirmTransfer("QTR","normal");
  Logout();
},180000);