it("Logout", async function() {
	kony.automation.flexcontainer.click(["frmDashboardAggregated","customFooter","flxMore"]);
	await kony.automation.playback.waitFor(["frmDashboardAggregated","Hamburger","flxLogout"]);
	kony.automation.widget.touch(["frmDashboardAggregated","Hamburger","flxLogout"], null,null,[50,31]);
	await kony.automation.playback.wait(3000);
	await kony.automation.playback.waitFor(["frmLogout","btnLogIn"]);
	kony.automation.button.click(["frmLogout","btnLogIn"]);
	await kony.automation.playback.waitFor(["frmLogin","tbxUsername"]);
});