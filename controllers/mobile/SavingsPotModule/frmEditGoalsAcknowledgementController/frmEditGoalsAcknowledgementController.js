define({

    init: function() {},

    preShow: function() {
        if (kony.os.deviceInfo().name === "iPhone") {
            this.view.flxHeader.isVisible = false;
        }

        this.initActions();

    },
    initActions: function() {
        this.view.btnFund.onClick = this.continueOnClick;
        this.setDataToForm();
    },
    setDataToForm: function() {
        var navManager = applicationManager.getNavigationManager();
        var goalDetails = navManager.getCustomInfo("frmSavingsGoalViewDetails");
        var formatUtil = applicationManager.getFormatUtilManager();
        var SavingsPotMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SavingsPotModule");
        var fromDetails = SavingsPotMod.presentationController.getMaskedAccountName();
        this.view.lblGoalAn.text = goalDetails.potName;
        this.view.lblAmountAn.text = formatUtil.formatAmountandAppendCurrencySymbol(goalDetails.targetAmount, goalDetails.currency);
        this.view.lblCurrentbalanceValue.text = formatUtil.formatAmountandAppendCurrencySymbol(goalDetails.availableBalance, goalDetails.currency);
        this.view.lblContributionAn.text = formatUtil.formatAmountandAppendCurrencySymbol(goalDetails.periodicContribution, goalDetails.currency);
        this.view.lblPeriodAn.text = goalDetails.targetPeriod;
        this.view.lblDayAn.text = goalDetails.frequencyDay;
        this.view.lblRemainingbalanceValue.text = goalDetails.remainingSavings;
        this.view.lblAccAn.text = fromDetails;
        var today = new Date();
        var month = today.getMonth() + 1;
        var date = today.getDate();
        if (month < 10) {
            month = "0" + month;
        }
        if (date < 10) {
            date = "0" + date;
        }
        var createdDate = month + "/" + date + "/" + today.getFullYear();
        this.view.lblModifiedvalue.text = createdDate;
        applicationManager.getPresentationUtility().dismissLoadingScreen();
    },
    postShow: function() {

    },
    continueOnClick: function() {
        var SavingsPotMod = applicationManager.getModulesPresentationController("SavingsPotModule");
        SavingsPotMod.setSavingsFlow("");
        SavingsPotMod.clearCreateData();
        var accountsID = SavingsPotMod.getAccountId();
        SavingsPotMod.getAllSavingsPot(accountsID);
    }

});