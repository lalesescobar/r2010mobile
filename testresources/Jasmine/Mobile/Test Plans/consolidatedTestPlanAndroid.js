require(["Test Suites/ManageRecipient_Add_DeleteSamebankRecipient_Suite"], function() {
	require(["Test Suites/TransfersOwnAccount"], function() {
		require(["Test Suites/TransfersP2P"], function() {
			require(["Test Suites/SettingsSuite"], function() {
				require(["Test Suites/MyBills"], function() {
										jasmine.getEnv().execute();
				});
			});
		});
	});
});