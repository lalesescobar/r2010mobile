define({
  init: function () {
    var scope=this;
    var currentFormObject = kony.application.getCurrentForm();
    var currentForm=currentFormObject.id;
    applicationManager.getPresentationFormUtility().initCommonActions(this, "CALLBACK", currentForm, scope.navigateCustomBack);
  },
  navigateCustomBack: function() {
    var accountMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
    accountMod.presentationController.showDashboard();
  },
  preShow: function () {
    if (kony.os.deviceInfo().name === "iPhone") {
      this.view.flxHeader.isVisible = false;
    }
    this.initActions();
    this.setupUI();
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
  },
  initActions: function () {
    var scope = this;
    var transferModule = applicationManager.getModulesPresentationController("TransferModule");
    //this.view.customHeader.btnRight.onClick = this.cancelOnClick;
    scope.view.customHeader.flxSearch.onClick = scope.enableShare;
    scope.view.btnDashboard.onClick = function (){
      transferModule.haveLimitsBeenFetched = false;
      transferModule.clearEuropeFlowAtributes();
      var accountMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
      accountMod.presentationController.showDashboard();
      
    };
    scope.view.btnNewTransfer.onClick = function () {
      var navMan = applicationManager.getNavigationManager();
      //navMan.setEntryPoint("ManageMMFlow","frmMMTransferFromAccount");
      navMan.setEntryPoint("europeTransferFlow","frmDashboardAggregated");
      navMan.setEntryPoint("startFromFlow","frmEuropeTransferFromAccount");
      transferModule.clearEuropeFlowAtributes();
      applicationManager.getPresentationUtility().showLoadingScreen();
      transferModule.getFromAccounts();
    };
    scope.view.btnTryAgain.onClick=function(){
      var navMan = applicationManager.getNavigationManager();
      //navMan.setEntryPoint("ManageMMFlow","frmMMTransferFromAccount");
      navMan.setEntryPoint("europeTransferFlow","frmDashboardAggregated");
      navMan.setEntryPoint("startFromFlow","frmEuropeTransferFromAccount");
      transferModule.clearEuropeFlowAtributes();
      applicationManager.getPresentationUtility().showLoadingScreen();
      transferModule.getFromAccounts();
    };
    scope.view.flxFurtherDetailsHeader.onTouchEnd = function() {
      if (scope.view.flxFurtherDetails.isVisible === false) {
        scope.view.imgUpArrow.src = "arrowup.png";
        scope.view.flxFurtherDetails.isVisible = true;
      }
      else {
        scope.view.imgUpArrow.src = "arrowdown.png";
        scope.view.flxFurtherDetails.isVisible = false;
      }
    };    
  },
  setupUI : function () {
    var transactionManager = applicationManager.getTransactionManager();
    var transferPresentationController = applicationManager.getModulesPresentationController("TransferModule");
    var transferObject = transactionManager.getTransactionObject();
    if (!kony.sdk.isNullOrUndefined(transferObject.errmsg)){
      this.view.flxConfirmationMain.isVisible = false;
      this.view.flxError.isVisible = true;
      this.view.flxButtons.isVisible = false;
      this.view.lblTitle.text = transferObject.errmsg;
      if (kony.os.deviceInfo().name === "iPhone") {
        var disableButtonConfig = {"animated":false,"items":[]};
		this.view.setRightBarButtonItems(disableButtonConfig);
      }
      else {
      	this.view.customHeader.flxSearch.isVisible = false;
      }  
    }
    else {
      if (transferObject.isScheduled === "0")
        this.view.lblSuccessMessage.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.TransfersEurope.successfulTransfer");
      else
        this.view.lblSuccessMessage.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.MM.TransferScheduled");
      this.view.flxConfirmationMain.isVisible = true;
      this.view.flxError.isVisible = false;
      this.view.flxButtons.isVisible = true;
      if (transferPresentationController.getEuropeFlowType() === "EXTERNAL") {
        this.setSavedRecipientLabel(transferObject);
        this.view.flxFurtherDetailsHeader.isVisible = true;
        this.view.flxFurtherDetails.isVisible = true;
      }
      else {
        if (transferPresentationController.isLoansAccountType) {
          this.view.lblSavedRecipient.isVisible = true;
          this.view.lblSavedRecipient.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.loans.AckMessage");
          this.view.flxFurtherDetailsHeader.isVisible = false;
          this.view.flxFurtherDetails.isVisible = false;
        }
        else {
        	this.view.lblSavedRecipient.isVisible = false;
        	this.view.flxFurtherDetailsHeader.isVisible = false;
        	this.view.flxFurtherDetails.isVisible = false;
        }  
      }
      this.setSegmentData();
    }
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  },
  
  setSavedRecipientLabel : function(transferObject) {
    if(transferObject.isRecipientAdded) {
      this.view.lblSavedRecipient.isVisible = true;
      this.view.lblSavedRecipient.text = transferObject.addedRecipientDetails.beneficiaryName + "\n" + applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Europe.AddedRecipientMessage");
    } 
    else {
      this.view.lblSavedRecipient.isVisible = false;	  
    }
  },
  
  setSegmentData : function () {
    var transferPresentationController = applicationManager.getModulesPresentationController("TransferModule");
    var segData = transferPresentationController.getConfirmationScreenData();
    this.view.segDetails.widgetDataMap = this.getWidgetDataMap();
    this.view.segDetails.setData(segData);
    if (transferPresentationController.getEuropeFlowType() === "EXTERNAL") {
      var extraData = transferPresentationController.getFurtherDetailsData();
      this.view.segFurtherDetails.widgetDataMap = this.getWidgetDataMap();
      this.view.segFurtherDetails.setData(extraData);
      if (scope_TransfersPresentationController.transactionMode !== applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.transfer.OtherKonyBankMembers")) {
        var chargesData = transferPresentationController.getChargesBreakdown();
        this.view.segChargesBreakdown.widgetDataMap = this.getWidgetDataMap();
        this.view.segChargesBreakdown.setData(chargesData);
      }
      else {
        this.view.flxChargesBreakdown.isVisible = false;
      }
    }
  },
  getWidgetDataMap : function () {
    var map = {
      lblTitle:"property",
      lblDetails:"value"
    }
    return map;
  },
  

  showSharePopup : function (base64String, transactionID) {
    var fileName = transactionID + "Report.pdf";
	  this.view.socialshare.shareWithBase64(base64String, fileName);
  },

  bindGenericError: function (errorMsg) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var scopeObj = this;
    applicationManager.getDataProcessorUtility().showToastMessageError(scopeObj, errorMsg);
  },

  enableShare: function() {
    var transferModule = applicationManager.getModulesPresentationController("TransferModule");
    transferModule.enableShare();
  }
//   cancelOnClick : function () {
//     var transferModule = applicationManager.getModulesPresentationController("TransferModule");
//     transferModule.cancelCommon();
//   },
});