define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnStatements **/
    AS_Button_a8ae3be830d345b389929cbf8da4ccec: function AS_Button_a8ae3be830d345b389929cbf8da4ccec(eventobject) {
        var self = this;
        this.getStatements();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_d5f57b37ce714b9c887134435b7f515d: function AS_BarButtonItem_d5f57b37ce714b9c887134435b7f515d(eventobject) {
        var self = this;
        this.gotoAccountInfo();
    },
    /** init defined for frmMySavingsPot **/
    AS_Form_cb67feab8f6d40d58545aa9583690a5e: function AS_Form_cb67feab8f6d40d58545aa9583690a5e(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmMySavingsPot **/
    AS_Form_de54c2cd6304459b9e2af82bc8ce1a10: function AS_Form_de54c2cd6304459b9e2af82bc8ce1a10(eventobject) {
        var self = this;
        this.preShow();
    }
});