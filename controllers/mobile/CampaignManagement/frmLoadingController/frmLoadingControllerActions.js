define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmLoading **/
    AS_Form_b91ff9cbc8e14faf84262de27178e54d: function AS_Form_b91ff9cbc8e14faf84262de27178e54d(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmLoading **/
    AS_Form_dff55bc8afdd48308a1c05a68d702c26: function AS_Form_dff55bc8afdd48308a1c05a68d702c26(eventobject) {
        var self = this;
        return self.preShow.call(this);
    },
    /** onDownloadComplete defined for imgAd1 **/
    AS_Image_cf646e6a5d854696a0553cf511009ce3: function AS_Image_cf646e6a5d854696a0553cf511009ce3(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 1);
    },
    /** onDownloadComplete defined for imgAd2 **/
    AS_Image_f703b22776984654b8b817b076771b35: function AS_Image_f703b22776984654b8b817b076771b35(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 2);
    },
    /** onDownloadComplete defined for imgAd3 **/
    AS_Image_e7fc91a9286d4be1b4135772f47cdef4: function AS_Image_e7fc91a9286d4be1b4135772f47cdef4(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 3);
    },
    /** onDownloadComplete defined for imgAd4 **/
    AS_Image_ae6b8d9762a649d0b83456596794c610: function AS_Image_ae6b8d9762a649d0b83456596794c610(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 4);
    },
    /** onDownloadComplete defined for imgAd5 **/
    AS_Image_f591ede3a39743ca902cea29c44733d6: function AS_Image_f591ede3a39743ca902cea29c44733d6(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 5);
    }
});