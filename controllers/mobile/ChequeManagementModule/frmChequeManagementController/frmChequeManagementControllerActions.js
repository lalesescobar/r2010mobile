define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Form_h6120a17ea924617bb1b40deb9ee8538: function AS_Form_h6120a17ea924617bb1b40deb9ee8538(eventobject) {
        var self = this;
        this.initActions();
    },
    AS_Form_j15ed94953934b9ba5fe050d9782a242: function AS_Form_j15ed94953934b9ba5fe050d9782a242(eventobject) {
        var self = this;
        this.postShow();
    },
    AS_Form_e16f80b335704f548f75afe9a7ec1c35: function AS_Form_e16f80b335704f548f75afe9a7ec1c35(eventobject) {
        var self = this;
        this.preShow();
    },
    AS_BarButtonItem_e954fd09878f4da08fb7de4319c6b500: function AS_BarButtonItem_e954fd09878f4da08fb7de4319c6b500(eventobject) {
        var self = this;
        this.navigateCustomBack();
    }
});