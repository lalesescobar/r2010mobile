define(function(){

  return {
    formInfo:[],
    previousFormType:"",
    approvalOrReq : "",
    ACHModule:"",
    ApprovalModule:"",
    detailsData:"",
    timerCounter:0,


    init:function(){
      try{
        var navManager = applicationManager.getNavigationManager();
        var currentForm = navManager.getCurrentForm();
        applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
        this.view.preShow = this.preShowActions;
        this.view.postShow = this.postShowAction;
      }catch(er){
      }
    },
    onNavigate:function() {
      try { 
      }catch(error){
        kony.print("frmACHTransactionsdetails onnavigateerror-->"+error);
      }
    },
    postShowAction:function(){
      try{
        this.setupNavBarSkinForiPhone();
      }catch(er){

      }
    },
    preShowActions:function()
    {
      try { 
        // this.setACHTransactionalDetails();
        if(applicationManager.getPresentationFormUtility().getDeviceName()==="iPhone"){
          this.view.flxHeader.isVisible = false;
        }else{
          this.view.flxHeader.isVisible = true;
        }
        this.view.segDestinationaccount.removeAll();
        this.view.segApprovalHistory.removeAll();
        this.view.segDatalist.removeAll();
        this.ACHModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule');
        this.ApprovalModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ApprovalsReqModule');
        var navManager = applicationManager.getNavigationManager();
        var formFlow = navManager.getCustomInfo("formFlow");
        this.previousFormType=formFlow;     
        this.bindevents();
        if(formFlow === "ACHFileList"){
          this.detailsData = navManager.getCustomInfo("ACHFileDetails");
          this.loadAchDetails(this.detailsData);	
          this.view.flxTransfer.isVisible = false;
          this.view.flxdesinationaccount.isVisible = false;
          this.view.segDestinationaccount.isVisible = false;
          this.view.flxapprovalhistory.isVisible = true;
          this.view.lblheaderapproval.text=kony.i18n.getLocalizedString("kony.mb.achtransationdetail.ApprovalHistoryInformation");
          this.getApprovalReqHistory({"Request_id":this.detailsData[0].Request_id});
          this.btnConfigApprovalDetails(formFlow,this.detailsData[0].amICreator, this.detailsData[0].amIApprover,this.detailsData[0].lblStatus);
        }else if(formFlow === "ACHTransactionsList"){
          this.detailsData = navManager.getCustomInfo("ACHTranactionDetails");
          this.loadTransactionDetails(this.detailsData);
          this.view.lblTransfer.text=kony.i18n.getLocalizedString("kony.mb.Europe.TotalAmount");
          this.view.lblTransferValue.text=this.detailsData[0].lblAmount;
          this.fetchDestinationAccounts({"Transaction_id": this.detailsData[0].Transaction_id});
          this.view.flxTransfer.isVisible = true;
          this.view.flxdesinationaccount.isVisible = true;
          this.view.segDestinationaccount.isVisible = true;
          this.view.flxapprovalhistory.isVisible = true;
          this.view.lblheaderapproval.text=kony.i18n.getLocalizedString("kony.mb.achtransationdetail.ApprovalHistoryInformation");
          this.getApprovalReqHistory({"Request_id":this.detailsData[0].Request_id});
          this.btnConfigApprovalDetails(formFlow,this.detailsData[0].amICreator, this.detailsData[0].amIApprover,this.detailsData[0].Status);        
        }else if(formFlow === "TransactionDetailsApprovals"){  
          kony.print("formFlow ::"+formFlow);
          this.detailsData = navManager.getCustomInfo("generalTransactionDetails");
          this.loadTransactionDetailsApprovals(this.detailsData);
          this.view.lblTransfer.text = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.TransferAmount");
          this.view.lblTransferValue.text=this.detailsData[0].lblTransactionAmountAP.text;
          this.view.flxTransfer.isVisible = true;
          this.view.flxdesinationaccount.isVisible = false;
          this.view.segDestinationaccount.isVisible = false;
          this.view.flxapprovalhistory.isVisible = false;
          this.btnConfigApprovalDetailsApprovalReq(formFlow,this.detailsData[0].amICreator, this.detailsData[0].amIApprover,this.detailsData[0].status);        
        }else if(formFlow ==="ACHTransactionDetailsApprovals"){
          this.detailsData = navManager.getCustomInfo("ACHTranactionDetails");
          kony.print("test"+JSON.stringify(this.detailsData[0]));
          this.loadAchTransactionDetailsApprovals(this.detailsData);
          this.view.lblTransfer.text=kony.i18n.getLocalizedString("kony.mb.Europe.TotalAmount");
          this.view.lblTransferValue.text=this.detailsData[0].lblTransactionAmountAP.text;
          this.fetchDestinationAccounts({"Transaction_id": this.detailsData[0].data.Transaction_id});
          this.view.flxTransfer.isVisible = true;
          this.view.flxdesinationaccount.isVisible = true;
          this.view.segDestinationaccount.isVisible = true;
          this.view.flxapprovalhistory.isVisible = true;
          this.view.lblheaderapproval.text=kony.i18n.getLocalizedString("kony.mb.achtransationdetail.ApprovalHistoryInformation");
          this.getApprovalReqHistory({"Request_id":this.detailsData[0].request_id});
          this.btnConfigApprovalDetailsApprovalReq(formFlow,this.detailsData[0].data.amICreator, this.detailsData[0].data.amIApprover,this.detailsData[0].data.Status);        
        }else if(formFlow === "ACHFileListApprovals"){
          this.detailsData = navManager.getCustomInfo("ACHFileDetails");
          this.loadAchFileDetailsApprovals(this.detailsData);
          this.view.flxTransfer.isVisible = false;
          this.view.flxdesinationaccount.isVisible = false;
          this.view.segDestinationaccount.isVisible = false;
          this.view.flxapprovalhistory.isVisible = true;
          this.view.lblheaderapproval.text=kony.i18n.getLocalizedString("kony.mb.achtransationdetail.ApprovalHistoryInformation");
          this.getApprovalReqHistory({"Request_id":this.detailsData[0].data.Request_id});
          this.btnConfigApprovalDetailsApprovalReq(formFlow,this.detailsData[0].data.amICreator, this.detailsData[0].data.amIApprover,this.detailsData[0].data.FileStatus);
        }else if(formFlow === "TransactionDetailsRequests"){  
          kony.print("formFlow ::"+formFlow);
          this.detailsData = navManager.getCustomInfo("generalTransactionDetails");
          this.loadTransactionDetailsRequest(this.detailsData);  
          this.view.lblTransfer.text = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.TransferAmount");
          this.view.lblTransferValue.text=this.detailsData[0].lblTransactionAmount.text;
          this.view.flxTransfer.isVisible = true;
          this.view.flxdesinationaccount.isVisible = false;
          this.view.segDestinationaccount.isVisible = false;
          this.view.flxapprovalhistory.isVisible = false;
          this.btnConfigApprovalDetailsRequest(formFlow,this.detailsData[0].amICreator, this.detailsData[0].amIApprover,this.detailsData[0].status);        
        }else if(formFlow ==="ACHTransactionDetailsRequests"){
          this.detailsData = navManager.getCustomInfo("ACHTranactionDetails");
          kony.print("test"+JSON.stringify(this.detailsData[0]));
          this.loadAchTransactionDetailsRequest(this.detailsData);
          this.view.lblTransfer.text=kony.i18n.getLocalizedString("kony.mb.Europe.TotalAmount");
          this.view.lblTransferValue.text=this.detailsData[0].lblTransactionAmount.text;
          this.fetchDestinationAccounts({"Transaction_id": this.detailsData[0].data.Transaction_id});
          this.view.flxTransfer.isVisible = true;
          this.view.flxdesinationaccount.isVisible = true;
          this.view.segDestinationaccount.isVisible = true;
          this.view.flxapprovalhistory.isVisible = true;
          this.view.lblheaderapproval.text=kony.i18n.getLocalizedString("kony.mb.achtransationdetail.requestedHistoryStatus");
          this.getApprovalReqHistory({"Request_id":this.detailsData[0].request_id});
          this.btnConfigApprovalDetailsRequest(formFlow,this.detailsData[0].data.amICreator, this.detailsData[0].data.amIApprover,this.detailsData[0].data.Status);        
        }else if(formFlow === "ACHFileListRequests"){
          this.detailsData = navManager.getCustomInfo("ACHFileDetails");
          this.loadAchFileDetailsRequest(this.detailsData);
          this.view.flxTransfer.isVisible = false;
          this.view.flxdesinationaccount.isVisible = false;
          this.view.segDestinationaccount.isVisible = false;
          this.view.flxapprovalhistory.isVisible = true;
          this.view.lblheaderapproval.text=kony.i18n.getLocalizedString("kony.mb.achtransationdetail.requestedHistoryStatus");
          this.getApprovalReqHistory({"Request_id":this.detailsData[0].data.Request_id});
          this.btnConfigApprovalDetailsRequest(formFlow,this.detailsData[0].data.amICreator, this.detailsData[0].data.amIApprover,this.detailsData[0].data.FileStatus);
        }
        this.view.flxConfirmationPopUp.isVisible = false;
        this.setupNavBarSkinForiPhone();
      }catch(error){
        kony.print("frmACHTransactionsdetails preShowActions-->"+error);
        kony.print(error);
      }
    },

    setupNavBarSkinForiPhone : function () {
      if (applicationManager.getPresentationFormUtility().getDeviceName() !== "iPhone") return;
      try{
        var titleBarAttributes = this.view.titleBarAttributes;
        titleBarAttributes["tintColor"] = "003e7500";
        titleBarAttributes["translucent"] = false;
        this.view.titleBarAttributes = titleBarAttributes;
      }
      catch(er){
      }
    },

    bindevents:function()
    {
      try {  
        this.view.segDestinationaccount.isVisible=true;
        this.view.flxRejectpopup.isVisible=false;
        this.view.imgUpArrow.src="blue_uparrow.png";
        this.view.imgapprovalhis.src="blue_uparrow.png";
        this.view.flximgDesniationacccount.onClick=this.showDesinationAccount;
        this.view.flximgApprovalhistory.onClick=this.showApprovalHistory;
        this.view.btnReject.onClick=this.rejectDetails;
        this.view.flxRejectpopup.onClick=this.dummyFunction;
        this.view.rejectPopUp.txtRejectreason.onTextChange=this.rejectEnabledButton;
        this.view.rejectPopUp.flxReject.onClick = this.onClickOkcommon;
        this.view.btnApprove.onClick = this.approveServicecall;
        this.view.btnWithdraw.onClick = this.confirmWithdrawalPopup;
        this.view.customHeader.flxBack.onClick = this.backNavigation;
        this.view.rejectPopUp.flxCancel.onClick = this.rejectCancel;
        this.view.flxConfirmationPopUp.onClick = this.closePopup;
        this.view.confirmationAlertPopup.onClickflxNo = this.closePopup;
        this.view.confirmationAlertPopup.onClickflxYes = this.withdrawHandler;
        this.view.rejectPopUp.flxContainer.onClick = this.dummyFunction;
        this.view.onDeviceBack=this.dummyFunction;
        if(this.previousFormType==="ACHTransactionsList"){
          this.view.flxTransfer.isVisible = true;
          this.view.lblTransfer.text = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.TransferAmount");
          this.view.lblTransferValue.text = "$15,00,000.54";
          this.view.flxdetails.isVisible = true;
          if(applicationManager.getPresentationFormUtility().getDeviceName()==="iPhone"){
            this.view.title = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.TransactionDetails");
          }else{
            this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.TransactionDetails");
          }

        }else if(this.previousFormType==="ACHFileList"){
          this.view.flxTransfer.isVisible = false;
          this.view.flxdetails.isVisible = true;
          if(applicationManager.getPresentationFormUtility().getDeviceName()==="iPhone"){
            this.view.title = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.FileDetails");
          }else{
            this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.FileDetails");
          }
          if(this.previousFormType==="ACHTransactionDetailsApprovals"){ 
            this.view.flxdetails.isVisible = true;
            this.view.flxTransfer.isVisible = true;
            this.view.lblTransfer.text = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.TotalAmount");
            this.view.lblTransferValue.text = "$15,00,000.54";
            this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.ACHTransactionDetails");
          }
        }else if(this.previousFormType==="TransactionDetailsApprovals"){
          this.view.flxTransfer.isVisible = true;
          this.view.flxdetails.isVisible = true;
          if(applicationManager.getPresentationFormUtility().getDeviceName()==="iPhone"){
            this.view.title = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.TransactionDetails");
          }else{
            this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.TransactionDetails");
          }
        }else if(this.previousFormType==="ACHTransactionDetailsApprovals"){
          this.view.flxTransfer.isVisible = true;
          this.view.flxdetails.isVisible = true;
          if(applicationManager.getPresentationFormUtility().getDeviceName()==="iPhone"){
            this.view.title = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail. ACHTransactionDetails");
          }else{
            this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail. ACHTransactionDetails");
          }
        }else if(this.previousFormType==="ACHFileListApprovals"){
          this.view.flxTransfer.isVisible = true;
          this.view.flxdetails.isVisible = true;
          if(applicationManager.getPresentationFormUtility().getDeviceName()==="iPhone"){
            this.view.title = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.ACHfileDetails");
          }else{
            this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.ACHfileDetails");
          }
        }else if(this.previousFormType==="TransactionDetailsRequests"){
          this.view.flxTransfer.isVisible = true;
          this.view.flxdetails.isVisible = true;
          if(applicationManager.getPresentationFormUtility().getDeviceName()==="iPhone"){
            this.view.title = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.TransactionDetails");
          }else{
            this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.TransactionDetails");
          }
        }else if(this.previousFormType==="ACHTransactionDetailsRequests"){
          this.view.flxTransfer.isVisible = true;
          this.view.flxdetails.isVisible = true;
          if(applicationManager.getPresentationFormUtility().getDeviceName()==="iPhone"){
            this.view.title = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail. ACHTransactionDetails");
          }else{
            this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail. ACHTransactionDetails");
          }
        }else if(this.previousFormType==="ACHFileListRequests"){
          this.view.flxTransfer.isVisible = true;
          this.view.flxdetails.isVisible = true;
          if(applicationManager.getPresentationFormUtility().getDeviceName()==="iPhone"){
            this.view.title = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.ACHfileDetails");
          }else{
            this.view.customHeader.lblLocateUs.text = kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.ACHfileDetails");
          }
        }
      }catch(error){
        kony.print("frmACHTransactionsdetails bindevents-->"+error);
      }       
    },

    backNavigation:function(){
      try{
        var navMan=applicationManager.getNavigationManager();
        navMan.goBack();
      }catch(er){

      }
    },
    btnConfigApprovalDetailsApprovalReq:function(formFlow,amICreator,amIApprover,status){
      try{
        kony.print("Entered in btnConfigApprovalDetailsApprovalReq"+formFlow +" "+amICreator+" "+amIApprover+" "+status);
        if(status === "Executed" || status === "executed" || status === "Rejected" || status === "rejected" || status.toLowerCase() == "approved"){
          this.view.flxbtnApproveReject.isVisible = false;
          this.view.flxbtnWithdraw.isVisible = false;
          return;
        }
        switch(formFlow){
          case "TransactionDetailsApprovals" :
            kony.print("swich case TransactionDetailsApprovals");
            if(status.toLowerCase() === "pending"){
              if(amIApprover === "true" && amICreator ==="false"){
                this.view.flxbtnApproveReject.isVisible = true;
                this.view.flxbtnWithdraw.isVisible = false;
              }else if(amIApprover === "false" && amICreator === "true"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = false;
              }else if (amIApprover === "true" && amICreator === "true"){
                this.view.flxbtnApproveReject.isVisible = true;
                this.view.flxbtnWithdraw.isVisible = false;
              }else if(amIApprover === "false" && amICreator === "false"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = false;
              }
            }else{
              this.view.flxbtnApproveReject.isVisible = false;
              this.view.flxbtnWithdraw.isVisible = false;
            }
            break;
          case "ACHTransactionDetailsApprovals":
            if(status.toLowerCase() === "pending"){
              if(amIApprover === "true" && amICreator ==="false"){
                this.view.flxbtnApproveReject.isVisible = true;
                this.view.flxbtnWithdraw.isVisible = false;
              }else if(amIApprover === "false" && amICreator === "true"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = false;
              }else if (amIApprover === "true" && amICreator === "true"){
                this.view.flxbtnApproveReject.isVisible = true;
                this.view.flxbtnWithdraw.isVisible = false;
              }else if(amIApprover === "false" && amICreator === "false"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = false;
              }
            }else{
              this.view.flxbtnApproveReject.isVisible = false;
              this.view.flxbtnWithdraw.isVisible = false;
            }
            break;
          case "ACHFileListApprovals":
            if(status.toLowerCase() === "pending"){
              if(amIApprover === "true" && amICreator ==="false"){
                this.view.flxbtnApproveReject.isVisible = true;
                this.view.flxbtnWithdraw.isVisible = false;
              }else if(amIApprover === "false" && amICreator === "true"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = false;
              }else if (amIApprover === "true" && amICreator === "true"){
                this.view.flxbtnApproveReject.isVisible = true;
                this.view.flxbtnWithdraw.isVisible = false;
              }else if(amIApprover === "false" && amICreator === "false"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = false;
              }
            }else{
              this.view.flxbtnApproveReject.isVisible = false;
              this.view.flxbtnWithdraw.isVisible = false;
            }
            break;
        }
      }catch(er){
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        kony.print("Exception in btnConfigApprovalDetailsApprovalReq"+er);
      }
    },
    loadTransactionDetailsApprovals:function(detailsData){
      try{
        kony.print("Entered loadTransactionDetailsApprovals"+JSON.stringify(detailsData[0]));
        var detailsArr=[
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.Payee"),
            "lblColon":":",
            "lblValue":detailsData[0].lblTransactionAP,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.debitAccountNo"),
            "lblColon":":",
            "lblValue":detailsData[0].debitAccount,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.transaction.transactionType"),
            "lblColon":":",
            "lblValue":detailsData[0].data.featureName,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.cardLess.transactionId"),
            "lblColon":":",
            "lblValue":detailsData[0].transaction_id,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.transaction.transactionDate"),
            "lblColon":":",
            "lblValue":detailsData[0].lblDateAP,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.createdBy"),
            "lblColon":":",
            "lblValue":detailsData[0].data.userName,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.transaction.frequency"),
            "lblColon":":",
            "lblValue":detailsData[0].frequency,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.transaction.recurrence"),
            "lblColon":":",
            "lblValue":detailsData[0].reccurence,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
        ];
        var jsonData={};
        if(this.detailsData[0].data.Status === "pending" || this.detailsData[0].data.Status === "Pending"){
          jsonData={
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
            "lblColon":"",
            "lblValue":this.detailsData[0].data.receivedApprovals +" "+"of"+" "+this.detailsData[0].data.requiredApprovals+" "+"Approved",
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
            "flxSeperatorTrans3":{isVisible: false},
          };
          detailsArr.push(jsonData);                  
        }else if(this.detailsData[0].data.Status === "approved" || this.detailsData[0].data.Status === "Approved"){
          jsonData={
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
            "lblColon":"",
            "lblValue":this.detailsData[0].data.receivedApprovals +" "+"of"+" "+this.detailsData[0].data.requiredApprovals+" "+"Approved",
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
            "flxSeperatorTrans3":{isVisible: false},
          };
          detailsArr.push(jsonData);                  
        }
        else if(this.detailsData[0].data.Status === "rejected" || this.detailsData[0].data.Status === "Rejected"){
          jsonData={
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
            "lblColon":"",
            "lblValue":1+" "+kony.i18n.getLocalizedString("kony.mb.achtransactions.Rejection"),
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
            "flxSeperatorTrans3":{isVisible: false},
          };
          detailsArr.push(jsonData); 
        }
        else{
          jsonData={
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
            "lblColon":"",
            "lblValue":this.detailsData[0].data.requiredApprovals+" "+kony.i18n.getLocalizedString("kony.mb.approvalRequest.approvalstatus"),
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
            "flxSeperatorTrans3":{isVisible: false},
          };
          detailsArr.push(jsonData); 
        }
        this.view.segDatalist.removeAll();
        this.view.segDatalist.setData(detailsArr);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }catch(er){
        kony.print("Exception loadTransactionDetailsApprovals"+er);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }
    },
    btnConfigApprovalDetailsRequest:function(formFlow,amICreator,amIApprover,status){
      try{
        kony.print("Entered in btnConfigApprovalDetailsApprovalReq"+formFlow +" "+amICreator+" "+amIApprover+" "+status);
        if(status === "Executed" || status === "executed" || status === "Rejected" || status === "rejected" ||
           status.toLowerCase() === "approved" ||status === "Failed" || status ==="sent" || status === "Sent"){
          this.view.flxbtnApproveReject.isVisible = false;
          this.view.flxbtnWithdraw.isVisible = false;
          return;
        }
        switch(formFlow){
          case "TransactionDetailsRequests" :
            if(status.toLowerCase() === "pending"){
              if(amIApprover === "true" && amICreator ==="false"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = false;
              }else if(amIApprover === "false" && amICreator === "true"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = true;
              }else if (amIApprover === "true" && amICreator === "true"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = true;
              }else if(amIApprover === "false" && amICreator === "false"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = false;
              }
            }else{
              this.view.flxbtnApproveReject.isVisible = false;
              this.view.flxbtnWithdraw.isVisible = false;
            }
            break;
          case "ACHTransactionDetailsRequests":
            if(status.toLowerCase() === "pending"){
              if(amIApprover === "true" && amICreator ==="false"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = false;
              }else if(amIApprover === "false" && amICreator === "true"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = true;
              }else if (amIApprover === "true" && amICreator === "true"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = true;
              }else if(amIApprover === "false" && amICreator === "false"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = false;
              }
            }else{
              this.view.flxbtnApproveReject.isVisible = false;
              this.view.flxbtnWithdraw.isVisible = false;
            }
            break;
          case "ACHFileListRequests":
            if(status.toLowerCase() === "pending"){
              if(amIApprover === "true" && amICreator ==="false"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = false;
              }else if(amIApprover === "false" && amICreator === "true"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = true;
              }else if (amIApprover === "true" && amICreator === "true"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = true;
              }else if(amIApprover === "false" && amICreator === "false"){
                this.view.flxbtnApproveReject.isVisible = false;
                this.view.flxbtnWithdraw.isVisible = false;
              }
            }else{
              this.view.flxbtnApproveReject.isVisible = false;
              this.view.flxbtnWithdraw.isVisible = false;
            }
            break;
        }
      }catch(er){
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        kony.print("Exception in btnConfigApprovalDetailsApprovalReq"+er);
      }
    },
    loadAchTransactionDetailsApprovals:function(detailsData){
      try{
        applicationManager.getPresentationUtility().showLoadingScreen();
        var configManager = applicationManager.getConfigurationManager();
        var detailsArr=[
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achtransationdetail.TemplateName"),
            "lblColon":":",
            "lblValue":detailsData[0].templateName.text,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.transaction.transactionType"),
            "lblColon":":",
            "lblValue":detailsData[0].data.TransactionTypeValue,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.requestType"),
            "lblColon":":",
            "lblValue":detailsData[0].data.RequestType,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.debitAccountNo"),
            "lblColon":":",
            "lblValue":detailsData[0].accountName.text,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.createdOn"),
            "lblColon":":",
            "lblValue":detailsData[0].createdOn,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.createdBy"),
            "lblColon":":",
            "lblValue":detailsData[0].createdBy.text,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achtransationdetail.EffectiveDate"),
            "lblColon":":",
            "lblValue": kony.sdk.isNullOrUndefined(detailsData[0].lblEffectiveDate)?"N/A":detailsData[0].lblEffectiveDate,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.Amount"),
            "lblColon":":",
            "lblValue":configManager.getCurrencyCode()+detailsData[0].amount,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.status"),
            "lblColon":":",
            "lblValue":detailsData[0].data.Status,
            "lblSubValue" : {text :"", isVisible : false},
            "flxSeperatorTrans3":{isVisible: false},
          },
        ];
        this.view.segDatalist.removeAll();
        this.view.segDatalist.setData(detailsArr);
      }catch(er){
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }finally {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }
    },

    loadAchFileDetailsApprovals:function(detailsData){
      try{

        if(!kony.sdk.isNullOrUndefined(detailsData)){
          if(Array.isArray(detailsData)){
            var detailsArr=[
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.fileName"),
                "lblColon":":",
                "lblValue":detailsData[0].lblTransactionAP,
                "lblSubValue" : {text :"", isVisible : false},
              },{
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.fileType"),
                "lblColon":":",
                "lblValue":detailsData[0].data.FileFormatType,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.requestType"),
                "lblColon":":",
                "lblValue":detailsData[0].data.FileRequestType,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.status"),
                "lblColon":":",
                "lblValue":detailsData[0].data.FileStatus,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.uploadedBy"),
                "lblColon":":",
                "lblValue":detailsData[0].userName,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.uploadDate"),
                "lblColon":":",
                "lblValue":detailsData[0].tpdatedDateAndTime,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.totalDebitAmount"),
                "lblColon":":",
                "lblValue":detailsData[0].lblTransactionAmountAP,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.totalCreditAmount"),
                "lblColon":":",
                "lblValue":detailsData[0].totalCreditAmount,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.numberOfDebits"),
                "lblColon":":",
                "lblValue":detailsData[0].data.NumberOfDebits,
                "lblSubValue" : {text :"", isVisible : false},
              },    
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.numberOfCredits"),
                "lblColon":":",
                "lblValue":detailsData[0].data.NumberOfCredits,
                "lblSubValue" : {text :"", isVisible : false},
              },    
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.NoOfPreNotes"),
                "lblColon":":",
                "lblValue":detailsData[0].data.NumberOfPrenotes,
                "lblSubValue" : {text :"", isVisible : false},
              },     
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.NoOfRecords"),
                "lblColon":":",
                "lblValue":detailsData[0].data.NumberOfRecords,
                "lblSubValue" : {text :"", isVisible : false},
                "flxSeperatorTrans3":{isVisible: false},
              }];
            this.view.segDatalist.removeAll();
            this.view.segDatalist.setData(detailsArr);
          }
        }
      }catch(er){
        kony.print("catch error"+er);
      }
    },

    loadTransactionDetailsRequest:function(detailsData){
      try{
        kony.print("Entered loadTransactionDetailsRequest"+JSON.stringify(detailsData[0]));
        var detailsArr=[
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.Payee"),
            "lblColon":":",
            "lblValue":detailsData[0].lblTransaction,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.debitAccountNo"),
            "lblColon":":",
            "lblValue":detailsData[0].debitAccount,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.transaction.transactionType"),
            "lblColon":":",
            "lblValue":detailsData[0].data.featureName,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.cardLess.transactionId"),
            "lblColon":":",
            "lblValue":detailsData[0].transaction_id,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.transaction.transactionDate"),
            "lblColon":":",
            "lblValue":detailsData[0].lblDate,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.createdBy"),
            "lblColon":":",
            "lblValue":detailsData[0].data.userName,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.transaction.frequency"),
            "lblColon":":",
            "lblValue":detailsData[0].frequency,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.transaction.recurrence"),
            "lblColon":":",
            "lblValue":detailsData[0].reccurence,
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
          },
        ];
        var jsonData={};
        if(this.detailsData[0].data.Status === "pending" || this.detailsData[0].data.Status === "Pending"){
          jsonData={
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
            "lblColon":"",
            "lblValue":this.detailsData[0].data.receivedApprovals +" "+"of"+" "+this.detailsData[0].data.requiredApprovals+" "+"Approved",
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
            "flxSeperatorTrans3":{isVisible: false},
          };
          detailsArr.push(jsonData);                  
        }else if(this.detailsData[0].data.Status === "approved" || this.detailsData[0].data.Status === "Approved"){
          jsonData={
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
            "lblColon":"",
            "lblValue":this.detailsData[0].data.requiredApprovals+" "+kony.i18n.getLocalizedString("kony.mb.approvalRequest.approvalstatus"),
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
            "flxSeperatorTrans3":{isVisible: false},
          };
          detailsArr.push(jsonData);                  
        }
        else if(this.detailsData[0].data.Status === "rejected" || this.detailsData[0].data.Status === "Rejected"){
          jsonData={
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
            "lblColon":"",
            "lblValue":1+" "+kony.i18n.getLocalizedString("kony.mb.achtransactions.Rejection"),
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
            "flxSeperatorTrans3":{isVisible: false},
          };
          detailsArr.push(jsonData); 
        }
        else{
          jsonData={
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
            "lblColon":"",
            "lblValue":this.detailsData[0].data.requiredApprovals+" "+kony.i18n.getLocalizedString("kony.mb.approvalRequest.approvalstatus"),
            "lblSubValue" : {text : detailsData[0].data.TransactionType, isVisible : false},
            "flxSeperatorTrans3":{isVisible: false},
          };
          detailsArr.push(jsonData); 
        }
        this.view.segDatalist.removeAll();
        this.view.segDatalist.setData(detailsArr);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }catch(er){
        kony.print("Exception loadTransactionDetailsRequest"+er);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }
    },

    loadAchTransactionDetailsRequest:function(detailsData){
      try{
        applicationManager.getPresentationUtility().showLoadingScreen();
        var configManager = applicationManager.getConfigurationManager();
        var detailsArr=[
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achtransationdetail.TemplateName"),
            "lblColon":":",
            "lblValue":detailsData[0].templateName,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.transaction.transactionType"),
            "lblColon":":",
            "lblValue":detailsData[0].data.TransactionTypeValue,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.requestType"),
            "lblColon":":",
            "lblValue":detailsData[0].data.RequestType,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.debitAccountNo"),
            "lblColon":":",
            "lblValue":detailsData[0].accountName,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.createdOn"),
            "lblColon":":",
            "lblValue":detailsData[0].createdOn,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.createdBy"),
            "lblColon":":",
            "lblValue":detailsData[0].createdBy,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achtransationdetail.EffectiveDate"),
            "lblColon":":",
            "lblValue":detailsData[0].lblEffectiveDate,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.Amount"),
            "lblColon":":",
            "lblValue":configManager.getCurrencyCode()+detailsData[0].amount,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.status"),
            "lblColon":":",
            "lblValue":detailsData[0].data.Status,
            "lblSubValue" : {text :"", isVisible : false},
            "flxSeperatorTrans3":{isVisible: false},
          },
        ];


        this.view.segDatalist.removeAll();
        this.view.segDatalist.setData(detailsArr);
      }catch(er){
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        kony.print("loadAchTransactionDetailsRequest catch--->"+er);
      }finally {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }
    },

    loadAchFileDetailsRequest:function(detailsData){
      try{
        kony.print("loadAchFileDetailsRequest"+detailsData);
        if(!kony.sdk.isNullOrUndefined(detailsData)){
          if(Array.isArray(detailsData)){
            var detailsArr=[
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.fileName"),
                "lblColon":":",
                "lblValue":detailsData[0].lblTransaction,
                "lblSubValue" : {text :"", isVisible : false},
              },{
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.fileType"),
                "lblColon":":",
                "lblValue":detailsData[0].data.FileFormatType,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.requestType"),
                "lblColon":":",
                "lblValue":detailsData[0].data.FileRequestType,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.status"),
                "lblColon":":",
                "lblValue":detailsData[0].data.FileStatus,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.uploadedBy"),
                "lblColon":":",
                "lblValue":detailsData[0].userName,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.uploadDate"),
                "lblColon":":",
                "lblValue":detailsData[0].updatedDateAndTime,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.totalDebitAmount"),
                "lblColon":":",
                "lblValue":detailsData[0].totalDebitAmount,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.totalCreditAmount"),
                "lblColon":":",
                "lblValue":detailsData[0].totalCreditAmount,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.numberOfDebits"),
                "lblColon":":",
                "lblValue":detailsData[0].data.NumberOfDebits,
                "lblSubValue" : {text :"", isVisible : false},
              },    
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.numberOfCredits"),
                "lblColon":":",
                "lblValue":detailsData[0].data.NumberOfCredits,
                "lblSubValue" : {text :"", isVisible : false},
              },    
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.NoOfPreNotes"),
                "lblColon":":",
                "lblValue":detailsData[0].data.NumberOfPrenotes,
                "lblSubValue" : {text :"", isVisible : false},
              },     
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.NoOfRecords"),
                "lblColon":":",
                "lblValue":detailsData[0].data.NumberOfRecords,
                "lblSubValue" : {text :"", isVisible : false},
                "flxSeperatorTrans3":{isVisible: false},
              }];
            this.view.segDatalist.removeAll();
            this.view.segDatalist.setData(detailsArr);
          }
        }
      }catch(er){
        kony.print("catch error"+er);
      }
    },

    loadTransactionDetails:function(detailsData){
      try{
        applicationManager.getPresentationUtility().showLoadingScreen();
        var configManager = applicationManager.getConfigurationManager();
        var detailsArr=[
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achtransationdetail.TemplateName"),
            "lblColon":":",
            "lblValue":detailsData[0].TemplateName,
            "lblSubValue" : {text :"", isVisible : false},
          },{
            "lblKey":kony.i18n.getLocalizedString("kony.mb.transaction.transactionType"),
            "lblColon":":",
            "lblValue":detailsData[0].TransactionTypeValue,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.requestType"),
            "lblColon":":",
            "lblValue":detailsData[0].RequestType,
            "lblSubValue" : {text :"", isVisible : false},
          },

          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.debitAccountNo"), 
            "lblColon":":",
            "lblValue":detailsData[0].AccountName,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.createdOn"),
            "lblColon":":",
            "lblValue":detailsData[0].createdOn,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.createdBy"),
            "lblColon":":",
            "lblValue":detailsData[0].createdBy,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achtransationdetail.EffectiveDate"),
            "lblColon":":",
            "lblValue":detailsData[0].EffectiveDate,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.common.Amount"),
            "lblColon":":",
            "lblValue":configManager.getCurrencyCode()+detailsData[0].amount,
            "lblSubValue" : {text :"", isVisible : false},
          },
          {
            "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.status"),
            "lblColon":":",
            "lblValue":detailsData[0].Status,
            "lblSubValue" : {text :"", isVisible : false},
          },
        ];
        this.view.segDatalist.removeAll();
        this.view.segDatalist.setData(detailsArr);
      }catch(er){
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }finally {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }
    },

    loadAchDetails:function(detailsData){
      try{

        if(!kony.sdk.isNullOrUndefined(detailsData)){
          if(Array.isArray(detailsData)){
            var detailsArr=[
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.fileName"),
                "lblColon":":",
                "lblValue":detailsData[0].lblFilename,
                "lblSubValue" : {text :"", isVisible : false},
              },{
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.fileType"),
                "lblColon":":",
                "lblValue":detailsData[0].fileType,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.requestType"),
                "lblColon":":",
                "lblValue":detailsData[0].RequestType,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.status"),
                "lblColon":":",
                "lblValue":detailsData[0].lblStatus,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.uploadedBy"),
                "lblColon":":",
                "lblValue":detailsData[0].lblAdmin,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.uploadDate"),
                "lblColon":":",
                "lblValue":detailsData[0].uploadDate,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.totalDebitAmount"),
                "lblColon":":",
                "lblValue":detailsData[0].totalDebitAccount,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.totalCreditAmount"),
                "lblColon":":",
                "lblValue":detailsData[0].totalCreditAccount,
                "lblSubValue" : {text :"", isVisible : false},
              },
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.numberOfDebits"),
                "lblColon":":",
                "lblValue":detailsData[0].numberOfDebits,
                "lblSubValue" : {text :"", isVisible : false},
              },    
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.numberOfCredits"),
                "lblColon":":",
                "lblValue":detailsData[0].numberOfCredits,
                "lblSubValue" : {text :"", isVisible : false},
              },    
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.NoOfPreNotes"),
                "lblColon":":",
                "lblValue":detailsData[0].numberOfPreNotes,
                "lblSubValue" : {text :"", isVisible : false},
              },     
              {
                "lblKey":kony.i18n.getLocalizedString("kony.mb.achfiledetail.NoOfRecords"),
                "lblColon":":",
                "lblValue":detailsData[0].numberOfrecords,
                "lblSubValue" : {text :"", isVisible : false},
              }];
            this.view.segDatalist.removeAll();
            this.view.segDatalist.setData(detailsArr);
          }
        }
      }catch(er){

      }
    },

    ///////********showDesinationAccount is used to show the desination account flex*****////////

    showDesinationAccount:function()
    {
      try {  

        if(this.view. imgUpArrow.src=="blue_uparrow.png"){
          this.view.segDestinationaccount.isVisible = false;
          this.view. imgUpArrow.src = "blue_downarrow.png";
        }else{
          this.view.segDestinationaccount.isVisible = true;
          this.view. imgUpArrow.src = "blue_uparrow.png";
        }

      }catch(error){
        kony.print("frmACHTransactionsdetails bindevents-->"+error);
      }       
    },

    ///////********showApprovalHistory is used to show the Approvalhistory flex*****////////
    showApprovalHistory:function()
    {
      try {  

        if(this.view. imgapprovalhis.src==="blue_uparrow.png"){
          this.view.segApprovalHistory.isVisible = false;
          this.view. imgapprovalhis.src = "blue_downarrow.png";
        }else{
          this.view.segApprovalHistory.isVisible = true;
          this.view. imgapprovalhis.src = "blue_uparrow.png";
        }

      }catch(error){
        kony.print("frmACHTransactionsdetails bindevents-->"+error);
      }       
    },

    ///////********rejectDetails is used to showrejectpopup****////////
    rejectDetails:function()
    {
      try {  
        this.view.rejectPopUp.lblContent.isVisible = true;
        this.view.rejectPopUp.lblContentreject.isVisible = true;
        this.view.rejectPopUp.txtRejectreason.isVisible = true;
        this.view.rejectPopUp.lblTitle.top = "10dp";
        this.view.rejectPopUp.flxBtns.top = "10dp";
        this.view.rejectPopUp.lblTitle.text= kony.i18n.getLocalizedString("kony.mb.ApprovalRequests.Reject");
        this.view.rejectPopUp.lblYes.text= kony.i18n.getLocalizedString("kony.mb.ApprovalRequests.Reject");
        this.view.rejectPopUp.lblContent.text=kony.i18n.getLocalizedString("kony.mb.request.rejectmsg");
        this.view.rejectPopUp.lblContentreject.text=kony.i18n.getLocalizedString("kony.mb.approve.rejectreason");
        var isiPhone = applicationManager.getPresentationFormUtility().getDeviceName() === "iPhone";
        if(isiPhone) {
          this.view.rejectPopUp.lblTitle.skin = "sknLbl494949semibold45px";
          this.view.rejectPopUp.lblContent.skin = "sknlbl424242SSPR15dp";
          this.view.rejectPopUp.lblContentreject.skin = "sknlbl424242SSPR15dp";
        }
        this.view.rejectPopUp.txtRejectreason.text="";
        if(this.view.rejectPopUp.txtRejectreason.text==="" || this.view.rejectPopUp.txtRejectreason.text===null){
          this.view.rejectPopUp.flxReject.setEnabled(false);
        }else{
          this.view.rejectPopUp.flxReject.setEnabled(true);
        }
        this.view.flxRejectpopup.isVisible = true;
      }catch(error){
        kony.print("frmACHTransactionsdetails rejectDetails-->"+error);
      }       
    },
    rejectEnabledButton:function()
    {
      try {  
        if(this.view.rejectPopUp.txtRejectreason.text==="" || this.view.rejectPopUp.txtRejectreason.text===null){
          this.view.rejectPopUp.flxReject.setEnabled(false);
        }else{
          this.view.rejectPopUp.flxReject.setEnabled(true);
        }
      }catch(error){
        kony.print("frmACHTransactionsdetails rejectCancel-->"+error);
      }       
    },

    rejectCancel:function()
    {
      try {  
        this.view.flxRejectpopup.isVisible = false;
        this.view.rejectPopUp.txtRejectreason.text="";
      }catch(error){
        kony.print("frmACHTransactionsdetails rejectCancel-->"+error);
      }       
    },

    rejectServicecall:function()
    {
      try {  
        var formType=this.previousFormType;
        var scope =this;
        applicationManager.getPresentationUtility().showLoadingScreen();
        switch(formType)
        {
          case "ACHTransactionsList":
            var navigationObject={
              "requestData":{"Request_id":this.detailsData[0].Request_id,
                             "Comments":this.view.rejectPopUp.txtRejectreason.text,
                             "Action":"Rejected"},
              formData : scope
            };
            var ApprovalRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
            ApprovalRequestsModule. presentationController.rejectACHTransaction(navigationObject);
            //this.fetchRejectACHTransactionsSuccess();
            break;

          case "ACHFileList":
            var navigationObject={
              "requestData":{"Request_id":this.detailsData[0].Request_id,
                             "Comments":this.view.rejectPopUp.txtRejectreason.text,
                             "Action":"Rejected"},
              formData : scope
            };
            var ApprovalRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
            ApprovalRequestsModule. presentationController.rejectACHFile(navigationObject);
            //this.fetchRejectACHFilesSuccess();
            break;      
          case "TransactionDetailsApprovals": 
            var navigationObject={
              "requestData":{"Request_id":this.detailsData[0].request_id,
                             "Comments":this.view.rejectPopUp.txtRejectreason.text,
                             "Action":"Rejected"},
              formData : scope
            };
            var ApprovalRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
            ApprovalRequestsModule. presentationController.rejectBBGeneralTransactions(navigationObject);
            break;
          case "ACHTransactionDetailsApprovals": 
            var navigationObject={
              "requestData":{"Request_id":this.detailsData[0].request_id,
                             "Comments":this.view.rejectPopUp.txtRejectreason.text,
                             "Action":"Rejected"},
              formData : scope
            };
            var ApprovalRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
            ApprovalRequestsModule. presentationController.rejectACHTransaction(navigationObject);
            break;
          case "ACHFileListApprovals": 
            var navigationObject={
              "requestData":{"Request_id":this.detailsData[0].request_id,
                             "Comments":this.view.rejectPopUp.txtRejectreason.text,
                             "Action":"Rejected"},
              formData : scope
            };
            var ApprovalRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
            ApprovalRequestsModule. presentationController.rejectACHFile(navigationObject);
            break;
        }
      }catch(error){
        kony.print("frmACHTransactionsdetails rejectDetails-->"+error);
      }finally {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }       
    },


    approveServicecall:function()
    {
      try { 
        var formType=this.previousFormType;
        var scope=this;
        applicationManager.getPresentationUtility().showLoadingScreen();
        switch(formType)
        {
          case "ACHTransactionsList":
            var navigationObject={
              "requestData":{"Request_id":this.detailsData[0].Request_id,
                             "Comments":"Approved",
                             "Action":"Approved"},
              formData : scope
            };
            var ApprovalRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
            ApprovalRequestsModule. presentationController.approveACHTransactions(navigationObject);
            //             this.fetchApproveACHTransactionsSuccess();
            break;
          case "ACHFileList":
            var navigationObject={
              "requestData":{"Request_id":this.detailsData[0].Request_id,
                             "Comments":"Approved",
                             "Action":"Approved"},
              formData : scope
            };
            var ApprovalRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
            ApprovalRequestsModule. presentationController.approveACHFiles(navigationObject);
            //             this.fetchApproveACHFilesSuccess();
            break;
          case "TransactionDetailsApprovals": 
            var navigationObject={
              "requestData":{"Request_id":this.detailsData[0].request_id,
                             "Comments":"Approved",
                             "Action":"Approved"},
              formData : scope
            };
            var ApprovalRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
            ApprovalRequestsModule.presentationController.approveBBGeneralTransactions(navigationObject);
            break;
          case "ACHTransactionDetailsApprovals":
            var navigationObject={
              "requestData":{"Request_id":this.detailsData[0].request_id,
                             "Comments":"Approved",
                             "Action":"Approved"},
              formData : scope
            };
            var ApprovalRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
            ApprovalRequestsModule.presentationController.approveACHTransactions(navigationObject);
            break;
          case "ACHFileListApprovals":
            var navigationObject={
              "requestData":{"Request_id":this.detailsData[0].request_id,
                             "Comments":"Approved",
                             "Action":"Approved"},
              formData : scope
            };
            var ApprovalRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
            ApprovalRequestsModule.presentationController.approveACHFiles(navigationObject);
            break;
        }

      }catch(error){
        kony.print("frmACHTransactionsdetails approveServicecall-->"+error);
      }finally {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }       
    },


    withdrawServicecall:function()
    {
      try { 
        var formType=this.previousFormType;
        var scope=this;
        applicationManager.getPresentationUtility().showLoadingScreen();
        var navigationObject={};
        var ApprovalRequestsModule;
        switch(formType)
        {
          case "ACHTransactionsList":
            navigationObject={
              "requestData":{"Request_id":this.detailsData[0].Request_id},
              formData : scope
            };
            ApprovalRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
            ApprovalRequestsModule. presentationController.withdrawACHTransaction(navigationObject);
            //this.fetchACTransactionWithdrawlSuccess();
            break;
          case "ACHFileList":
            navigationObject={
              "requestData":{"Request_id":this.detailsData[0].Request_id},
              formData : scope
            };
            ApprovalRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
            ApprovalRequestsModule.presentationController.withdrawACHFileRequests(navigationObject);
            // this.fetchACHFileWithdrawlSuccess();
            break;
          case "TransactionDetailsRequests":
            navigationObject={
              "requestData":{"Request_id":this.detailsData[0].request_id},
              formData : scope
            };
            ApprovalRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
            ApprovalRequestsModule.presentationController.withdrawTransactionRequest(navigationObject);
            break;
          case "ACHTransactionDetailsRequests":
            navigationObject={
              "requestData":{"Request_id":this.detailsData[0].request_id},
              formData : scope
            };
            ApprovalRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
            ApprovalRequestsModule. presentationController.withdrawACHTransaction(navigationObject);
            //this.fetchACTransactionWithdrawlSuccess();
            break;
          case "ACHFileListRequests":
            navigationObject={
              "requestData":{"Request_id":this.detailsData[0].request_id},
              formData : scope
            };
            ApprovalRequestsModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("ApprovalsReqModule");
            ApprovalRequestsModule.presentationController.withdrawACHFileRequests(navigationObject);
            // this.fetchACHFileWithdrawlSuccess();
            break;
        }
      }catch(error){
        kony.print("frmACHTransactionsdetails approveServicecall-->"+error);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }finally {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }       
    },
    fetchApproveACHTransactionsSuccess : function(response){
      var formType=this.previousFormType;
      var formFlow={};
      var navManager = applicationManager.getNavigationManager();
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      if(!kony.sdk.isNullOrUndefined(response)){
        if(response.Status==="Approved"){
          switch(formType){
            case "ACHTransactionsList":
              formFlow={
                "FormType" : "ACHTransaction",
                "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.approved"),
                "imgIconKey":"Approval",
              };
              applicationManager.getPresentationUtility().dismissLoadingScreen();
              navManager.setCustomInfo("ACHTransactionDetails",formFlow);
              break;  
            case "ACHTransactionDetailsApprovals":
              formFlow={
                "FormType" : "ACHTransactionApprovals",
                "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.approved"),
                "imgIconKey":"Approval",
              };
              applicationManager.getPresentationUtility().dismissLoadingScreen();
              navManager.setCustomInfo("ACHTransactionDetails",formFlow);
              break; 
          }
          navManager.navigateTo("StausMessage");
        }
      }
    },
    fetchRejectACHTransactionsSuccess : function(response){
      var formType=this.previousFormType;
      var formFlow={};
      var navManager = applicationManager.getNavigationManager();
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      if(!kony.sdk.isNullOrUndefined(response)){
        if(response.Status==="Rejected"){
          switch(formType){
            case "ACHTransactionsList":
              formFlow={
                "FormType" : "ACHTransaction",
                "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.reject"),
                "imgIconKey":"Reject",
              };
              applicationManager.getPresentationUtility().dismissLoadingScreen();
              navManager.setCustomInfo("ACHTransactionDetails",formFlow);
              break;
            case "ACHTransactionDetailsApprovals":
              formFlow={
                "FormType" : "ACHTransactionApprovals",
                "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.reject"),
                "imgIconKey":"Reject",
              };
              applicationManager.getPresentationUtility().dismissLoadingScreen();
              navManager.setCustomInfo("ACHTransactionDetails",formFlow);
              break;
          }
          navManager.navigateTo("StausMessage");
        }
      }
    },

    fetchApproveACHFilesSuccess : function(response){
      var formType=this.previousFormType;
      var formFlow={};
      var navManager = applicationManager.getNavigationManager();
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      if(!kony.sdk.isNullOrUndefined(response)){
        if(response.Status==="Approved"){
          switch(formType){
            case "ACHFileList":
              formFlow={
                "FormType" : "ACHFiles",
                "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.approved"),
                "imgIconKey":"Approval",
              };
              applicationManager.getPresentationUtility().dismissLoadingScreen();
              navManager.setCustomInfo("ACHTransactionDetails",formFlow);
              break;
            case "ACHFileListApprovals":
              formFlow={
                "FormType" : "ACHFilesApprovals",
                "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.approved"),
                "imgIconKey":"Approval",
              };
              applicationManager.getPresentationUtility().dismissLoadingScreen();
              navManager.setCustomInfo("ACHTransactionDetails",formFlow);
              break;
          }
          navManager.navigateTo("StausMessage");
        }
      }
    },

    fetchRejectACHFilesSuccess : function(response){
      var formType=this.previousFormType;
      var formFlow={};
      var navManager = applicationManager.getNavigationManager();
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      if(!kony.sdk.isNullOrUndefined(response)){
        if(response.Status==="Rejected"){
          switch(formType){
            case "ACHFileList":
              formFlow={
                "FormType" : "ACHFiles",
                "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.reject"),
                "imgIconKey":"Reject",
              };
              applicationManager.getPresentationUtility().dismissLoadingScreen();
              navManager.setCustomInfo("ACHTransactionDetails",formFlow);
              break;
            case "ACHFileListApprovals":
              formFlow={
                "FormType" : "ACHFilesApprovals",
                "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.reject"),
                "imgIconKey":"Reject",
              };
              applicationManager.getPresentationUtility().dismissLoadingScreen();
              navManager.setCustomInfo("ACHTransactionDetails",formFlow);
              break;
          }
          navManager.navigateTo("StausMessage");
        }
      }
    },

    fetchACTransactionWithdrawlSuccess : function(response){
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      var formType=this.previousFormType;
      var formFlow={};
      if(!kony.sdk.isNullOrUndefined(response)){
        if(response.Status==="Withdrawn"){
          switch(formType){
            case "ACHTransactionsList":
              formFlow={
                "FormType" : "ACHTransaction",
                "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.withdraw"),
                "imgIconKey":"withdraw",
              };
              break;
            case "ACHTransactionDetailsRequests":
              formFlow={
                "FormType" : "ACHTransactionApprovals",
                "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.withdraw"),
                "imgIconKey":"withdraw",
              };
              break;
          }
          applicationManager.getPresentationUtility().dismissLoadingScreen();
          var navManager = applicationManager.getNavigationManager();
          navManager.setCustomInfo("ACHTransactionDetails",formFlow);
          navManager.navigateTo("StausMessage");
        }
      }
    },

    fetchACHFileWithdrawlSuccess : function(response){
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      var formType=this.previousFormType;
      var formFlow={};
      if(!kony.sdk.isNullOrUndefined(response)){
        if(response.Status==="Withdrawn"){
          switch(formType){          
            case "ACHFileList":
              formFlow={
                "FormType" : "ACHFiles",
                "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.withdraw"),
                "imgIconKey":"withdraw",
              };
              break;
            case "ACHFileListRequests":
              formFlow={
                "FormType" : "ACHFilesRequest",
                "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.withdraw"),
                "imgIconKey":"withdraw",
              };
              break;
            case "ACHFileListApprovals":
              formFlow={
                "FormType" : "ACHFilesApprovals",
                "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.withdraw"),
                "imgIconKey":"withdraw",
              };
              break;
          }
          applicationManager.getPresentationUtility().dismissLoadingScreen();
          var navManager = applicationManager.getNavigationManager();
          navManager.setCustomInfo("ACHTransactionDetails",formFlow);
          navManager.navigateTo("StausMessage");
        }
      }
    },

    fetchBBGeneralTransactionsSucces : function(response){
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      if(!kony.sdk.isNullOrUndefined(response)){
        if(response.Status==="Approved"){
          var formFlow={
            "FormType" : "GeneralTransactions",
            "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.approved"),
            "imgIconKey":"Approval",
          };
          applicationManager.getPresentationUtility().dismissLoadingScreen();
          var navManager = applicationManager.getNavigationManager();
          navManager.setCustomInfo("ACHTransactionDetails",formFlow);
          navManager.navigateTo("StausMessage");
        }}
    },

    fetchRejectBBGeneralTransactionsSuccess : function(response){
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      if(!kony.sdk.isNullOrUndefined(response)){
        if(response.Status==="Rejected"){
          var formFlow={
            "FormType" : "GeneralTransactions",
            "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.reject"),
            "imgIconKey":"Reject",
          };
          applicationManager.getPresentationUtility().dismissLoadingScreen();
          var navManager = applicationManager.getNavigationManager();
          navManager.setCustomInfo("ACHTransactionDetails",formFlow);
          navManager.navigateTo("StausMessage");
        }}
    },

    fetchBBGeneralTransactionsWithdrawlSuccess : function(response){
      applicationManager.getPresentationUtility().dismissLoadingScreen();
      if(!kony.sdk.isNullOrUndefined(response)){
        if(response.Status==="Withdrawn"){
          var formFlow={
            "FormType" : "GeneralTransactions",
            "FormData" : kony.i18n.getLocalizedString("kony.mb.achtransactionsdetail.withdraw"),
            "imgIconKey":"withdraw",
          };
          applicationManager.getPresentationUtility().dismissLoadingScreen();
          var navManager = applicationManager.getNavigationManager();
          navManager.setCustomInfo("ACHTransactionDetails",formFlow);
          navManager.navigateTo("StausMessage");
        }
      }
    },

    dummyFunction : function(){
      kony.print("Entered do nothing");
    },
    /**
         * fetchDestinationAccounts : fetch the required transaction Records.
         * @member of {frmACHDashboardController}
         * @param {JSON Onject} inputparams - transaction details from Template or Transaction
         * @return {}
         * @throws {}
         */
    fetchDestinationAccounts: function (inputparams) {
      try{
        applicationManager.getPresentationUtility().showLoadingScreen();
        var scopeObj = this;
        var navObj = {
          requestData: inputparams,
        };
        scopeObj.ACHModule.presentationController.getDestinationAccountsRecords(navObj);
      }catch(er){
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }finally {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }
    },

    getACHFilesDestinationAccntSuccessCallBack:function(response){
      try{
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        
        if(!kony.sdk.isNullOrUndefined(response)){

          if(Array.isArray(response)){
            this.view.segDestinationaccount.widgetDataMap={
              "lblRecipientname":"lblRecipientname",
              "lblAccountnumber":"lblAccountnumber",
              "lblAmount":"lblAmount",
              "flxSep":"flxSep",
              "flxSeperatorTrans4":"flxSeperatorTrans4",
            };
            if(response.length>0){
               this.view.flxdesinationaccount.isVisible = true;
              this.view.segDestinationaccount.isVisible = true;
              this.view.segDestinationaccount.setData(response);
            }else{
              this.view.segDestinationaccount.removeAll();
              var nodataArr=[];
              var nodataJson = {
                "lblRecipientname":"No data found",
                "lblAccountnumber":"",
                "lblAmount":"",
                "flxSep":{isVisible:false},
                "flxSeperatorTrans4":{isVisible:false},
              };
              nodataArr.push(nodataJson);
               this.view.flxdesinationaccount.isVisible = true;
              this.view.segDestinationaccount.isVisible = true;
              this.view.segDestinationaccount.setData(nodataArr);
            }
          }else{

            this.view.flxdesinationaccount.isVisible = false;
            this.view.segDestinationaccount.isVisible = false;
          }
        }else{

          this.view.flxdesinationaccount.isVisible = false;
          this.view.segDestinationaccount.isVisible = false;
        }
      }catch(er){
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }
    },

    getApprovalReqHistory:function(inputparams){
      try{
        applicationManager.getPresentationUtility().showLoadingScreen();
        var navObj = {
          requestData: inputparams,
        };
        this.ApprovalModule.presentationController.getRequestsHistory(navObj);
      }catch(er){
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }finally {
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }
    },
    getApprovalReqHistorySuccessCB:function(response){
      try{
        applicationManager.getPresentationUtility().dismissLoadingScreen();
        var jsonData={};
        var nodataArr=[];
        var nodataJson = {};
        if(!kony.sdk.isNullOrUndefined(response)){
          if(response.length <=0){
            jsonData={
              "lblRecipientname":{
                "text" : kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
                "skin":"sknlbl949494SSPR13px",
              },
              "lblAccountnumber":{
                "text" : "N/A",
                "skin":"sknlbl424242ssp40px",
              },
              "lblAmount":{
                "text" : "",
              },
              "flxSep":{isVisible:false},
              "flxSeperatorTrans4":{isVisible:false},
            };
            response.push(jsonData);
            if(Array.isArray(response)){
              this.view.segApprovalHistory.widgetDataMap={
                "lblRecipientname":"lblRecipientname",
                "lblAccountnumber":"lblAccountnumber",
                "lblAmount":"lblAmount",
                "flxSep":"flxSep",
                "flxSeperatorTrans4":"flxSeperatorTrans4",
              };
              if(response.length > 0){
                this.view.segApprovalHistory.setData(response);
              }else{
                this.view.segApprovalHistory.removeAll();
                nodataArr=[];
                nodataJson = {
                  "lblRecipientname":"No Request History Records Found",
                  "lblAccountnumber":"",
                  "lblAmount":"",
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                nodataArr.push(nodataJson);
                this.view.segApprovalHistory.setData(nodataArr);
              }
            }
          }else{
            if(this.previousFormType === "ACHTransactionDetailsApprovals" || this.previousFormType === "ACHFileListApprovals" ||
               this.previousFormType === "ACHTransactionDetailsRequests" ||this.previousFormType === "ACHFileListRequests"){
              if(kony.sdk.isNullOrUndefined(this.detailsData[0].data.Status))
              {
                //condition check for ACHFilerequest
                if(!kony.sdk.isNullOrUndefined(this.detailsData[0].data.FileStatus))
                {
                  this.detailsData[0].data.Status=this.detailsData[0].data.FileStatus;
                }
              }
              if(this.detailsData[0].data.Status === "pending" || this.detailsData[0].data.Status === "Pending"){
                jsonData={
                  "lblRecipientname":{
                    "text" : kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
                    "skin":"sknlbl949494SSPR13px",
                  },
                  "lblAccountnumber":{
                    "text" : this.detailsData[0].data.receivedApprovals +" "+"of"+" "+this.detailsData[0].data.requiredApprovals+" "+"Approved",
                    "skin":"sknlbl424242ssp40px",
                  },
                  "lblAmount":{
                    "text" : "",
                  },
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                response.push(jsonData);                  
              }else if(this.detailsData[0].data.Status === "approved" || this.detailsData[0].data.Status === "Approved"){
                jsonData={
                  "lblRecipientname":{
                    "text" : kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
                    "skin":"sknlbl949494SSPR13px",
                  },
                  "lblAccountnumber":{
                    "text":this.detailsData[0].data.requiredApprovals+" "+kony.i18n.getLocalizedString("kony.mb.approvalRequest.approvalstatus"),
                    "skin":"sknlbl424242ssp40px",
                  },
                  "lblAmount":{
                    "text" : "",
                  },
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                response.push(jsonData);                  
              }
              else if(this.detailsData[0].data.Status === "rejected" || this.detailsData[0].data.Status === "Rejected"){
                jsonData={
                  "lblRecipientname":{
                    "text" : kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
                    "skin":"sknlbl949494SSPR13px",
                  },
                  "lblAccountnumber":{
                    "text" : 1+" "+kony.i18n.getLocalizedString("kony.mb.achtransactions.Rejection"),
                    "skin":"sknlbl424242ssp40px",
                  },
                  "lblAmount":{
                    "text" : "",
                  },
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                response.push(jsonData); 
              }else{
                jsonData={
                  "lblRecipientname":{
                    "text" : kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
                    "skin":"sknlbl949494SSPR13px",
                  },
                  "lblAccountnumber":{
                    "text" :this.detailsData[0].data.requiredApprovals+" "+kony.i18n.getLocalizedString("kony.mb.approvalRequest.approvalstatus"),
                    "skin":"sknlbl424242ssp40px",
                  },
                  "lblAmount":{
                    "text" : "",
                  },
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                response.push(jsonData); 
              }
            }
            else if(this.previousFormType ==="ACHTransactionsList"){
              if(this.detailsData[0].Status === "pending" || this.detailsData[0].Status === "Pending"){
                jsonData={
                  "lblRecipientname":{
                    "text" : kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
                    "skin":"sknlbl949494SSPR13px",
                  },
                  "lblAccountnumber":{
                    "text" : this.detailsData[0].receivedApprovals +" "+"of"+" "+this.detailsData[0].requiredApprovals+" "+"Approved",
                    "skin":"sknlbl424242ssp40px",
                  },
                  "lblAmount":{
                    "text" : "",
                  },
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                response.push(jsonData);                  
              }else if(this.detailsData[0].Status === "approved" || this.detailsData[0].Status === "Approved"){
                jsonData={
                  "lblRecipientname":{
                    "text" : kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
                    "skin":"sknlbl949494SSPR13px",
                  },
                  "lblAccountnumber":{
                    "text":this.detailsData[0].requiredApprovals+" "+kony.i18n.getLocalizedString("kony.mb.approvalRequest.approvalstatus"),
                    "skin":"sknlbl424242ssp40px",
                  },
                  "lblAmount":{
                    "text" : "",
                  },
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                response.push(jsonData);                  
              }
              else if(this.detailsData[0].Status === "rejected" || this.detailsData[0].Status === "Rejected"){
                jsonData={
                  "lblRecipientname":{
                    "text" : kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
                    "skin":"sknlbl949494SSPR13px",
                  },
                  "lblAccountnumber":{
                    "text" : 1+" "+kony.i18n.getLocalizedString("kony.mb.achtransactions.Rejection"),
                    "skin":"sknlbl424242ssp40px",
                  },
                  "lblAmount":{
                    "text" : "",
                  },
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                response.push(jsonData); 
              }else{
                jsonData={
                  "lblRecipientname":{
                    "text" : kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
                    "skin":"sknlbl949494SSPR13px",
                  },
                  "lblAccountnumber":{
                    "text" :this.detailsData[0].requiredApprovals+" "+kony.i18n.getLocalizedString("kony.mb.approvalRequest.approvalstatus"),
                    "skin":"sknlbl424242ssp40px",
                  },
                  "lblAmount":{
                    "text" : "",
                  },
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                response.push(jsonData); 
              }
            }
            else{
              if(this.detailsData[0].lblStatus === "pending" || this.detailsData[0].lblStatus === "Pending"){
                jsonData={
                  "lblRecipientname":{
                    "text" : kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
                    "skin":"sknlbl949494SSPR13px",
                  },
                  "lblAccountnumber":{
                    "text" : this.detailsData[0].receivedApprovals +" "+"of"+" "+this.detailsData[0].requiredApprovals+" "+"Approved",
                    "skin":"sknlbl424242ssp40px",
                  },
                  "lblAmount":{
                    "text" : "",
                  },
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                response.push(jsonData); 
              }
              else if(this.detailsData[0].lblStatus === "approved" || this.detailsData[0].lblStatus === "Approved"){
                jsonData={
                  "lblRecipientname":{
                    "text" : kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
                    "skin":"sknlbl949494SSPR13px",
                  },
                  "lblAccountnumber":{
                    "text":this.detailsData[0].requiredApprovals+" "+kony.i18n.getLocalizedString("kony.mb.approvalRequest.approvalstatus"),
                    "skin":"sknlbl424242ssp40px",
                  },
                  "lblAmount":{
                    "text" : "",
                  },
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                response.push(jsonData);                  
              }
              else if(this.detailsData[0].lblStatus === "rejected" || this.detailsData[0].lblStatus === "Rejected"){
                jsonData={
                  "lblRecipientname":{
                    "text" : kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
                    "skin":"sknlbl949494SSPR13px",
                  },
                  "lblAccountnumber":{
                    "text" : 1+" "+kony.i18n.getLocalizedString("kony.mb.achtransactions.Rejection"),
                    "skin":"sknlbl424242ssp40px",
                  },
                  "lblAmount":{
                    "text" : "",
                  },
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                response.push(jsonData); 
              }else{
                jsonData={
                  "lblRecipientname":{
                    "text" : kony.i18n.getLocalizedString("kony.mb.achtransactions.ApprovalStatus"),
                    "skin":"sknlbl949494SSPR13px",
                  },
                  "lblAccountnumber":{
                    "text" : this.detailsData[0].requiredApprovals+" "+kony.i18n.getLocalizedString("kony.mb.approvalRequest.approvalstatus"),
                    "skin":"sknlbl424242ssp40px",
                  },
                  "lblAmount":{
                    "text" : "",
                  },
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                response.push(jsonData); 
              }
            }
            if(Array.isArray(response)){
              this.view.segApprovalHistory.widgetDataMap={
                "lblRecipientname":"lblRecipientname",
                "lblAccountnumber":"lblAccountnumber",
                "lblAmount":"lblAmount",
                "flxSep":"flxSep",
                "flxSeperatorTrans4":"flxSeperatorTrans4",
              };
              if(response.length > 0){
                this.view.segApprovalHistory.setData(response);
              }else{
                this.view.segApprovalHistory.removeAll();
                nodataArr=[];
                nodataJson = {
                  "lblRecipientname":"No Request History Records Found",
                  "lblAccountnumber":"",
                  "lblAmount":"",
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                nodataArr.push(nodataJson);
                this.view.segApprovalHistory.setData(nodataArr);
              }
            }
          }
        }
      }catch(er){
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }
    },
    btnConfigApprovalDetails:function(formFlow,amICreator,amIApprover,status){
      try{
        if(status.toLowerCase() === "pending"){
          var approvalRejectPermission = "";

          if(formFlow === "ACHFileList"){
            approvalRejectPermission = applicationManager.getConfigurationManager().checkUserPermission("ACH_FILE_APPROVE");
          }else if(formFlow === "TransactionDetailsApprovals" || formFlow === "ACHTransactionsList" ){
            if(this.detailsData[0].lblPayment.indexOf("Payment") > -1){
              approvalRejectPermission = applicationManager.getConfigurationManager().checkUserPermission("ACH_PAYMENT_APPROVE");
            }else if(this.detailsData[0].lblPayment.indexOf("Collection") > -1){
              approvalRejectPermission = applicationManager.getConfigurationManager().checkUserPermission("ACH_COLLECTION_APPROVE");
            }
            else if(this.detailsData[0].lblPayment.indexOf("Tax") > -1) //condition check for federal tax records
            {
              approvalRejectPermission = applicationManager.getConfigurationManager().checkUserPermission("ACH_PAYMENT_APPROVE");  
            }
          }
          if(!approvalRejectPermission){
            this.view.flxbtnApproveReject.isVisible = false;
            this.view.flxbtnWithdraw.isVisible = false;
            return;
          }

          if(amIApprover === "true" && amICreator ==="false"){
            this.view.flxbtnApproveReject.isVisible = true;
            this.view.flxbtnWithdraw.isVisible = false;
          }else if(amIApprover === "false" && amICreator === "true"){
            this.view.flxbtnApproveReject.isVisible = false;
            this.view.flxbtnWithdraw.isVisible = true;
          }else if (amIApprover === "true" && amICreator === "true"){
            this.view.flxbtnApproveReject.isVisible = true;
            this.view.flxbtnWithdraw.isVisible = true;
          }else if(amIApprover === "false" && amICreator === "false"){
            this.view.flxbtnApproveReject.isVisible = false;
            this.view.flxbtnWithdraw.isVisible = false;
          }
        }else{
          this.view.flxbtnApproveReject.isVisible = false;
          this.view.flxbtnWithdraw.isVisible = false;
        }
      }catch(er){
      }
    }, 
    fetchErrorBack:function(response)
    {
      try {   
        if(!kony.sdk.isNullOrUndefined(response)){
          var scopeObj=this;
          var errorResponse = "";
          if(!kony.sdk.isNullOrUndefined(response.errorMessage)){
            errorResponse= response.errorMessage;
          }
          else{
            errorResponse= "Something went wrong while making service call.";
          }
          this.view.customPopup.lblPopup.text = errorResponse;    
          if(!kony.sdk.isNullOrUndefined(this.timerCounter)){
            this.timerCounter = parseInt(this.timerCounter)+1;
          }
          else{
            this.timerCounter = 1;
          }
          var timerId="timerPopupErrorACHTransactionDetail"+this.timerCounter;
          this.view.flxPopup.skin = "sknFlxf54b5e";
          this.view.customPopup.imgPopup.src = "errormessage.png";   
          this.view.flxPopup.setVisibility(true);
          kony.timer.schedule(timerId, function() {
            scopeObj.view.flxPopup.setVisibility(false);
          }, 1.5, false);            
        }
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }catch(error){
        kony.print("frmACHTransactions ACHFileListload_rowclick-->"+error);
        applicationManager.getPresentationUtility().dismissLoadingScreen();
      }      
    },

    onClickOkcommon:function()
    {
      var formType=this.previousFormType;
      if(formType=== "TransactionDetailsRequests" || formType=== "ACHTransactionDetailsRequests" || formType === "ACHFileListRequests"){
        this.withdrawServicecall();
      }else{
        this.rejectServicecall();
      }
    },

    /*This function is used to show the confiramation popup */
    confirmWithdrawalPopup:function()
    {
      try{
        var formType=this.previousFormType;
        this.view.rejectPopUp.lblTitle.text= kony.i18n.getLocalizedString("kony.mb.ApprovalRequests.Withdraw");
        this.view.rejectPopUp.lblYes.text=kony.i18n.getLocalizedString("kony.mb.ApprovalRequests.Withdraw");
        this.view.rejectPopUp.lblContent.text=kony.i18n.getLocalizedString("kony.mb.achwithdrawal.confirmpopup");
        this.view.rejectPopUp.lblContentreject.text=kony.i18n.getLocalizedString("kony.mb.withdrawreason");
        /**withdraw flow for Request**///
        if(formType === "TransactionDetailsRequests" || formType === "ACHTransactionDetailsRequests" || formType === "ACHFileListRequests"){
          var isiPhone = applicationManager.getPresentationFormUtility().getDeviceName() === "iPhone";
          if(isiPhone) {
            var msgText = kony.i18n.getLocalizedString("kony.mb.achwithdrawal.confirmpopup");
            var basicConfig = {message: msgText,
                               alertTitle:"",
                               alertIcon:null,
                               alertType: constants.ALERT_TYPE_CONFIRMATION,
                               yesLabel:applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.ApprovalRequests.Withdraw"),
                               noLabel: applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.AlertNo"),
                               alertHandler: this.withdrawConfirmIphone
                              };
            var pspConfig = {};
            applicationManager.getPresentationUtility().showAlertMessage(basicConfig, pspConfig);
          }else{
            this.view.flxRejectpopup.isVisible = true;
            this.view.rejectPopUp.lblTitle.isVisible = false;
            this.view.rejectPopUp.lblContent.skin = "sknLbl494949SSP40px";
            this.view.rejectPopUp.lblContentreject.skin = "sknLbl494949SSP40px";
            this.view.rejectPopUp.lblContent.isVisible = true;
            this.view.rejectPopUp.lblContentreject.isVisible = false;
            this.view.rejectPopUp.txtRejectreason.isVisible = false;
            this.view.rejectPopUp.lblContent.top = "25dp";
            this.view.rejectPopUp.flxBtns.top = "15dp";
            this.view.rejectPopUp.lblContent.text = kony.i18n.getLocalizedString("kony.mb.achwithdrawal.confirmpopup");
          }
        }
        else{  
          /**withdraw flow for ACH**/
          this.withdrawServicecall();       
        }

      }
      catch(er)
      {
        kony.print("catch of confirmWithdrawalPopup"+er); 
      }
    },

    withdrawConfirmIphone : function(response){
      if(response === true)
      {
        this.withdrawServicecall();
      }
    },
    /*This function is used to handle the  popup action*/
    withdrawHandler:function()
    { 
      this.withdrawServicecall();
    },

    /*This function is used to close the popup */
    closePopup:function(obj){
      try{
        kony.print("try of closePopup");
        var id = obj.id;
        switch(id){
          case"flxConfirmationPopUp":
          case "flxNo":
            this.view.flxConfirmationPopUp.isVisible=false;
            break;
          default:
            break;
        }
      }catch(er){
        kony.print("catch of closePopup"+er);
      }
    },
  };
});