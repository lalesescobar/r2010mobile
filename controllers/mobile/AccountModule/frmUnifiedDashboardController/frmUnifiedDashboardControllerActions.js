define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmUnifiedDashboard **/
    AS_Form_afdd309d4f7840038605f9d642d03cb8: function AS_Form_afdd309d4f7840038605f9d642d03cb8(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmUnifiedDashboard **/
    AS_Form_h735b85568be4874b480cbb5aa2ab465: function AS_Form_h735b85568be4874b480cbb5aa2ab465(eventobject) {
        var self = this;
        this.preshow();
    },
    /** postShow defined for frmUnifiedDashboard **/
    AS_Form_f7a6ed7a50ab441a98540d15de73db4b: function AS_Form_f7a6ed7a50ab441a98540d15de73db4b(eventobject) {
        var self = this;
        this.postShow();
    },
    /** onTouchStart defined for flxDummyHorizontalScroll **/
    AS_FlexScrollContainer_g6584c4709cc4755936aa5f22dde64ef: function AS_FlexScrollContainer_g6584c4709cc4755936aa5f22dde64ef(eventobject, x, y) {
        var self = this;
        this.bringFlxDashboardHeaderToFront()
    }
});