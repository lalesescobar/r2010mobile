define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_ab60ec5bfd274d3db17c6ce1e852acba: function AS_BarButtonItem_ab60ec5bfd274d3db17c6ce1e852acba(eventobject) {
        var self = this;
        var transferModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("BillPayModule");
        transferModule.presentationController.cancelCommon();
    },
    /** init defined for frmMMConfirmation **/
    AS_Form_f0fff1b04d1149d181104afde7bba03e: function AS_Form_f0fff1b04d1149d181104afde7bba03e(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmMMConfirmation **/
    AS_Form_c1311f5e11844409ba58bb4fa07f97f7: function AS_Form_c1311f5e11844409ba58bb4fa07f97f7(eventobject) {
        var self = this;
        this.preShow();
    }
});