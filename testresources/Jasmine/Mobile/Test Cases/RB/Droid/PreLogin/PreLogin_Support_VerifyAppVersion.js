it("Support_AppVersion", async function() {
	await kony.automation.playback.waitFor(["frmLogin","btnSupport"]);
	kony.automation.button.click(["frmLogin","btnSupport"]);

	await kony.automation.playback.waitFor(["frmSupport","flxAppVersion","lblAppVersion"]);
	var appVersion = kony.automation.widget.getWidgetProperty(["frmSupport","flxAppVersion","lblAppVersion"],"text");

	expect(appVersion).toEqual('App Version 2020.07.00');
	kony.automation.flexcontainer.click(["frmSupport","customHeader","flxBack"]);
	await kony.automation.playback.waitFor(["frmLogin","btnSupport"]);
});