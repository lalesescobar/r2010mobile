define({
  keypadString:'',
  isPeriodUsed : false,
  timerCounter: 0,
  init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
  },
  preShow: function(){
    if(applicationManager.getPresentationFormUtility().getDeviceName()==="iPhone"){
      // this.view.flxHeader.isVisible = false;
      this.view.flxHeader.setVisibility(false);
    }
    this.initActions();
  },
  postShow: function(){
    this.updateUI();
    this.view.txtFirstCheque.setFocus(true);
    this.view.txtLastCheque.onTextChange = this.textChange;
  },
  updateUI:function(){
    var transObj = applicationManager.getModulesPresentationController("ChequeManagementModule");
    var objData=transObj.getTransObject();
    if(objData.checkNumber1===null&&objData.checkNumber2===null){
      this.view.txtFirstCheque.text= "";
      this.view.txtLastCheque.text="";
      this.view.btnContinue.skin = "sknBtnOnBoardingInactive";
      this.view.btnContinue.setEnabled(false);
      this.view.lblDifference.setVisibility(false);
    }},
  initActions: function(){
    this.view.btnContinue.onClick =this.continueOnClick;
    this.view.customHeader.flxBack.onClick = function(){
    var commonBack = applicationManager.getModulesPresentationController("ChequeManagementModule");
    commonBack.commonFunctionForgoBack();
    };
    this.view.customHeader.btnRight.onClick =this.cancelOnClick;
  },
  cancelOnClick:function()
  {
    var presentation = applicationManager.getModulesPresentationController("ChequeManagementModule");
    presentation.commonCancel();
  },

  differenceNumber: function(){
    var firstCheque = this.view.txtFirstCheque.text;
    var lastCheque = this.view.txtLastCheque.text;
    var difference = (lastCheque - firstCheque)+1;
    this.view.lblDifference.text = difference+ " Cheques selected";
},
  textChange: function(){
    if(this.view.txtLastCheque.text === null || this.view.txtLastCheque.text === ""){
      this.view.btnContinue.skin = "sknBtnOnBoardingInactive";
      this.view.btnContinue.setEnabled(false);
      this.view.lblDifference.setVisibility(false);
    }else{
      this.view.btnContinue.skin = "sknBtn0095e4RoundedffffffSSP26px";
      this.view.btnContinue.setEnabled(true);
      this.view.lblDifference.setVisibility(true);
      this.differenceNumber();
    }
  },
  continueOnClick :function()
  {
var selectedFirstCheque = this.view.txtFirstCheque.text;
var selectedLastCheque = this.view.txtLastCheque.text;
//     var cheques = applicationManager.getModulesPresentationController("ChequeManagementModule");
//     cheques.getChequeNumbers(chequeNumbers);
//     if(selectedFirstCheque >= data.first && selectedFirstCheque <= data.last){
//       this.view.btnContinue.skin = "sknBtnOnBoardingInactive";
//       this.view.btnContinue.setEnabled(false);
//       this.bindGenericError(errorMsg);
//     }
//     else{
    var chequeSeries = applicationManager.getModulesPresentationController("ChequeManagementModule");
    chequeSeries.seriesChequeNumber(selectedFirstCheque, selectedLastCheque);
    var navMan = applicationManager.getModulesPresentationController("ChequeManagementModule");
                if( scope_ChequePresentationController.isReview==true){
         navMan.commonFunctionForNavigation("frmSCReview");
    }else{
             navMan.commonFunctionForNavigation("frmIssueDate");
            }
 //   }
    },
  bindGenericError: function (errorMsg) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var scopeObj = this;
    applicationManager.getDataProcessorUtility().showToastMessageError(scopeObj, errorMsg);
    this.view.lblSeriesError.setVisibility(true);
    this.view.lblSeriesError.text = errorMsg;
    this.view.lblSeriesError.skin = "sknLbl0095e422px";
    this.view.txtFirstCheque.skin = "sknTbx424242SSP28pxBorder1PxBgff5d6eTab";
    this.view.txtLastCheque.skin = "sknTbx424242SSP28pxBorder1PxBgff5d6eTab";
  }
});
