define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_gf50d36628d947ccb95d0c7f14fdc324: function AS_BarButtonItem_gf50d36628d947ccb95d0c7f14fdc324(eventobject) {
        var self = this;
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
        accountModule.presentationController.commonFunctionForNavigation("frmPFMTransactionDetails");
    },
    /** preShow defined for frmMMFrequency **/
    AS_Form_h03d342943174de9a7c752a5d922314c: function AS_Form_h03d342943174de9a7c752a5d922314c(eventobject) {
        var self = this;
        this.preshow();
    },
    /** init defined for frmMMFrequency **/
    AS_Form_d7d3b56876634362bfc2905c9f1387e8: function AS_Form_d7d3b56876634362bfc2905c9f1387e8(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmMMFrequency **/
    AS_Form_fb9fa1a1bc104f8881ee7b9d25f64747: function AS_Form_fb9fa1a1bc104f8881ee7b9d25f64747(eventobject) {
        var self = this;
        this.preShow();
    }
});