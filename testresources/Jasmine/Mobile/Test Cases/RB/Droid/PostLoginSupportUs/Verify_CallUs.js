it("Verify_CallUs", async function() {
	kony.automation.textbox.enterText(["frmLogin","tbxUsername"],"dbpolbuser");
	kony.automation.textbox.enterText(["frmLogin","tbxPassword"],"Kony@1234");
	kony.automation.button.click(["frmLogin","btnLogIn"]);
	await kony.automation.playback.waitFor(["frmDashboardAggregated","customHeader","flxBack"]);
	await kony.automation.playback.wait(1000);
	kony.automation.flexcontainer.click(["frmDashboardAggregated","customHeader","flxBack"]);
	kony.automation.gesture.swipe(["frmDashboardAggregated","flxHamburgerWrapper"], {"swipeDirection":0,"fingers":1,"point":[511.951904296875,1344.43310546875]});
	kony.automation.gesture.swipe(["frmDashboardAggregated","flxHamburgerWrapper"], {"swipeDirection":3,"fingers":1,"point":[514.984130859375,1171.402099609375]});
	kony.automation.gesture.swipe(["frmDashboardAggregated","flxHamburgerWrapper"], {"swipeDirection":0,"fingers":1,"point":[540.9722900390625,1314.9283447265625]});
	kony.automation.gesture.swipe(["frmDashboardAggregated","flxHamburgerWrapper"], {"swipeDirection":0,"fingers":1,"point":[569.959716796875,1304.42724609375]});
	kony.automation.gesture.swipe(["frmDashboardAggregated","flxHamburgerWrapper"], {"swipeDirection":0,"fingers":1,"point":[603.4625244140625,1307.916259765625]});
	kony.automation.segmentedui.click(["frmDashboardAggregated","Hamburger","segHamburger[0,16]"]);
	kony.automation.button.click(["frmSupport","btnCallBranch"]);
	await kony.automation.playback.wait(8000);
	kony.automation.flexcontainer.click(["frmSupport","customHeader","flxBack"]);
	kony.automation.widget.touch(["frmSupport","Hamburger","flxLogout"], [35,35],null,[35,35]);
});