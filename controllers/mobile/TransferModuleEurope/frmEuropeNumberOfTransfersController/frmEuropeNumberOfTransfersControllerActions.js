define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Form_a1a2c335eebc4b38b3e6ada3bf0b9914: function AS_Form_a1a2c335eebc4b38b3e6ada3bf0b9914(eventobject) {
        var self = this;
        this.init();
    },
    AS_Form_gdca5c2d92f0483c9d8e6c62f70e37ee: function AS_Form_gdca5c2d92f0483c9d8e6c62f70e37ee(eventobject) {
        var self = this;
        this.preShow();
    },
    AS_BarButtonItem_gf7b12b426f14f36aec4f9395f6ab07a: function AS_BarButtonItem_gf7b12b426f14f36aec4f9395f6ab07a(eventobject) {
        var self = this;
        this.navigateCustomBack();
    },
    AS_BarButtonItem_jef7b6412af1480bbda9cb8214a5dee2: function AS_BarButtonItem_jef7b6412af1480bbda9cb8214a5dee2(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    AS_Button_e47f0dcc2dc540139742e422fe62b17d: function AS_Button_e47f0dcc2dc540139742e422fe62b17d(eventobject) {
        var self = this;
        this.setKeypadChar(8);
    },
    AS_Button_j2886dd1ba034b50a748ad568218b966: function AS_Button_j2886dd1ba034b50a748ad568218b966(eventobject) {
        var self = this;
        this.setKeypadChar(5);
    },
    AS_Button_i8d5ac6fb4094b5eb2108ee7a147f3a0: function AS_Button_i8d5ac6fb4094b5eb2108ee7a147f3a0(eventobject) {
        var self = this;
        this.setKeypadChar(4);
    },
    AS_Button_e0ce4f39acf34408968f5e3f534e2158: function AS_Button_e0ce4f39acf34408968f5e3f534e2158(eventobject) {
        var self = this;
        this.setKeypadChar(9);
    },
    AS_Button_d0fd90a6b5034f4e8956a5fbc3461af1: function AS_Button_d0fd90a6b5034f4e8956a5fbc3461af1(eventobject) {
        var self = this;
        this.setKeypadChar(1);
    },
    AS_Button_c96c70d40e5c4a62ba0f29f600644a94: function AS_Button_c96c70d40e5c4a62ba0f29f600644a94(eventobject) {
        var self = this;
        this.setKeypadChar(7);
    },
    AS_Button_c3531e91a21149d8bef4c8ce9564b4d0: function AS_Button_c3531e91a21149d8bef4c8ce9564b4d0(eventobject) {
        var self = this;
        this.setKeypadChar(6);
    },
    AS_Button_f19db7fc4af54d579a674b504920d0f8: function AS_Button_f19db7fc4af54d579a674b504920d0f8(eventobject) {
        var self = this;
        this.setKeypadChar(3);
    },
    AS_Button_c327c7581e664486b7e17d08ba683a81: function AS_Button_c327c7581e664486b7e17d08ba683a81(eventobject) {
        var self = this;
        this.setKeypadChar(2);
    },
    AS_Button_j8643954943c4785816f875482d9eb1f: function AS_Button_j8643954943c4785816f875482d9eb1f(eventobject) {
        var self = this;
        this.setKeypadChar(0);
    },
    AS_Image_ed120b5930d2494ca268f95bce570c82: function AS_Image_ed120b5930d2494ca268f95bce570c82(eventobject, x, y) {
        var self = this;
        this.clearKeypadChar();
    }
});