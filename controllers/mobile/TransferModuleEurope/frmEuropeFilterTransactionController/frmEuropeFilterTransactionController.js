define({
  init: function () {
    var scope=this;
    var currentFormObject = kony.application.getCurrentForm();
    var currentForm=currentFormObject.id;
    applicationManager.getPresentationFormUtility().initCommonActions(this, "CALLBACK", currentForm, scope.navigateCustomBack);
  },
  navigateCustomBack: function() {
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    transMod.commonFunctionForgoBack();
  },
  preShow: function () {
    if (kony.os.deviceInfo().name === "iPhone") {
      this.view.flxHeader.isVisible = false;
    } else {
      this.view.flxHeader.isVisible = true;
    }
    this.setupUI();
    this.initActions();
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  },

  setupUI: function() {
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    if (transMod.filterTypeSelected === "BOTH") {
      this.markShowPayments(true);
      this.markShowTransfers(true);
    }
    else if (transMod.filterTypeSelected === "TRANSFER") {
      this.markShowPayments(false);
      this.markShowTransfers(true);
    }
    else {
      this.markShowPayments(true);
      this.markShowTransfers(false);
    }
    
    if (this.view.imgPaymentsCheck.src === "checkboxempty.png" && this.view.imgTransfersCheck.src === "checkboxempty.png") {
      this.disableApplyButton();
    }
    else {
      this.enableApplyButton();
    }
  },

  markShowPayments: function(flag) {
    this.view.imgPaymentsCheck.src = (flag) ? "checkboxtick.png" : "checkboxempty.png";
  },

  markShowTransfers: function(flag) {
    this.view.imgTransfersCheck.src = (flag) ? "checkboxtick.png" : "checkboxempty.png";
  },
  
  enableApplyButton: function() {
    this.view.btnApply.setEnabled(true);
    this.view.btnApply.skin = "sknBtn0095e4RoundedffffffSSP26px";
  },

  disableApplyButton: function() {
    this.view.btnApply.setEnabled(false);
    this.view.btnApply.skin = "sknBtnE2E9F0SSP31PX";
  },

  initActions: function () {
    this.view.customHeader.btnRight.onClick = this.cancelOnClick;
    this.view.customHeader.flxBack.onClick = this.navigateCustomBack;
    this.view.flxShowPayments.onClick = this.flxShowPaymentsOnClick;
    this.view.flxShowTransfers.onClick = this.flxShowTransfersOnClick; 
    this.view.btnApply.onClick = this.btnApplyOnClick;
  },

  flxShowPaymentsOnClick: function () {
    if (this.view.imgPaymentsCheck.src === "checkboxempty.png")
      this.markShowPayments(true);
    else
      this.markShowPayments(false);
    if (this.view.imgPaymentsCheck.src === "checkboxempty.png" && this.view.imgTransfersCheck.src === "checkboxempty.png") {
      this.disableApplyButton();
    }
    else {
      this.enableApplyButton();
    }
  },

  flxShowTransfersOnClick: function () {
    if (this.view.imgTransfersCheck.src === "checkboxempty.png")
      this.markShowTransfers(true);
    else
      this.markShowTransfers(false);
    if (this.view.imgPaymentsCheck.src === "checkboxempty.png" && this.view.imgTransfersCheck.src === "checkboxempty.png") {
      this.disableApplyButton();
    }
    else {
      this.enableApplyButton();
    }
  },

  cancelOnClick: function() {
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    transMod.cancelCommon();
  },

  btnApplyOnClick: function() {
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    if (this.view.imgPaymentsCheck.src === "checkboxtick.png" && this.view.imgTransfersCheck.src === "checkboxtick.png") {
      transMod.filterTypeSelected = "BOTH";
    }
    else if (this.view.imgPaymentsCheck.src === "checkboxtick.png" && this.view.imgTransfersCheck.src === "checkboxempty.png") {
      transMod.filterTypeSelected = "PAYMENT";
    }
    else {
      transMod.filterTypeSelected = "TRANSFER";
    }
    transMod.commonFunctionForNavigation("frmEuropeTransferActivities");
  }

});