define(["CommonUtilities"], function(CommonUtilities) {
  /**
     * User defined presentation controller
     * @constructor
     * @extends kony.mvc.Presentation.BasePresenter
     */
  function PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);  
  }

  inheritsFrom(PresentationController, kony.mvc.Presentation.BasePresenter);

  /**
     * Overridden Method of kony.mvc.Presentation.BasePresenter
     * This method gets called when presentation controller gets initialized
     * @method
     */
  PresentationController.prototype.initializePresentationController = function() {
    this.ACHManager = applicationManager.getACHManager();

  };

  PresentationController.prototype.commonFunctionForNavigation = function(formName) {
    var navManager = applicationManager.getNavigationManager();
    navManager.navigateTo(formName);
  };

  PresentationController.prototype.commonFunctionNavigationWithData = function(formname) {   
    var navigateToForm = new kony.mvc.Navigation(formname);   
    navigateToForm.navigate();
  };

  PresentationController.prototype.commonNavigationFunction = function(formname) {  
    var navigateToForm = new kony.mvc.Navigation(formname);   
    navigateToForm.navigate();
  };

  /** getACHTransactionsData : Method to Fetch all ACH Transactions - Business Controller Call*///

  PresentationController.prototype.getACHTransactionsData = function ( navObj ) {  
    var scopeObj = this;
    var params = navObj.requestData; 
    var ACHmodule= kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHManager').businessController;
    ACHmodule.getACHTransactionsData( params,
                                     scopeObj.onGetACHTransactionsSuccess,
                                     scopeObj.onGetACHTransactionsFailure);

  };

  PresentationController.prototype.onGetACHTransactionsSuccess = function ( response ){  

    var proccessedResponse = PresentationController.prototype.dataProcessorForGenerateTransaction(response);
    var viewController = applicationManager.getPresentationUtility().getController('frmACHTransactions', true);
    viewController.fetchACHTransactionSuccessCallBack(proccessedResponse);
  };

  PresentationController.prototype.onGetACHTransactionsFailure = function ( responseError ) { 
    var viewController = applicationManager.getPresentationUtility().getController('frmACHTransactions', true);
    viewController.fetchErrorcallBack(responseError);
  };

  /**
    * dataProcessorForGenerateTransaction : Method to format Data for ACH Trasactions  */// 
  PresentationController.prototype.dataProcessorForGenerateTransaction = function ( response ) { 
    try{     
      var setdataarray=[];
      var configManager = applicationManager.getConfigurationManager();
      var isiPhone = applicationManager.getPresentationFormUtility().getDeviceName() === "iPhone";
      if(Array.isArray(response)){
        for(var i=0;i<response.length;i++){
          var requestType="";
          if(!kony.sdk.isNullOrUndefined(response[i].RequestType) && (response[i].RequestType.includes("PPD") || response[i].RequestType.includes("CCD") || response[i].RequestType.includes("CTX")) ) {
            reqtype=String(response[i].RequestType).split("(");
            requestType=(response[i].RequestType).substring(0, 3) + " " + response[i].TransactionTypeValue;
          }
          if(!kony.sdk.isNullOrUndefined(response[i].RequestType) && response[i].RequestType.includes("Web") ) {
            requestType = response[i].RequestType;
          }
          if(!kony.sdk.isNullOrUndefined(response[i].RequestType) && response[i].RequestType.includes("Tax") ) {
            requestType = response[i].RequestType + " " + response[i].TransactionTypeValue;
          }

         if(!kony.sdk.isNullOrUndefined(response[i].EffectiveDate)) {
            response[i].EffectiveDate = response[i].EffectiveDate.slice(0, 10) + "T12:00:00.001Z";
          }

          var parsedResponse={
            template:"flxAchtransation",
            "data":response[i],
            "lblPayment": requestType,
            "lblToAccount":"To: "+CommonUtilities.truncateStringWithGivenLength(response[i].AccountName+"....",20)+CommonUtilities.getLastSixDigit(response[i].AccountName),
            "lblAmount": configManager.getCurrencyCode()+""+CommonUtilities.formatCurrencyWithCommas(response[i].TotalAmount, true),
            "lblDate":CommonUtilities.getFrontendDateString(response[i].EffectiveDate,"mm/dd/yyyy"),
            "createdOn":CommonUtilities.getFrontendDateString(response[i].CreatedOn,"mm/dd/yyyy"),
            "amount":kony.sdk.isNullOrUndefined(response[i].MaxAmount)?"-": response[i].MaxAmount,
            "createdBy":kony.sdk.isNullOrUndefined(response[i].userName)?"-": response[i].userName,
            "Status":kony.sdk.isNullOrUndefined(response[i].Status)?"-": response[i].Status,
            "TemplateRequestType_id":kony.sdk.isNullOrUndefined(response[i].TemplateRequestType_id)?"-": response[i].TemplateRequestType_id,
            "Request_id":kony.sdk.isNullOrUndefined(response[i].Request_id)?"-": response[i].Request_id,
            "userName":kony.sdk.isNullOrUndefined(response[i].Request_id)?"-": response[i].Request_id, 
            "TotalAmount": configManager.getCurrencyCode()+""+CommonUtilities.formatCurrencyWithCommas(response[i].TotalAmount, true),
            "MaxAmount": configManager.getCurrencyCode()+""+CommonUtilities.formatCurrencyWithCommas(response[i].MaxAmount, true),
            "ConfirmationNumber":kony.sdk.isNullOrUndefined(response[i].ConfirmationNumber)?"-": response[i].ConfirmationNumber,
            "DebitAccount":kony.sdk.isNullOrUndefined(response[i].DebitAccount)?"-": response[i].DebitAccount,
            "TemplateType_id":kony.sdk.isNullOrUndefined(response[i].TemplateType_id)?"-": response[i].TemplateType_id,
            "CompanyName":kony.sdk.isNullOrUndefined(response[i].CompanyName)?"-": response[i].CompanyName,
            "RequestType":kony.sdk.isNullOrUndefined(response[i].RequestType)?"-": response[i].RequestType,
            "TemplateDescription":kony.sdk.isNullOrUndefined(response[i].TemplateDescription)?"-": response[i].TemplateDescription,
            "TemplateTypeValue":kony.sdk.isNullOrUndefined(response[i].TemplateTypeValue)?"-": response[i].TemplateTypeValue,
            "TransactionType_id":kony.sdk.isNullOrUndefined(response[i].TransactionType_id)?"-": response[i].TransactionType_id,
            //"createdby":kony.sdk.isNullOrUndefined(response[i].createdby)?"-": response[i].createdby,   
            "Company_id":kony.sdk.isNullOrUndefined(response[i].Company_id)?"-": response[i].Company_id,    
            "Transaction_id":kony.sdk.isNullOrUndefined(response[i].Transaction_id)?"-": response[i].Transaction_id,
            "EffectiveDate":CommonUtilities.getFrontendDateString(response[i].EffectiveDate,"mm/dd/yyyy"),
            "TemplateName":kony.sdk.isNullOrUndefined(response[i].TemplateName)?"-": response[i].TemplateName,
            "featureActionId":kony.sdk.isNullOrUndefined(response[i].featureActionId)?"-": response[i].featureActionId,
            "AccountName":kony.sdk.isNullOrUndefined(response[i].AccountName)?"-": response[i].AccountName,
            "requiredApprovals":kony.sdk.isNullOrUndefined(response[i].requiredApprovals)?"-": response[i].requiredApprovals,
            "receivedApprovals":kony.sdk.isNullOrUndefined(response[i].receivedApprovals)?"-": response[i].receivedApprovals,
            "amICreator":kony.sdk.isNullOrUndefined(response[i].amICreator)?"-": response[i].amICreator,
            "amIApprover":kony.sdk.isNullOrUndefined(response[i].amIApprover)?"-": response[i].amIApprover,
            "TransactionTypeValue":kony.sdk.isNullOrUndefined(response[i].TransactionTypeValue)?"-": response[i].TransactionTypeValue,
          };
          setdataarray.push(parsedResponse);
        }
      }
      return(setdataarray);
    }catch(err){
      kony.print("err--"+err);
    }
  };

  PresentationController.prototype.getDestinationAccountsRecords = function( navObject ) {
    var scopeObj = this;
    scopeObj.ACHManger = applicationManager.getACHManager();
    var ACHmodule= kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHManager').businessController;
    ACHmodule.fetchACHTransactionRecords(
      navObject.requestData,
      scopeObj.getDestinationAccountsRecordsSuccess,
      scopeObj.getDestinationAccountsRecordsFailure,
    );
      };
      PresentationController.prototype.getDestinationAccountsRecordsSuccess = function (response) {
      try
      {
        var subRecordsMap = [];
        var isDone = false;
        var isValid = false;
        var successCallSubrecord = function(TransactionRecord_id, subrecords) {
          if(subrecords === null || subrecords === [] || subrecords === undefined)
            subRecordsMap[TransactionRecord_id] = [];
          else
            subRecordsMap[TransactionRecord_id] = subrecords;

          for (var subrecord in subRecordsMap) {
            if(subRecordsMap[subrecord] === null) {
              isDone = false;
              break;
            }
            isDone = true;
          }

          if(isDone === true) {
            for (var subRecord in subRecordsMap) {
              if (subRecordsMap[subRecord] === "error") {
                isValid = false;
                break;
              }
              isValid = true;
            }
            if (isValid === true) { 
              response.forEach(function(obj) {
                if (obj.TransactionRecord_id) {
                  obj.taxSubType = subRecordsMap[obj.TransactionRecord_id][0].taxSubType;
                  obj.TaxSubCategory_id = subRecordsMap[obj.TransactionRecord_id][0].TaxSubCategory_id;
                  obj.TranscationSubRecord_id = subRecordsMap[obj.TransactionRecord_id][0].TranscationSubRecord_id;
                  obj.Amount = CommonUtilities.formatCurrencyWithCommas(subRecordsMap[obj.TransactionRecord_id][0].Amount,true);
                }
                var proccessedResponse = PresentationController.prototype.dataProcessorForDestinationSubAccnts(response);
      var viewController = applicationManager.getPresentationUtility().getController('frmACHTransactionDetail', true);
      viewController.getACHFilesDestinationAccntSuccessCallBack(proccessedResponse);

              });

            } 
            else {
              var viewController = applicationManager.getPresentationUtility().getController('frmACHTransactionDetail', true);
              viewController.fetchErrorBack(response);
            }
          }
        };

        var failureCallSubrecord = function(TransactionRecord_id) {
          subRecordsMap[TransactionRecord_id] = "error";
          var viewController = applicationManager.getPresentationUtility().getController('frmACHTransactionDetail', true);
          viewController.fetchErrorBack(response);
        };

        var scopeObj=this;
        if(!kony.sdk.isNullOrUndefined(response)){
          response.forEach(function(obj) {
            if (obj.TransactionRecord_id && /Tax/.test(obj.TemplateRequestTypeValue)) {
              subRecordsMap[obj.TransactionRecord_id] = null;
            }
          });
          //for federal tax record need to call getACHTransactionSubRecords
          if(Object.keys(subRecordsMap).length !== 0) {
            response.forEach(function(obj) {
              if (obj.TransactionRecord_id && /Tax/.test(obj.TemplateRequestTypeValue)) {
                PresentationController.prototype.getACHTransactionSubRecords(obj.TransactionRecord_id, 
                                                                             successCallSubrecord.bind(scopeObj,obj.TransactionRecord_id),
                                                                             failureCallSubrecord.bind(scopeObj,obj.TransactionRecord_id));
              }
            });
          }
          else
          {
            var proccessedResponse = PresentationController.prototype.dataProcessorForDestinationAccnts(response);
            var viewController = applicationManager.getPresentationUtility().getController('frmACHTransactionDetail', true);
            viewController.getACHFilesDestinationAccntSuccessCallBack(proccessedResponse);
          }
        }
      }
      catch(err)
      {
       kony.print("Error in getDestinationAccountsRecordsSuccess"+err);
      }
    };
      PresentationController.prototype.getDestinationAccountsRecordsFailure = function (response) {
        var viewController = applicationManager.getPresentationUtility().getController('frmACHTransactionDetail', true);
        viewController.fetchErrorBack(response);
      };
    PresentationController.prototype.dataProcessorForDestinationAccnts = function(response){
      try{
        var templateData=[];
        var jsonData;
        if(!kony.sdk.isNullOrUndefined(response)){
          if(Array.isArray(response)){
            if(response.length > 0){
              for(var i = 0; i < response.length;i++){
                if(i !== (response.length) -1 ){
                  jsonData={
                  "lblRecipientname":kony.sdk.isNullOrUndefined(response[i].Record_Name)?"-":response[i].Record_Name,
                  "lblAccountnumber":kony.sdk.isNullOrUndefined(response[i].ToAccountNumber)?"-":response[i].ToAccountNumber,
                  "lblAmount":kony.sdk.isNullOrUndefined(response[i].Amount)?"-":CommonUtilities.formatCurrencyWithCommas(response[i].Amount),
                  "flxSep":{isVisible:true},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                }else{
                  jsonData={
                  "lblRecipientname":kony.sdk.isNullOrUndefined(response[i].Record_Name)?"-":response[i].Record_Name,
                  "lblAccountnumber":kony.sdk.isNullOrUndefined(response[i].ToAccountNumber)?"-":response[i].ToAccountNumber,
                  "lblAmount":kony.sdk.isNullOrUndefined(response[i].Amount)?"-":CommonUtilities.formatCurrencyWithCommas(response[i].Amount),
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
                }
                templateData.push(jsonData);
              }
            }
          }
        }
        return  templateData;
      }catch(er){

      }
    };
    
    PresentationController.prototype.getACHFileSubRecord = function(navobj){
      try{
        var scopeObj = this;
        scopeObj.ACHManger = applicationManager.getACHManager();
        var ACHmodule= kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHManager').businessController;
        ACHmodule.fetchACHFileSubRecords(
          navobj.requestData,
          scopeObj.getACHFileSubRecordSuccess,
          scopeObj.getACHFileSubRecordFailure);
      }catch(er){

      }
    },
      PresentationController.prototype.getACHFileSubRecordSuccess = function(response){
        try{
        var proccessedResponse = PresentationController.prototype.dataProcessorForACHFileSubRecord(response.AchFileSubrecords);
        var viewController = applicationManager.getPresentationUtility().getController('frmACHTransactionDetail', true);
        viewController.getACHFilesDestinationAccntSuccessCallBack(proccessedResponse);
        }catch(er){
          
        }
      },
      PresentationController.prototype.dataProcessorForACHFileSubRecord = function(response){
      
      try{
        var templateData = [];
        var jsonData = {};
        if(!kony.sdk.isNullOrUndefined(response)){
          if(Array.isArray(response)){
            if(response.length > 0){
              for(var i = 0; i < response.length;i++){
                if(i !== (response.length) -1 ){
                jsonData={
                  "lblRecipientname":kony.sdk.isNullOrUndefined(response[i].receiverName)?"-":response[i].receiverName,
                  "lblAccountnumber":kony.sdk.isNullOrUndefined(response[i].receiverAccountNumber)?"-":response[i].receiverAccountNumber,
                  "lblAmount":kony.sdk.isNullOrUndefined(response[i].amount)?"-":CommonUtilities.formatCurrencyWithCommas(response[i].amount),
                  "flxSep":{isVisible:true},
                  "flxSeperatorTrans4":{isVisible:false},
                };
              }else{
                jsonData={
                  "lblRecipientname":kony.sdk.isNullOrUndefined(response[i].receiverName)?"-":response[i].receiverName,
                  "lblAccountnumber":kony.sdk.isNullOrUndefined(response[i].receiverAccountNumber)?"-":response[i].receiverAccountNumber,
                  "lblAmount":kony.sdk.isNullOrUndefined(response[i].amount)?"-":CommonUtilities.formatCurrencyWithCommas(response[i].amount),
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
              }
              templateData.push(jsonData);
              }
            }
          }
          return templateData;
        }
        
      }catch(er){
        
      }
      
    },
      PresentationController.prototype.getACHFileSubRecordFailure = function(errorResponse){
        try{
          var viewController = applicationManager.getPresentationUtility().getController('frmACHTransactionDetail', true);
          viewController.fetchErrorcallBack(errorResponse);
        }catch(er){
          
        }
      },
    
    PresentationController.prototype.getACHFileRecord = function(navObj){
      try{
        var scopeObj = this;
        scopeObj.ACHManger = applicationManager.getACHManager();
        var ACHmodule= kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHManager').businessController;
        ACHmodule.fetchACHFileRecords(
          navObj.requestData,
          scopeObj.getACHFileRecordRecordsSuccess,
          scopeObj.getACHFileRecordFailure);
      }catch(er){

      }
    },
      
       PresentationController.prototype.dataProcessorForACHFileRecord = function (response) {
           try {
             if (!kony.sdk.isNullOrUndefined(response.AchFileRecords)) {
               if (Array.isArray(response.AchFileRecords)) {
                 if (response.AchFileRecords.length === 1) {
                   var achFileRecordId = response.AchFileRecords[0].achFileRecordId;
                   return achFileRecordId;
                 }
                 if (response.AchFileRecords.length > 1) {
                   var arr = [];
                   response.AchFileRecords.forEach(function (data) {
                     arr.push(data.achFileRecordId);
                   });
                   return arr;
                 }
               }
             }
           } catch (er) {}
         },

         PresentationController.prototype.fileDataStore = function (response) {
           try {
             var subRecords = response.AchFileSubrecords;
             kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule').presentationController.files++;
             subRecords.forEach(function (data) {
               kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule').presentationController.fileCollection.push(data);
             });
           } catch (er) {}
           try {
             if (kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule').presentationController.files >= kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule').presentationController.filesLength) {
               PresentationController.prototype.dataProcessorForACHFileSubRecord(kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule').presentationController.fileCollection);
               try {
                 var data = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule').presentationController.fileCollection;
                 var proccessedResponse = PresentationController.prototype.dataProcessorForACHFileSubRecord(data);
                 var viewController = applicationManager.getPresentationUtility().getController('frmACHTransactionDetail', true);
                 viewController.getACHFilesDestinationAccntSuccessCallBack(proccessedResponse);
               } catch (er) {}
             }
           } catch (er) {}
         },

         PresentationController.prototype.fetchFileData = function (response) {
           try {
             var ACHFileRecordID = response;
             var scopeObj = this;
             scopeObj.ACHManger = applicationManager.getACHManager();
             var ACHmodule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHManager').businessController;
             ACHmodule.fetchACHFileSubRecords({
                 "achFileRecordId": ACHFileRecordID
               },
               kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule').presentationController.fileDataStore,
               kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule').presentationController.getACHFileSubRecordFailure);
           } catch (er) {}
         },
      
     PresentationController.prototype.getACHFileRecordRecordsSuccess = function(response){
      try{
        
        if(!kony.sdk.isNullOrUndefined(response.AchFileRecords)){
          if (Array.isArray(response.AchFileRecords)) {
            if (response.AchFileRecords.length >0){
        var proccessedResponse = PresentationController.prototype.dataProcessorForACHFileRecord(response);
        var navigationManager = applicationManager.getNavigationManager();
        var originFormName = navigationManager.getCustomInfo("formFlow");
        var viewController = "";
        if(Array.isArray(proccessedResponse) && originFormName === "ACHFileList") { //limiting to ACHFileList as other flows might cause issues!
          kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule').presentationController.files = 0;
          kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule').presentationController.filesLength = proccessedResponse.length;
          kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule').presentationController.fileCollection = [];
          proccessedResponse.forEach(function(data) {
            kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHModule').presentationController.fetchFileData(data);
          });
        }
        else {
          if(originFormName === "ACHFileList"){
            viewController = applicationManager.getPresentationUtility().getController('frmACHTransactions', true);
            viewController.getACHFileRecordsSuccessCB(proccessedResponse);
          }else if(originFormName === "ACHFileListApprovals"){
            viewController = applicationManager.getPresentationUtility().getController('frmApprovalsList', true);
            viewController.getACHFileRecordsSuccessCB(proccessedResponse);
          }else if(originFormName === "ACHFileListRequests"){
            viewController = applicationManager.getPresentationUtility().getController('frmRequestList', true);
            viewController.getACHFileRecordsSuccessCB(proccessedResponse);
          }
        }
      }}}

      }catch(er){}
    },
       PresentationController.prototype.getACHFileRecordFailure = function(errorResponse){
      try{
        var viewController = applicationManager.getPresentationUtility().getController('frmACHTransactionDetail', true);
        viewController.fetchErrorcallBack(errorResponse);
      }catch(er){
        
      }
    },
    PresentationController.prototype.getAllACHFiles = function(navObj) { 
      var scopeObj = this;
      scopeObj.ACHManger = applicationManager.getACHManager();
      var ACHmodule= kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHManager').businessController;
      ACHmodule.getFilesData(
        navObj.requestData,
        scopeObj.getACHFilesTransactionRecordsSuccess,
        scopeObj.getACHFilesTransactionRecordsFailure);
    };
    PresentationController.prototype.dataProcessorForFiles = function(response){
      var templateData=[];
      var isiPhone = applicationManager.getPresentationFormUtility().getDeviceName() === "iPhone";
      var configManager = applicationManager.getConfigurationManager();
      if(!kony.sdk.isNullOrUndefined(response)){
        if(Array.isArray(response)){
          if(response.length > 0){
            for(var i = 0; i < response.length;i++){
              if(!kony.sdk.isNullOrUndefined(response[i].UpdatedDateAndTime) && response[i].UpdatedDateAndTime !== "") {
                if(isiPhone && response[i].UpdatedDateAndTime.includes(" ")) {
                  response[i].UpdatedDateAndTime = response[i].UpdatedDateAndTime.replace(" ", "T") + "Z";
                }
              }
              var jsonData={
                template:"flxAchFilelist",
                "data":response[i],
                "lblFilename":kony.sdk.isNullOrUndefined(response[i].FileName)?"-": response[i].FileName,
                "lblAdmin":kony.sdk.isNullOrUndefined(response[i].userName)?"-": response[i].userName,
                "lblStatus":kony.sdk.isNullOrUndefined(response[i].FileStatus)?"-": response[i].FileStatus,
                "fileType":kony.sdk.isNullOrUndefined(response[i].FileFormatType)?"-": response[i].FileFormatType,
                "RequestType":kony.sdk.isNullOrUndefined(response[i].FileRequestType)?"-": response[i].FileRequestType,
                "uploadDate":kony.sdk.isNullOrUndefined(response[i].UpdatedDateAndTime)?"-":CommonUtilities.getFrontendDateString( response[i].UpdatedDateAndTime,"mm/dd/yyyy"),
                "totalDebitAccount": configManager.getCurrencyCode()+""+CommonUtilities.formatCurrencyWithCommas(response[i].TotalDebitAmount, true),
                "totalCreditAccount":configManager.getCurrencyCode()+""+CommonUtilities.formatCurrencyWithCommas(response[i].TotalCreditAmount, true),
                "numberOfDebits":kony.sdk.isNullOrUndefined(response[i].NumberOfDebits)?"-": response[i].NumberOfDebits,
                "numberOfCredits":kony.sdk.isNullOrUndefined(response[i].NumberOfCredits)?"-": response[i].NumberOfCredits,
                "numberOfPreNotes":kony.sdk.isNullOrUndefined(response[i].NumberOfPrenotes)?"-": response[i].NumberOfPrenotes,
                "numberOfrecords":kony.sdk.isNullOrUndefined(response[i].NumberOfRecords)?"-": response[i].NumberOfRecords,
                "ACHFileID":kony.sdk.isNullOrUndefined(response[i].ACHFileID)?"-": response[i].ACHFileID,
                "amICreator":kony.sdk.isNullOrUndefined(response[i].amICreator)?"-": response[i].amICreator,
                "amIApprover":kony.sdk.isNullOrUndefined(response[i].amIApprover)?"-": response[i].amIApprover,
                "Request_id":kony.sdk.isNullOrUndefined(response[i].Request_id)?"-": response[i].Request_id,
                "receivedApprovals":kony.sdk.isNullOrUndefined(response[i].receivedApprovals)?"-": response[i].receivedApprovals,
                "requiredApprovals":kony.sdk.isNullOrUndefined(response[i].requiredApprovals)?"-": response[i].requiredApprovals,
                "flxsep":{skin:"sknFlxe3e3e3",isVisible:true},
              };
              templateData.push(jsonData);
            }
          }
        }
      }

      return templateData;
    },
      PresentationController.prototype.getACHFilesTransactionRecordsSuccess = function (response) {
      var proccessedResponse = PresentationController.prototype.dataProcessorForFiles(response);
      var viewController = applicationManager.getPresentationUtility().getController('frmACHTransactions', true);
      viewController.getACHFilesTransactionRecordsSuccessCallBack(proccessedResponse);
    },
      PresentationController.prototype.getACHFilesTransactionRecordsFailure = function (responseError) {
      var viewController = applicationManager.getPresentationUtility().getController('frmACHTransactions', true);
      viewController.fetchErrorcallBack(responseError);
    };
    PresentationController.prototype.onGetApprovalCountsSuccess = function (response){ 
      var navigationManager = applicationManager.getNavigationManager();
      var originFormName = navigationManager.getCustomInfo("originFormForApprovalCount");
      navigationManager.setCustomInfo("originFormForApprovalCount", null);
      var viewController = applicationManager.getPresentationUtility().getController(originFormName, true);
      viewController.getApprovalCountsSuccessCallBack(response);
    },
      PresentationController.prototype.onGetApprovalCountsFailure = function (errorResponse){
      var navigationManager = applicationManager.getNavigationManager();
      var originFormName = navigationManager.getCustomInfo("originFormForApprovalCount");
      navigationManager.setCustomInfo("originFormForApprovalCount", null);
      var viewController = applicationManager.getPresentationUtility().getController(originFormName, true);
      viewController.fetchErrorBack(errorResponse);
    },
    
    PresentationController.prototype.getApprovalCounts = function (originFormName) {
      var scopeObj = this;
      scopeObj.ACHManger = applicationManager.getACHManager();
      var ACHmodule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHManager').businessController;
      var navigationManager = applicationManager.getNavigationManager();
      navigationManager.setCustomInfo("originFormForApprovalCount", originFormName);
      ACHmodule.fetchApprovalRequestCounts(scopeObj.onGetApprovalCountsSuccess.bind(originFormName),scopeObj.onGetApprovalCountsFailure.bind(originFormName));
    };

    PresentationController.prototype.getACHTransactionSubRecords = function(transactionId, successcallback, failurecallback) {
      var scopeObj = this;
      var requestInputs = { "TransactionRecord_id" : transactionId};
      var ACHmodule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule('ACHManager').businessController;
      ACHmodule.fetchACHTransactionSubRecords(
        requestInputs,
        scopeObj.getACHTransactionSubRecordsSuccess.bind(scopeObj, successcallback),
        scopeObj.getACHTransactionSubRecordsFailure.bind(scopeObj, failurecallback)
      );
    };
    PresentationController.prototype.getACHTransactionSubRecordsSuccess = function(successcallback, response) {
      successcallback(response);
    };

    PresentationController.prototype.getACHTransactionSubRecordsFailure = function(failurecallback){
      failurecallback();
      
    };
    PresentationController.prototype.dataProcessorForDestinationSubAccnts = function(response){
      try{
        var jsonData;
        var templateData=[];
        if(Array.isArray(response)){
          if(response.length > 0){
            for(var i = 0; i < response.length;i++){
              if(i !== (response.length) -1 ){
                jsonData={
                  "lblRecipientname":kony.sdk.isNullOrUndefined(response[i].TaxType)?"-":response[i].TaxType,
                  "lblAccountnumber":kony.sdk.isNullOrUndefined(response[i].ToAccountNumber)?"-":response[i].ToAccountNumber,
                  "lblAmount":kony.sdk.isNullOrUndefined(response[i].Amount)?"-":CommonUtilities.formatCurrencyWithCommas(response[i].Amount),
                  "flxSep":{isVisible:true},
                  "flxSeperatorTrans4":{isVisible:false},
                };
              }else{
                jsonData={
                  "lblRecipientname":kony.sdk.isNullOrUndefined(response[i].TaxType)?"-":response[i].TaxType,
                  "lblAccountnumber":kony.sdk.isNullOrUndefined(response[i].ToAccountNumber)?"-":response[i].ToAccountNumber,
                  "lblAmount":kony.sdk.isNullOrUndefined(response[i].Amount)?"-":CommonUtilities.formatCurrencyWithCommas(response[i].Amount),
                  "flxSep":{isVisible:false},
                  "flxSeperatorTrans4":{isVisible:false},
                };
              }
              templateData.push(jsonData);
            }
          }
        }

        return templateData;

      }
      catch(err)
      {
       kony.print("Error in dataprocessorsubrecords"+err);
      }
    };
   
    return PresentationController;

  });