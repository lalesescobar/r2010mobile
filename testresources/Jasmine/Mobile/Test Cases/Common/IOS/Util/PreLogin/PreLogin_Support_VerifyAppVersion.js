it("PreLogin_Support_VerifyAppVersion", async function() {
	kony.automation.button.click(["frmLogin","btnSupport"]);
	await kony.automation.playback.waitFor(["frmSupport","lblAppVersion"]);
	expect(kony.automation.widget.getWidgetProperty(["frmSupport","lblAppVersion"], "text")).toEqual("App Version 2020.07.00");
	await kony.automation.device.deviceBack();
	await kony.automation.playback.waitFor(["frmLogin","tbxUsername"]);
});