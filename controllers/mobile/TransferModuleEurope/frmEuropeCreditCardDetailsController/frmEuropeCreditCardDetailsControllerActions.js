define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_e75c4816156146de805238458baf8571: function AS_BarButtonItem_e75c4816156146de805238458baf8571(eventobject) {
        var self = this;
        this.navigateCustomBack();
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_bb53b1432cd740db85bb55e9c39cff2e: function AS_BarButtonItem_bb53b1432cd740db85bb55e9c39cff2e(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    /** init defined for frmEuropeCreditCardDetails **/
    AS_Form_da0d615f051648d398ff55a195ba31de: function AS_Form_da0d615f051648d398ff55a195ba31de(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmEuropeCreditCardDetails **/
    AS_Form_c31fcfcf5f914adf9971c490c1681719: function AS_Form_c31fcfcf5f914adf9971c490c1681719(eventobject) {
        var self = this;
        this.preShow();
    }
});