define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Form_c973e7cd0e7d4fceb38329be3efcab2c: function AS_Form_c973e7cd0e7d4fceb38329be3efcab2c(eventobject) {
        var self = this;
        this.init();
    },
    AS_Form_ac8fbc6011c346cd87dd288aa45f2b98: function AS_Form_ac8fbc6011c346cd87dd288aa45f2b98(eventobject) {
        var self = this;
        this.postShow();
    },
    AS_Form_d7bf21f755ec421184b8a5ad78289dde: function AS_Form_d7bf21f755ec421184b8a5ad78289dde(eventobject) {
        var self = this;
        this.preShow();
    },
    AS_BarButtonItem_e0c7947fb03d43f69285909c3bd474f4: function AS_BarButtonItem_e0c7947fb03d43f69285909c3bd474f4(eventobject) {
        var self = this;
        this.navigateCustomBack();
    },
    AS_BarButtonItem_ae8ef7ea051d4e52ab15146641b35a38: function AS_BarButtonItem_ae8ef7ea051d4e52ab15146641b35a38(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    AS_Segment_c692dc57eaaf4b40a51fcb10f5f2d79f: function AS_Segment_c692dc57eaaf4b40a51fcb10f5f2d79f(eventobject, sectionNumber, rowNumber) {
        var self = this;
        this.onRowClick();
    }
});