define({
  segData: [],
  searchText: "",
  dataToAfterSearch: [],
  checkDepositToInit: function() {
    var loggerManager=applicationManager.getLoggerManager();
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
  },
  preShow:function(){
    this.view.tbxSearch.text = "";
    this.renderTitleBar();
    this.setSegmentData();
    this.view.customHeader.flxBack.onClick=this.flxBackOnClick;
    this.view.customHeader.btnRight.onClick = this.onCancelClick;
    this.view.btnAddAccount.onClick = this.btnAddAccountOnClick;
    this.view.tbxSearch.onTextChange = this.onSearchTextChange;
  applicationManager.getPresentationUtility().dismissLoadingScreen();
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
  },
  renderTitleBar: function(){
   var deviceUtilManager = applicationManager.getDeviceUtilManager();
   var isIphone = deviceUtilManager.isIPhone();
   if (isIphone) {
     this.view.flxHeader.setVisibility(false);
   }
 },
  flxBackOnClick:function(){
     var navManager = applicationManager.getNavigationManager();
     navManager.goBack();
  },
  onCancelClick: function() {
    var navManager = applicationManager.getNavigationManager();
    var prevForm = navManager.getPreviousForm();
    var checkDepositModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CheckDepositModule");
    checkDepositModule.presentationController.commonFunctionForNavigation(prevForm);
  },
  getWidgetDataMap : function(){
    var dataMap = {
      "lblHeader" : "lblHeader",
      "flxTypeOneShadow" : "flxTypeOneShadow",
      "flximgUp" : "flximgUp",
      "imgUpArrow" : "imgUpArrow",
      "lblAccountNumber": "lblAccountNumber",
      "lblAccountName":"nickName",
      "lblBankName" : "lblBankName",
      "lblAccountBalValue":"lblAccountBalValue",
      "lblAccountBal":"AvailableBalStaticLabel",
      "imgBank": "imgBank",
      "imgWarn":"imgWarn",
      "flximgBank" :"flximgBank",
      "flxAccountType":"flxAccountType",
      "imgAccountType":"imgAccountType",
      
    };
    return dataMap;
  },
  segToAccountOnClick:function(){
      var selectedToAccountData = this.view.segToAccount.selectedRowItems[0];
       var checkDepositModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CheckDepositModule");
      checkDepositModule.presentationController.setSelectedAccountData(selectedToAccountData);
  },
  setSegmentData : function(segData){
    var navManager = applicationManager.getNavigationManager();
   var configurationManager = applicationManager.getConfigurationManager();
    var dataToSet = [];
    if(kony.sdk.isNullOrUndefined(segData)){
      this.segData	= navManager.getCustomInfo("frmCheckDepositTo");
      dataToSet = this.segData;
      dataToAfterSearch = dataToSet;
    }
    else {
      dataToSet = segData;
    }
   if(dataToSet && dataToSet.length !== 0) {
     var dataMap = this.getWidgetDataMap();
     this.view.segToAccount.widgetDataMap = dataMap;
     if(configurationManager.isCombinedUser === "true"){
       dataToSet.forEach(element=>element[1]
                         .forEach(elem=>{if(elem.nickName.length>25){
                           elem["nickName"]= elem.nickName.substr(0,25);
                         }}));
     } else {
       dataToSet.forEach(element =>{if(element.nickName.length>25){
         element["nickName"]=element.nickName.substr(0,25);
       }});
     }
     this.view.segToAccount.setData(dataToSet);
     this.view.segToAccount.setVisibility(true);
     this.view.flxNoTransactions.setVisibility(false);}
    else
    {
      this.view.segToAccount.setVisibility(false);
      this.view.flxNoTransactions.setVisibility(true);
    }
  },
  onSearchTextChange: function() {
    var searchText = this.view.tbxSearch.text.toLowerCase();
    var navManager = applicationManager.getNavigationManager();
    var configurationManager = applicationManager.getConfigurationManager();
    try{
    if(searchText && this.segData.length !== 0) {
      this.view.segToAccount.removeAll();
      var data = this.segData;
      var searchSegData = [];
      if( configurationManager.isCombinedUser === "true"){
        searchSegData = this.commonSectionSegmentSearch("nickName", searchText, data);
      }
      else {
        searchSegData = applicationManager.getDataProcessorUtility().commonSegmentSearch("nickName", searchText, data);
      }
      this.setSegmentData(searchSegData);
    }
    else if(!searchText && this.segData.length !== 0){
      if( configurationManager.isCombinedUser === "true"){
        var segEachData = [],
            combinedRowHeader = [],
            finalArr = [];
        var data=this.segData;
        for (var i = 0; i < data.length; i++) {
          segEachData = [];
          combinedRowHeader = [];
			if(!kony.sdk.isNullOrUndefined(data[i][1])){
          for (var j = 0; j < data[i][1].length; j++) {
            segEachData.push(data[i][1][j]);
          }
            }
          if(!kony.sdk.isNullOrUndefined(data[i][0])){
          data[i][0].lblHeader.text = (data[i][0].lblHeader.text).split("(")[0] + "(" +segEachData.length+")";
          combinedRowHeader.push(data[i][0]);
          }
          combinedRowHeader.push(segEachData);
          finalArr.push(combinedRowHeader);
        }
        this.setSegmentData(finalArr);
      }
      else{
        this.setSegmentData(this.segData);
      }
    }
    }
    catch(err){
      loggerManager.log("#### in catch" + JSON.stringify(err) + " ####");
    }
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  },
  commonSectionSegmentSearch : function(field, searchText, data) {
    try{
    var segEachData = [],
        combinedRowHeader = [],
        finalArr = [];
    for (var i = 0; i < data.length; i++) {
      segEachData = [];
      combinedRowHeader = [];

      for (var j = 0; j < data[i][1].length; j++) {
        if(typeof(data[i][1][j][field]) == "string" && typeof(data[i][1][j].lblAccountName) == "string" && typeof(data[i][1][j].lblAccountNumber) == "string"){
          if (kony.sdk.isNullOrUndefined(data[i][1][j][field]) !== undefined && data[i][1][j][field].toLowerCase().indexOf(searchText.toLowerCase()) >= 0
              ||data[i][1][j].lblAccountName.toLowerCase().indexOf(searchText.toLowerCase()) >= 0
              ||data[i][1][j].lblAccountNumber.toLowerCase().indexOf(searchText.toLowerCase()) >= 0) {
            segEachData.push(data[i][1][j]);
          }
        }
      }
      data[i][0].lblHeader.text = (data[i][0].lblHeader.text).split("(")[0] + "(" +segEachData.length+")";

      combinedRowHeader.push(data[i][0]);

      combinedRowHeader.push(segEachData);
      finalArr.push(combinedRowHeader);

    }
    return finalArr;
    }catch(er){
      kony.print(er)
    }
  },
});