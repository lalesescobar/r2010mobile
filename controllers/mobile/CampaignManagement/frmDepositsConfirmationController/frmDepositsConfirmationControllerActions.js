define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnContinue **/
    AS_Button_eb5fdfc4d75649a7bca8472e5c4ec134: function AS_Button_eb5fdfc4d75649a7bca8472e5c4ec134(eventobject) {
        var self = this;
        var transModPresentationController = applicationManager.getModulesPresentationController("TransferModule");
        transModPresentationController.commonFunctionForNavigation("frmTransfers");
    },
    /** onClick defined for undefined **/
    AS_BarButtonItem_b5236a69f55949cd8649286bd9382e2c: function AS_BarButtonItem_b5236a69f55949cd8649286bd9382e2c(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    /** init defined for frmDepositsConfirmation **/
    AS_Form_b3bf54ef08ed4c7aaa184e43b0757758: function AS_Form_b3bf54ef08ed4c7aaa184e43b0757758(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmDepositsConfirmation **/
    AS_Form_b6e81eb21d314fdb993becd20fb79a23: function AS_Form_b6e81eb21d314fdb993becd20fb79a23(eventobject) {
        var self = this;
        this.preShow();
    }
});