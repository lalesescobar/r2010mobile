define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmUserDetails **/
    AS_Form_de34b661208a41d18dab4cfcee9fb249: function AS_Form_de34b661208a41d18dab4cfcee9fb249(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmUserDetails **/
    AS_Form_i009f473282244788f6a6a053e2a415f: function AS_Form_i009f473282244788f6a6a053e2a415f(eventobject) {
        var self = this;
        this.formPreShow();
    },
    /** postShow defined for frmUserDetails **/
    AS_Form_fa9be54355f044b5a2e89a93fc0c37db: function AS_Form_fa9be54355f044b5a2e89a93fc0c37db(eventobject) {
        var self = this;
        this.formPostShow();
    }
});