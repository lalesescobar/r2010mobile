define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** init defined for frmLogout **/
    AS_Form_f824c96d565f4432b2a24d5744f1ee97: function AS_Form_f824c96d565f4432b2a24d5744f1ee97(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmLogout **/
    AS_Form_a831301cdd5b4ab5afb7b8d986efccf7: function AS_Form_a831301cdd5b4ab5afb7b8d986efccf7(eventobject) {
        var self = this;
        return self.frmLoginPreShow.call(this);
    },
    /** onClick defined for btnLogIn **/
    AS_Button_f5442c04e9d842829adfa1e68c4a5318: function AS_Button_f5442c04e9d842829adfa1e68c4a5318(eventobject) {
        var self = this;
        return
    },
    /** onDownloadComplete defined for imgAd1 **/
    AS_Image_cf750a7da8004e38aadfb5a6417ea931: function AS_Image_cf750a7da8004e38aadfb5a6417ea931(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 1);
    },
    /** onDownloadComplete defined for imgAd2 **/
    AS_Image_b22ac462c7d146e8a69f6e50cfe51618: function AS_Image_b22ac462c7d146e8a69f6e50cfe51618(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 2);
    },
    /** onDownloadComplete defined for imgAd3 **/
    AS_Image_a207f67304a7478b8cbe7c7ac0970e7f: function AS_Image_a207f67304a7478b8cbe7c7ac0970e7f(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 3);
    },
    /** onDownloadComplete defined for imgAd4 **/
    AS_Image_ee07749e0e6147e683d93ed1bc83a2a7: function AS_Image_ee07749e0e6147e683d93ed1bc83a2a7(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 4);
    },
    /** onDownloadComplete defined for imgAd5 **/
    AS_Image_h5cbce2ed38e4bd395d917b5038d2d1e: function AS_Image_h5cbce2ed38e4bd395d917b5038d2d1e(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 5);
    }
});