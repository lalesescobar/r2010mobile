define({
  
init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
},
 
  preshow:function(){
  if(applicationManager.getPresentationFormUtility().getDeviceName() !== "iPhone"){
      this.view.flxHeader.isVisible = true;
    }
    else{
   
      this.view.flxHeader.isVisible = false;
    }
    this.initActions();
    this.chartLoad();
  }, 
  postShow:function(){
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  },
  initActions:function(){
     this.view.customHeader.flxBack.onClick = this.onBack;
     this.view.customHeader.btnRight.onClick = this.onEdit;
     this.view.btnSaveConfirm.onClick = this.fundNav;
     this.view.btnClose.onClick = this.withdrawNav;
     this.setDataToForm();
     this.view.browserChart.onSuccess=this.chartLoad;
     this.view.browserChart.onPageFinished=this.chartLoad;
  },
  
  setDataToForm: function(){
        var navManager = applicationManager.getNavigationManager();
    var SavingsPotMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SavingsPotModule");
         var goalDetails=JSON.parse(JSON.stringify(SavingsPotMod.presentationController.getEditObj()));
	navManager.setCustomInfo("frmSavingsGoalViewDetails", goalDetails);
            SavingsPotMod.presentationController.setSavingsFlow("");
         this.view.customHeader.lblLocateUs.text=goalDetails.potName;
  this.view.title=goalDetails.potName;
    var formatUtil=applicationManager.getFormatUtilManager();
    var currentdateObj = formatUtil.getDateObjectfromString(goalDetails.creationDate,"MM/DD/YYYY");
    var currentdate = formatUtil.getFormatedDateString(currentdateObj, formatUtil.getApplicationDateFormat());
    var startDateobj = formatUtil.getDateObjectfromString(goalDetails.startDate,"MM/DD/YYYY");
    var startDate = formatUtil.getFormatedDateString(startDateobj, formatUtil.getApplicationDateFormat());
    var endDateobj = formatUtil.getDateObjectfromString(goalDetails.endDate,"MM/DD/YYYY");
    var endDate = formatUtil.getFormatedDateString(endDateobj, formatUtil.getApplicationDateFormat());
     if(goalDetails.amountWithdrawn === "0"){
      this.view.flxWithDraw.setVisibility(false);
    }else {
    this.view.flxWithDraw.setVisibility(true);
    this.view.lblWithdrawValue.text = formatUtil.formatAmountandAppendCurrencySymbol(goalDetails.amountWithdrawn,goalDetails.currency);
    }
  //  this.view.lblWithdrawValue.text = formatUtil.formatAmountandAppendCurrencySymbol(goalDetails.amountWithdrawn,goalDetails.currency);
    this.view.lblMonthlyAmountValue.text = formatUtil.formatAmountandAppendCurrencySymbol(goalDetails.periodicContribution,goalDetails.currency);
    this.view.lblMonthsValue.text = goalDetails.targetPeriod;
  this.view.lblFrequencyvalue.text = goalDetails.frequencyDay;
    this.view.lblCreatedDateValue.text = currentdate;  
    this.view.lblStartDateValue.text = startDate;  
    this.view.lblFinalDateValue.text = endDate;  
    this.view.lblGoalAmountvalue.text = formatUtil.formatAmountandAppendCurrencySymbol(goalDetails.targetAmount,goalDetails.currency);  
    this.view.lblCurrentBalancevalue.text = formatUtil.formatAmountandAppendCurrencySymbol(goalDetails.availableBalance,goalDetails.currency);
    var SavingsPotMod = applicationManager.getModulesPresentationController("SavingsPotModule");
    SavingsPotMod.setSavingsPotId(goalDetails.savingsPotId);
    SavingsPotMod.setTransactPotId(goalDetails.potAccountId);
    SavingsPotMod.setSavingsPotName(goalDetails.potName);
    if(goalDetails.availableBalance > goalDetails.targetAmount){
      this.view.lblSavingsAmount.text = "$0.00";
    }else {
    this.view.lblSavingsAmount.text = formatUtil.formatAmountandAppendCurrencySymbol(goalDetails.remainingSavings,goalDetails.currency);
    }
    if( parseFloat(goalDetails.targetAmount) < parseFloat(goalDetails.availableBalance) ){
      var excessAmount = goalDetails.availableBalance - goalDetails.targetAmount;
      this.view.flxSavings.setVisibility(false);
      this.view.flxExcessAmount.setVisibility(true);
      this.view.lblExcessAmountValue.text = formatUtil.formatAmountandAppendCurrencySymbol(excessAmount,goalDetails.currency);
    } else {
       var excessAmount = goalDetails.targetAmount - goalDetails.availableBalance;
       this.view.flxExcessAmount.setVisibility(false);
       this.view.flxSavings.setVisibility(true);
       this.view.lblSavingsAmount.text = formatUtil.formatAmountandAppendCurrencySymbol(excessAmount,goalDetails.currency);
    }
    if(goalDetails.potCurrentStatus === "On Track"){
     this.view.flxNotonTrack.isVisible = false;
     this.view.flxOnTrack.isVisible = true;
     this.view.lblOnTrack.text = goalDetails.potCurrentStatus;
      this.view.flxInSufficient.isVisible = false;
    } else if(goalDetails.potCurrentStatus === "Not On Track") {
     this.view.flxOnTrack.isVisible = false;
     this.view.flxNotonTrack.isVisible = true;
     this.view.lblNotOnTrack.text = goalDetails.potCurrentStatus;
     this.view.flxInSufficient.isVisible = true;
     this.view.imgAlert.src = "alert.png";
     this.view.lblMessage.text =  "You might not reach your goal on final date";  
    } else if(goalDetails.potCurrentStatus === "Completed" ){
     this.view.flxNotonTrack.isVisible = false;
     this.view.flxOnTrack.isVisible = true;
     this.view.lblOnTrack.text = goalDetails.potCurrentStatus;
     this.view.flxInSufficient.isVisible = true;
     this.view.imgAlert.src = "greentick.png";
      if(this.view.lblCreatedDateValue.text != this.view.lblFinalDateValue.text){
        this.view.lblMessage.text =  "You achieved the Goal Amount too soon! And you will be saving more till your Final Date";
      }
      else{
     this.view.lblMessage.text =  "You achieved the Goal Amount!";
      }
    } 

  },
  chartLoad: function(){
        var navManager = applicationManager.getNavigationManager();
    var budgetDetails = navManager.getCustomInfo("frmSavingsGoalViewDetails");
	var percentage=(budgetDetails.availableBalance*100)/budgetDetails.targetAmount;
    var remaining=100-parseInt(percentage);
	if(percentage>=100){
          percentage=100;
      this.view.lblPercent.text=percentage+"%";
      this.view.lblCur.skin="lbldotgreen";
       var x = this.view.browserChart.evaluateJavaScript("AddDonutChart(" + JSON.stringify([]) +","+JSON.stringify([0,0,percentage])+");");
        }
       else{
       percentage=parseInt(percentage);
               this.view.lblPercent.text=percentage+"%";
              this.view.lblCur.skin="lbldotblue";
         var x = this.view.browserChart.evaluateJavaScript("AddDonutChart(" + JSON.stringify([]) +","+JSON.stringify([percentage,remaining,])+");");
       }
  },
  fundNav: function(){
    var SavingsPotMod = applicationManager.getModulesPresentationController("SavingsPotModule");
    SavingsPotMod.setSavingsType("Goal");
    var SavingsPotModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SavingsPotModule");
    SavingsPotModule.presentationController.clearFundWithdrawData();
    var navManager = applicationManager.getNavigationManager();
    navManager.navigateTo("frmGoalfundAmount");
  },
  withdrawNav: function(){
    var navManager = applicationManager.getNavigationManager();
     var Details = navManager.getCustomInfo("frmSavingsGoalViewDetails");
     var SavingsPotMod = applicationManager.getModulesPresentationController("SavingsPotModule");
     var potName = SavingsPotMod.getSavingsPotName();
    if(Details.availableBalance === "0"){
      alert("You have zero balance in your"+" "+potName+". You can close the pot now!");
    } else {
    SavingsPotMod.setSavingsType("Goal");
    var SavingsPotModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SavingsPotModule");
    SavingsPotModule.presentationController.clearFundWithdrawData();
     navManager.navigateTo("frmWithdrawAmount");
      }
  },
  onEdit:function(){
       var SavingsPotMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SavingsPotModule");
    SavingsPotMod.presentationController.setSavingsFlow("YES");
    SavingsPotMod.presentationController.initiatePot("Goal","frmEditSavingsGoal");
  },
    onBack : function () {
    var navigationMan=applicationManager.getNavigationManager();
    navigationMan.goBack();
  },
  bindGenericError: function (errorMsg){
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var scopeObj = this;
     applicationManager.getDataProcessorUtility().showToastMessageError(scopeObj, errorMsg);
}
  
});