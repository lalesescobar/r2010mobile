define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for undefined **/
    AS_BarButtonItem_i9b81c6797fb48c5a3ede956c5e8632e: function AS_BarButtonItem_i9b81c6797fb48c5a3ede956c5e8632e(eventobject) {
        var self = this;
        var accountModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AccountModule");
        accountModule.presentationController.commonFunctionForNavigation("frmPFMTransactionDetails");
    },
    /** preShow defined for frmMMDuration **/
    AS_Form_f271a976e0a0437eb7903dde29b9396b: function AS_Form_f271a976e0a0437eb7903dde29b9396b(eventobject) {
        var self = this;
        this.preshow();
    },
    /** init defined for frmMMDuration **/
    AS_Form_d48ab3149eb64c7785bdd2c403620ea5: function AS_Form_d48ab3149eb64c7785bdd2c403620ea5(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmMMDuration **/
    AS_Form_f63a1991b2a4427b9cd755c544e1f97d: function AS_Form_f63a1991b2a4427b9cd755c544e1f97d(eventobject) {
        var self = this;
        this.preShow();
    }
});