define({
	cardlessAccounts:null,
   preShow:function(){
     var self = this;
   this.view.customHeader.flxBack.onClick=this.flxBackOnClick;
   this.view.customHeader.flxHeader.btnRight.onClick = this.flxBackOnClick;
   this.view.tbxSearch.onTouchStart = this.tbxSearchOnTouchEnd;
   this.view.customSearchbox.btnCancel.onClick = this.btnCancelOnClick;
     if(applicationManager.getPresentationFormUtility().getDeviceName()==="iPhone"){
       this.view.flxHeader.isVisible = false;
     }else{
       this.view.flxHeader.isVisible = true;
     }
	var navMan=applicationManager.getNavigationManager();
    var fromAccountsList = navMan.getCustomInfo("frmCardLessFrom");
    var fromaccounts = [];
     if( !kony.sdk.isNullOrUndefined(fromAccountsList.fromaccounts) && fromAccountsList.fromaccounts !== "" ) {
       fromaccounts = fromAccountsList.fromaccounts;
     }
	this.view.customSearchbox.tbxSearch.text="";
    this.view.flxSearch.top = "0dp";
    this.view.tbxSearch.text="";
    this.view.customSearchbox.tbxSearch.onTextChange=this.tbxSearchOnTextChange;
    var accProcessedData = (JSON.parse(JSON.stringify(fromaccounts)));
    this.setSegmentData(accProcessedData);
    this.btnCancelOnClick();
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);
	applicationManager.getPresentationUtility().dismissLoadingScreen();
  },
  init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"YES",currentForm);
 },
  segToAccountOnRowClick:function(){
	applicationManager.getPresentationUtility().showLoadingScreen();
    var navMan=applicationManager.getNavigationManager();
    var cardlessModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardLessModule");
    var preAccData = this.view.segToAccount.selectedItems[0];
    cardlessModule.presentationController.setFromAccountDetails(preAccData);
    var txnDetails=cardlessModule.presentationController.getTransactionObject();
	txnDetails=cardlessModule.presentationController.processAccountsData(txnDetails);
    navMan.setCustomInfo("frmCardLessWithdraw",txnDetails);
    cardlessModule.presentationController.commonFunctionForNavigation("frmCardLessWithdraw");
  },
  flxBackOnClick:function(){
    var navMan=applicationManager.getNavigationManager();
    navMan.goBack();
  },
  btnRightOnCLick:function(){
    var cardlessModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardLessModule");
	cardlessModule.presentationController.cancelCommon();
  },
   tbxSearchOnTouchEnd: function() {
            var scope = this;
       kony.timer.schedule("timerId", function() {
                           scope.view.customSearchbox.tbxSearch.setFocus(true);
                           }, 0.2, false);
         this.view.tbxSearch.setFocus(true);
         this.view.flxHeader.setVisibility(false);
         this.view.flxHeaderSearchbox.setVisibility(true);
         this.view.flxMainContainer.top = "40dp";
          this.view.segToAccount.top ="0dp";
         this.view.flxSearch.setVisibility(false);
     },
     btnCancelOnClick: function() {
        this.view.customSearchbox.tbxSearch.text="";
    	this.view.tbxSearch.text="";
         this.view.flxHeaderSearchbox.setVisibility(false);
         this.view.flxSearch.setVisibility(true);
             this.view.flxNoTransactions.isVisible=false;
          	//this.view.flxHeaderNT.isVisible=false;
          	//this.view.flxSeperator3.isVisible=false;
    	    this.view.lblNoTransaction.isVisible=false;
        	this.view.segToAccount.isVisible=true;
         if (applicationManager.getPresentationFormUtility().getDeviceName() === "iPhone") {
             this.view.flxHeader.isVisible = false;
             this.view.flxMainContainer.top = "0dp";
         } else {
             this.view.flxHeader.isVisible = true;
             this.view.flxMainContainer.top = "56dp";
         }
          this.view.segToAccount.top ="55dp";
         if( !kony.sdk.isNullOrUndefined(this.cardlessAccounts) ) {
           this.view.segToAccount.setData(this.cardlessAccounts);
         }
         else {
           this.view.segToAccount.setData([]);
         }
  },
  setSegmentData:function(res){
    var dataMap=this.getDataMap();
    this.view.segToAccount.widgetDataMap=dataMap;
    if(res.length === 0)
    {
      this.view.segToAccount.isVisible=false;
      this.view.flxNoTransactions.isVisible=true;
      //this.view.flxHeaderNT.isVisible=false;
      //this.view.flxSeperator3.isVisible=false;
      this.view.lblNoTransaction.isVisible=true;
    }
    else
    {
      this.view.segToAccount.isVisible=true;
      this.view.flxNoTransactions.isVisible=false;
      //this.view.flxHeaderNT.isVisible=false;
      //this.view.flxSeperator3.isVisible=false;
      this.view.lblNoTransaction.isVisible=false;
      var forUtility=applicationManager.getFormatUtilManager();
      var configurationManager = applicationManager.getConfigurationManager();
      if( configurationManager.isCombinedUser === "true"){
        for(var i = 0; i<res.length; i++){
          res[i].template = "flxCombinedAccounts";
          res[i].lblAccountName = res[i].accountName;
          var formatUtility=applicationManager.getFormatUtilManager();
          var availBal = formatUtility.formatAmountandAppendCurrencySymbol(res[i].availableBalance,res[i].currencyCode);
			res[i].amount=res[i].availableBalance;          
			res[i].availableBalance = availBal;
          res[i].lblAccountNumber = "..." + (res[i].accountID).substr((res[i].accountID).length - 4);
          res[i].lblAccountBal = "Available Balance";
          
          res[i].imgAccountType = {
            "src": res[i].isBusinessAccount ==="true" ? "businessaccount.png" :"personalaccount.png"
          };
          res[i].flximgBank = {
            "isVisible": false
          };
          res[i].accountType = {"text" : res[i].accountType};
        }

        var filterBusinessAcc = res.filter(function(el){
          return el.isBusinessAccount == "true";
        });
        var filterPersonalAcc = res.filter(function(el){
          return el.isBusinessAccount == "false";
        });

        var segData = [];
        segData.push([
          {
            "template" : "flxTransHeader",
            "lblHeader" : {"text" : "Personal Accounts ("+ filterPersonalAcc.length +")"},
            "flxTypeOneShadow" : {
              "isVisible" : true
            },
            "flximgUp" : {
              "isVisible" : true
            },
            "imgUpArrow" : {
              "src" : "arrowup.png"
            }
          },
          filterPersonalAcc
        ],[
          {
            "template" : "flxTransHeader",
            "lblHeader" : {"text" : "Business Accounts ("+ filterBusinessAcc.length +")"},
            "flxTypeOneShadow" : {
              "isVisible" : true
            },
            "flximgUp" : {
              "isVisible" : true
            },
            "imgUpArrow" : {
              "src" : "arrowup.png"
            }
          },
          filterBusinessAcc
        ]);
        segData.forEach(element=>element[1]
                        .forEach(elem=>{if(elem.nickName.length>25){
                          elem["nickName"]= elem.nickName.substr(0,25);
                        }}));
         this.view.segToAccount.setData(segData);
        this.cardlessAccounts=segData;
      }
      else {
        for(var i =0 ; i < res.length;i++)
        {
          res[i].lblAccountBal="Available Balance";
          res[i].amount=res[i].availableBalance;
          res[i].availableBalance=forUtility.formatAmountandAppendCurrencySymbol(res[i].availableBalance,res[i].currencyCode);
        }
        res.forEach(element =>{if(element.nickName.length>25){
         element["nickName"]=element.nickName.substr(0,25);
       }});
        this.view.segToAccount.setData(res);
        this.cardlessAccounts=res;
      }
    }
  },
  getDataMap : function(){
    var dataMap={};
    dataMap = {
       "lblAccountName":"nickName",
      "lblAccountBal":"lblAccountBal",
      "lblAccountBalValue":"availableBalance",
	  "lblBankName":"accountType",
      "accountID":"accountID",
      "amount":"amount",
       "lblHeader" : "lblHeader",
      "flxTypeOneShadow" : "flxTypeOneShadow",
      "flximgUp" : "flximgUp",
      "imgUpArrow" : "imgUpArrow",
      "lblAccountNumber": "lblAccountNumber",
      "lblAccountBal":"AvailableBalStaticLabel",
      "imgBank": "imgBank",
      "imgWarn":"imgWarn",
      "flximgBank" :"flximgBank",
      "flxAccountType":"flxAccountType",
      "imgAccountType":"imgAccountType",
    };
    return dataMap;
  },
  tbxSearchOnTextChange:function(){
    var searchtext=this.view.customSearchbox.tbxSearch.text.toLowerCase();
    var cardlessModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("CardLessModule");
    var data = this.cardlessAccounts;
    var configurationManager = applicationManager.getConfigurationManager();
    if( configurationManager.isCombinedUser === "true"){
      var searchSegData = this.commonSectionSegmentSearch("accountName", searchtext, data);
      this.view.segToAccount.setData(searchSegData);      
    }else {
      //var searchSegmentData = applicationManager.getDataProcessorUtility().commonSegmentSearch("accountName",searchtext,data);
      var searchSegmentData = this.commonSegmentSearch("accountName", searchtext, data);
      if(searchSegmentData && searchSegmentData.length===0){
        this.view.flxNoTransactions.isVisible=true;
        //this.view.flxHeaderNT.isVisible=false;
        //this.view.flxSeperator3.isVisible=false;
        this.view.flxNoTransactions.top ="0dp";
        this.view.lblNoTransaction.isVisible=true;
        this.view.segToAccount.isVisible=false;
      }
      else{
        this.view.flxNoTransactions.isVisible=false;
        //this.view.flxHeaderNT.isVisible=false;
        //this.view.flxSeperator3.isVisible=false;
        this.view.lblNoTransaction.isVisible=false;
        this.view.segToAccount.isVisible=true;
        this.view.segToAccount.setData(searchSegmentData);
      }
    }

  },
  
   commonSectionSegmentSearch : function(field, searchText, data) {
     try{
    var segEachData = [],
        combinedRowHeader = [],
        finalArr = [];
    for (var i = 0; i < data.length; i++) {
      segEachData = [];
      combinedRowHeader = [];
      for (var j = 0; j < data[i][1].length; j++) {
        if(typeof(data[i][1][j][field]) == "string" && typeof(data[i][1][j].lblAccountNumber) == "string" 
                  && typeof(data[i][1][j].nickName) == "string"){
          if (data[i][1][j][field] !== undefined && data[i][1][j][field].toLowerCase().indexOf(searchText.toLowerCase()) >= 0
              ||data[i][1][j].lblAccountNumber.toLowerCase().indexOf(searchText.toLowerCase()) >= 0 
              ||data[i][1][j].nickName.toLowerCase().indexOf(searchText.toLowerCase()) >= 0) {
            segEachData.push(data[i][1][j]);
          }
        }
      }
      data[i][0].lblHeader.text = (data[i][0].lblHeader.text).split("(")[0] + "(" +segEachData.length+")";

      combinedRowHeader.push(data[i][0]);

      combinedRowHeader.push(segEachData);
      finalArr.push(combinedRowHeader);
    }
    return finalArr;
     }catch(er){
       kony.print(er);
     }
  },
  commonSegmentSearch:function(field, searchText, data) {
    var searchdata = [];
    for (var i = 0; i < data.length; i++) {
      if(typeof(data[i][field]) == "string" && typeof(data[i].nickName) == "string"){
        if (data[i][field] !== undefined && data[i][field].toLowerCase().indexOf(searchText) >= 0 
            ||data[i].nickName.toLowerCase().indexOf(searchText) >= 0) {
          searchdata.push(data[i]);
        }
      }
    }
    return searchdata;
  }
  
  
 });
