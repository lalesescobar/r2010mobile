describe("IOSLoginAndLogout", function() {
	it("LoginAndWaitForDashboard", async function() {
		await kony.automation.playback.waitFor(["frmLogin","tbxUsername"]);
		kony.automation.textbox.enterText(["frmLogin","tbxUsername"],"dbpolbuser");
		kony.automation.textbox.enterText(["frmLogin","tbxPassword"],"Kony@1234");
		await kony.automation.playback.waitFor(["frmLogin","btnLogIn"]);
		kony.automation.button.click(["frmLogin","btnLogIn"]);
		//Verifying Terms and Condition page -
			var frmTnC = await kony.automation.playback.waitFor(["frmTermsAndCondition","flxCheckBox"],20000);
			if(frmTnC){
				kony.automation.flexcontainer.click(["frmTermsAndCondition","flxCheckBox"]);
				await kony.automation.playback.waitFor(["frmTermsAndCondition","btnContinue"]);
				kony.automation.button.click(["frmTermsAndCondition","btnContinue"]);
		}
		
		await kony.automation.playback.waitFor(["frmDashboardAggregated","lblBankName"],5000);
	},26000);
	
	it("Logout", async function() {
		kony.automation.flexcontainer.click(["frmDashboardAggregated","customFooter","flxMore"]);
		await kony.automation.playback.waitFor(["frmDashboardAggregated","Hamburger","flxLogout"]);
		kony.automation.widget.touch(["frmDashboardAggregated","Hamburger","flxLogout"], null,null,[50,31]);
		await kony.automation.playback.wait(3000);
		await kony.automation.playback.waitFor(["frmLogout","btnLogIn"]);
		kony.automation.button.click(["frmLogout","btnLogIn"]);
		await kony.automation.playback.waitFor(["frmLogin","tbxUsername"]);
	});
});