define([], function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;

	//Create the Repository Class
	function FeatureRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};

	//Setting BaseRepository as Parent to this Repository
	FeatureRepository.prototype = Object.create(BaseRepository.prototype);
	FeatureRepository.prototype.constructor = FeatureRepository;

	//For Operation 'getAllFeatures' with service id 'GetAllFeatures5841'
	FeatureRepository.prototype.getAllFeatures = function(params, onCompletion){
		return FeatureRepository.prototype.customVerb('getAllFeatures', params, onCompletion);
	};

	return FeatureRepository;
})