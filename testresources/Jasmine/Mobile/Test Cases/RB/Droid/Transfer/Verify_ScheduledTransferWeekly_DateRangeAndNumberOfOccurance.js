it("Verify_ScheduledTransferWeekly_DateRangeAndNumberOfOccurance", async function() {
	SearchInFromAndToScreen("normal");
	EnterAmount();
	SelectFrequencyWeekly();
	SelectDateRangeAndConfirmTransfer("Weekly","normal");
  
	SearchInFromAndToScreen("normal");
	EnterAmount();
	SelectFrequencyWeekly();
	SelectNumberOfTransfersAndThenConfirmTransfer("Weekly","normal");
  Logout();
},180000);