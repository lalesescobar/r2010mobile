define({
  base64: null,
  fileNames: [],
  fileContents: [],
  requestPayload: {},
  fileTypes: [],
  fileNamePrefix:'Attachment',
  init: function () {
    var scope=this;
    var currentFormObject = kony.application.getCurrentForm();
    var currentForm=currentFormObject.id;
    applicationManager.getPresentationFormUtility().initCommonActions(this, "CALLBACK", currentForm, scope.navigateCustomBack);
  },
  navigateCustomBack: function() {
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    transMod.commonFunctionForgoBack();
  },
  
  preShow: function () {
    applicationManager.getPresentationUtility().showLoadingScreen();
    this.view.btnTransfer.skin="sknBtnOnBoardingInactive";
    this.view.btnTransfer.setEnabled(false);
    this.view.flxMMAmount.setVisibility(false);
    this.view.flxAddIcon.setVisibility(true);
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    transMod.getReviewScreenData();
  },

  proceedWithPreshow: function(segData) {
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    var transObj= transMod.getTransObject();
    transObj.validate = true;
    applicationManager.getPresentationUtility().showLoadingScreen();
    transMod.validateATransfer(segData);
  },
  
  afterValidateTransaction:function(segData){
    if (kony.os.deviceInfo().name === "iPhone") {
      this.view.flxHeader.isVisible = false;
    }
    this.view.flxMMAmount.setVisibility(true);
    this.view.btnTransfer.skin="sknBtn0095e4RoundedffffffSSP26px";
    this.view.btnTransfer.setEnabled(true);
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    var transObj= transMod.getTransObject();
    this.changeButtonText();
    this.setFromAccountData();
    this.setToAccountData();
    this.setAmountData();
    this.setSegmentData(segData);
    this.initActions();
   
    var navManager = applicationManager.getNavigationManager();
    var currentForm = navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().logFormName(currentForm);

    applicationManager.getPresentationUtility().dismissLoadingScreen();
  },
  postShow: function(){
    this.view.flxAddIcon.onClick = this.showFileSelectionOption;
    this.hideFileSelectionOption();
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    this.view.btnPhoto.onClick = this.fileSelectionFromGallery;
    this.view.btnDocument.onClick = this.selectDocuments;
    this.view.btnClose.onClick = this.hideFileSelectionOption;
    this.view.Camera.onCapture = this.openCamera;
    this.view.lblAttachmentError.text = "";
    this.view.lblTitle.text = "";
    this.view.flxAttachmentsList.setVisibility(false);
    var optional = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Europe.AttachmentsOptional");
    this.view.lblTitle.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Europe.Attachments")+" ("+optional+")";
    this.view.segAttachmentsList.setData([]);
     this.fileNames=[];
    this.view.btnTransfer.onClick = this.continueOnClick;
  },
  initActions: function () {
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    var navMan = applicationManager.getNavigationManager();
    this.view.customHeader.flxBack.onClick = this.navigateCustomBack;
    this.view.segDetails.onRowClick = function() {
      transMod.initializeStateData(true,"frmEuropeVerifyTransferDetails");
      transMod.commonFunctionForNavigation("frmEuropeFrequency");
    }
    this.view.flxFromAccount.onTouchEnd = function(){
      transMod.initializeStateData(true,"frmEuropeVerifyTransferDetails");
      transMod.commonFunctionForNavigation("frmEuropeTransferFromAccount");
    }
    this.view.flxToAccount.onTouchEnd = function(){
      transMod.initializeStateData(true,"frmEuropeVerifyTransferDetails");
      if (transMod.europeFlowType === "INTERNAL") {
        transMod.filterToAccountsByExludingFromAccount();
        transMod.commonFunctionForNavigation("frmEuropeTransferToAccount");
      }
      else {
        transMod.commonFunctionForNavigation("frmEuropeTransferToAccountSM");
      }
    }
    this.view.flxAmount.onTouchEnd = function(){
      var transactionObject = transMod.getTransObject();
      if (transactionObject.toAccountType === "CreditCard") {
        transMod.initializeStateData(false, "");
        var formname = "frmEuropeCreditCardDetails";
        var index = navMan.getFormIndex(formname);
        var stackLength = navMan.stack.length;
        if (index === null) navMan.setFormIndex(formname, stackLength - 1);
        else {
          for (var i = stackLength - 1; i > index; i--) {
            navMan.removeFormIndex(navMan.stack[i]);
            navMan.stack.pop();
          }   
        }
        var transactionManager = applicationManager.getTransactionManager();
        transactionManager.setTransactionAttribute("isScheduled", "0");
        transactionManager.setTransactionAttribute("amount", null);
        transactionManager.setTransactionAttribute("reference", null);
        transactionManager.setTransactionAttribute("transactionsNotes", null);
        transMod.commonFunctionForNavigation("frmEuropeCreditCardDetails");
      }
      else {
        transMod.initializeStateData(true,"frmEuropeVerifyTransferDetails");
        transMod.commonFunctionForNavigation("frmEuropeTransferAmount");
      }  
    }
    this.view.segDetails.onRowClick = this.segmentOnROwClick;
    this.view.btnTransfer.onClick = this.continueOnClick;
    this.view.customHeader.btnRight.onClick = this.cancelOnClick;
  },
  cancelOnClick:function(){
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    transMod.cancelCommon();
  },
  segmentOnROwClick:function(){
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    var rowindex = this.view.segDetails.selectedRowIndex[1];
    var selectedRow = this.view.segDetails.data[rowindex];
    if ((selectedRow.property!== applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.mm.FrequencyDetails") && selectedRow.value !== applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.mm.RemoveRepeatingTransfer"))){
    	transMod.initializeStateData(true, "frmEuropeVerifyTransferDetails");
    }
    else{
      transMod.initializeStateData(false, "");
    }
    transMod.reviewRowClick(selectedRow);
  },

  getWidgetDataMap:function(){
    var map = {
      lblTitle:"property",
      lblValue:"value",
      imgArrow:"chevronImg"
    }
    return map;
  },
  
  setSegmentData: function(segData){
    // var transMod = applicationManager.getModulesPresentationController("TransferModule");
    //var segData = transMod.getReviewScreenData();
    // if (segData !== null) {
      this.bindDataToSegment(segData);
    // }
  },

  bindDataToSegment : function(segData) {
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    this.view.segDetails.widgetDataMap = this.getWidgetDataMap();
    var countToCheckEnabled = 1;
    for (var row in segData){
      if ((segData[row].property!== applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.mm.FrequencyDetails") && segData[row].value !== applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.mm.RemoveRepeatingTransfer")) && !(segData[row].value)) {
        countToCheckEnabled=0;
        break;
      }
    }
    var fromaccountdata= transMod.getTransferObjectById();
    var toAccountData = transMod.getToAccountData();
    var forUtility = applicationManager.getFormatUtilManager();
    var configManager = applicationManager.getConfigurationManager();
    var endDate = forUtility.getDateObjectFromCalendarString(transMod.getTransObject().frequencyEndDate, configManager.getCalendarDateFormat());
    var startDate = forUtility.getDateObjectFromCalendarString(transMod.getTransObject().frequencyStartDate, configManager.getCalendarDateFormat());
    if (kony.sdk.isNullOrUndefined(fromaccountdata[0].accountID)){
      countToCheckEnabled = 0;
      this.clearFromAccountData();
    }
    if (kony.sdk.isNullOrUndefined(toAccountData.toAccountNumber) && kony.sdk.isNullOrUndefined(toAccountData.IBAN)) {
      countToCheckEnabled = 0;
      this.clearToAccountData();
    }
    if(fromaccountdata[0].accountID == toAccountData.toAccountNumber){
      countToCheckEnabled=0;
      this.clearToAccountData();
    }
    if(startDate && endDate && startDate >= endDate){
      segData = this.clearEndDate(segData);
      countToCheckEnabled=0;
    }
    if(countToCheckEnabled==0){
      this.view.btnTransfer.skin="sknBtnOnBoardingInactive";
      this.view.btnTransfer.setEnabled(false);
    }
    else{
      this.view.btnTransfer.skin="sknBtn0095e4RoundedffffffSSP26px";
      this.view.btnTransfer.setEnabled(true);
    }
    this.view.segDetails.setData(segData);
  },

  clearEndDate:function(segData){
    for(var i in segData){
      if(segData[i].property == applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Transfers.EndDate")){
        segData[i].value = "";
        break;
      }
    }
    return segData;
  },
  clearToAccountData:function(){
    this.view.lblToAccountValue.text = "";
    this.view.lblToBalanceValue.text = "";
    this.view.lblToAvailableBalance.text = "";
    this.view.lblToAvailableBalance.setVisibility(true);
  },
  clearFromAccountData: function() {
    this.view.lblFromAccountValue.text = "";
    this.view.lblFromBalanceValue.text = "";
    this.view.lblFromavailableBal.text = "";
    this.view.lblFromavailableBal.setVisibility(true);
  },
  setFromAccountData:function()
  {
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    var fromaccountdata = transMod.getTransferObjectById("from");
    var name="";
    if (fromaccountdata[0].nickName === null || fromaccountdata[0].nickName === undefined) {
      name = fromaccountdata[0].accountName;
    } else {
      name =fromaccountdata[0].nickName;
    }
    this.view.lblFromAccountValue.text=applicationManager.getPresentationUtility().formatText(name,10,fromaccountdata[0].accountID,4);
    this.view.lblFromBalanceValue.text=fromaccountdata[0].availableBalance;
    this.view.lblFromavailableBal.text=fromaccountdata[0].accountBalanceType + ":";
  },
  setToAccountData:function(){
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    var navMan=applicationManager.getNavigationManager();
    var entryPoint=navMan.getEntryPoint("europeTransferFlow");
    var toAccountData = (entryPoint === "frmEuropeTransferActivities") ? transMod.getToAccountDataEditFlow() : transMod.getToAccountData();
    if(entryPoint==="frmAccountDetails" && toAccountData.toAccountType === "Loan") {
      this.setUIForCreateFlow();
      this.setDisabledToAccount(toAccountData);
      this.view.flxToAccount.setVisibility(false);
      this.view.flxToAccountDisabled.setVisibility(true);
    }
    else if (entryPoint === "frmEuropeTransferActivities") {
      this.setUIForEditFlow();
      this.view.lblToAccountValueEdit.text = toAccountData.toProcessedName || toAccountData.toAccountName;
    }
    else {
      this.setUIForCreateFlow();
      this.setEditableToAccount(toAccountData);
      this.view.flxToAccount.setVisibility(true);
      this.view.flxToAccountDisabled.setVisibility(false);
    }  
  },
  setEditableToAccount:function(toAccountData){
    if(toAccountData.transactionType == "P2P"){
      this.view.lblToAccountValue.text = toAccountData.payPersonName;
      this.view.lblToBalanceValue.text = toAccountData.p2pContact;
      this.view.lblToAvailableBalance.setVisibility(false);
    }
    else if(toAccountData.transactionType == "InternalTransfer"){
      this.view.lblToAccountValue.text = toAccountData.toAccountName;
      this.view.lblToBalanceValue.text = toAccountData.availableBalance;
	  this.view.lblToAvailableBalance.text = toAccountData.accountBalanceType + ":";
      this.view.lblToAvailableBalance.setVisibility(true);
    }
    else{
      this.view.lblToAccountValue.text = toAccountData.toAccountName;
      this.view.lblToBalanceValue.text = toAccountData.bankName;
      this.view.lblToAvailableBalance.setVisibility(false);
    }
  },
  setDisabledToAccount:function(toAccountData){
    if(toAccountData.transactionType == "P2P"){
      this.view.lblToAccountValueDisabled.text = toAccountData.payPersonName;
      this.view.lblToBalanceValueDisabled.text = toAccountData.p2pContact;
      this.view.lblToAvailableBalanceDisabled.setVisibility(false);
    }
    else if(toAccountData.transactionType == "InternalTransfer"){
      this.view.lblToAccountValueDisabled.text = toAccountData.toAccountName;
      this.view.lblToBalanceValueDisabled.text = toAccountData.availableBalance;
	  this.view.lblToAvailableBalanceDisabled.text = toAccountData.accountBalanceType;
      this.view.lblToAvailableBalanceDisabled.setVisibility(true);
    }
    else{
      this.view.lblToAccountValueDisabled.text = toAccountData.toAccountName;
      this.view.lblToBalanceValueDisabled.text = toAccountData.bankName;
      this.view.lblToAvailableBalanceDisabled.setVisibility(false);
    }
  },
   setAmountData:function(){
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    var transObj= transMod.getTransObject();
    var amountValue = transMod.formatAmountAndAppendCurrencyEurope(transObj.amount,transObj.transactionCurrency);
    if (transMod.transactionMode === applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.transfer.OtherBankAccounts") || transMod.transactionMode === applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.transfer.OtherKonyBankMembers")){
      if (transObj.transactionCurrency !== transObj.fromAccountCurrency) {
        amountValue = null;
      }
    }     
    this.view.lblAmountValue.text = amountValue;
     //if not international payment, don't show the exchange rate
    if(transMod.transactionMode=== applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.transfer.InternationalTransfer")){
       this.view.flxAmountDetails.setVisibility(true);
      
      this.view.lblExchangeRateValue.text = transMod.formatAmountAndAppendCurrencyEurope(1,transObj.fromAccountCurrency)  + ' = ' + transMod.formatAmountAndAppendCurrencyEurope(transObj.exchangeRate,transObj.transactionCurrency)+ '  ';
      if (transObj.amount && transObj.exchangeRate)
      	var exchangeAmount = transObj.amount * transObj.exchangeRate;
      this.view.lblYouSendValue.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Europe.youSend") +' ' + transMod.formatAmountAndAppendCurrencyEurope(exchangeAmount,transObj.fromAccountCurrency);

    }else{
       this.view.flxAmountDetails.setVisibility(false);
    }
   },
  continueOnClick: function(){
    applicationManager.getPresentationUtility().showLoadingScreen();
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    this.constructFileInput();
    transMod.checkForOverrides(this.requestPayload);
  },
  changeButtonText : function() {
    var transMod = applicationManager.getModulesPresentationController("TransferModule");
    var transObj= transMod.getTransObject();
    if (transObj.isScheduled === "1")
      this.view.btnTransfer.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.MM.ScheduledTransfer");
    else if(transMod.getEuropeFlowType() === "INTERNAL"){
      this.view.btnTransfer.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Transfers.Confirm");
    }else{
      this.view.btnTransfer.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.common.continue");
    }
  },
  
  setUIForEditFlow: function(){
    this.view.flxToAccountEditFlow.isVisible = true;
    this.view.flxToAccount.isVisible = false;
    this.view.flxToAccountDisabled.isVisible = false;
    this.view.flxAmount.top = "74dp";
    this.view.flxToAndFrom.height = "146dp";
    this.view.flxAttachment.setVisibility(false);
  },
  setUIForCreateFlow: function(){
     this.view.flxToAccountEditFlow.isVisible = false;
    this.view.flxToAccount.isVisible = true;
    this.view.flxToAccountDisabled.isVisible = false;
    this.view.flxAmount.top = "146dp";
    this.view.flxToAndFrom.height = "220dp";
    this.view.flxAttachment.setVisibility(true);
  },

  fileSelectionFromGallery: function () {
    var queryContext = {
      mimetype:"image/*"
    };
    try {
      kony.phone.openMediaGallery(this.fileSelectionCallback.bind(this),queryContext);
    } catch (error) {
      this.handleError(error);
    }
  },
  selectDocuments: function () {
    var queryContext = {
      mimetype:"application/*"
    };
    try {
      kony.phone.openMediaGallery(this.fileSelectionCallback.bind(this), queryContext);
    } catch (error) {
      this.handleError(error);
    }
  },
  fileSelectionCallback: function (rawBytes, permissionStatus, mimeType) {
    this.hideFileSelectionOption();
    var fileMimeType = mimeType.substring(mimeType.lastIndexOf("/")+1);
    var fileName = this.fileNamePrefix+(this.fileNames.length+1)+"."+fileMimeType;
    var configManager = applicationManager.getConfigurationManager();
    var maxFileSize =  configManager.maxFileSizeAllowed;
    var selectedFile = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Europe.AttachmentTypeErrorMsg1");
    var typeError = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Europe.AttachmentTypeErrorMsg2");
    var sizeError = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Europe.AttachmentSizeErrorMsg")+maxFileSize+"mb.";    
    if(mimeType !== "image/jpeg" && mimeType !== "application/pdf")
    {
      this.view.lblAttachmentError.text = selectedFile+" "+fileName+" "+typeError;
      this.view.flxAttachmentsError.setVisibility(true);
      this.view.lblAttachmentError.setVisibility(true);
      applicationManager.getPresentationUtility().dismissLoadingScreen();
    }else{
      if (rawBytes !== null) {
        applicationManager.getPresentationUtility().showLoadingScreen();
        var base64 = kony.convertToBase64(rawBytes);
        if ( base64 !== null && base64 !== undefined && base64 !== "") {
          var fileSize = ((base64.length*0.75 )/1024);
          if(fileSize > (maxFileSize*1000)){
            this.view.lblAttachmentError.text = selectedFile+" "+fileName+" "+sizeError;
            this.view.flxAttachmentsError.setVisibility(true);
            this.view.lblAttachmentError.setVisibility(true);
            applicationManager.getPresentationUtility().dismissLoadingScreen();
          }else {
            this.fileNames.push(fileName);
            var navMan = applicationManager.getNavigationManager();
            navMan.setCustomInfo("uploadedAttachments", this.fileNames);
            this.fileContents.push(base64);
            this.fileTypes.push(fileMimeType);
            this.setAttachmentsDataToSegment();
          }
        } 
      }
      applicationManager.getPresentationUtility().dismissLoadingScreen();
    }
  },
  openCamera: function () {
    applicationManager.getPresentationUtility().showLoadingScreen();
    this.hideFileSelectionOption();
    var rawBytes = this.view.Camera.rawBytes;
    var configManager = applicationManager.getConfigurationManager();
    var maxFileSize =  configManager.maxFileSizeAllowed;
    var fileType = "jpeg";
    if (rawBytes) {
      var imgObject=kony.image.createImage(rawBytes);
      var base64 = "";
      var fileName = this.fileNamePrefix+(this.fileNames.length+1)+".jpeg";
      var fileSize = "";
      base64 = kony.convertToBase64(rawBytes);
      fileSize=((base64.length*0.75 )/1024);
      // for(var scaleLabel=1;scaleLabel>0;){
      //   var rawBytesTemp = rawBytes;
      //   imgObject=kony.image.createImage(rawBytesTemp);
      //   imgObject.scale(scaleLabel);
      //   var tempRawBytes= imgObject.getImageAsRawBytes();
      //   base64 = kony.convertToBase64(tempRawBytes);
      //   fileSize=((base64.length*0.75 )/1024);
      //   if(fileSize<=maxFileSize*1000){
      //     break;
      //   }
      //  scaleLabel= scaleLabel-0.25;
      // }
      if(fileSize > maxFileSize*1000){
        var scaleLabel= (maxFileSize*1000)/(fileSize+1);
        imgObject.scale(scaleLabel);
        var tempRawBytes= imgObject.getImageAsRawBytes();
        base64 = kony.convertToBase64(tempRawBytes);
      }
        this.fileContents.push(base64);
        this.fileNames.push(fileName);
        this.fileTypes.push(fileType);
        this.setAttachmentsDataToSegment();

        // var selectedFile = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Europe.AttachmentTypeErrorMsg1");
        // var sizeError = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Europe.AttachmentSizeErrorMsg")+maxFileSize+"mb.";
        // this.view.lblAttachmentError.text = selectedFile+" "+fileName+" "+sizeError;
        // this.view.flxAttachmentsError.setVisibility(true);
        // applicationManager.getPresentationUtility().dismissLoadingScreen();
    }
  },
  setAttachmentsDataToSegment: function(){
    this.view.flxAttachmentsList.setVisibility(true);
    var configManager = applicationManager.getConfigurationManager();
    var maxAttachmentsAllowed =  configManager.maxAttachmentsAllowed;
    var attachmentsData = [];
    for (var i = 0; i < this.fileNames.length; i++) {
      attachmentsData[i] = {};
      attachmentsData[i].filename = this.fileNames[i];
      attachmentsData[i]["imgRemoveAttachment"] = {
        "src": "remove_attachment.png"
      };
    }
    this.view.segAttachmentsList.widgetDataMap = {
      "lblAttachment": "filename",
      "imgRemoveAttachment": "imgRemoveAttachment",
    };
    this.view.segAttachmentsList.setData(attachmentsData);
    var configManager = applicationManager.getConfigurationManager();
    var maxAttachmentsAllowed =  configManager.maxAttachmentsAllowed;
    
    if (this.fileNames.length === 0){
      this.view.lblTitle.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Europe.Attachments")+" ("+applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Europe.AttachmentsOptional")+")";
    }
   if (this.fileNames.length >= maxAttachmentsAllowed) {
      this.view.flxAddIcon.setVisibility(false);
      this.view.lblTitle.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Europe.Attachments")+" ("+this.fileNames.length+"/"+maxAttachmentsAllowed+")";
     applicationManager.getPresentationUtility().dismissLoadingScreen();
    } else{
      this.view.flxAddIcon.setVisibility(true);
      this.view.lblTitle.text = applicationManager.getPresentationUtility().getStringFromi18n("kony.mb.Europe.Attachments")+" ("+this.fileNames.length+"/"+maxAttachmentsAllowed+")";
    }
    this.hideFileSelectionOption();
    this.constructFileInput();
    this.view.lblAttachmentError.text = "";
    this.view.lblAttachmentError.setVisibility(false);
    applicationManager.getPresentationUtility().dismissLoadingScreen();
  },
  
  showFileSelectionOption: function () {
    this.view.flxActions.setVisibility(true);
  },

  hideFileSelectionOption: function () {
    this.view.flxActions.setVisibility(false);
  },
  
  removeAttachments: function(file){
    for (var i = 0; i < this.fileNames.length; i++) {
        if (this.fileNames[i] === file.filename) {
          this.fileNames.splice(i, 1);
          this.fileContents.splice(i, 1);
          this.fileTypes.splice(i, 1);
          break;
        }
      }
    this.constructFileInput();
    this.setAttachmentsDataToSegment();
  },
  
  constructFileInput: function(){
    var uploadattachments = [];
    for(var i=0;i< this.fileNames.length; i++){
      var fileInputs = "";
      fileInputs = this.fileNames[i]+'-'+this.fileTypes[i]+'-'+this.fileContents[i];
      uploadattachments.push(fileInputs);
    }
    this.requestPayload = uploadattachments.toString();
  },
  
  handleError: function(error){
    this.view.lblAttachmentError.text = error;
    this.view.flxAttachmentsError.setVisibility(true);
    this.view.lblAttachmentError.setVisibility(true);
  },
  
  bindGenericError: function(errorMsg) {
    applicationManager.getPresentationUtility().dismissLoadingScreen();
    var scopeObj = this;
    applicationManager.getDataProcessorUtility().showToastMessageError(scopeObj, errorMsg);
  }
  
});