it("Verify_ScheduledTransferQTR_DateRangeAndNoOfOccurance_P2P", async function() {
  
  SearchInFromAndToScreen("P2P");
  EnterAmount();
  SelectFrequencyQuarterly();
  SelectDateRangeAndConfirmTransfer("QTR","P2P");
  
  SearchInFromAndToScreen("P2P");
  EnterAmount();
  SelectFrequencyQuarterly();
  SelectNumberOfTransfersAndThenConfirmTransfer("QTR","P2P");
  Logout();
},180000);