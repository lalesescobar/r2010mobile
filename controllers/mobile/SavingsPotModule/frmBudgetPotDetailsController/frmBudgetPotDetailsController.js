define({ 
init : function(){
    var navManager = applicationManager.getNavigationManager();
    var currentForm=navManager.getCurrentForm();
    applicationManager.getPresentationFormUtility().initCommonActions(this,"NO",currentForm);
  },
preShow:function(){
  applicationManager.getPresentationUtility().dismissLoadingScreen();
  if(applicationManager.getPresentationFormUtility().getDeviceName()==="iPhone"){
      this.view.flxHeader.setVisibility(false);
    }

    this.initActions();
    this.view.browserChart.onSuccess=this.chartLoad;
    this.view.browserChart.onPageFinished=this.chartLoad;
    this.chartLoad();
},
  initActions: function(){
    this.view.btnFund.onClick = this.navFund;
    this.view.btnWithdraw.onClick = this.withdraw;
    this.view.customHeader.flxBack.onClick = this.onBack;
    this.view.customHeader.btnRight.onClick = this.onEdit;
    this.setDataToForm();
       applicationManager.getPresentationUtility().dismissLoadingScreen();
  },
  
  setDataToForm: function(){
            var navManager = applicationManager.getNavigationManager();
    var SavingsPotMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SavingsPotModule");
         var budgetDetails=JSON.parse(JSON.stringify(SavingsPotMod.presentationController.getEditObj()));
	navManager.setCustomInfo("frmBudgetPotDetails", budgetDetails);
            SavingsPotMod.presentationController.setSavingsFlow("");
      this.view.customHeader.lblLocateUs.text=budgetDetails.potName;
  this.view.title=budgetDetails.potName;
    var formatUtil=applicationManager.getFormatUtilManager();
    var date = formatUtil.getDateObjectfromString(budgetDetails.creationDate,"MM/DD/YYYY");
    var currentdate = formatUtil.getFormatedDateString(date, formatUtil.getApplicationDateFormat());
    if(budgetDetails.amountWithdrawn === "0"){
      this.view.flxWithdrawn.setVisibility(false);
    }else {
    this.view.flxWithdrawn.setVisibility(true);
    this.view.lblWithdrawnValue.text = formatUtil.formatAmountandAppendCurrencySymbol(budgetDetails.amountWithdrawn,budgetDetails.currency);
    }
     if(parseFloat(budgetDetails.availableBalance) > parseFloat(budgetDetails.targetAmount)){
       this.view.flxExcessAmount.setVisibility(true);
       var excessAmount = parseFloat(budgetDetails.availableBalance) - parseFloat(budgetDetails.targetAmount);
       this.view.lblExcessAmountValue.text = formatUtil.formatAmountandAppendCurrencySymbol(excessAmount,budgetDetails.currency);
     } else {
        this.view.flxExcessAmount.setVisibility(false);
     }
    this.view.lblDateValue.text = currentdate;   
    this.view.lblBudgetAmountAns.text = formatUtil.formatAmountandAppendCurrencySymbol(budgetDetails.targetAmount,budgetDetails.currency);
    this.view.lblCurrBalanceAns.text = formatUtil.formatAmountandAppendCurrencySymbol(budgetDetails.availableBalance,budgetDetails.currency);
        if(budgetDetails.potCurrentStatus === "Completed"){
	this.view.lblWarn.src="greentick.png";
    this.view.lblMessage.text = "You achieved the Budget Amount";
    } 
    else if(budgetDetails.potCurrentStatus === "Yet to Fund"){
     this.view.lblWarn.src="alert.png";
     this.view.lblMessage.text = "Yet to Fund";
    }
    else {
     this.view.lblWarn.src="alert.png";
     this.view.lblMessage.text = "Partially Funded";
    }
    var SavingsPotMod = applicationManager.getModulesPresentationController("SavingsPotModule");
    SavingsPotMod.setSavingsPotId(budgetDetails.savingsPotId);
    SavingsPotMod.setTransactPotId(budgetDetails.potAccountId);
    SavingsPotMod.setSavingsPotName(budgetDetails.potName);
  },
  
  navFund: function(){
    var SavingsPotMod = applicationManager.getModulesPresentationController("SavingsPotModule");
    SavingsPotMod.setSavingsType("Budget");
    var SavingsPotModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SavingsPotModule");
    SavingsPotModule.presentationController.clearFundWithdrawData();
    var navManager = applicationManager.getNavigationManager();
    navManager.navigateTo("frmGoalfundAmount"); 
},
  withdraw: function(){
     var navManager = applicationManager.getNavigationManager();
     var Details = navManager.getCustomInfo("frmBudgetPotDetails");
     var SavingsPotMod = applicationManager.getModulesPresentationController("SavingsPotModule");
     var potName = SavingsPotMod.getSavingsPotName();
    if(Details.availableBalance === "0"){
      alert("You have zero balance in your"+" "+potName+". You can close the pot now!");
    } else {
    SavingsPotMod.setSavingsType("Budget");
    var SavingsPotModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SavingsPotModule");
    SavingsPotModule.presentationController.clearFundWithdrawData();
    navManager.navigateTo("frmWithdrawAmount");
    }  
  },
    onBack : function () {
    var navigationMan=applicationManager.getNavigationManager();
    navigationMan.goBack();
  },
  onEdit : function () {
           var SavingsPotMod = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("SavingsPotModule");
    SavingsPotMod.presentationController.setSavingsFlow("YES");
    var navManager = applicationManager.getNavigationManager();
    navManager.navigateTo("frmEditBudget");
  },
  chartLoad: function(){
        var navManager = applicationManager.getNavigationManager();
    var budgetDetails = navManager.getCustomInfo("frmBudgetPotDetails");
	var percentage=(budgetDetails.availableBalance*100)/budgetDetails.targetAmount;
    var remaining=100-parseInt(percentage);
	if(percentage>=100){
          percentage=100;
      this.view.lblPercent.text=percentage+"%";
      this.view.lblCur.skin="lbldotgreen";
       var x = this.view.browserChart.evaluateJavaScript("AddDonutChart(" + JSON.stringify([]) +","+JSON.stringify([0,0,percentage])+");");
        }
       else{
       percentage=parseInt(percentage);
          this.view.lblPercent.text=percentage+"%";
         this.view.lblCur.skin="lbldotblue";
         var x = this.view.browserChart.evaluateJavaScript("AddDonutChart(" + JSON.stringify([]) +","+JSON.stringify([percentage,remaining,])+");");
       }
  }


 });