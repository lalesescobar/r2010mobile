it("Verify_ScheduledTransferWeekly_DateRangeAndNoOfOccurance_P2P", async function() {
  
  SearchInFromAndToScreen("P2P");
  EnterAmount();
  SelectFrequencyWeekly();
  SelectDateRangeAndConfirmTransfer("Weekly","P2P");
  
  SearchInFromAndToScreen("P2P");
  EnterAmount();
  SelectFrequencyWeekly();
  SelectNumberOfTransfersAndThenConfirmTransfer("Weekly","P2P");
  Logout();
},180000);