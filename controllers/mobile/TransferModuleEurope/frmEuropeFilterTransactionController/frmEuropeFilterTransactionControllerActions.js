define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Form_hbc28cd9a90048d8a4ae9875585b3453: function AS_Form_hbc28cd9a90048d8a4ae9875585b3453(eventobject) {
        var self = this;
        this.init();
    },
    AS_Form_f6a0a2d1943e4d198967049316611172: function AS_Form_f6a0a2d1943e4d198967049316611172(eventobject) {
        var self = this;
        this.preShow();
    },
    AS_BarButtonItem_gc25db1afd314783a5f575099f6acc91: function AS_BarButtonItem_gc25db1afd314783a5f575099f6acc91(eventobject) {
        var self = this;
        this.navigateCustomBack();
    },
    AS_BarButtonItem_g527530ff7cc49d0ad78decad8fcdd42: function AS_BarButtonItem_g527530ff7cc49d0ad78decad8fcdd42(eventobject) {
        var self = this;
        this.cancelOnClick();
    }
});