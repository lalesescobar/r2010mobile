define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Form_f1b3b4074e4f4fe1ae562491d4b92d56: function AS_Form_f1b3b4074e4f4fe1ae562491d4b92d56(eventobject) {
        var self = this;
        return self.init.call(this);
    },
    AS_Form_cdf8b0471c434f16b29a20ba0acc20a7: function AS_Form_cdf8b0471c434f16b29a20ba0acc20a7(eventobject) {
        var self = this;
        return self.preshow.call(this);
    },
    AS_Segment_f4e7bcebb64047faa2b1cad45b310cfb: function AS_Segment_f4e7bcebb64047faa2b1cad45b310cfb(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.onSegRowClick.call(this);
    }
});